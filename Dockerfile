FROM harbor.maxiv.lu.se/dockerhub/library/node:16.0.0-alpine AS domni-build

WORKDIR /home/app

COPY package.json package-lock.json ./
RUN npm install --only=prod --production --no-cache

COPY src src
COPY public public
ARG REACT_APP_MODE
ENV REACT_APP_MODE=$REACT_APP_MODE
RUN npm run build

FROM harbor.maxiv.lu.se/dockerhub/library/nginx:1.15.0-alpine

EXPOSE 80

COPY --from=domni-build /home/app/build /usr/share/nginx/html
COPY assets/nginx.conf /etc/nginx/
