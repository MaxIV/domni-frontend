* 'edit/add external id' should not be a system event. either an edit event or something unique
* 'upload additional file' should not be a system event. either an edit event or something unique
* every supervisorAction, comment, edit and systemevent should have a role, describing the role of the person who took the action. 
This will make the broken Helper.resolveStepperRole obsolute, except for as a fallback for legacy database data.
