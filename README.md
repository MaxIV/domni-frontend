# Domni

This readme covers the both the Domni frontend as well as the Domni backend (https://gitlab.maxiv.lu.se/kits-ims/domni-backend). Domni is webapp for submitting various MAX IV forms. Currently, the following forms are available:

- Purchase application (for the approval or puchases > ~100k SEK)
- Travel approval
- Employment request
- Grant initiative (GIF)
- Additional funding request
- Catering form
- Hotel booking
- The MAX IV annual safety test
- The Safety group risk assessment

The frontend is implementing using:

- React (functional components and the `context` API for global state management).
- TypeScript for static typing.
- Linting with eslint, using the `react-app` config.
- The MUI version 5 React UI framework.
- Prettier for code formatting.

The backend is implemented using:

- Nodejs (with TypeScript),
- Express, a web application framework
- Mongoose, a MongoDB modelling framework
- A MongoDB database

# Local installation

- Make sure you have Node.js and npm installed (Node.js typically comes bundled with npm installations)
- Clone the backend (git@gitlab.maxiv.lu.se:kits-ims/domni-backend.git) and the frontend (git@gitlab.maxiv.lu.se:kits-ims/domni-frontend.git)
- For each project, go to their respective root directory and run `npm install`
- Domni uses MAX IV's new SSO service (auth.maxiv.lu.se). Authenticating with this service sets a jwt cookie with user credentials that is valid across all MAX IV subdomain. This means that the development URL also has to be a MAX IV subdomain. The easiest way to do this is to edit the `HOST` file (`/etc/hosts/` on a Unix based OS, or `<win install dir>\system32\drivers\etc\hosts` on Windows), and adding the following line:
  `127.0.0.1 local.maxiv.lu.se`. This allows you to run locally against the URL local.maxiv.lu.se, and your browser will accept the domain-wide cookie.
- Backend configuration is stored in a file called `.env`, located in the root directory of the project. This file needs to be created and contain the following:

```
    MONGO=mongodb://forms-test:6oWvQBPyS2@192.168.19.104:27017/forms-test
    #MONGO=mongodb://domni-cypress:domni-cypress@192.168.19.104:27017/domni-cypress //Alternative database for testing
    DOMAIN_NAME=http://local.maxiv.lu.se:3000
    COOKIE_NAME=maxiv-jwt-auth-test
    DECODE_URL=https://auth-test.maxiv.lu.se/v1/decode
    #Below is LDAP test environmnent config, only needed for `/people` search in the `PersonWidget` that pops up when clicking on a user.
    LDAP_SERVER=ldaps://adauth.maxiv.lu.se:25636
    LDAP_USERNAME=duoadm@maxlab.lu.se
    LDAP_PASSWORD=akdMiHpoMawj97vp
```

- The frontend can be started from the root directory with `npm start`.
- The backend can be started from the root directory with `npm start`. This will run the `prebuild`, `build`, `prestart` and `start` scripts in package.json, in that order, which will run `tslint`, compile the current TypeScript code into JavaScript and save it to the dist directory, which is then run as a node project. However, this is quite slow for local development. To speed things up, the following approach is better: Run `npm run dev`. This will start the server with nodemon, which restarts automatically whenever changes to the JavaScript code base are detected. Since you'll be writing ts code rather than js code directly, also start `tsc-watch` to monitor any ts changes and automatically rebuild the corresponding js file upon save. In vs code it should be avilable as a command (Ctrl+Shift+B) called `tsc: watch - tsconfig.json`. Run this. An alternative way of running the backend is with `ts-node`, e.g. `npx ts-node --files src/index.ts`.

# CI/CD

This project is using the IMS CI/CD setup. Each project has a dockerfile and a .gitlab-ci.yml file, the ims-ansible project has a docker-compose file, and there's a template set up in AWX. Commits to the develop branch updates the test environment (domni-test.maxiv.lu.se). Commits to the master branch updates production (domni.maxiv.lu.se).

# Testing

Domni is using cypress for frontend testning. To run the tests with browser simulation type `npm run cypress`. In order for this to work domni frontend has to be running on `https://local.maxiv.lu.se:3000`. Setting up the MAX IV subdomain is covered in the installation part. To run with https locally, use `HTTPS=true npm start` (UNIX), `set HTTPS=true&&npm start` (Windows cmd) or `($env:HTTPS = "true") -and (npm start)` (Windows Powershell).

Additionally, the backend must be running as well (without any https or subdomain requirements), and it must be connected to the cypress test database (`mongodb://domni-cypress:domni-cypress@192.168.19.104:27017/domni-cypress`)

Cypress has its own README.md. In addition to the information above, it also describes the custom cypress functions (or `commands`) that have been implemented, as well how the DOM attribute `data-testid` is used to assign unique identifiers to most of the Domni components for Cypress to use. These ids are also listed in a somewhat structured way.

# Domni users

In order to use Domni at all, even locally, one must be logged in. Authentication in Domni is done via the MAX IV SSO service located at `auth.maxiv.lu.se`, which in turn authenticates again the MAX IV AD. However, when developing locally, Domni will instead authenticate against `auth-test.maxiv.lu.se`, which is using a test instance of AD. Note that the test deployment of Domni (domni-test.maxiv.lu.se) still uses live auth service, for various reasons:

- Domni is using AD in a read-only capacity, so there is no risk of inadvertedly chaning something.
- Before going into production, various stakeholders are typicall asked to evaluate and test a new form type. This is much easier, and makes testing more reliable, if they can use their regular user accounts.
  On the other hand, while testing locally, the test instance of AD needed on order to be able to take on different users roles. There are three users in the test AD made specifically for local development and testing:

| Username           | Password | Email                           | Role                             |
| ------------------ | -------- | ------------------------------- | -------------------------------- |
| domniuser          | Lund123+ | domniuser@maxlab.lu.se          | External user (non-staff)        |
| domnistaff         | Lund123+ | domnistaff@maxlab.lu.se         | Regular staff user               |
| domnisupervisor    | Lund123+ | domnisupervisor@maxlab.lu.se    | Regular staff user               |
| domniadministrator | Lund123+ | domniadministrator@maxlab.lu.se | Staff user with all admin groups |
