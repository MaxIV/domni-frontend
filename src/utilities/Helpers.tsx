import React from "react";
import moment from "moment";
import { getFormConfig, getFormTypes } from "./Config";
import { useState, useEffect } from "react";
import * as constants from "../constants";

const getWindowDimensions = (): System.WindowDimensions => {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
    xs: true,
    sm: width >= 600,
    md: width >= 960,
    lg: width >= 1280,
    xl: width >= 1920,
  };
};

/**
 * Custom hook for getting the current width and height (in px) of the window.
 */
export function useWindowDimensions(): System.WindowDimensions {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
}

export const getSize = (size: number): string => {
  if (size < 1000) {
    return size + " bytes";
  }
  if (size < 1000000) {
    return (size / 1000).toFixed(1) + " kB";
  }
  return (size / 1000000).toFixed(1) + " MB";
};

export const getLatestSupervisorAction = (
  form: System.Form
): System.SupervisorAction => {
  const actions = getSortedSupervisorActions(form);
  return actions[actions.length - 1];
};

export const getEarliestSupervisorAction = (
  form: System.Form
): System.SupervisorAction => {
  const actions = getSortedSupervisorActions(form);
  return actions[0];
};

export const formatDate = (date: Date | undefined) => {
  return date ? moment(date).format("YYYY-MM-DD HH:mm") : "-";
};
export const formatDateInterval = (
  dateFrom: Date | undefined,
  dateTo: Date | undefined
) => {
  if (!dateFrom && dateTo) {
    return "-";
  }
  if (!dateFrom) {
    return "Until " + moment(dateTo).format("YYYY-MM-DD");
  }
  if (!dateTo) {
    return "Starting at " + moment(dateFrom).format("YYYY-MM-DD");
  }
  return (
    "From " +
    moment(dateFrom).format("YYYY-MM-DD") +
    " until " +
    moment(dateTo).format("YYYY-MM-DD")
  );
};
/**
 * The oldest supervisor is at [0]
 * @param form
 */
export const getSortedSupervisorActions = (
  form: System.Form,
  reversed = false
): System.SupervisorAction[] => {
  const sign = reversed ? -1 : 1;
  return form.supervisorActions.sort((a, b) =>
    a.timestamp < b.timestamp ? -1 * sign : 1 * sign
  );
};

//domni.maxiv.lu.se
export const isInProduction = () => {
  return process.env.REACT_APP_MODE === "production";
};
//domni-test.maxiv.lu.se - always false when on localhost
export const isInDevelopment = () => {
  return process.env.REACT_APP_MODE === "development";
};
export const isInLocalDevelopment = () => {
  return !process.env.REACT_APP_MODE;
};

export const StatusTag = (props: {
  status: System.FormStatus;
  formId?: string;
  testid?: string;
}) => {
  let className = "";
  switch (props.status) {
    case "APPROVED":
      className = "status-tag-approved";
      break;
    case "PENDING":
      className = "status-tag-pending";
      break;
    case "REJECTED":
      className = "status-tag-rejected";
      break;
  }
  return (
    <div
      data-testid={props.testid || `form-${props.formId || "0"}-status-tag-div`}
      className={`status-tag ${className}`}
    >
      {props.status.toLowerCase()}
    </div>
  );
};

export const getAdminDescriptionArray = (
  person: System.LDAPPerson
): string[] => {
  const result: string[] = [];
  getFormTypes().forEach((formType) => {
    if (
      getFormConfig(formType).adminGroups.some((group: string) =>
        person.memberOf.includes(group)
      )
    ) {
      result.push(getFormConfig(formType).department);
    } else if (getFormConfig(formType).adminUsers.includes(person.username)) {
      result.push(getFormConfig(formType).name);
    }
  });
  if (person.username === "jonros") {
    result.push("Domni maintainer");
  }
  return result.filter((formName, index) => result.indexOf(formName) === index);
};
/**
 *
 * @param list The `extends unknown` is just to tell the compiler we are in fact creating a generic function here
 * @returns
 */
export const createSet = <T extends unknown>(list: T[]) => {
  return list.filter((value, index) => list.indexOf(value) === index);
};
export const possessive = (word: string): string => {
  if (word.endsWith("s")) {
    return word + "'";
  }
  return word + "'s";
};

/** Returns true if the user is an AD admin (i.e. is allowed to update AD users). */
export const isADAdmin = (user: System.User) => {
  return user.groups.includes(constants.AD_GROUP_AD_ADMIN);
};
/**Only needed before state.user has been defined. O.w. use state.user.isAdmin[<formType>] */
export const isAdmin = (
  userGroups: string[],
  username: string,
  formType: System.FormType
) => {
  try {
    return (
      getFormConfig(formType).adminGroups.some((group: string) =>
        userGroups.includes(group)
      ) ||
      (!isInProduction() && userGroups.includes(constants.AD_GROUP_IMS)) ||
      getFormConfig(formType).adminUsers.includes(username)
    );
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const isStaff = (user: System.User) => {
  return user.groups.map((group) => group.toLowerCase()).includes("staff");
};
/**
 * Returns the current comments of form, with a new comment appended to the end, assuming msg is define
 */
export const pushComment = (
  form: System.Form,
  msg: string,
  source: System.CommentSource,
  state: System.IState
): System.Comment[] => {
  const comments = [...(form.comments || [])];
  if (!msg) {
    return comments;
  }
  comments.push(createComment(form, msg, source, state));
  return comments;
};

/**
 * Creates and returns a new comment
 */
export const createComment = (
  form: System.Form,
  msg: string,
  source: System.CommentSource,
  state: System.IState
): System.Comment => {
  return {
    eventType: "Comment",
    timestamp: new Date(),
    updated: new Date(),
    source,
    msg,
    author: state.user.name,
    status: form.status,
    role: state.user.admin[form.formType] ? "Administrator" : "Supervisor",
    supervisor: getLatestSupervisorAction(form).name,
  };
};
export const getFormAccess = (
  form: System.Form,
  user: System.User,
  context: string //there are just so many calls and so many bugs related to this, that context help debugging a lot
): System.FormAccess => {
  const { status, submitterEmail, supervisorActions, formType } = form;
  if (!user.email) {
    return returnAccess(
      {
        readAccess: "NONE",
        canEdit: false,
        canResolve: false,
        canDelete: false,
        isStaff: false,
        isSubmitter: false,
        isSupervisor: false,
        isAdmin: false,
      },
      context
    );
  }
  const isSubmitter = !!(
    user.email &&
    submitterEmail &&
    submitterEmail.toLowerCase() === user.email.toLowerCase()
  );
  const isSupervisor = !!(
    user.email &&
    supervisorActions
      .map((supervisor) => supervisor.email.toLowerCase())
      .includes(user.email.toLowerCase())
  );
  const isAdmin = user.admin[formType];
  const formAccess: System.FormAccess = {
    readAccess: "NONE",
    canEdit: false,
    canResolve: false,
    canDelete: false,
    isStaff: isStaff(user),
    isSubmitter,
    isSupervisor,
    isAdmin,
  };
  if (!form._id) {
    return returnAccess(
      { ...formAccess, readAccess: "FULL", canEdit: true },
      context
    );
  }
  if (status === "DELETED") {
    return returnAccess(formAccess, context);
  }
  //If draft, allow owner to do anything, and don't allow anyone else to do anything.
  if (status === "DRAFT" && isSubmitter) {
    return returnAccess(
      {
        ...formAccess,
        readAccess: "FULL",
        canEdit: true,
        canDelete: true,
      },
      context
    );
  } else if (status === "DRAFT" && !isSubmitter) {
    return returnAccess(
      {
        ...formAccess,
        readAccess: "NONE",
        canEdit: false,
        canDelete: false,
      },
      context
    );
  }
  const formConfig = getFormConfig(formType);
  formAccess.readAccess =
    isSupervisor || isAdmin ? "FULL" : isSubmitter ? "DEFAULT" : "NONE";
  if (
    isSubmitter &&
    !isAdmin &&
    formConfig.hideApprovedForms &&
    status === "APPROVED"
  ) {
    formAccess.readAccess = "BASIC";
  }
  if (form.isArchived) {
    return returnAccess(
      { ...formAccess, canResolve: false, canEdit: false, canDelete: false },
      context
    );
  }
  formAccess.canResolve =
    (isAdmin || isSupervisor) &&
    form.status !== "REJECTED" &&
    (form.status !== "APPROVED" || isAdmin);

  formAccess.canEdit =
    (isAdmin && formConfig.canEditAdmin && status === "PENDING") ||
    (isSupervisor && formConfig.canEditSupervisor && status === "PENDING") ||
    (isSubmitter && status === "REJECTED");

  formAccess.canDelete =
    (isAdmin && status === "REJECTED") || (isSubmitter && status === "DRAFT");
  return returnAccess(formAccess, context);
};
const returnAccess = (formAccess: System.FormAccess, context: string) => {
  const DEBUG = false; //if true, print access before each return statement
  if (DEBUG) {
    console.log("form Access:", context);
    console.log(formAccess);
  }
  return formAccess;
};

export const resolveStepperRole = (form: System.Form, actor: string) => {
  if (form.submitterName.toLowerCase() === actor.toLowerCase()) {
    return "Submitter";
  }
  if (
    form.supervisorActions.filter(
      (s) => s.name.toLowerCase() === actor.toLowerCase()
    ).length > 0
  ) {
    return "Supervisor";
  }
  return "Admin";
};

export const isCurrentSupervisor = (form: System.Form, user: System.User) => {
  const actions = getSortedSupervisorActions(form);
  return (
    actions[actions.length - 1].email.toLowerCase() === user.email.toLowerCase()
  );
};

export const validateMaxIVEmail = (email: string) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return (
    re.test(String(email).toLowerCase()) &&
    (email.toLowerCase().endsWith("@maxiv.lu.se") ||
      email.toLowerCase().endsWith("@maxlab.lu.se"))
  );
};

export const FileLink = (fileName: { name: string; size: any }) => {
  if (!fileName.name) {
    return <i>Not found</i>;
  }
  const path = "/api/files/" + fileName.name;
  return (
    <div key={fileName.name}>
      <a
        href={path}
        className="link"
        download
        data-testid={`file-${fileName.name}-download-link`}
      >
        {fileName.name.slice(14)} ({fileName.size})
      </a>
    </div>
  );
};

/**
 * Creates a ValidationResult from a single error message string, interpreting an empty string as a successful validation.
 * ValidationResults are the expected return value from the optional `validate` function in modalProps, which is executed when the user click on the confirm button in a modal
 *
 * @param errorMessage the error message (if validation failed), or an empty string (if validation was successful)
 */
export const createSimpleValidationResult = (
  errorMessage: string
): System.ValidationResult => {
  return errorMessage === ""
    ? {
        status: "SUCCESS",
      }
    : {
        status: "FAILURE",
        errorElement: <>{errorMessage}</>,
      };
};
export const GrayBlock = (props: { display?: boolean; height?: string }) => {
  return !props.display === false ? null : (
    <div
      style={{
        height: props.height || "3.5em",
        borderRadius: "0.5em",
        background:
          "repeating-linear-gradient(45deg,hsla(0,0%,93%,.2),hsla(0,0%,93%,.2) 5px,hsla(0,0%,65%,.15) 0,hsla(0,0%,62%,.15) 10px)",
      }}
    ></div>
  );
};
export const Highlight = (props: {
  query: string | undefined;
  msg: string | undefined;
  "data-testid"?: string | undefined;
}): JSX.Element => {
  const { query, msg } = props;
  if (!msg) {
    return <></>;
  }
  if (!query) {
    return <span data-testid={props["data-testid"] || ""}>{props.msg}</span>;
  }
  const index = msg.toLowerCase().indexOf(query.toLowerCase());
  if (index < 0) {
    return <span data-testid={props["data-testid"] || ""}>{props.msg}</span>;
  }
  if (index === 0) {
    return (
      <span data-testid={props["data-testid"] || ""}>
        <span className="highlight">{msg.substring(0, query.length)}</span>
        {msg.substring(query.length)}
      </span>
    );
  }

  return (
    <span data-testid={props["data-testid"] || ""}>
      {msg.substring(0, index)}
      <span className="highlight">
        {msg.substring(index, index + query.length)}
      </span>
      {msg.substring(index + query.length)}
    </span>
  );
};

export const getAllYears = () => {
  const START_YEAR = 2020;
  const END_YEAR = new Date().getFullYear();
  const years = [];
  for (let year = START_YEAR; year <= END_YEAR; year++) {
    years.push(year);
  }
  return years.sort((a, b) => a - b);
};
