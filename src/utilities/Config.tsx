import moment from "moment";
import React from "react";
import * as Constants from "../constants";
import AdditionalFundingForm from "../pages/AdditionalFundingForm";
import CateringRequest from "../pages/CateringRequest";
import EmploymentRequest from "../pages/EmploymentRequest";
import GrantInitiativeForm from "../pages/GrantInitiativeForm";
import HotelForm from "../pages/HotelForm";
import Inquiry from "../pages/Inquiry";
import PurchaseForm from "../pages/PurchaseForm";
import TravelForm from "../pages/TravelForm";
import RiskAssessment from "../pages/RiskAssessment";
import SafetyTestForm from "../pages/SafetyTest";
import { DomniLink } from "../components/DomniLink";
import { isInProduction, isInDevelopment } from "./Helpers";
import { SafetyClassificationLED } from "../components/SafetyClassificationLED";
import RiskAssessmentPrint from "../components/print/RiskAssessmentPrint";
export const authURL =
  isInDevelopment() || isInProduction()
    ? "https://auth.maxiv.lu.se/v1/decode"
    : "https://auth-test.maxiv.lu.se/v1/decode";
export const loginURL =
  isInDevelopment() || isInProduction()
    ? "https://auth.maxiv.lu.se/login"
    : "https://auth-test.maxiv.lu.se/login";
export const COOKIE =
  isInDevelopment() || isInProduction()
    ? "maxiv-jwt-auth"
    : "maxiv-jwt-auth-test";
export const itemsPerPage = 10;

/**A description of each field in the formConfig can be found in its interface declaration in src/types/global.d.ts */
export const formConfigs: System.FormConfigs = {
  forms: {
    PURCHASE_APPLICATION: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      visibility: "STAFF",
      icon: "euro",
      path: "purchase-application",
      description: "Used for applying for purchases between 100k-700k SEK",
      formType: "PURCHASE_APPLICATION",
      name: "Purchase Application",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: "Finance",
      isSimple: false,
      hasExternalId: true,
      renderSubmissionSummary: (form) => (
        <>{(form as System.PurchaseForm).product}</>
      ),
      formComponent: PurchaseForm,
      helpComponent:
      <p>Please fill in the information below to apply for purchases between 100k-700k SEK. If you need help with a business credit check contact <DomniLink href="mailto:finance@maxiv.lu.se">finance@maxiv.lu.se</DomniLink>.</p>,
      additionalHelpComponent: Constants.PURCHASE_HELP_COMPONENT,
      fileUpload:
        "Upload any files required, e.g. correspondence with the supplier or offers from other bidders.",
      submitMessage: Constants.PURCHASE_SUBMIT_MSG,
      customSearchFields: [
        { value: "supplierName", label: "Supplier / Winning bid" },
        { value: "project", label: "Project" },
        { value: "product", label: "Product" },
        { value: "description", label: "Description" },
        { value: "motivation", label: "Motivation" },
      ],
      isInProduction: true,
    },
    TRAVEL: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      visibility: "STAFF",
      icon: "train",
      path: "travel-approval",
      description: "Used for applying for travel approval or reimbursement",
      formType: "TRAVEL",
      name: "Travel Approval",
      requiresApproval: true,
      canEditAdmin: false,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: "Finance",
      isSimple: false,
      hasExternalId: false,
      renderSubmissionSummary: (form) => (
        <>{(form as System.TravelForm).destination}</>
      ),
      formComponent: TravelForm,
      helpComponent:
        "Please fill in the information below and submit it along with all of your travel claims.",
      additionalHelpComponent: Constants.TRAVEL_HELP_COMPONENT,
      submitMessage: Constants.TRAVEL_SUBMIT_MSG,
      fileUpload: "Upload any travel claims or other relevant documents.",
      isInProduction: true,
      customSearchFields: [
        { value: "travelPurpose", label: "Travel purpose" },
        { value: "destination", label: "Destination" },
      ],
    },
    EMPLOYMENT_REQUEST: {
      adminUsers: ["lenqvi"],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      visibility: "STAFF",
      icon: "assignment_ind",
      path: "employment-request",
      description: "Used for applying for the approval of a new employment",
      formType: "EMPLOYMENT_REQUEST",
      name: "Employment Request",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: "Finance",
      isSimple: false,
      hasExternalId: true,
      renderSubmissionSummary: (form) => (
        <>
          {(form as System.EmploymentRequest).position} in{" "}
          {(form as System.EmploymentRequest).group}
        </>
      ),
      formComponent: EmploymentRequest,
      fileUpload: "Upload relevant files, e.g job description.",
      additionalHelpComponent: Constants.EMPLOYMENT_HELP_COMPONENT,
      submitMessage: Constants.EMPLOYMENT_SUBMIT_MSG,
      isInProduction: false,
      approvalMessage: (
        <>
          {" "}
          <p>
            You can either finalize the approval now, in which case the original
            submitter gets notified, or assign an additional supervisor who also
            needs to review this Employment Request before it gets finalized.{" "}
          </p>
          <p>
            If the request is out of budget, please send it to DM-ex if
            applicable. Do this by choosing DM-ex in the list below before
            approval. Does this form require additional approval by DM-ex or a
            director?
          </p>
        </>
      ),
      customSearchFields: [
        { value: "position", label: "Position" },
        { value: "group", label: "Group" },
        { value: "team", label: "Team" },
        { value: "employmentType", label: "Employment Type" },
        { value: "primulaGroup", label: "Primula Group" },
      ],
    },
    GRANT_INITIATIVE: {
      adminUsers: ["dororb"],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      visibility: "STAFF",
      icon: "local_offer",
      path: "grant-initiative",
      description: "Application for grant initiatives",
      formType: "GRANT_INITIATIVE",
      name: "Grant Initiative",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: "Finance",
      isSimple: false,
      hasExternalId: true,
      helpComponent: (
        <>
          Please fill in the information below for all Grant Applications in
          which MAX IV is involved, regardless of affiliation of the main
          applicant. Please submit this form no later than two weeks before
          application deadline. See the{" "}
          <DomniLink href="https://intranet.maxiv.lu.se/administration/grant-office/">
            Grant Office web pages
          </DomniLink>{" "}
          for more details.
        </>
      ),
      renderSubmissionSummary: (form) => (
        <>{(form as System.GrantInitiativeForm).projectName}</>
      ),
      formComponent: GrantInitiativeForm,
      isInProduction: true,
      fileUpload: "Upload any relevant files.",
      approvalMessage: (
        <>
          <p>
            The grant initiative form must be checked by Finance and approved by
            both the Group manager and Director before final approval.{" "}
          </p>
          <p>
            <b>
              If you are approving this as a Group Manager, please choose the
              appropriate Director from the supervisor list below.
            </b>
          </p>
          <p>
            <b>
              If you are approving this as a Director, please finalize the
              approval.
            </b>
          </p>
          <p>
            Does this form require the approval by an additional supervisor?
          </p>
        </>
      ),
      customSearchFields: [
        { value: "responsible", label: "Responsible" },
        { value: "coordinator", label: "Coordinator" },
        { value: "otherParties", label: "Other Parties" },
        { value: "collaborators", label: "Collaborators" },
        { value: "fundingAgency", label: "Funding Agency" },
        { value: "projectName", label: "Project Name" },
      ],
    },
    ADDITIONAL_FUNDING: {
      adminUsers: ["lenqvi"],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      visibility: "STAFF",
      icon: "account_balance_wallet",
      path: "additional-funding",
      description:
        "Used to apply for additional funding not in beamline or group budget",
      formType: "ADDITIONAL_FUNDING",
      name: "Additional Funding Request",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: true,
      hideApprovedForms: false,
      department: "Finance",
      isSimple: false,
      hasExternalId: false,
      renderSubmissionSummary: (form) => (
        <>
          {(form as System.AdditionalFundingForm).applicant} for{" "}
          {(form as System.AdditionalFundingForm).organizationalUnitName}
        </>
      ),
      formComponent: AdditionalFundingForm,
      fileUpload: "Upload any relevant files.",
      finalApprovalValidation(form: System.AdditionalFundingForm) {
        const errors: string[] = [];

// SEE RT TICKET #28038
//        if (form.totalAmountApproved <= 0) {
//          errors.push(
//            "The director needs to enter the Total amount approved (SEK)"
//          );
//        }
//        if (form.fundingSource === "") {
//          errors.push("The director needs to select a Funding source");
//        }
        if (errors.length === 0) {
          return {
            status: "SUCCESS",
          };
        }
        return {
          status: "FAILURE",
          errorElement: (
            <>
              <p>
                The approval can not be finalized as the form is incomplete:
              </p>
              <ul>
                {errors.map((error) => (
                  <li key={error}>{error}</li>
                ))}
              </ul>
              <p>
                Please edit the form and add the missing data, or assign the
                form to another supervisor.
              </p>
            </>
          ),
        };
      },
      isInProduction: false,
      customSearchFields: [
        { value: "responsibleDirector", label: "Responsible Director" },
        { value: "applicant", label: "Applicant" },
        { value: "organizationalUnitName", label: "Beamline or group" },
        { value: "fundingSource", label: "Funding Source" },
      ],
    },
    HOTEL_BOOKING: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_ADMIN],
      visibility: "STAFF",
      icon: "hotel",
      path: "hotel-booking",
      description: "Used for booking the guest hotel (Forskarhotellet)",
      formType: "HOTEL_BOOKING",
      name: "Hotel Booking",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: Constants.AD_GROUP_ADMIN,
      isSimple: false,
      hasExternalId: false,
      renderSubmissionSummary: (form) => {
        const hotelForm = form as System.HotelForm;
        return (
          <>
            {hotelForm.guestName} between{" "}
            {moment(hotelForm.checkInDate).format("YYYY-MM-DD")} and{" "}
            {moment(hotelForm.checkOutDate).format("YYYY-MM-DD")}
          </>
        );
      },

      formComponent: HotelForm,
      submitMessage: Constants.HOTEL_SUBMIT_MSG,
      isInProduction: false,
    },
    CATERING_REQUEST: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_ADMIN],
      visibility: "STAFF",
      icon: "restaurant",
      path: "catering-request",
      description: "Used for registering food catering",
      formType: "CATERING_REQUEST",
      name: "Catering Request",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: Constants.AD_GROUP_ADMIN,
      isSimple: false,
      hasExternalId: false,
      renderSubmissionSummary: (form) => (
        <>
          For {(form as System.CateringRequest).nameOfTheMeeting} on{" "}
          {moment((form as System.CateringRequest).deliveryDate).format(
            "YYYY-MM-DD"
          )}
        </>
      ),
      formComponent: CateringRequest,
      submitMessage: Constants.CATERING_SUBMIT_MSG,
      isInProduction: true,
    },
    RISK_ASSESSMENT: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_EST],
      visibility: "PUBLIC",
      icon: "errorOutline",
      path: "risk-assessment",
      description: "Used to carry out an experimental risk assessment",
      formType: "RISK_ASSESSMENT",
      name: "Experimental Safety Risk Assessment (ESRA)",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: "Safety",
      isSimple: false,
      hasExternalId: true,
      customSearchFields: [
        {
          value: "safetyClassification",
          label: "Safety Classification (Green, Yellow or Red) ",
        },
      ],

      renderSubmissionSummary: (form, oneLine) => {
        const f = form as System.RiskAssessmentForm;
        if (oneLine) {
          return <>{f.title || <i>No title</i>}</>;
        }
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            {f.title || <i>No title</i>}
            <SafetyClassificationLED value={f.safetyClassification} />
          </div>
        );
      },
      formComponent: RiskAssessment,
      fileUpload: "Upload any files referenced in any of the above segments.",
      isInProduction: true,
      printComponent: RiskAssessmentPrint,
      customColumns: [{
        name:"CLASS", func: (form) => {
          const f = form as System.RiskAssessmentForm;
          return <SafetyClassificationLED value={f.safetyClassification}/>
        }
      }]
    },
    SAFETY_TEST: {
      adminUsers: ["isalin", "andros"],
      adminGroups: [Constants.AD_GROUP_ADMIN, Constants.AD_GROUP_RADIATION_SAFETY],
      visibility: "STAFF",
      icon: "assignment_turned_in",
      path: "safety-test",
      description: "The MAX IV annual safety test",
      formType: "SAFETY_TEST",
      name: "Safety Test",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      hideApprovedForms: true,
      department: "Safety",
      isSimple: false,
      hasExternalId: false,
      renderSubmissionSummary: (form) => {
        const formData = form as System.SafetyTestForm;
        return (
          <>
            Basic Safety {formData.hasChemical ? "• Extended Chemical " : " "}
            {formData.hasBio ? "• Biological " : " "}
            {formData.hasRadiation ? "• Controlled Area Radiation " : " "}
          </>
        );
      },
      formComponent: SafetyTestForm,
      helpComponent: (
        <>
          {" "}
          Please study the relevant safety information found{" "}
          <DomniLink href="https://www.maxiv.lu.se/user-access/safety/safety-requirements-for-employees/">
            here
          </DomniLink>{" "}
          before taking the test. If you are unable to take the online version
          of the test, you can download and fill out a pdf version of this test
          from {" "}
          <DomniLink href="https://www.maxiv.lu.se/user-access/safety/safety-requirements-for-contractors/">
            here
          </DomniLink>{" "}
          .
        </>
      ),
      submitMessage: Constants.SAFETY_TEST_MSG,
      isInProduction: true,
    },
    ADD_FORM: {
      adminUsers: [],
      adminGroups: [],
      visibility: "STAFF",
      icon: "add",
      path: "add-form",
      description: "Used for inquiring about adding a new form in Domni",
      formType: "ADD_FORM",
      name: "Add Form",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      hideApprovedForms: false,
      department: "KITS",
      isSimple: true,
      hasExternalId: false,
      renderSubmissionSummary: () => <></>,
      formComponent: Inquiry,
      submitMessage: Constants.ADD_FORM_MSG,
      isInProduction: true,
    },
  },
};

interface FormFilterProps {
  excludeSimple?: boolean;
  excludeNonProd?: boolean;
  visibility?: System.FormVisibility;
}

/**
 * @param formFilterProps defaults to no filtering if omitted
 */
export const getFormTypes = (
  formFilterProps?: FormFilterProps
): System.FormType[] => {
  return (Object.keys(formConfigs.forms) as System.FormType[]).filter(
    (formType) => {
      let pass = true;
      if (
        formFilterProps?.excludeSimple &&
        formConfigs.forms[formType].isSimple
      ) {
        pass = false;
      }
      if (
        formFilterProps?.excludeNonProd &&
        !formConfigs.forms[formType].isInProduction
      ) {
        pass = false;
      }
      if (
        formFilterProps?.visibility &&
        formConfigs.forms[formType].visibility !== "PUBLIC" &&
        formConfigs.forms[formType].visibility !== formFilterProps.visibility
      ) {
        pass = false;
      }
      return pass;
    }
  );
};

export const getFormConfigs = (
  formFilterProps?: FormFilterProps
): System.FormConfig[] => {
  return getFormTypes(
    formFilterProps || {
      excludeNonProd: false,
      excludeSimple: false,
      visibility: "PUBLIC",
    }
  ).map((key) => formConfigs.forms[key]);
};
export const getFormConfig = (formType: System.FormType): System.FormConfig => {
  return formConfigs.forms[formType];
};

export const getFormConfigByPath = (
  matchParams: string[]
): System.FormConfig => {
  return getFormConfig(getFormTypeByPath(matchParams));
};

export const getFormTypeByPath = (matchParams: string[]): System.FormType => {
  const path = matchParams[0];
  let formType = undefined;
  Object.keys(formConfigs.forms).forEach((form) => {
    if (formConfigs.forms[form as System.FormType].path === path) {
      formType = form;
    }
  });
  if (!formType) {
    console.log("Unknown form type for path " + path);
    throw new Error("Unknown form type for path " + path);
  }
  return formType;
};
/**
 * Returns and object with a key/property for each formType, with defaultData as value. Useful to generate e.g. initial state data for supers and projects. Of non-primitive types, only arrays are allowed as defaultData - for which a shallow copy is created for each form type. Using any other non-primitive data needs to be handled in a similar way.
 * @param formFilterProps defaults to no filtering of omitted
 */

export function getEmptyFormTypeObject<T>(
  defaultData: T,
  formFilterProps?: FormFilterProps
): { [key in System.FormType]: T } {
  const result: any = {};
  getFormTypes(
    formFilterProps || { excludeSimple: false, excludeNonProd: false }
  ).forEach((formType) => {
    result[formType] = Array.isArray(defaultData)
      ? defaultData.slice(0)
      : defaultData;
  });
  return result;
}
