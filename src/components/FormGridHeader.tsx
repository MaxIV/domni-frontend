import { Grid, Tooltip } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { getFormConfig } from "../utilities/Config";

interface Props {
  form: System.Form;
  formAccess: System.FormAccess;
  helpText: string | JSX.Element | undefined;
}

export const FormGridHeader = (props: Props) => {
  const { form, formAccess } = props;
  const formConfig = getFormConfig(form.formType);
  const getLabel = (formAccess: System.FormAccess): JSX.Element | null => {
    if (form.status === "UNSAVED") {
      return null;
    }
    if (form.status === "DRAFT") {
      return (
        <Tooltip
          title="This form has not yet been submitted and is not visible to supervisors or administrators yet."
          placement="top-start"
        >
          <div
            data-testid="form-header-status-label"
            className="edit-mode-label"
          >
            DRAFT
          </div>
        </Tooltip>
      );
    }
    if (form.status === "REJECTED") {
      return (
        <Tooltip
          title={
            form.isArchived
              ? "This form is archived and cannot be edited"
              : "This form is currently under revision." +
                (formAccess.canEdit
                  ? "Make any nessessary changes and the click on 'Update' to submit your revised version"
                  : "The supervisor will be noticed as soon as a revised version has been submitted")
          }
          placement="top-start"
        >
          <div
            data-testid="form-header-status-label"
            className="edit-mode-label"
          >
            {form.isArchived ? "ARCHIVED" : "UNDER REVISION"}
          </div>
        </Tooltip>
      );
    }
    if (form.status === "APPROVED") {
      return (
        <Tooltip
          title="This form is finalized and cannot be edited"
          placement="top-start"
        >
          <div
            data-testid="form-header-status-label"
            className="edit-mode-label"
          >
            {form.status}
          </div>
        </Tooltip>
      );
    }
    if (form.status === "PENDING" && formAccess.canEdit) {
      return (
        <Tooltip
          title={
            "You are currently editing an already submitted form. Clicking 'Save' will update the form with your changes, and log that you've made an edit."
          }
          placement="top-start"
        >
          <div
            data-testid="form-header-status-label"
            className="edit-mode-label"
          >
            ADMIN EDIT
          </div>
        </Tooltip>
      );
    }
    if (form.status === "PENDING" && !formAccess.canEdit) {
      return (
        <Tooltip
          title={
            "This form has been submitted and is currently being processed by the designated supervisor and/or administrator"
          }
          placement="top-start"
        >
          <div
            data-testid="form-header-status-label"
            className="edit-mode-label"
          >
            PENDING
          </div>
        </Tooltip>
      );
    }
    return null;
  };
  return (
    <Grid
      item
      xs={12}
      style={{ display: "flex", justifyContent: "space-between" }}
    >
      <div>
        <h2 data-testid="form-title-h2">
          <span className="category-preface">Form</span>
          <span className="primary-dark-color">
            {formConfig.name} {form._id}{" "}
          </span>
        </h2>
        {form.externalId && (
          <Tooltip title="External ID" placement="top-start">
            <div
              data-testid="form-externalId-div"
              style={{
                fontSize: "1.25em",
                color: "#aaa",
                marginBottom: "0.5em",
                marginTop: "-0.5em",
              }}
            >
              {form.externalId}
            </div>
          </Tooltip>
        )}
        <div>
          {props.helpText}
          {formConfig.additionalHelpComponent && (
            <>
              {" "}
              Click{" "}
              <Link
                to={`/${formConfig.path}/help/`}
                target="_blank"
                className="link"
              >
                here
              </Link>{" "}
              for help.
            </>
          )}
        </div>
      </div>
      <div>{getLabel(formAccess)}</div>
    </Grid>
  );
};
