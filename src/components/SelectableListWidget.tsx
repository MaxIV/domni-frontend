/* eslint-disable react-hooks/exhaustive-deps */
import { List, ListItem } from "@mui/material";
import React from "react";
import { useEffect } from "react";
import { useWindowDimensions } from "../utilities/Helpers";

interface Props<T> {
  items: T[];
  isSelected: (t: T) => boolean;
  onSelect: (t: T) => void;
  getLabel: (t: T) => string;
  getId: (t: T) => string;
  getAddtionalLabel: (t: T) => string;
  sort?: (a: T, b: T) => number;
  style?: React.CSSProperties;
  "data-testid"?: string;
}
export function SelectableListWidget<T>(props: Props<T>) {
  const { width } = useWindowDimensions();
  const listRef = React.createRef<HTMLDivElement>();
  const itemRef = React.createRef<HTMLDivElement>();
  useEffect(() => {
    const isVisible = () => {
      if (!itemRef.current) {
        return false;
      }
      const top = itemRef.current.getBoundingClientRect().top;
      return top >= 0 && top <= window.innerHeight;
    };
    if (
      !isVisible() &&
      itemRef &&
      itemRef.current &&
      listRef &&
      listRef.current
    ) {
      listRef.current.scrollTo({
        top: itemRef.current.offsetTop,
        behavior: "smooth",
      });
    }
  }, [props.items]);
  const sort = props.sort
    ? props.sort
    : (a: T, b: T) => props.getLabel(a).localeCompare(props.getLabel(b));

  const style: React.CSSProperties = { minHeight: "10em", ...props.style };
  if (width < 600) {
    style.maxHeight = "none";
  }
  return (
    <List component="nav" className="editable-list" style={style} ref={listRef}>
      {props.items.sort(sort).map((item) => (
        <ListItem
          button
          TouchRippleProps={{ style: { color: "#cb8300" } }}
          data-testid={`${props["data-testid"] || "list"}-item-${props.getLabel(
            item
          )}`}
          selected={props.isSelected(item)}
          ref={props.isSelected(item) ? itemRef : null}
          key={props.getId(item)}
          onClick={() => props.onSelect(item)}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <div>{props.getLabel(item)}</div>
            <div style={{ color: "#aaa" }}>{props.getAddtionalLabel(item)}</div>
          </div>
        </ListItem>
      ))}
    </List>
  );
}
