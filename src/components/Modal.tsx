import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import TextField from "@mui/material/TextField";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useStore } from "../store/Store";
import { formAction } from "../store/Reducer";
import { SupervisorSelect } from "./SupervisorSelect";
import { ValidatorForm } from "react-material-ui-form-validator";
import { Checkbox, FormControl, FormControlLabel } from "@mui/material";
import { loginURL } from "../utilities/Config";
import { DatePicker } from "./DateWidgets";

export const getModalMsgDispatch = (
  title: string,
  content: string | JSX.Element
): formAction => {
  return {
    type: "SET_MODAL_PROPS",
    modalProps: {
      open: true,
      title,
      content,
      btn1Text: "OK",
      size: "sm",
    },
  };
};

export const getInfoModal = (title: string, msg: string): formAction => {
  return {
    type: "SET_MODAL_PROPS",
    modalProps: {
      open: true,
      title: title,
      content: msg,
      btn1Text: "OK",
    },
  };
};
export const getModalEditSuccessDispatch = (
  buttonOnClick: () => void
): formAction => {
  return {
    type: "SET_MODAL_PROPS",
    modalProps: {
      open: true,
      title: "Saved",
      content: "The form was successfully updated",
      btn1Text: "OK",
      btn1OnClick: buttonOnClick,
    },
  };
};

//modal for API errors. Default values for title and message if only code is specified.
export const getModalAPIErrorDispatch = (
  error: System.APIError
): formAction => {
  let msg: string | JSX.Element = error.msg || "";
  if (!msg) {
    switch (error.code) {
      case 400:
        msg = "The server could not handle this request.";
        break;
      case 401:
        msg = (
          <>
            Your session has expired. Click{" "}
            <a
              className="link"
              href={`${loginURL}?redirect=${window.location.href}`}
            >
              here
            </a>{" "}
            to renew it.
          </>
        );
        break;
      case 403:
        msg = "You are not authorized to perform this action.";
        break;
      case 404:
        msg = "The requested resource was not found on the server.";
        break;
      case 500:
        msg =
          "Something went wrong processing your request. If the problem persists, please contact IMS.";
        break;
      case 504:
        msg =
          "Unable to connect to the database. Please try again later or contact IMS if the problem persists";
        break;
    }
  }

  return {
    type: "SET_MODAL_PROPS",
    modalProps: {
      open: true,
      title: error.title || "Error",
      content: msg,
      btn1Text: "OK",
    },
  };
};

export const getEmptyModal = (): System.ModalProps => {
  return {
    open: false,
    title: "",
    content: "",
    btn1Text: "",
    btn1Color: undefined,
    btn1OnClick: undefined,
    btn2Text: "",
    btn2Color: undefined,
    btn2OnClick: undefined,
    size: "xs",
    inputs: undefined,
  };
};
export const Modal = () => {
  const { state, dispatch } = useStore();
  const {
    open,
    title,
    content,
    btn1Text,
    btn1Color,
    btn1OnClick,
    btn2Text,
    btn2Color,
    btn2OnClick,
    size,
    validate,
    supervisorSelector,
  } = state.modalProps;

  const [supervisor, setSupervisor] = useState<System.Supervisor | undefined>(
    undefined
  );
  const [validationResult, setValidationResult] =
    useState<System.ValidationResult | null>(null);
  const [inputs, setInputs] = useState<{ [id: string]: System.ModalInput }>(
    state.modalProps.inputs || {}
  );
  useEffect(() => {
    setInputs(state.modalProps.inputs || {});
    setSupervisor(undefined);
  }, [state.modalProps.inputs, supervisorSelector]);
  const errorStyle: React.CSSProperties = {
    color: "#c73f3f",
    minHeight: "2em",
    fontSize: "0.85em",
  };
  return (
    <Dialog
      fullWidth={true}
      maxWidth={size || "xs"}
      open={open}
      onClose={() =>
        dispatch({ type: "SET_MODAL_PROPS", modalProps: getEmptyModal() })
      }
    >
      {title && <DialogTitle id="dialogTitle">{title}</DialogTitle>}
      <DialogContent>
        {content}
        <ValidatorForm onSubmit={() => null}>
          {supervisorSelector && (
            <>
              <DialogContentText style={{ marginTop: "0.5em" }}>
                {supervisorSelector.label}
              </DialogContentText>
              <FormControl variant="outlined" fullWidth={true}>
                <SupervisorSelect
                  testid="approval-modal-supervisor-select"
                  formType={supervisorSelector.formType}
                  supervisorAlias={supervisorSelector.alias}
                  allowEmptyValue={supervisorSelector.allowEmptyValue}
                  selectedEmail={supervisor?.email || ""}
                  onChange={(supervisor) => setSupervisor(supervisor)}
                />
              </FormControl>
            </>
          )}
          {Object.keys(inputs).map((id: string) => {
            switch (inputs[id].type) {
              case "text":
                return (
                  <div key={id}>
                    <TextField
                      label={inputs[id].label}
                      inputProps={{
                        "data-testid": `modal-input-${id}`,
                      }}
                      value={inputs[id].value}
                      style={{ width: "100%" }}
                      margin="normal"
                      onChange={(event) =>
                        setInputs({
                          ...inputs,
                          [id]: { ...inputs[id], value: event.target.value },
                        })
                      }
                    />
                  </div>
                );
              case "checkbox":
                return (
                  <FormControlLabel
                    key={id}
                    className="link"
                    control={
                      <Checkbox
                        size="medium"
                        checked={inputs[id].value.toLowerCase() === "true"}
                        onChange={(event) =>
                          setInputs({
                            ...inputs,
                            [id]: {
                              ...inputs[id],
                              value: event.target.checked ? "true" : "false",
                            },
                          })
                        }
                      />
                    }
                    label={inputs[id].label}
                  />
                );
              case "date":
                return (
                  <DatePicker
                    key={id}
                    disabled={false}
                    start={{
                      date: new Date(inputs[id].value),
                      label: inputs[id].label,
                      name: id,
                      setDate: (date) => {
                        setInputs({
                          ...inputs,
                          [id]: {
                            ...inputs[id],
                            value: date ? date.toString() : "",
                          },
                        });
                      },
                    }}
                    inputVariant="standard"
                    singleInputFullWidth={true}
                  />
                );
              default:
                return null;
            }
          })}
        </ValidatorForm>
        <div style={errorStyle} data-testid="modal-validation-result-div">
          {validationResult &&
            validationResult.status === "FAILURE" &&
            validationResult.errorElement}
        </div>
      </DialogContent>
      <DialogActions style={{ margin: "1em" }}>
        {btn1Text && (
          <Button
            data-testid="modal-button-1"
            onClick={() => {
              if (validate) {
                const validationResult = validate(inputs, supervisor);
                setValidationResult(validationResult);
                if (validationResult.status === "FAILURE") {
                  return;
                }
              }
              dispatch({
                type: "SET_MODAL_PROPS",
                modalProps: getEmptyModal(),
              });
              setValidationResult(null);
              if (btn1OnClick) {
                btn1OnClick(inputs, supervisor);
              }
            }}
            variant="contained"
            color={btn1Color || "primary"}
            autoFocus
          >
            {btn1Text}
          </Button>
        )}
        {btn2Text && (
          <Button
            data-testid="modal-button-2"
            onClick={() => {
              dispatch({
                type: "SET_MODAL_PROPS",
                modalProps: getEmptyModal(),
              });
              if (btn2OnClick) {
                btn2OnClick(inputs, supervisor);
              }
              setValidationResult(null);
            }}
            variant="contained"
            color={btn2Color || "info"}
            autoFocus
          >
            {btn2Text}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};
