import { FormControl, TextField } from "@mui/material";
import React from "react";
import { TextValidator } from "react-material-ui-form-validator";

/**
 * This component is a complete mess and needs to be replaced as soon as possible. mui5 introduced braking for
 * TextValidator from the npm package `react-material-ui-form-validator` and this is a temporary fix.
 */
export const DomniTextValidator = React.forwardRef(
  (
    props: {
      style?: React.CSSProperties;
      disabled?: boolean;
      value: string | number | undefined;
      testId?: string;
      name?: string;
      label?: string;
      onChange?: (event: any) => void;
      validators?: string[];
      errorMessages?: string[] | string;
      fullWidth?: boolean;
      type?: string;
      variant?: "outlined" | "standard" | "filled" | undefined;
      inputProps?: any;
      multiline?: boolean;
      placeholder?: string;
      onFocus?: (event: any) => void;
      rows?: number;
      size?: "small" | "medium" | undefined;
      containerStyle?: React.CSSProperties;
    },
    ref
  ) => {
    const {
      onFocus,
      rows,
      size,
      style,
      disabled,
      value,
      testId,
      name,
      label,
      onChange,
      validators,
      errorMessages,
      fullWidth,
      type,
      variant,
      inputProps,
      multiline,
      placeholder,
      containerStyle,
      ...rest
    } = props;
    return (
      <div ref={ref as any} {...rest} style={containerStyle}>
        <FormControl fullWidth={true}>
          <TextValidator
            type={props.type || "text"}
            disabled={props.disabled}
            value={props.value}
            inputProps={{
              "data-testid": props.testId || props.name || "",
              ...inputProps,
            }}
            validators={props.validators}
            errorMessages={props.errorMessages}
            style={props.style}
            name={props.name || ""}
            label={props.label || ""}
            onChange={props.onChange}
            variant={props.variant || "standard"}
            fullWidth={props.fullWidth !== false}
            placeholder={props.placeholder}
            multiline={props.multiline}
            onFocus={props.onFocus}
            rows={props.rows}
            size={props.size}
          />
        </FormControl>
      </div>
    );
  }
);

export const TextValidatorFixed2 = React.forwardRef(
  (
    props: {
      style?: React.CSSProperties;
      disabled?: boolean;
      value: string | number | undefined;
      testId?: string;
      name?: string;
      label: string;
      onChange?: (event: any) => void;
      validators?: string[];
      errorMessages?: string[] | string;
      fullWidth?: boolean;
      type?: string;
      variant: "outlined" | "standard" | "filled" | undefined;
      inputProps?: any;
      multiline?: boolean;
      placeholder?: string;
      onFocus?: (event: any) => void;
      rows?: number;
      size?: "small" | "medium" | undefined;
    },
    ref
  ) => {
    return (
      <div ref={ref as any}>
        <TextValidator
          type={props.type || "text"}
          disabled={props.disabled}
          value={props.value}
          inputProps={{
            "data-testid": props.testId || "",
          }}
          style={props.style}
          name={props.name || ""}
          label={props.label}
          onChange={props.onChange}
          variant={props.variant}
          fullWidth={props.fullWidth !== false}
          placeholder={props.placeholder}
          multiline={props.multiline}
          onFocus={props.onFocus}
          rows={props.rows}
          size={props.size}
        />
      </div>
    );
  }
);
export const TextValidator2 = (props: {
  style?: React.CSSProperties;
  disabled?: boolean;
  value: string | number | undefined;
  testId?: string;
  name?: string;
  label: string;
  onChange?: (event: any) => void;
  validators?: string[];
  errorMessages?: string[] | string;
  fullWidth?: boolean;
  type?: string;
  variant: "outlined" | "standard" | "filled" | undefined;
  inputProps?: any;
  multiline?: boolean;
  placeholder?: string;
  onFocus?: (event: any) => void;
  rows?: number;
  size?: "small" | "medium" | undefined;
}) => {
  //todo implement validation
  return (
    <TextField
      sx={props.style}
      type={props.type || "text"}
      disabled={props.disabled}
      value={props.value}
      data-testid={props.testId}
      name={props.name}
      label={props.label}
      onChange={props.onChange}
      variant={props.variant}
      fullWidth={props.fullWidth !== false}
      placeholder={props.placeholder}
      multiline={props.multiline}
      onFocus={props.onFocus}
      rows={props.rows}
      size={props.size}
    />
  );
};
