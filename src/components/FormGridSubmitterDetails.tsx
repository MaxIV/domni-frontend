import { Grid, Tooltip, Typography } from "@mui/material";
import * as Constants from "../constants";
import { useStore } from "../store/Store";
import { DomniTextValidator } from "./TextValidator";

interface Props {
  form: System.Form;
}

export const FormGridSubmitterDetails = (props: Props) => {
  const { state } = useStore();

  return (
    <>
      <Grid item xs={12}>
        <Typography className="form-heading">Submitter Details</Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <DomniTextValidator
          disabled
          fullWidth={true}
          label="Submitter name"
          name="submitterName"
          value={props.form.submitterName || state.user.name}
          variant="outlined"
          testId="form-submitter-name-input"
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Tooltip title={Constants.EMAIL_TOOLTIP} placement="top-start">
          <DomniTextValidator
            disabled
            fullWidth={true}
            label="Submitter email"
            name="submitterEmail"
            value={props.form.submitterEmail || state.user.email}
            variant="outlined"
            testId="form-submitter-email-input"
          />
        </Tooltip>
      </Grid>
    </>
  );
};
