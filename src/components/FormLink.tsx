import React from "react";
import { Link } from "react-router-dom";
import { getFormConfig } from "../utilities/Config";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import { Tooltip, IconButton } from "@mui/material";
interface Props {
  formType: System.FormType;
  id?: string;
  color?: string; //if defined, overrides the icon color
  editable: boolean; //if false, just render an empty div
  style?: React.CSSProperties;
}
export const FormLink = (props: Props) => {
  const { formType, id, editable, style } = props;
  return (
    <Tooltip
      title={
        editable
          ? "View or edit the content of this submission"
          : "View the content of this submission"
      }
      placement="top"
      arrow
    >
      <Link
        data-testid={`form-${id}-view-edit-form-link`}
        to={`/${getFormConfig(formType).path}/form/${id}`}
        style={style || {}}
      >
        <IconButton
          style={{ color: props.color || "#de9300" }}
          size="small"
          aria-label="edit"
        >
          <ExitToAppIcon />
        </IconButton>
      </Link>
    </Tooltip>
  );
};
