import React from "react";
import { useStore } from "../store/Store";
import { selectSupervisors } from "../store/Reducer";
import { SelectValidator } from "react-material-ui-form-validator";

interface Props {
  formType: System.FormType;
  selectedEmail: string;
  allowEmptyValue?: boolean;
  testid?: string;
  supervisorAlias?: string; //if you want a different name than "supervisor"
  onChange: (supervisor: System.Supervisor) => void;
}
export const SupervisorSelect = (props: Props) => {
  const { state } = useStore();
  const supervisors = selectSupervisors(state, props.formType);
  let selectedSupervisorId = "";
  try {
    selectedSupervisorId = supervisors.filter(
      (s) => s.email.toLowerCase() === props.selectedEmail.toLowerCase()
    )[0]._id;
  } catch (error) {}

  return (
    <SelectValidator
      variant="outlined"
      fullWidth={true}
      SelectProps={{
        native: true,
      }}
      inputProps={{
        "data-testid": props.testid || "supervisor-select",
      }}
      name="supervisorEmail"
      value={selectedSupervisorId}
      onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
        props.onChange(
          supervisors.filter((s) => s._id === (event.target.value as string))[0]
        )
      }
      validators={["required"]}
      errorMessages={["This field is required"]}
    >
      <option value="" disabled={!props.allowEmptyValue}>
        {props.supervisorAlias || "Supervisor"}
      </option>
      {supervisors.map((s) => (
        <option key={s._id} value={s._id}>
          {s.name}
        </option>
      ))}
    </SelectValidator>
  );
};
