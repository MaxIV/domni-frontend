import React, { useState } from "react";
import { getAllYears } from "../utilities/Helpers";
import { SimpleSelect } from "./SimpleSelect";
import moment from "moment";
import { Button, TextField, Tooltip } from "@mui/material";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { DesktopDatePicker, MobileDatePicker, TimePicker } from "@mui/lab";
import { useWindowDimensions } from "../utilities/Helpers";
interface Props {
  start: {
    hidden?: boolean;
    label: string;
    name: string;
    date: Date | undefined;
    setDate: (date: Date | undefined) => void;
    tooltip?: string;
  };
  end?: {
    hidden?: boolean;
    label: string;
    name: string;
    date: Date | undefined;
    setDate: (date: Date | undefined) => void;
    tooltip?: string;
  };
  inputVariant?: "outlined" | "standard" | "filled";
  containerStyle?: React.CSSProperties;
  singleInputFullWidth?: boolean; //if true, width = 100% for single input rendered
  disabled: boolean; //disabled fieldset only disables input, not date picker button
  visible?: boolean;
}
export const DatePicker = (props: Props) => {
  const { md } = useWindowDimensions();
  const handleStartDateChange = (newValue: Date | undefined) => {
    props.start.setDate(newValue);
  };

  const handleEndDateChange = (newValue: Date | undefined) => {
    if (!props.end) {
      return;
    }
    const date = moment(newValue).endOf("day").toDate();

    props.end.setDate(date);
  };
  const inbetweenPadding = props.end ? "0.5em" : "0";
  return (
    <div style={props.containerStyle}>
      <Tooltip title={props.start.tooltip || ""} placement="top-start">
        <div
          style={{
            width: props.singleInputFullWidth && !props.end ? "100%" : "50%",
            boxSizing: "border-box",
            display: props.start.hidden === true ? "none" : "inline-flex",
          }}
        >
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            {md ? (
              <DesktopDatePicker
                mask="____-__-__"
                renderInput={(params) => (
                  <TextField
                    name={props.start.name}
                    sx={{ width: "100%", paddingRight: inbetweenPadding }}
                    variant={props.inputVariant || "outlined"}
                    {...params}
                  />
                )}
                disabled={props.disabled}
                label={props.start.label}
                inputFormat="yyyy-MM-dd"
                value={props.start.date || null}
                onChange={(date: Date | null) =>
                  handleStartDateChange(date ? date : undefined)
                }
              />
            ) : (
              <MobileDatePicker
                mask="____-__-__"
                renderInput={(params) => (
                  <TextField
                    name={props.start.name}
                    sx={{ width: "100%", paddingRight: inbetweenPadding }}
                    variant={props.inputVariant || "outlined"}
                    {...params}
                  />
                )}
                disabled={props.disabled}
                label={props.start.label}
                inputFormat="yyyy-MM-dd"
                value={props.start.date || null}
                onChange={(date: Date | null) =>
                  handleStartDateChange(date ? date : undefined)
                }
              />
            )}
          </LocalizationProvider>
        </div>
      </Tooltip>
      {props.end && !props.end.hidden && (
        <Tooltip title={props.end.tooltip || ""} placement="top-start">
          <div
            style={{
              display: "inline-flex",
              width:
                props.singleInputFullWidth && !props.start.hidden
                  ? "100%"
                  : "50%",
            }}
          >
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              {md ? (
                <DesktopDatePicker
                  mask="____-__-__"
                  renderInput={(params) => (
                    <TextField
                      name={props.end?.name || ""}
                      sx={{ width: "100%", paddingLeft: inbetweenPadding }}
                      variant={props.inputVariant || "outlined"}
                      {...params}
                    />
                  )}
                  disabled={props.disabled}
                  label={props.end.label}
                  inputFormat={"yyyy-MM-dd"}
                  value={props.end.date || null}
                  onChange={(date: Date | null) =>
                    handleEndDateChange(date ? date : undefined)
                  }
                />
              ) : (
                <MobileDatePicker
                  mask="____-__-__"
                  renderInput={(params) => (
                    <TextField
                      name={props.end?.name || ""}
                      sx={{ width: "100%", paddingLeft: inbetweenPadding }}
                      variant={props.inputVariant || "outlined"}
                      {...params}
                    />
                  )}
                  disabled={props.disabled}
                  label={props.end.label}
                  inputFormat={"yyyy-MM-dd"}
                  value={props.end.date || null}
                  onChange={(date: Date | null) =>
                    handleEndDateChange(date ? date : undefined)
                  }
                />
              )}
            </LocalizationProvider>
          </div>
        </Tooltip>
      )}
    </div>
  );
};

export const DomniTimePicker = (props: {
  label: string;
  value: Date | null;
  disabled: boolean;
  testId?: string;
  onChange: (date: any, keyboardInputValue?: string | undefined) => void;
}) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <TimePicker
        disabled={props.disabled}
        ampm={false}
        label={props.label}
        value={props.value}
        renderInput={(params) => (
          <TextField data-testid={props.testId} fullWidth {...params} />
        )}
        onChange={props.onChange}
      />
    </LocalizationProvider>
  );
};
/**
 * A widget presenting 2 inputs for week (input type number) and year (dropdown containing years 2020-now), as well as a 'SET' button.
 * Upon clicking on SET, `onSelect` is triggered and provides the date interval covering the entire ISO week (Monday morning till Sunday night)
 */
export const YearAndWeekPicker = (props: {
  onSelect: (startDate: Date, endDate: Date) => void;
  defaultYear?: number;
  defaultWeek?: number;
  buttonTooltip?: string | JSX.Element;
}) => {
  const [year, setYear] = useState<number>(
    props.defaultYear || new Date().getFullYear()
  );
  const [week, setWeek] = useState<number>(
    props.defaultWeek || moment().isoWeek()
  );
  return (
    <>
      <TextField
        fullWidth={false}
        style={{ width: "5em", marginRight: "1em" }}
        name="week"
        type="number"
        value={week}
        label="Week no."
        onChange={(e) => {
          setWeek(parseInt(e.target.value));
        }}
        variant="standard"
      />
      <YearPicker
        style={{ width: "5em" }}
        value={year}
        onSelect={(year) => setYear(year)}
      />
      <Tooltip title={props.buttonTooltip || ""} placement="top-start">
        <Button
          color="primary"
          onClick={() => {
            const startOfWeek = getDateOfISOWeek(week, year);
            const endOfWeek = moment(new Date(startOfWeek))
              .day(7)
              .endOf("day")
              .toDate();
            props.onSelect(startOfWeek, endOfWeek);
          }}
        >
          SET
        </Button>
      </Tooltip>
    </>
  );
};

export const YearPicker = (props: {
  onSelect: (year: number) => void;
  style?: React.CSSProperties;
  value: number;
  testid?: string;
}) => {
  return (
    <SimpleSelect
      testid={props.testid || ""}
      disabled={{
        value: false,
      }}
      options={getAllYears().map((year) => {
        return { value: year };
      })}
      selectedValue={props.value}
      fullWidth={false}
      style={props.style}
      variant="standard"
      title="Year"
      name="selectedYear"
      onChange={(event) => {
        props.onSelect(event.target.value);
      }}
    />
  );
};

const getDateOfISOWeek = (week: number, year: number) => {
  var simple = new Date(year, 0, 1 + (week - 1) * 7);
  var dow = simple.getDay();
  var ISOweekStart = simple;
  if (dow <= 4) ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
  else ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
  return ISOweekStart;
};
