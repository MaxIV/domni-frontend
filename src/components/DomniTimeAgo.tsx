import TimeAgo from "react-timeago";
import moment from "moment";
import React from "react";
import { Tooltip } from "@mui/material";
const DomniTimeAgo = (props: { datetime: Date }) => {
  return (
    <Tooltip
      title={moment(props.datetime).format("YYYY-MM-DD hh:mm")}
      placement="top-start"
    >
      <span style={{ cursor: "pointer" }}>
        <TimeAgo date={props.datetime} />
      </span>
    </Tooltip>
  );
};
export default DomniTimeAgo;
