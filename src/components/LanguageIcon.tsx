import React from "react";
import { Tooltip } from "@mui/material";
interface Props {
  lang: "sv" | "en";
  testId?: string;
  onClick: () => void;
}
export const LanguageIcon = (props: Props) => {
  const { lang, onClick } = props;
  return (
    <Tooltip
      placement="top-start"
      title={
        "Click to show this test in " + (lang === "sv" ? "English" : "Swedish")
      }
    >
      <img
        alt={lang}
        className="language-icon"
        data-testid={props.testId || "language"}
        src={
          process.env.PUBLIC_URL + "/" + (lang === "sv" ? "en" : "sv") + ".png"
        }
        onClick={onClick}
      />
    </Tooltip>
  );
};
