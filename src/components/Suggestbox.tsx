import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Autocomplete from "@mui/material/Autocomplete";
import { SyntheticEvent } from "react";

// interface Props<T> {
//   suggestions: T[];
//   renderValue: (sugvaluegestion: T | null) => Element | string;
//   label: string;
//   value: T | null;
//   onChange: (value: string | Element | null) => void;
// }
// export function SuggestBox<T>(props: Props<T>) {
//   return (
//     <Autocomplete
//       autoComplete
//       autoHighlight
//       value={props.renderValue(props.value)}
//       onChange={(
//         event: SyntheticEvent<Element, Event>,
//         newValue: string | Element | null
//       ) => {
//         props.onChange(newValue);
//       }}
//       options={props.suggestions.map((suggestion) =>
//         props.renderValue(suggestion)
//       )}
//       renderInput={(params) => <TextField {...params} label={props.label} />}
//     />
//   );
// }

export function SuggestBox(props: {
  option: any[];
  value: string;
  onChange: (value: string | Element | null) => void;
  getOptionLabel: (option: any) => string;
}) {
  return (
    <Autocomplete
      autoComplete
      autoHighlight
      freeSolo
      value={props.value}
      onChange={(
        event: SyntheticEvent<Element, Event>,
        newValue: string | Element | null
      ) => {
        props.onChange(newValue);
      }}
      options={props.option}
      renderInput={(params) => (
        <TextField {...params} label={props.getOptionLabel(props.option)} />
      )}
    />
  );
}
