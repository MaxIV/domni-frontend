import React from "react";
import { IconButton, Button, Tooltip } from "@mui/material";
import "./DynamicInputTable.css";
import DeleteIcon from "@mui/icons-material/Delete";
interface Props<T> {
  emptyMessage: string;
  onAddRow: () => void;
  onDeleteRow: (rowIndex: number) => void;
  canAddRow?: boolean;
  data: T[];
  addRowText?: string;
  disabled: boolean;
  compactMode?: boolean; //defaults to true, e.g. assumes that inputs are in small/compact mode
  deleteRowText?: string;
  columnOrder: (keyof T)[];
  testId?: string;
  columnDefinitions: {
    [property in keyof T]: {
      title: string;
      isStateful?: boolean;
      Component: (props: { rowIndex: number }) => JSX.Element;
      width?: string;
    };
  };
}
export function DynamicInputTable<T extends object>(props: Props<T>) {
  const {
    columnDefinitions,
    onAddRow,
    onDeleteRow,
    emptyMessage,
    columnOrder,
    data,
    addRowText,
    deleteRowText,
    compactMode,
    disabled,
    canAddRow,
    testId,
  } = props;
  const addRow = () => {
    onAddRow();
  };
  const deleteRow = (rowIndex: number) => {
    onDeleteRow(rowIndex);
  };
  const rootTestId = `dynamicInputTable${testId ? "-" + testId : ""}`;
  return (
    <div>
      <div style={{ margin: "0em -1em" }}>
        <section>
          <header>
            {columnOrder.map((columnName) => {
              return (
                <div
                  key={"header-" + (columnName as string)}
                  className="col table-header"
                >
                  {columnDefinitions[columnName].title}
                </div>
              );
            })}
            <div className="col table-header"></div>
          </header>
          {data.map((_, rowIndex) => {
            return (
              <div key={rowIndex}>
                {columnOrder.map((columnName) => {
                  const { width } = columnDefinitions[columnName];
                  const style = width ? { width: width } : {};
                  if (columnDefinitions[columnName].isStateful) {
                    const { Component } = columnDefinitions[columnName];
                    return (
                      <div
                        key={columnName as string}
                        className="col table-row"
                        style={style}
                      >
                        {/* careful - this const assignment is really problematic because it unmounts between each render. However, it is the only option of the component is stateful*/}
                        <Component key={rowIndex} rowIndex={rowIndex} />
                      </div>
                    );
                  } else {
                    return (
                      <div
                        key={columnName as string}
                        className="col table-row"
                        style={style}
                      >
                        {columnDefinitions[columnName].Component({ rowIndex })}
                      </div>
                    );
                  }
                })}
                <div className="col">
                  {!disabled && (
                    <Tooltip
                      title={deleteRowText || "Delete this row"}
                      placement="top-start"
                    >
                      <IconButton
                        data-testid={`${rootTestId}-delete-row-${rowIndex}-button`}
                        size={compactMode !== false ? "small" : "large"}
                        onClick={() => deleteRow(rowIndex)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  )}
                </div>
              </div>
            );
          })}
        </section>
      </div>
      {data.length === 0 && (
        <div
          className="empty-msg"
          style={{ padding: compactMode ? "0.5em" : "1em" }}
        >
          {emptyMessage ||
            "Nothing to display! Click on the plus button to add a row"}
        </div>
      )}
      {!disabled && (
        <Button
          style={{ marginTop: "1em" }}
          variant="contained"
          size="small"
          onClick={addRow}
          disabled={canAddRow === false}
          data-testid={`${rootTestId}-new-row-button`}
        >
          {addRowText || "Add row"}
        </Button>
      )}
    </div>
  );
}
