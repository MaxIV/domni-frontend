import React, { useEffect } from "react";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import { formatDate, resolveStepperRole } from "../utilities/Helpers";
import { useState } from "react";
import { Tooltip } from "@mui/material";
import { useStore } from "../store/Store";

const StepIcon = (props: {
  active: boolean;
  completed: boolean;
  node: Node;
  form: System.Form;
}) => {
  const { node } = props;

  return (
    <Tooltip
      title={
        node.description ? (
          node.action !== "SUPPLANTED" ? (
            <i>{node.description}</i>
          ) : (
            node.description
          )
        ) : (
          ""
        )
      }
      placement="top"
      arrow
    >
      <div
        className="step-icon"
        style={{
          color: node.color,
          borderColor: node.color,
          borderStyle: node.action === "PENDING" ? "dashed" : "solid",
        }}
      >
        {node.action.toLowerCase()}
      </div>
    </Tooltip>
  );
};

type NodeType =
  | "rootNode"
  | "comment"
  | "edit"
  | "supervisorAction"
  | "systemEvent";
interface Node {
  action: NodeAction;
  timestamp: Date | undefined;
  agent: string;
  role: string;
  description: string;
  color: string;
  nodeType: NodeType;
}
type NodeAction =
  | "PENDING"
  | "APPROVED"
  | "REJECTED"
  | "SUBMITTED"
  | "NOTIFIED"
  | "SUPPLANTED"
  | "COMMENTED"
  | "EDITED";
const getActionColor = (action: NodeAction) => {
  return action === "PENDING"
    ? "#aaa"
    : action === "APPROVED"
    ? "#328732"
    : action === "REJECTED"
    ? "#c20c0c"
    : "#aaa";
};
export default function SupervisorStepper(props: { form: System.Form }) {
  const { state } = useStore();
  const [nodes, setNodes] = useState<Node[]>([]);
  const { form } = props;
  useEffect(() => {
    const {
      submitterName,
      submissionDate,
      status,
      edits,
      supervisorActions,
      systemEvents,
      comments,
    } = props.form;
    setNodes(
      [
        {
          agent: submitterName,
          timestamp: submissionDate,
          action:
            status === "PENDING" ? "SUBMITTED" : ("NOTIFIED" as NodeAction),
          role: "Submitter",
          description: "",
          color: status === "PENDING" ? "#333" : "#328732",
          nodeType: "rootNode" as NodeType,
        },
        ...supervisorActions.map((action) => {
          return {
            agent: action.name,
            timestamp: action.decisionDate,
            action: action.action as NodeAction,
            role: action.role || resolveStepperRole(props.form, action.name),
            description: action.comment,
            color: getActionColor(action.action as NodeAction),
            nodeType: "supervisorAction" as NodeType,
          };
        }),
        ...(edits || []).map((edit) => {
          return {
            agent: edit.name,
            timestamp: edit.timestamp,
            action: "EDITED" as NodeAction,
            role: edit.role || resolveStepperRole(props.form, edit.name),
            description: "",
            color: "#de9300",
            nodeType: "edit" as NodeType,
          };
        }),
        ...(systemEvents || []).map((event) => {
          return {
            agent: event.agent,
            timestamp: event.timestamp,
            action: event.action as NodeAction,
            role: event.role,
            description: event.description,
            color: event.color,
            nodeType: "systemEvent" as NodeType,
          };
        }),
        ...(comments || [])
          .filter((comment) => comment.source === "REGULAR COMMENT")
          .map((comments) => {
            return {
              agent: comments.author,
              timestamp: comments.timestamp,
              action: "COMMENTED" as NodeAction,
              role:
                comments.role ||
                resolveStepperRole(props.form, comments.author),
              description: comments.msg,
              color: "#de9300",
              nodeType: "comment" as NodeType,
            };
          }),
      ].sort((a, b) => {
        if (!a.timestamp) {
          return 1;
        }
        if (!b.timestamp) {
          return -1;
        }
        return a.timestamp > b.timestamp ? 1 : -1;
      })
    );
  }, [form.formType, props, state]);
  return (
    <div style={{ display: "flex", marginBottom: "2em" }}>
      <div style={{ flex: "1 1 100%", width: 0 }}>
        <Stepper style={{ overflowX: "auto" }} alternativeLabel activeStep={-1}>
          {nodes.map((node: Node, index: number) => {
            return (
              <Step
                data-testid={`form-${form._id}-step-${index + 1}-div`}
                key={
                  node.timestamp
                    ? new Date(node.timestamp).getTime()
                    : ((node.agent + node.action) as string)
                }
              >
                <StepLabel
                  StepIconComponent={(props) =>
                    StepIcon({ ...props, node, form })
                  }
                >
                  <div>{node.agent}</div>
                  <small style={{ display: "block" }}>{node.role}</small>
                  <small>{formatDate(node.timestamp)}</small>
                </StepLabel>
              </Step>
            );
          })}
        </Stepper>
      </div>
    </div>
  );
}
