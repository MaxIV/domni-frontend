import React from "react";
import {
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Tooltip,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { ChangeEvent } from "react";
import { isInLocalDevelopment } from "../utilities/Helpers";

interface Props {
  options: System.selectOption[];
  selectedValue: string | number;
  title?: string;
  size?: "small" | "medium";
  name: string;
  forceNonNative?: boolean;
  variant?: "outlined" | "standard" | "filled" | undefined;
  style?: React.CSSProperties;
  selectStyle?: React.CSSProperties;
  darkTheme?: boolean;
  toolTip?: string; //tooltip for select. Overridden disabled.message if defined.
  disabled?: { value: boolean; msg?: string };
  fullWidth?: boolean;
  onChange: (
    event: React.ChangeEvent<{
      name: string;
      value: any;
      type: any;
      checked?: boolean;
    }>
  ) => void;
  testid?: string;
}

export const SimpleSelect = (props: Props) => {
  const {
    options,
    selectedValue,
    title,
    name,
    onChange,
    variant,
    fullWidth,
    darkTheme,
    testid,
    disabled,
    toolTip,
    forceNonNative,
    style,
    size,
  } = props;
  const darkThemeSelect = makeStyles({
    root: {
      color: "#aaa !important",
      borderBottom: "1px solid #aaa",
    },
    select: {
      color: "#ccc !important",
    },
    icon: {
      color: "#aaa",
    },
  });
  const darkClassesSelect = darkThemeSelect();
  const darkThemeInput = makeStyles({
    root: {
      color: "#aaa !important",
    },
  });
  //note - this is for the label that says "page size" (moves up on select)
  const darkClassesInput = darkThemeInput();
  const toolTipTitle =
    disabled && disabled.msg ? disabled.msg : toolTip ? toolTip : "";
  return (
    <Tooltip title={toolTipTitle} placement="top-start">
      <FormControl
        variant={variant || "outlined"}
        fullWidth={fullWidth !== false}
        style={style}
        size={size || "medium"}
      >
        <InputLabel classes={darkTheme ? darkClassesInput : {}}>
          {title}
        </InputLabel>
        {/* This MUI widget is very problematic. It's a design mess that doesn't even use a real select. Untargetable in testing. 
        Use SelectValidator instead - not bloated, uses native select under the hood, and supports custom validation.
        SelectValidator is also problematic - must be inside a FormValidator component, and has ugly native rendering. 
        Current solution is render as either native or non-native depending on if you're in local dev or not
        In addtional to all simpleselect use, it's used in around 10 more places, mostly riskassessment. All should be replaced */}
        <Select
          classes={darkTheme ? darkClassesSelect : {}}
          name={name}
          disabled={disabled && disabled.value}
          style={props.selectStyle}
          label={title}
          inputProps={{ "data-testid": testid }}
          native={isInLocalDevelopment() && !forceNonNative}
          onChange={(event) => {
            onChange(
              event as ChangeEvent<{
                name: string;
                value: any;
                type: any;
                checked?: boolean;
              }>
            );
          }}
          value={selectedValue}
        >
          {options.map((option) => {
            if (isInLocalDevelopment() && !forceNonNative) {
              return (
                <option
                  disabled={option.disabled === true}
                  key={option.value}
                  value={option.value}
                >
                  {option?.label || option.value}
                </option>
              );
            }
            return (
              <MenuItem
                disabled={option.disabled === true}
                key={option.value}
                value={option.value}
              >
                {option?.label || option.value}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Tooltip>
  );
};
