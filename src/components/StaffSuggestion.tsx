import React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CircularProgress from "@mui/material/CircularProgress";
import { ldapSearch } from "../api/ldap";

interface Props {
  onChange: (selectedValue: System.LDAPPerson | null) => void;
  placeholder?: string;
  size?: "small" | "medium" | undefined;
}

export const StaffSuggestion = (props: Props) => {
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [suggestions, setSuggestions] = React.useState<System.LDAPPerson[]>([]);

  React.useEffect(() => {
    const search = async () => {
      if (searchQuery.length > 1) {
        setLoading(true);
        const people = await ldapSearch({ searchQuery, attribute: "fullName" });
        setSuggestions(people);
      }
    };
    search();
  }, [searchQuery]);
  React.useEffect(() => {
    setLoading(false);
  }, [suggestions]);

  return (
    <Autocomplete
      noOptionsText={searchQuery.length > 1 ? "No matches!" : "Type a name"}
      size={props.size}
      autoHighlight={true}
      open={open}
      onChange={(event: object, value: System.LDAPPerson | null) => {
        props.onChange(value);
      }}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      isOptionEqualToValue={(option, value) => option.DN === value.DN}
      getOptionLabel={(option) => option.fullName}
      options={suggestions}
      loading={loading}
      renderInput={(params) => (
        <TextField
          {...params}
          style={{ width: "10em", marginLeft: "1em" }}
          label={props.placeholder || "MAX IV Staff"}
          variant="standard"
          value={searchQuery}
          onChange={(e) => {
            setSearchQuery(e.target.value);
          }}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? <CircularProgress color="info" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
};
