import React from "react";
import { useStore } from "../store/Store";
import { selectProjects, selectCategorizedProjects } from "../store/Reducer";
import { SelectValidator } from "react-material-ui-form-validator";

interface Props {
  formType: System.FormType;
  selectedProject: string;
  projectAlias?: string; //if you want a different name than "project"
  disabled?: boolean;
  style?: React.CSSProperties;
  required: boolean;
  onChange: (project: System.Project) => void;
}
export const ProjectSelect = (props: Props) => {
  const { state } = useStore();
  const projects = selectProjects(state, props.formType);
  const categorizedProjects = selectCategorizedProjects(state, props.formType);
  let selectedProjectId = "";
  try {
    selectedProjectId = projects.filter(
      (s) => s.project === props.selectedProject
    )[0]._id;
  } catch (error) {}

  return (
    <SelectValidator
      style={props.style}
      disabled={props.disabled === true}
      name="supervisorProject"
      variant="outlined"
      fullWidth={true}
      inputProps={{
        "data-testid": "project-select",
      }}
      onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
        props.onChange(
          projects.filter((p) => p._id === (event.target.value as string))[0]
        );
      }}
      value={selectedProjectId}
      SelectProps={{
        native: true,
      }}
      validators={props.required ? ["required"] : []}
      errorMessages={props.required ? ["This field is required"] : []}
    >
      <option value="" disabled>
        {props.projectAlias || "Project"}
      </option>
      {Object.keys(categorizedProjects).map((category) => (
        <optgroup label={category} key={category}>
          {categorizedProjects[category].map((project: System.Project) => (
            <option key={project._id} value={project._id}>
              {project.project}
            </option>
          ))}
        </optgroup>
      ))}
    </SelectValidator>
  );
};
