import React from "react";
export const SafetyClassificationLED = (props: { value: string }) => {
  let className = "";
  switch (props.value) {
    case "green":
      className = "led-green";
      break;
    case "yellow":
      className = "led-yellow";
      break;
    case "red":
      className = "led-red";
      break;
    default:
      className = "led-gray";
  }
  return <div className={`led ${className}`} />;
};
