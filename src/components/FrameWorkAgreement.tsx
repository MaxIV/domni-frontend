import React from "react";
import { Collapse, TextField, Tooltip, Grid } from "@mui/material";

type Props = {
  show: boolean;
  onChange: (
    event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => any;
  supplier: string;
  bidder1: string;
  bidder2: string;
  bidder3: string;
  motivation: string;
};
const Noframe = (props: Props) => {
  return (
    <Collapse in={props.show}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div style={{ fontSize: "0.85em", color: "#777" }}>
            If there is NO Framework Agreement, you must get at least three (3)
            offers/bids from different suppliers who qualify. Then please state
            the suppliers and motivate the choice of supplier.
          </div>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Tooltip
            title="Enter the party that will be supplying the product and motivate below why their bid was chosen over those of the other bidders."
            placement="top-start"
          >
            <TextField
              name="supplierName"
              fullWidth={true}
              label="Supplier / winning bid"
              value={props.supplier}
              onChange={(event) => props.onChange(event)}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth={true}
            name="bidder1"
            label="Other bidder"
            value={props.bidder1}
            onChange={(event) => props.onChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth={true}
            name="bidder2"
            label="Other bidder"
            value={props.bidder2}
            onChange={(event) => props.onChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            fullWidth={true}
            name="bidder3"
            label="Other bidder"
            value={props.bidder3}
            onChange={(event) => props.onChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <div className="form-heading">Motivation</div>
        </Grid>
        <Grid item xs={12}>
          <Tooltip
            title="Motivate your choice of supplier; the motivation can be the price, quality, availability etc."
            placement="top-start"
          >
            <TextField
              fullWidth={true}
              label="Enter motivation"
              onChange={(event) => props.onChange(event)}
              name="motivation"
              value={props.motivation}
              variant="outlined"
              multiline={true}
            />
          </Tooltip>
        </Grid>
      </Grid>
    </Collapse>
  );
};

export default Noframe;
