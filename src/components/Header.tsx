import {
  AppBar,
  Button,
  Divider,
  Drawer,
  IconButton,
  Toolbar,
  Tooltip,
  useMediaQuery,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import LogoutIcon from "@mui/icons-material/Logout";
import React, { useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import logo from "../images/maxiv.png";
import { useStore } from "../store/Store";
import {
  getFormConfig,
  getFormConfigs,
  getFormTypes,
} from "../utilities/Config";
import {
  getLatestSupervisorAction,
  isInProduction,
  isStaff,
} from "../utilities/Helpers";
import "./Header.css";

export const Header = (props: any) => {
  const { state } = useStore();
  const largeScreen = useMediaQuery("(min-width:960px)");
  const nbrNotifications = (
    Object.keys(state.pendingSupervisorCases) as System.FormType[]
  )
    .map(
      (formType) =>
        state.pendingSupervisorCases[formType].filter(
          (form) =>
            getLatestSupervisorAction(form).email.toLowerCase() ===
            state.user.email.toLowerCase()
        ).length
    )
    .reduce((prev, curr) => prev + curr);
  let loginMessage = (
    <div className="header-login-message secondary-main">
      {state.user.name}
      {nbrNotifications > 0 && (
        <Tooltip
          title={`You have ${nbrNotifications} pending supervisor cases that require action`}
          placement="top"
          arrow
        >
          <div className="notification-tag">{nbrNotifications}</div>
        </Tooltip>
      )}
    </div>
  );
  const [drawerIsOpen, setDrawerIsOpen] = React.useState(false);
  const [formsMenuOpen, setFormsMenuOpen] = React.useState(false);
  const [archivesMenuOpen, setArchivesMenuOpen] = React.useState(false);
  const [settingsMenuOpen, setSettingsMenuOpen] = React.useState(false);
  const [adminOptions, setAdminOptions] = React.useState<System.FormType[]>([]);
  const isPrint = useLocation().pathname.includes("/print/");

  useEffect(() => {
    // check if logged in user's groups list matches any admin groups, then set archives list to reflect that
    if (state.user.admin) {
      const formTypes: System.FormType[] = getFormTypes({
        excludeSimple: true,
      });
      setAdminOptions(
        formTypes.filter((formType) => state.user.admin[formType])
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.user.admin]);

  const handleHamburgerToggle = () => {
    setDrawerIsOpen(!drawerIsOpen);
  };
  if (isPrint) {
    return null;
  }
  const domniTitle =
    !isInProduction() && state.dbData.type === "PRODUCTION" ? (
      <span style={{ color: "red", fontWeight: "bold" }}>
        WARNING! DOMNI PROD DB!
      </span>
    ) : (
      <>Domni</>
    );
  return (
    <div>
      {largeScreen ? (
        <>
          <div style={{ height: "4em" }}></div>
          <AppBar elevation={5} position="fixed">
            <Toolbar className="header-toolbar">
              <div
                style={{ display: "flex", alignItems: "center", width: "40%" }}
              >
                <IconButton
                  edge="start"
                  aria-label="menu"
                  href="http://www.maxiv.lu.se"
                  size="large"
                >
                  <img className="header-max-logo" src={logo} alt="Logo" />
                </IconButton>
                <div style={{ display: "flex" }}>
                  <div
                    data-testid="header-menu-form"
                    onMouseEnter={() => setFormsMenuOpen(true)}
                    onMouseLeave={() => setFormsMenuOpen(false)}
                  >
                    <div
                      className={
                        formsMenuOpen
                          ? "header-dropdown-button-active"
                          : "header-dropdown-button"
                      }
                    >
                      Forms
                    </div>
                    {formsMenuOpen && (
                      <div
                        className="header-dropdown-content"
                        style={{ padding: "0em" }}
                      >
                        <FormMenu onClick={() => setFormsMenuOpen(false)} />
                      </div>
                    )}
                  </div>

                  {adminOptions.length > 0 ? (
                    <>
                      <div
                        data-testid="header-menu-archive"
                        onMouseEnter={() => setArchivesMenuOpen(true)}
                        onMouseLeave={() => setArchivesMenuOpen(false)}
                      >
                        <div
                          className={
                            "header-dropdown-button " +
                            (archivesMenuOpen
                              ? "header-dropdown-button-active"
                              : "header-dropdown-button")
                          }
                        >
                          Archive
                        </div>
                        {archivesMenuOpen && (
                          <div className="header-dropdown-content">
                            {adminOptions.map((formType) => (
                              <Link
                                data-testid={`header-menu-archive-item-${formType}`}
                                to={`/${getFormConfig(formType).path}/archive/`}
                                key={getFormConfig(formType).name}
                                className="menu-link"
                                onClick={() => setArchivesMenuOpen(false)}
                                style={{ marginBottom: "0.2em" }}
                              >
                                {getFormConfig(formType).name}{" "}
                              </Link>
                            ))}
                          </div>
                        )}
                      </div>
                      <div
                        data-testid="header-menu-settings"
                        onMouseEnter={() => setSettingsMenuOpen(true)}
                        onMouseLeave={() => setSettingsMenuOpen(false)}
                      >
                        <div
                          className={
                            "header-dropdown-button " +
                            (settingsMenuOpen
                              ? "header-dropdown-button-active"
                              : "header-dropdown-button")
                          }
                        >
                          Settings
                        </div>
                        {settingsMenuOpen && (
                          <div className="header-dropdown-content">
                            {adminOptions.map((formType) => (
                              <Link
                                data-testid={`header-menu-settings-item-${formType}`}
                                onClick={() => setSettingsMenuOpen(false)}
                                to={`/${
                                  getFormConfig(formType).path
                                }/settings/`}
                                key={getFormConfig(formType).name}
                                className="menu-link"
                                style={{ marginBottom: "0.2em" }}
                              >
                                {getFormConfig(formType).name}{" "}
                              </Link>
                            ))}
                          </div>
                        )}
                      </div>
                    </>
                  ) : (
                    <>
                      <div
                        className="header-dropdown-button-disabled"
                        title="You're not the administrator of any forms"
                      >
                        Archive
                      </div>
                      <div
                        className="header-dropdown-button-disabled"
                        title="You're not the administrator of any forms"
                      >
                        Settings
                      </div>
                    </>
                  )}
                  <div className={"header-dropdown-button "}>
                    <Link
                      data-testid="header-item-people"
                      to={"/people/"}
                      className="menu-link"
                      style={{ marginBottom: "0.2em" }}
                    >
                      People
                    </Link>
                  </div>
                </div>
              </div>
              <Link to={"/"} data-testid="header-item-domni">
                <div data-testid="header-item-domni" className="header-domni">
                  {domniTitle}
                </div>
              </Link>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  width: "40%",
                }}
              >
                <Link
                  data-testid="header-item-profile"
                  style={{ alignSelf: "center" }}
                  to={"/profile/"}
                >
                  {loginMessage}
                </Link>
                <IconButton
                className="menu-link"
                size="small"
                onClick={props.onLogout}
                >
                  <LogoutIcon />
                </IconButton>
              </div>
            </Toolbar>
          </AppBar>
        </>
      ) : (
        // DRAWER
        <div>
          <div style={{ height: "4em" }}></div>
          <AppBar position="fixed" className="header-toolbar">
            <Toolbar className="header-mobile-nav-flex">
              <IconButton edge="start" aria-label="menu" href="/" size="large">
                <img className="header-max-logo" src={logo} alt="Logo" />
              </IconButton>
              <Link to={"/"} data-testid="header-item-domni">
                <div className="header-domni">{domniTitle}</div>
              </Link>
              <IconButton
                className="menu-link"
                aria-label="Menu"
                onClick={handleHamburgerToggle}
                size="large"
              >
                <MenuIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
          <Drawer
            onClick={handleHamburgerToggle}
            variant="persistent"
            open={drawerIsOpen}
          >
            <div className="header-center-login">
              <div className="header-mobile-login-message">
                <Link
                  data-testid="header-item-profile"
                  style={{ alignSelf: "center" }}
                  to={"/profile/"}
                >
                  {loginMessage}
                </Link>
              </div>
              <IconButton
                className="menu-link"
                size="small"
                onClick={props.onLogout}
              >
                <LogoutIcon />
              </IconButton>
            </div>
            <Divider style={{ backgroundColor: "#666" }} />
            <div className="header-drawer-menu-container">
              <span className="header-drawer-menu-category">Forms</span>
              {getFormConfigs({
                excludeNonProd: isInProduction(),
                visibility: isStaff(state.user) ? "STAFF" : "PUBLIC",
              }).map((config) => (
                <Link
                  data-testid={`header-menu-form-item-${config.formType}`}
                  to={`/${config.path}/form/`}
                  key={config.name}
                  className="menu-link"
                >
                  {config.name}
                </Link>
              ))}

              {adminOptions.length > 0 && (
                <>
                  <span className="header-drawer-menu-category">Archives</span>
                  {adminOptions.map((formType) => (
                    <Link
                      data-testid={`header-menu-archive-item-${formType}`}
                      to={`/${getFormConfig(formType).path}/archive/`}
                      key={getFormConfig(formType).name}
                      className="menu-link"
                    >
                      {getFormConfig(formType).name}{" "}
                    </Link>
                  ))}
                  <span className="header-drawer-menu-category">Settings</span>
                  {adminOptions.map((formType) => (
                    <Link
                      data-testid={`header-menu-settings-item-${formType}`}
                      to={`/${getFormConfig(formType).path}/settings/`}
                      key={getFormConfig(formType).name}
                      className="menu-link"
                      style={{ marginBottom: "0.2em" }}
                    >
                      {getFormConfig(formType).name}{" "}
                    </Link>
                  ))}
                </>
              )}
              <span className="header-drawer-menu-category">Other</span>
              <Link
                data-testid="header-item-people"
                to="/people/"
                key="people"
                className="menu-link"
              >
                People
              </Link>
              {/* add some space at the bottom */}
              <div style={{ paddingTop: "5em" }} />
            </div>
          </Drawer>
        </div>
      )}
    </div>
  );
};

const FormMenu = (props: { onClick: () => void }) => {
  const { state } = useStore();
  const configs = getFormConfigs({
    excludeNonProd: isInProduction(),
    visibility: isStaff(state.user) ? "STAFF" : "PUBLIC",
  });
  const map: Record<string, System.FormConfig[]> = {};
  configs.forEach((config) => {
    map[config.department] = map[config.department] || [];
    map[config.department].push(config);
  });
  return (
    <div style={{ display: "flex" }}>
      {Object.keys(map).map((category) => {
        return (
          <div style={{ padding: "1em 2em 2em 2em" }} key={category}>
            <div
              style={{ fontFamily: "Abel", color: "#bbb", fontSize: "1.2em" }}
            >
              {category}
            </div>
            <div>
              {map[category].map((config) => (
                <Link
                  data-testid={`header-menu-form-item-${config.formType}`}
                  to={`/${config.path}/form/`}
                  key={config.name}
                  className="menu-link"
                  onClick={props.onClick}
                  style={{ display: "block", lineHeight: "1.8" }}
                >
                  {config.name}
                </Link>
              ))}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Header;
