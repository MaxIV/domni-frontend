import React from "react";
import { Button, Menu, MenuItem } from "@mui/material";
interface MenuProps {
  menuLabel: string | JSX.Element;
  dropDownTitle?: string | JSX.Element;
  formId: string;
  actions: {
    label: string;
    disabled?: boolean;
    testId: string;
    onClick: () => void;
  }[];
}
export const ActionMenu: React.FC<MenuProps> = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { dropDownTitle, menuLabel, actions, children, formId } = props;

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <>
      <Button data-testid={`form-${formId}-action-menu`} onClick={handleClick}>
        {menuLabel}
      </Button>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {dropDownTitle && (
          <div
            style={{
              fontWeight: "bold",
              color: "#777",
              textTransform: "uppercase",
              margin: "0",
              textAlign: "center",
              paddingBottom: "0.5em",
              marginBottom: "0.5em",
              borderBottom: "1px solid #eee",
            }}
          >
            {dropDownTitle}
          </div>
        )}
        {actions &&
          actions.map((action) => {
            return (
              <MenuItem
                className={
                  "action-menu-container" +
                  (action.disabled === true
                    ? " action-menu-container-disabled"
                    : "")
                }
                disabled={action.disabled === true}
                data-testid={action.testId}
                key={action.label}
                onClick={() => {
                  action.onClick();
                  handleClose();
                }}
              >
                {action.label}
              </MenuItem>
            );
          })}
        {children}
      </Menu>
    </>
  );
};
