import React from "react";
import QRCode from "qrcode.react";
import "./RiskAssessmentPrint.css";
import { formatDate, getLatestSupervisorAction } from "../../utilities/Helpers";

const getPictogramUrl = (
  safetyClassification: System.SafetyClassificationType
) => {
  return `${process.env.PUBLIC_URL}/${safetyClassification
    .toLowerCase()
    .replaceAll(" ", "_")}.gif`;
};

const Pictogram = (props: {
  safetyClass: System.SafetyClassificationType;
  height: string;
}) => {
  /** Also known as hazard symbols */
  const { safetyClass, height } = props;
  return (
    <img
      height={height}
      alt={safetyClass}
      src={getPictogramUrl(safetyClass)}
    ></img>
  );
};

const CreateBasicInformationTable = (props: {
  form: System.RiskAssessmentForm;
}) => {
  const form: System.RiskAssessmentForm =
    props.form as System.RiskAssessmentForm;
  const basicInformationTable = [
    { label: "PROPOSAL ID", value: form.proposalId },
    { label: "TITLE", value: form.title },
    {
      label: "START DATE & TIME",
      value: form.hasDates ? form.startDate?.toString().split("T")[0] : "",
    },
    {
      label: "FINISH DATE & TIME",
      value: form.hasDates ? form.endDate?.toString().split("T")[0] : "",
    },
    { label: "BEAMLINE", value: form.beamline },
  ];
  const contactTable = [
    {
      label: "LOCAL CONTACT",
      rows: [
        { subLabel: "NAME", value: form.contactName },
        { subLabel: "CONTACT NUMBER", value: form.contactPhone },
      ],
    },
    {
      label: "LEADING PRINCIPAL INVESTIGATOR ON-SITE",
      rows: [
        { subLabel: "NAME", value: form.PIName },
        { subLabel: "CONTACT NUMBER", value: form.PIPhone },
      ],
    },
  ];

  return (
    <table className="basic-information-table">
      <tbody>
        {basicInformationTable.map((item, index) => (
          <tr key={index}>
            <td className="col-35" colSpan={2}>
              <p>
                <strong>{item.label}</strong>
              </p>
            </td>
            <td className="col-65">
              <p>{item.value}</p>
            </td>
          </tr>
        ))}
        {contactTable.map((item, index) => (
          <React.Fragment key={index}>
            {item.rows.map((row, rowIndex) => (
              <tr key={`${index}-${rowIndex}`}>
                {rowIndex === 0 && (
                  <td rowSpan={2} className="col-20">
                    <p>
                      <strong>{item.label}</strong>
                    </p>
                  </td>
                )}
                <td className="col-25">
                  <p>
                    <strong>{row.subLabel}</strong>
                  </p>
                </td>
                <td className="col-35">
                  <p>{row.value}</p>
                </td>
              </tr>
            ))}
          </React.Fragment>
        ))}
      </tbody>
    </table>
  );
};

const CreateSampleTable = (props: { form: System.RiskAssessmentForm }) => {
  const form: System.RiskAssessmentForm =
    props.form as System.RiskAssessmentForm;
  const riskAssessmentURL = window.location.href.replace("print", "form");

  const totalSafetyClassifications = Array.from(
    new Set(
      form.chemicalSamples.map((sample) => sample.safetyClassifications).flat(1)
    )
  );

  let nanoSampleClass = 0;
  if (form.hasNanoSamples) {
    for (let nanoSample of form.nanoSamples) {
      nanoSampleClass =
        nanoSample.class > nanoSampleClass ? nanoSample.class : nanoSampleClass;
    }
  }
  let gas_combination = {
    Gas: "No",
    Flammable: "No",
    Oxidizing: "No",
    Toxic: "No",
  };
  form.chemicalSamples.forEach((sample) => {
    if (sample.aggregationState === "Gas") {
      gas_combination["Gas"] = "Yes";
      let isGasUnderPressure = false;
      let isFlammable = false;
      let isOxidizing = false;
      let isToxic = false;

      sample.safetyClassifications.forEach((classification) => {
        if (classification === "Gas under Pressure") {
          isGasUnderPressure = true;
        } else if (classification === "Flammable") {
          isFlammable = true;
        } else if (classification === "Oxidizing") {
          isOxidizing = true;
        } else if (classification === "Acute toxicity") {
          isToxic = true;
        }
      });
      if (isGasUnderPressure && isFlammable) {
        gas_combination["Flammable"] = "Yes";
      }
      if (isGasUnderPressure && isOxidizing) {
        gas_combination["Oxidizing"] = "Yes";
      }
      if (isGasUnderPressure && isToxic) {
        gas_combination["Toxic"] = "Yes";
      }
    }
  });
  const sampleData = [
    {
      title: "Nanomaterials, Class",
      content: form.hasNanoSamples
        ? nanoSampleClass
          ? "Yes, Nano class " + nanoSampleClass
          : "No nano class"
        : "No",
      rowSpan: 5,
    },
    {
      title: "Chemicals",
      content: form.hasChemicalSamples ? "Yes" : "No",
    },
    {
      title: "Gases",
    },
    {
      title: "CMR classed",
      content: form.hasCMRCompound ? "Yes" : "No",
    },
    {
      title: "Biological",
      content: form.hasBioSamples ? "Yes" : "No",
    },
  ];

  return (
    <table className="samples-table">
      <tbody>
        <tr>
          <td className="col-20" rowSpan={sampleData[0].rowSpan} colSpan={4}>
            <p>
              <strong>SAMPLES</strong>
            </p>
          </td>
          <td className="col-25" colSpan={5}>
            <p>
              <strong>{sampleData[0].title}</strong>
            </p>
          </td>
          <td className="col-55" colSpan={11}>
            <p>{sampleData[0].content}</p>
          </td>
        </tr>
        <tr>
          <td className="col-25" colSpan={5}>
            <p>
              <strong>{sampleData[1].title}</strong>
            </p>
          </td>
          <td className="col-55" colSpan={11}>
            <p>{sampleData[1].content}</p>
          </td>
        </tr>
        <tr>
          <td className="col-25" colSpan={5}>
            <p>
              <strong>{sampleData[2].title}</strong>
            </p>
          </td>
          <>
            {gas_combination["Gas"] === "Yes" ? (
              <>
                <td className="col-10" colSpan={2}>
                  <p>{gas_combination["Gas"]}</p>
                </td>
                <td className="col-15" colSpan={3}>
                  <p>Flammable: {gas_combination["Flammable"]}</p>
                </td>
                <td className="col-15" colSpan={3}>
                  <p>Oxidizing: {gas_combination["Oxidizing"]}</p>
                </td>
                <td className="col-15" colSpan={3}>
                  <p>Toxic: {gas_combination["Toxic"]}</p>
                </td>
              </>
            ) : (
              <td className="col-55" colSpan={11}>
                <p>{gas_combination["Gas"]}</p>
              </td>
            )}
          </>
        </tr>
        <tr>
          <td className="col-25" colSpan={5}>
            <p>
              <strong>{sampleData[3].title}</strong>
            </p>
          </td>
          <td className="col-55" colSpan={11}>
            <p>{sampleData[3].content}</p>
          </td>
        </tr>
        <tr>
          <td className="col-25" colSpan={5}>
            <p>
              <strong>{sampleData[4].title}</strong>
            </p>
          </td>
          <td className="col-55" colSpan={11}>
            <p>{sampleData[4].content}</p>
          </td>
        </tr>
        <tr>
          <td className="col-20" colSpan={4}>
            <p>
              <strong>HAZARD PICTOGRAMS</strong>
            </p>
          </td>
          <td className="col-80" colSpan={16}>
            {totalSafetyClassifications.map((safetyClass) => {
              return (
                <Pictogram
                  safetyClass={safetyClass}
                  height="52mm"
                  key={safetyClass}
                />
              );
            })}
          </td>
        </tr>
        <tr>
          <td className="col-20" colSpan={4}>
            <p>
              <strong>SUMMARY OF OTHER HAZARDS</strong>
            </p>
          </td>
          <td className="col-50" colSpan={10}>
            {Object.entries(form.equipments)
              .filter(([_, value]) => value.checks.length > 0)
              .map(([key, value]) => {
                return (
                  <div key={key}>
                    <p>
                      {
                        {
                          vessles:
                            "Gas, vapour and liquid pressure equipment/vessels",
                          heater: "Heater/Furnace",
                          cryostat: "Cryostat or Cryo-magnet",
                          magnetic: "Electromagnetic field/wave generators",
                          electrochemical: "Electrochemical cells / battery",
                          laser: "Laser",
                          lamps: "LED, IR, UV, Hg lamps",
                          sonic: "Ultrasounds/sonic",
                          blowtorch: "Micro blowtorch",
                          ribbon: "Heating ribbon",
                        }[key]
                      }
                    </p>
                  </div>
                );
              })}
            <p>
              {form.hasOwnEquipment
                ? "The experiment involves bringing our own equipment to MAX IV"
                : ""}
            </p>
          </td>
          <td id="qrcode-container" className="col-30" colSpan={6}>
            <p>LINK TO ESRA FORM</p>
            <QRCode size={194} includeMargin={true} value={riskAssessmentURL} />
          </td>
        </tr>
        <tr>
          <td className="col-45" colSpan={9}>
            <p>
              <strong>LAB ACCESS</strong>
            </p>
          </td>
          <td className="col-55" colSpan={11}>
            <p>{form.labAccessRequired ? "Yes" : "No"}</p>
          </td>
        </tr>
        <tr>
          <td className="col-45" colSpan={9}>
            <p>
              <strong>SAMPLE REMOVAL</strong>
            </p>
          </td>
          <td className="col-55" colSpan={11}>
            <p>{form.sampleRemoved}</p>
          </td>
        </tr>
      </tbody>
    </table>
  );
};

const CreateSignatureTable = (props: { form: System.RiskAssessmentForm }) => {
  const form: System.RiskAssessmentForm =
    props.form as System.RiskAssessmentForm;
  const signatureData = [
    { value: "EST" },
    { value: form.safetyClassification === "green" ? "" : "FC" },
    { value: "LC" },
    { value: "Leading Principal Investigator On-Site" },
  ];
  const est_text =
    form.status.toLowerCase() !== "pending" ? (
      <div style={{ textTransform: "capitalize" }}>
        {formatDate(getLatestSupervisorAction(props.form).decisionDate)}
        <br />
        {`${form.status.toLowerCase()} by ${
          getLatestSupervisorAction(props.form).name
        }`}
      </div>
    ) : (
      "Pending"
    );
  return (
    <table className="signatures-table">
      <tbody>
        <tr>
          <td
            colSpan={4}
            style={{
              backgroundColor:
                form.safetyClassification === "green"
                  ? "#1ED760"
                  : form.safetyClassification,
            }}
          >
            <p>
              <strong>SIGNATURES</strong>
            </p>
          </td>
        </tr>
        <tr>
          <td rowSpan={2}>
            <p>{est_text}</p>
          </td>
          <td rowSpan={2}></td>
          <td>
            <p>{form.contactName}</p>
          </td>
          <td>
            <p>{form.PIName}</p>
          </td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
        <tr style={{ fontSize: "8pt" }}>
          {signatureData.map((item, index) => (
            <td key={index}>
              <p>
                <strong>{item.value}</strong>
              </p>
            </td>
          ))}
        </tr>
      </tbody>
    </table>
  );
};

const RiskAssessmentPrint = (props: { form: System.Form }) => {
  const form: System.RiskAssessmentForm =
    props.form as System.RiskAssessmentForm;
  return (
    <div className="a4-page">
      <h1 className="title-text">Experimental Safety Approval Form, ESAF</h1>
      <table className="safety-category-table">
        <tbody>
          <tr
            style={{
              backgroundColor:
                form.safetyClassification === "green"
                  ? "#1ED760"
                  : form.safetyClassification,
            }}
          >
            <td>SAFETY CATEGORY</td>
            <td>{form.safetyClassification.toUpperCase()}</td>
          </tr>
        </tbody>
      </table>
      <CreateBasicInformationTable form={form} />
      <CreateSampleTable form={form} />
      <CreateSignatureTable form={form} />
      <p id="footer">
        ESAF {form.proposalId}/{form._id}
      </p>
    </div>
  );
};
export default RiskAssessmentPrint;
