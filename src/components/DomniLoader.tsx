import React from "react";
interface Props {
  show: boolean;
  testid?: string;
}
const DomniLoader = (props: Props) => {
  return (
    <>
      {props.show && (
        <div data-testid={props.testid || ""} className="domni-loader" />
      )}
    </>
  );
};
export default DomniLoader;
