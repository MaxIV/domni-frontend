import {
  Chip,
  FormControl,
  Grid,
  InputLabel,
  TextField,
  Theme,
  Fade,
} from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import ErrorIcon from "@mui/icons-material/Error";
import React from "react";
import { useWindowDimensions } from "../utilities/Helpers";
import { SimpleSelect } from "./SimpleSelect";

type Props = {
  updateFormData: (form: System.RiskAssessmentForm) => void;
  formData: System.RiskAssessmentForm;
  disabled: boolean;
  embedded: boolean;
};

export const EquipmentRiskTable = (props: Props) => {
  const { formData, updateFormData, disabled } = props;
  const { md } = useWindowDimensions();
  const classes = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        "&.Mui-disabled": { opacity: 1 },
      },
    })
  )();
  return (
    <div
      style={{
        border: "1px solid #eee",
        borderLeft: "4px solid #de930038",
        borderBottom: "none",
      }}
    >
      {md && (
        <Grid
          container
          style={{
            background: "#eee",
            color: "rgb(94 94 94)",
            fontWeight: "bold",
            padding: "0.5em 1em",
          }}
          item
          xs={12}
        >
          <Grid item md={6}>
            Equipment hazards
          </Grid>
          <Grid style={{ paddingLeft: "1em" }} item md={6}>
            Declaration and mitigation
          </Grid>
        </Grid>
      )}
      {(Object.keys(equipmentData) as System.RiskType[]).map((key) => {
        const { checks, mitigations } = formData.equipments[key];
        return (
          <Grid
            container
            item
            xs={12}
            key={key}
            style={{
              padding: "1em",
              borderBottom: "1px solid #eee",
              display:
                disabled && formData.equipments[key].checks.length === 0
                  ? "none"
                  : "flex",
            }}
          >
            <Grid item xs={12} md={6} style={{ color: "#777", padding: "1em" }}>
              <div
                style={{
                  fontWeight: "bold",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <ErrorIcon
                  style={{
                    color: "#de9300c4",
                    paddingRight: "0.1em",
                    marginLeft: "-0.5em",
                    fontSize: "2em",
                  }}
                />
                {equipmentData[key].name}
              </div>{" "}
              <div style={{ fontStyle: "italic", fontSize: "0.9em" }}>
                {equipmentData[key].description}
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "baseline",
                  marginTop: "0.5em",
                  visibility: disabled ? "hidden" : "visible",
                }}
              >
                <span>Declare hazard:</span>

                <FormControl
                  size="small"
                  style={{ marginLeft: "0.5em", width: "8em" }}
                >
                  <InputLabel>Select...</InputLabel>
                  <SimpleSelect
                    selectedValue=""
                    variant="standard"
                    forceNonNative={true}
                    name={`risktype-${key}`}
                    testid={`risktype-${key}`}
                    title=""
                    options={equipmentData[key].options
                      .filter(
                        (option) =>
                          !formData.equipments[key].checks.includes(option)
                      )
                      .map((option) => {
                        return { value: option };
                      })}
                    onChange={(event) => {
                      if (
                        event.target.value === "" ||
                        formData.equipments[key].checks.includes(
                          event.target.value as string
                        )
                      ) {
                        return;
                      }
                      updateFormData({
                        ...formData,
                        equipments: {
                          ...formData.equipments,
                          [key]: {
                            ...formData.equipments[key],
                            checks: [
                              ...formData.equipments[key].checks,
                              event.target.value,
                            ],
                          },
                        },
                      });
                    }}
                  />
                </FormControl>
              </div>
            </Grid>
            <Grid item xs={12} md={6} style={{ padding: "1em" }}>
              {!md && <div>Declared hazards and mitigations</div>}
              {checks.length === 0 && (
                <div
                  style={{
                    fontSize: "0.8em",
                    padding: "0.5em",
                    fontStyle: "italic",
                    color: "#777",
                  }}
                >
                  No hazards have been declared
                </div>
              )}
              <div style={{ paddingBottom: "1em" }}>
                {checks.length > 0 && "Hazards:"}
                {checks.map((check) => (
                  <Chip
                    classes={classes}
                    key={check}
                    style={{ margin: "0.1em" }}
                    label={check}
                    disabled={disabled}
                    onDelete={
                      disabled
                        ? undefined
                        : () =>
                            updateFormData({
                              ...formData,
                              equipments: {
                                ...formData.equipments,
                                [key]: {
                                  ...formData.equipments[key],
                                  checks: formData.equipments[
                                    key
                                  ].checks.filter((c) => c !== check),
                                },
                              },
                            })
                    }
                  />
                ))}
              </div>
              <Fade in={formData.equipments[key].checks.length > 0}>
                <TextField
                  fullWidth={true}
                  disabled={disabled}
                  label="Mitigations"
                  onChange={(event) => {
                    updateFormData({
                      ...formData,
                      equipments: {
                        ...formData.equipments,
                        [key]: {
                          ...formData.equipments[key],
                          mitigations: event.target.value,
                        },
                      },
                    });
                  }}
                  name="mitigations"
                  value={mitigations}
                  variant="outlined"
                  multiline={true}
                  rows={props.embedded ? undefined : 2}
                />
              </Fade>
            </Grid>
          </Grid>
        );
      })}
    </div>
  );
};
const equipmentData: {
  [key in System.RiskType]: {
    name: string;
    description: string;
    options: string[];
  };
} = {
  vessles: {
    name: "Gas, vapour and liquid pressure equipment/vessels",
    description:
      "E.g. autoclave, high pressure cell, vacuum chamber, compressor, boosters etc.",
    options: [
      "Projection of fragments or liquid",
      "Gas or vapor leaks",
      "Burns",
      "Whipping of hoses",
      "Implosion",
      "Other",
    ],
  },
  heater: {
    name: "Heater/Furnace",
    description: "",
    options: [
      "Release of vapors",
      "Fire",
      "Thermal burns",
      "Electrical",
      "Other",
    ],
  },
  cryostat: {
    name: "Cryostat or Cryo-magnet",
    description: "",
    options: [
      "Cryogenic burns",
      "Quenching (sudden vaporisation of refrigerated liquid gas)",
      "Electrical",
      "Asphyxiation",
      "Harmful effects on human health",
      "Sudden attraction of metallic objects",
      "other",
    ],
  },
  magnetic: {
    name: "Electromagnetic field/wave generators",
    description:
      "Other equipment generating magnetic field and/or electromagnetic waves, e.g. inducation heaters.",
    options: [
      "Electrical",
      "Exposure to non-ionising radiation",
      "Harmful effects on human health",
      "Sudden attraction of metallic objects",
      "Other",
    ],
  },
  electrochemical: {
    name: "Electrochemical cells / battery",
    description: "",
    options: [
      "Leakage /exposure risk to internal content",
      "Electrical",
      "Other",
    ],
  },
  laser: {
    name: "Laser",
    description:
      "Specify Class, Wavelength (nm) and Power (mW). Also include your alignment procedure if applicable.",
    options: [
      "Accidental eye and skin exposure",
      "Electrical",
      "Fire",
      "Other",
    ],
  },
  lamps: {
    name: "LED, IR, UV, Hg lamps",
    description: "Specify Wavelenght (nm), Power (W) and other related details",
    options: ["Eye or skin exposure", "Contact burns", "Electrical", "Other"],
  },
  sonic: {
    name: "Ultrasounds/sonic",
    description: "",
    options: ["Hearing effects", "Other", "Use of flammable solvents"],
  },
  blowtorch: {
    name: "Micro blowtorch",
    description: "",
    options: ["Burns", "Fire", "Other"],
  },
  ribbon: {
    name: "Heating ribbon",
    description: "",
    options: ["Electrical", "Burns", "Other"],
  },
};
