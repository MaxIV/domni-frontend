import { Tooltip } from "@mui/material";
import React from "react";
import { createSimpleValidationResult, isADAdmin } from "../utilities/Helpers";
import { ldapSearch, update } from "../api/ldap";
import { useStore } from "../store/Store";
import { getAdminDescriptionArray, Highlight } from "../utilities/Helpers";
import PhoneIcon from "@mui/icons-material/Phone";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import PersonIcon from "@mui/icons-material/Person";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import EditIcon from "@mui/icons-material/Edit";
import { getModalAPIErrorDispatch } from "./Modal";
interface Props {
  name: string;
  email: string;
  testid?: string;
}
export const LdapPersonModal = (props: Props) => {
  const { dispatch } = useStore();

  const show = async () => {
    try {
      const result = await ldapSearch({
        searchQuery: props.email,
        attribute: "email",
      });
      dispatch({
        type: "SET_MODAL_PROPS",
        modalProps: {
          open: true,
          title: "",
          content: LdapPersonModalContent(result),
          btn1Text: "Close",
          btn1Color: "info",
        },
      });
    } catch (error) {}
  };
  return (
    <Tooltip title="Click for more info" placement="top-end" arrow>
      <span
        className="ldap-person-link"
        onClick={show}
        data-testid={props.testid || `email-${props.email}-ldap-person-link`}
      >
        {props.name}
      </span>
    </Tooltip>
  );
};

export const LdapPersonModalContent = (person: System.LDAPPerson[]) => {
  if (person.length === 0) {
    return <i>No additional personal information found</i>;
  }
  return (
    <div style={{ maxHeight: "30em" }}>
      {person.length > 1 && <i>Found {person.length} matches:</i>}
      {person.map((person) => (
        <LdapPersonCard key={person.username} person={person} />
      ))}
    </div>
  );
};

export const getFlair = (person: System.LDAPPerson): string => {
  switch (person.username) {
    case "aurfre":
      return "🍪";
    case "dapvan":
      return "⛷️";
    case "mikegu":
      return "🥔";
    case "paubel":
      return "🥜";
    case "abdamj":
      return "🌶️";
    case "verhon":
      return "⭐";
    case "marleo":
      return "🤌";
    default:
      return "";
  }
};

export const LdapPersonCard = (props: {
  person: System.LDAPPerson;
  className?: string;
  highlight?: string;
}) => {
  const { dispatch, state } = useStore();
  const { person } = props;
  const image = person.jpeg ? (
    <div>
      <button
        onClick={() =>
          dispatch({
            type: "SET_MODAL_PROPS",
            modalProps: {
              open: true,
              title: "",
              content: <img src={person.jpeg} alt="profile" />,
              btn1Text: "Close",
              btn1Color: "info",
            },
          })
        }
      >
        <Tooltip title={`Click for full size`} placement="top" arrow>
          <img
            style={{
              width: "130px",
              marginRight: "1em",
              cursor: "pointer",
            }}
            src={person.jpeg}
            alt="profile"
          />
        </Tooltip>
      </button>
    </div>
  ) : (
    <div
      style={{
        display: "inline-block",
        marginRight: "1em",
        width: "130px",
        minWidth: "130px",
        height: "170px",
        background:
          "repeating-linear-gradient(45deg,hsla(0,0%,93%,.2),hsla(0,0%,93%,.2) 5px,hsla(0,0%,65%,.15) 0,hsla(0,0%,62%,.15) 10px)",
      }}
    />
  );
  const { username, firstName, title, phone, email, office, fullName } = person;
  const flair = getFlair(person);
  return (
    <div
      data-testid={`ldap-person-card-${username}-container-div`}
      className={props.className || ""}
    >
      <div
        key={username}
        style={{
          padding: "0em",
          display: "flex",
          alignItems: "stretch",
        }}
      >
        {image}
        <div style={{ overflow: "hidden", flexGrow: 1 }}>
          <div
            style={{
              fontSize: "1.3em",
              marginTop: "0.2em",
              color: "#6c9e00",
              fontFamily: "'Abel', 'sans-serif'",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <Highlight
              query={props.highlight}
              msg={fullName}
              data-testid={`ldap-person-card-${username}-fullName-fragment`}
            />
            <div
              style={{
                paddingRight: "0.5em",
                fontSize: "1rem",
                display: "flex",
                color: "black",
              }}
            >
              {flair && <div>{flair}</div>}
              {isADAdmin(state.user) && (
                <Tooltip title={`Edit ${firstName}`} placement="top" arrow>
                  <div
                    className="edit-icon"
                    onClick={() => {
                      dispatch({
                        type: "SET_MODAL_PROPS",
                        modalProps: {
                          open: true,
                          content: "",
                          title: "Edit user",
                          btn1Text: "Save",
                          btn2Text: "Cancel",
                          btn1OnClick: async (inputs: System.ModalInputs) => {
                            try {
                              const result = await update({
                                fullName: inputs.fullName.value,
                                title: inputs.title.value,
                                phone: inputs.phone.value,
                                email: inputs.email.value,
                                office: inputs.office.value,
                                DN: person.DN,
                              });
                              dispatch({
                                type: "SET_LDAP_PERSON",
                                ldapPerson: result,
                              });
                            } catch (err) {
                              getModalAPIErrorDispatch(err as System.APIError);
                            }
                          },
                          validate: (inputs: System.ModalInputs) => {
                            return createSimpleValidationResult("");
                          },
                          inputs: {
                            fullName: {
                              type: "text",
                              label: "Full name",
                              value: fullName,
                            },
                            title: {
                              type: "text",
                              label: "Title",
                              value: title,
                            },
                            phone: {
                              type: "text",
                              label: "Phone",
                              value: phone,
                            },
                            email: {
                              type: "text",
                              label: "Email",
                              value: email,
                            },
                            office: {
                              type: "text",
                              label: "Office",
                              value: office,
                            },
                          },
                        },
                      });
                    }}
                  >
                    <EditIcon fontSize="small" />
                  </div>
                </Tooltip>
              )}
            </div>
          </div>
          <div
            style={{
              color: "#777",
              fontSize: "0.8em",
              marginTop: "-0.2em",
              textTransform: "uppercase",
              letterSpacing: "1px",
              marginBottom: "0.7em",
            }}
          >
            <Highlight
              query={props.highlight}
              msg={title}
              data-testid={`ldap-person-card-${username}-title-fragment`}
            />
          </div>
          <div style={{ fontSize: "0.9em", color: "#333" }}>
            <div style={{ display: "flex" }}>
              <Tooltip title={`Call ${firstName}`} placement="top" arrow>
                <span className="card-icon">
                  <a
                    className="link"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`tel:${phone}`}
                  >
                    <PhoneIcon style={{ fontSize: "20px" }} />
                  </a>
                </span>
              </Tooltip>
              <Highlight
                query={props.highlight}
                msg={phone}
                data-testid={`ldap-person-card-${username}-phone-fragment`}
              />
            </div>
            <div style={{ display: "flex" }}>
              <Tooltip title={`Email ${firstName}`} placement="top" arrow>
                <span className="card-icon">
                  <a
                    className="link"
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`mailto:${email}`}
                  >
                    <MailOutlineIcon style={{ fontSize: "20px" }} />
                  </a>
                </span>
              </Tooltip>
              <Highlight
                query={props.highlight}
                msg={email}
                data-testid={`ldap-person-card-${username}-email-fragment`}
              />
            </div>
            <div style={{ display: "flex" }}>
              <Tooltip title="Office" placement="top" arrow>
                <span className="card-icon">
                  <LocationOnIcon style={{ fontSize: "20px" }} />
                </span>
              </Tooltip>
              <Highlight
                query={props.highlight}
                msg={office}
                data-testid={`ldap-person-card-${username}-office-fragment`}
              />
            </div>
            <div style={{ display: "flex" }}>
              <Tooltip title="Username" placement="top" arrow>
                <span className="card-icon">
                  <PersonIcon style={{ fontSize: "20px" }} />
                </span>
              </Tooltip>
              <div style={{ fontFamily: "consolas, monospace" }}>
                <Highlight
                  query={props.highlight}
                  msg={username}
                  data-testid={`ldap-person-card-${username}-username-fragment`}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Tooltip
        title={`Domni categories and specific forms administered by ${firstName}`}
        placement="top"
        arrow
      >
        <div
          style={{
            textAlign: "center",
            fontStyle: "italic",
            fontSize: "0.8em",
            marginBottom: "0.5em",
            cursor: "pointer",
            overflow: "hidden",
          }}
        >
          {getAdminDescriptionArray(person).map((role, index) => {
            return (
              <span key={role.toString()}>
                {index === 0 ? "•" : ""} {role}
                {" • "}
              </span>
            );
          })}
        </div>
      </Tooltip>
    </div>
  );
};
