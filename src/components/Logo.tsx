import React from "react";
import logo from "../images/maxiv.png";
const Logo = (props:{style:React.CSSProperties}) => {
    return <div className="logo" style={props.style}>
        <div className="logo-domni">domni</div>
        <img style={{width: "100px"}} src={logo} alt="Logo" />
    </div>
}
export default Logo;