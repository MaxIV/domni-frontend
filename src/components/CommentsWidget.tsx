import {
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  TextField,
  Tooltip,
} from "@mui/material";
import React from "react";
import * as API from "../api/forms";
import { useState } from "react";
import { useStore } from "../store/Store";
import moment from "moment";
import Paper from "@mui/material/Paper/Paper";
import { Highlight, createComment } from "../utilities/Helpers";
import DomniTimeAgo from "./DomniTimeAgo";

const CommentsWidget = (props: {
  form: System.Form;
  onChange: (form: System.Form) => void;
}) => {
  const [comment, setComment] = useState<string>("");
  const [notifyListeners, setNotifylisteners] = useState<boolean>(true);
  const { state } = useStore();
  const { form } = props;
  const getContext = (comment: System.Comment) => {
    switch (comment.status) {
      case "APPROVED":
        return <>Posted after final approval</>;
      case "REJECTED":
        return <>Posted after rejection</>;
      default:
        return comment.supervisor ? (
          <>
            Posted while assigned to <b>{comment.supervisor}</b>
          </>
        ) : null;
    }
  };
  const addComment = async () => {
    const commentObject = createComment(
      form,
      comment,
      "REGULAR COMMENT",
      state
    );
    const updatedForm = await API.update({
      form,
      comment: commentObject,
      updateType: "COMMENT",
      disableNotify: notifyListeners === false,
    });
    props.onChange(updatedForm);
    setComment("");
  };
  return (
    <div
      style={{
        padding: "0em 2em",
        alignItems: "flex-end",
      }}
    >
      <Grid item xs={12} style={{ fontSize: "1.25em", marginTop: "1em" }}>
        Comments
      </Grid>
      <Grid item xs={12}>
        {form.comments && form.comments.length > 0 ? (
          form.comments.map((comment, index) => {
            return (
              <Paper
                key={comment.timestamp.toString()}
                elevation={2}
                style={{ padding: "0.5em", margin: "1em 0em" }}
                data-testid={`form-${form._id}-comment-${index + 1}-div`}
              >
                <div
                  style={{
                    marginBottom: "1em",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <div>
                    <span
                      style={{
                        color: "#777",
                        fontWeight: "bold",
                        marginRight: "0.5em",
                      }}
                    >
                      {comment.author === state.user.name
                        ? "You"
                        : comment.author}
                    </span>
                    {"• "}
                    <span
                      style={{ cursor: "pointer" }}
                      title={moment(comment.timestamp).format(
                        "YYYY-MM-DD hh:mm"
                      )}
                    >
                      <DomniTimeAgo datetime={comment.timestamp} />
                    </span>
                  </div>
                  <span
                    style={{
                      fontWeight: "bold",
                      fontSize: "0.8em",
                      color: "#999",
                    }}
                  >
                    {comment.source === "REGULAR COMMENT" ? "" : comment.source}
                  </span>
                </div>
                <div style={{ padding: "0em 1em" }}>
                  <Highlight query={state.commentHighlight} msg={comment.msg} />
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    borderTop: "1px solid #eee",
                    marginTop: "1em",
                    paddingTop: "0.5em",
                  }}
                >
                  <div style={{ color: "#777", fontSize: "0.8em" }}>
                    {" "}
                    {getContext(comment)}
                  </div>
                  <div className="index-tag" style={{ marginRight: "-0.3em" }}>
                    {index + 1}
                  </div>
                </div>
              </Paper>
            );
          })
        ) : (
          <div
            style={{
              margin: "1em 0em",
              fontStyle: "italic",
            }}
          >
            This form does not have any comments.
          </div>
        )}
      </Grid>
      <Grid item xs={12}>
        <TextField
          data-testid={`form-${form._id}-comment-input`}
          fullWidth={true}
          label="Leave a comment"
          onChange={(event) => setComment(event.target.value)}
          name="motivation"
          value={comment}
          variant="outlined"
          multiline={true}
          rows={3}
          style={{ background: "white" }}
        />

        <div style={{ color: "#777", fontSize: "0.9em" }}>
          Comments posted here are <b>not</b> send to the original submitter.
          They are only visible to you, other supervisors, and administrators.
        </div>
        <div
          style={{
            display: "flex",
            margin: "1em 0em",
            justifyContent: "space-between",
            alignItems: "end",
          }}
        >
          <Tooltip
            title="<i>Listeners</i> include the current supervisor as well as any previous supervisor that have explicitly opted in to receive notifications. It does not include administrators."
            placement="top-start"
          >
            <FormControlLabel
              className="link"
              control={
                <Checkbox
                  size="small"
                  value={notifyListeners}
                  checked={notifyListeners}
                  onChange={() => setNotifylisteners(!notifyListeners)}
                />
              }
              label="Notify listener about this comment"
            />
          </Tooltip>
          <Button
            data-testid={`form-${form._id}-comment-button`}
            type="submit"
            color="primary"
            variant="contained"
            onClick={addComment}
            disabled={!comment}
          >
            Add comment
          </Button>
        </div>
      </Grid>
    </div>
  );
};

export default CommentsWidget;
