import React from "react";
import { Link } from "react-router-dom";
import { getFormConfigs } from "../utilities/Config";
import { isInProduction } from "../utilities/Helpers";

export const QuickNav = (props: {
  onLogout: () => void;
  onClose: () => void;
}) => {
  const { onLogout, onClose } = props;
  const configs = getFormConfigs({ excludeNonProd: isInProduction() });
  return (
    <div style={{ display: "flex", justifyContent: "space-around" }}>
      <div>
        <ul>
          <li>
            <Link to={`/`} className="link" onClick={onClose}>
              Home
            </Link>
          </li>
          <li>
            <Link to={`/profile/`} className="link" onClick={onClose}>
              Profile
            </Link>
          </li>
          <li>
            <Link to={`/docs/`} className="link" onClick={onClose}>
              Docs
            </Link>
          </li>
          <li>
            <Link to={`/people/`} className="link" onClick={onClose}>
              People
            </Link>
          </li>
          <li>
            <Link
              to="#"
              className="link"
              onClick={() => {
                onLogout();
                onClose();
              }}
            >
              Logout
            </Link>
          </li>
        </ul>
      </div>
      <div>
        <ul>
          {configs.map((config) => (
            <li key={config.formType}>
              <Link
                to={`/${config.path}/form/`}
                className="link"
                onClick={onClose}
              >
                form
              </Link>{" "}
              <Link
                to={`/${config.path}/archive/`}
                className="link"
                onClick={onClose}
              >
                archive
              </Link>{" "}
              <Link
                to={`/${config.path}/settings/`}
                className="link"
                onClick={onClose}
              >
                settings
              </Link>
              <div style={{ paddingLeft: "1em", display: "inline-block" }}>
                <small>{config.name}</small>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};
