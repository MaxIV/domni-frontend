import React from "react";
interface Props {
  href: string;
}

export const DomniLink: React.FC<Props> = (props) => {
  const { children, href } = props;
  return (
    <a href={href} className="link" target="_blank" rel="noopener noreferrer">
      {children}
    </a>
  );
};
