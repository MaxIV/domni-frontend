import React, { useEffect } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import AddCircle from "@mui/icons-material/AddCircle";
import {
  Paper,
  Typography,
  Button,
  Grid,
  IconButton,
  Tooltip,
} from "@mui/material";
import {
  createCollection,
  updateCollection,
  deleteCollection,
} from ".././api/collection";
import { ValidatorForm } from "react-material-ui-form-validator";
import { useStore } from "../store/Store";
import { selectLists } from "../store/Reducer";
import { SimpleSelect } from "../components/SimpleSelect";
import { SelectableListWidget } from "../components/SelectableListWidget";
import { getModalAPIErrorDispatch } from "./Modal";
import { createSimpleValidationResult } from "../utilities/Helpers";
import { DomniTextValidator } from "./TextValidator";

interface State {
  selectedListId: string | undefined;
  selectedListItem: System.ListItem | undefined;
  lists: System.List[];
}
interface Props {
  formType: System.FormType;
}

export const CustomLists = (props: Props) => {
  const { dispatch, state } = useStore();
  const { formType } = props;

  const [localState, setLocalState] = React.useState<State>({
    selectedListId: selectLists(state, props.formType)[0]?._id,
    selectedListItem: selectLists(state, props.formType)[0]?.items[0],
    lists: selectLists(state, props.formType),
  });
  const selectedList = localState.lists.filter(
    (list) => list._id === localState.selectedListId
  )[0];
  useEffect(() => {
    setLocalState({
      ...localState,
      selectedListId: selectLists(state, props.formType)[0]?._id,
      selectedListItem: selectLists(state, props.formType)[0]?.items[0],
      lists: selectLists(state, props.formType),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.formType]);

  useEffect(() => {
    setLocalState({
      ...localState,
      lists: selectLists(state, props.formType),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.lists[props.formType]]);

  /**Create new list */
  const addList = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "New custom list",
        content: "",
        btn1Text: "Create",
        btn2Text: "Cancel",
        validate: (inputs) => {
          const errorMessage = !inputs.name.value ? "Name is required" : "";
          return createSimpleValidationResult(errorMessage);
        },
        btn1OnClick: async (inputs: System.ModalInputs) => {
          const newList: System.List = {
            _id: "",
            name: inputs.name.value,
            description: inputs.description.value,
            form: formType,
            defaultValue: "",
            items: [],
          };
          try {
            const listWithId = (await createCollection(
              "LIST",
              newList
            )) as System.List;
            setLocalState({
              ...localState,
              selectedListItem: undefined,
              selectedListId: listWithId._id,
            });
            dispatch({
              type: "SET_LISTS",
              formType,
              lists: [...localState.lists, listWithId],
            });
          } catch (err) {
            dispatch(getModalAPIErrorDispatch(err as System.APIError));
            return;
          }
        },
        inputs: {
          name: {
            type: "text",
            label: "Name",
            value: "",
          },
          description: {
            type: "text",
            label: "Description",
            value: "",
          },
        },
      },
    });
  };

  const addListItem = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "New list item",
        content: "",
        btn1Text: "Create",
        btn2Text: "Cancel",
        validate: (inputs) => {
          const errorMessage =
            !inputs.name.value ||
            selectedList?.items
              .map((item) => item.value)
              .includes(inputs.name.value)
              ? "The item must have a unique name"
              : "";
          return createSimpleValidationResult(errorMessage);
        },
        btn1OnClick: async (inputs: System.ModalInputs) => {
          const newList = { ...selectedList };
          newList.items.push({
            value: inputs.name.value,
            label: inputs.name.value,
          });

          const updatedList = (await updateCurrentList(newList)) as System.List;
          setLocalState({
            ...localState,
            selectedListItem: updatedList.items.filter(
              (item) => item.value === inputs.name.value
            )[0],
          });
          dispatch({
            type: "SET_LISTS",
            formType,
            lists: [
              ...localState.lists.filter(
                (list) => list._id !== updatedList._id
              ),
              updatedList,
            ],
          });
        },
        inputs: {
          name: {
            type: "text",
            label: "Name",
            value: "",
          },
        },
      },
    });
  };
  const editList = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Edit list",
        content: (
          <span className="warning-color">
            Warning! Do not change the <b>name</b> of this list if it's
            currently being used. The name is used to map the list to the form.
          </span>
        ),
        btn1Text: "Save",
        btn2Text: "Cancel",
        validate: (inputs) => {
          const errorMessage = !inputs.name.value
            ? "The list must have a name"
            : "";
          return createSimpleValidationResult(errorMessage);
        },
        btn1OnClick: async (inputs: System.ModalInputs) => {
          const updatedList = {
            ...selectedList,
            name: inputs.name.value,
            description: inputs.description.value,
          };
          await updateCurrentList(updatedList);
          dispatch({
            type: "SET_LISTS",
            formType,
            lists: [
              ...localState.lists.filter(
                (list) => list._id !== updatedList._id
              ),
              updatedList,
            ],
          });
        },
        inputs: {
          name: {
            type: "text",
            label: "Name",
            value: selectedList.name || "",
          },
          description: {
            type: "text",
            label: "Description",
            value: selectedList.description || "",
          },
        },
      },
    });
  };
  const removeCurrentList = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Confirm deletion",
        content: "Are you sure you want to delete this list?",
        btn1Text: "Delete",
        btn1Color: "secondary",
        btn2Text: "Cancel",

        btn1OnClick: async (inputs: System.ModalInputs) => {
          try {
            await deleteCollection("LIST", selectedList._id, props.formType);
            const newLists = localState.lists.filter(
              (s) => s._id !== selectedList._id
            );
            dispatch({
              type: "SET_LISTS",
              formType,
              lists: newLists,
            });
            setLocalState({
              ...localState,
              selectedListId: newLists[0]?._id,
              selectedListItem: newLists[0]?.items[0],
            });
          } catch (err) {
            dispatch(getModalAPIErrorDispatch(err as System.APIError));
            return;
          }
        },
      },
    });
  };

  /**called on: edit list (name/desc), add item, remove item, save item */
  const updateCurrentList = async (list: System.List) => {
    try {
      const updatedList = (await updateCollection("LIST", list)) as System.List;
      dispatch({
        type: "SET_LISTS",
        formType,
        lists: [
          ...localState.lists.filter((s) => s._id !== updatedList._id),
          updatedList,
        ],
      });
      return updatedList;
    } catch (err) {
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
      return;
    }
  };
  return (
    <Paper className="paper" elevation={2}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              fontSize: "1.5em",
              marginTop: "-0.5em",
              marginBottom: "0.5em",
              borderBottom: "1px solid rgb(238, 238, 238)",
            }}
          >
            Custom lists
            <Tooltip title="Create a new custom list" placement="top-start">
              <IconButton
                data-testid="custom-lists-new-list-button"
                onClick={addList}
                aria-label="add"
                size="large"
              >
                <AddCircle />
              </IconButton>
            </Tooltip>
          </div>
        </Grid>
        <Grid item xs={12}>
          {localState.lists.length > 0 ? (
            <div
              style={{
                display: "flex",
                paddingBottom: "1em",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <SimpleSelect
                fullWidth={false}
                disabled={{
                  value: false,
                }}
                style={{ width: "20em" }}
                options={localState.lists.map((list) => {
                  return { label: list.name, value: list._id };
                })}
                selectedValue={selectedList?._id || localState.lists[0]._id}
                title={"Selected custom lists"}
                testid="custom-lists-list-select"
                name={"customLists"}
                onChange={(e) => {
                  setLocalState({
                    ...localState,
                    selectedListId: e.target.value as string,
                    selectedListItem:
                      localState.lists.filter(
                        (list) => list._id === (e.target.value as string)
                      )[0]?.items[0] || undefined,
                  });
                }}
              />
              <div
                style={{
                  flexGrow: 1,
                  padding: "1em",
                  margin: "0em 1em",
                  background: "#eee",
                  borderRadius: "0.2em",
                  color: "#777",
                }}
              >
                {selectedList?.description || (
                  <i>This list has no description</i>
                )}
              </div>
              <div>
                <Tooltip
                  title="Change name and description of this list"
                  placement="top-start"
                >
                  <IconButton
                    onClick={editList}
                    data-testid="custom-lists-edit-list-button"
                    aria-label="edit"
                    size="large"
                  >
                    <EditIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Delete this list" placement="top-start">
                  <IconButton
                    data-testid="custom-lists-delete-list-button"
                    onClick={removeCurrentList}
                    aria-label="delete"
                    size="large"
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </div>
            </div>
          ) : (
            <i>
              This form has no custom lists. Click on the plus button to create
              one.
            </i>
          )}
        </Grid>
        {selectedList && (
          <>
            <Grid item xs={12}>
              <Typography
                className={"small-heading"}
                style={{
                  borderBottom: "1px solid #ddd",
                  paddingTop: "1em",
                  borderTop: "4px solid #f0f0f0",
                }}
              >
                <span data-testid="custom-lists-list-label-span">
                  {selectedList.name}
                </span>
                <Tooltip title="Add an item to this list" placement="top-start">
                  <IconButton
                    data-testid="custom-lists-add-list-item-button"
                    size={"small"}
                    onClick={addListItem}
                    aria-label="add"
                  >
                    <AddCircle />
                  </IconButton>
                </Tooltip>
              </Typography>
            </Grid>
            <Grid
              item
              xs={12}
              sm={6}
              style={{
                marginBottom: "1em",
                borderBottom: "4px solid #f0f0f0",
              }}
            >
              {(selectedList.items || []).length > 0 ? (
                <SelectableListWidget
                  getId={(item) => item.value}
                  sort={(a, b) => a.label.localeCompare(b.label)}
                  items={selectedList.items || []}
                  data-testid="custom-list-list"
                  isSelected={(item: System.ListItem) =>
                    item._id === localState.selectedListItem?._id
                  }
                  onSelect={(item) =>
                    setLocalState({
                      ...localState,
                      selectedListItem: item,
                    })
                  }
                  getLabel={(item) => {
                    if (item.value === selectedList.defaultValue) {
                      return `${item.label} (default)`;
                    }
                    return item.label;
                  }}
                  getAddtionalLabel={(item) =>
                    item.value === item.label ? "" : item.value
                  }
                />
              ) : (
                <i>
                  No items. Click on the plus icon above to add an item to this
                  list
                </i>
              )}
            </Grid>
            <Grid
              item
              xs={12}
              sm={6}
              style={{
                marginBottom: "1em",
                borderBottom: "4px solid #f0f0f0",
              }}
            >
              {localState.selectedListItem ? (
                <ValidatorForm
                  className="settings-flex-column"
                  onSubmit={(e) => {
                    const updatedList = {
                      ...selectedList,
                      items: [
                        ...selectedList.items.filter(
                          (item) =>
                            item._id !== localState.selectedListItem?._id
                        ),
                        localState.selectedListItem as System.ListItem,
                      ],
                    };
                    updateCurrentList(updatedList);
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      marginBottom: "1em",
                      alignItems: "center",
                    }}
                  >
                    <Typography className={"small-heading"}>
                      Selected Item
                    </Typography>
                    <IconButton
                      data-testid="custom-list-delete-item-button"
                      disabled={!localState.selectedListItem}
                      onClick={() => {
                        const list = selectedList;
                        const updatedList = {
                          ...list,
                          items: [
                            ...list.items.filter(
                              (item) =>
                                item._id !== localState.selectedListItem?._id
                            ),
                          ],
                        };
                        updateCurrentList(updatedList);
                        setLocalState({
                          ...localState,
                          selectedListItem: undefined,
                        });
                      }}
                      aria-label="delete"
                      size="large"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </div>
                  <DomniTextValidator
                    style={{ marginBottom: "2em" }}
                    disabled={!localState.selectedListItem}
                    value={localState.selectedListItem?.label || ""}
                    testId="custom-list-item-label-input"
                    name="label"
                    label="label"
                    onChange={(event: any) =>
                      setLocalState({
                        ...localState,
                        selectedListItem: {
                          ...(localState.selectedListItem as System.ListItem),
                          label: event.target.value as string,
                        },
                      })
                    }
                    validators={["required"]}
                    errorMessages={["This field is required"]}
                    variant="outlined"
                    fullWidth={true}
                  />
                  <DomniTextValidator
                    disabled={!localState.selectedListItem}
                    value={localState.selectedListItem?.value || ""}
                    testId="custom-list-item-value-input"
                    name="value"
                    label="value"
                    onChange={(event: any) =>
                      setLocalState({
                        ...localState,
                        selectedListItem: {
                          ...(localState.selectedListItem as System.ListItem),
                          value: event.target.value as string,
                        },
                      })
                    }
                    validators={["required"]}
                    errorMessages={["This field is required"]}
                    variant="outlined"
                    fullWidth={true}
                  />
                  <Button
                    disabled={
                      !localState.selectedListItem ||
                      localState.selectedListItem.value ===
                        selectedList.defaultValue
                    }
                    variant="text"
                    color="secondary"
                    data-testid="custom-list-item-make-default-button"
                    style={{
                      fontSize: "0.8em",
                      width: "12em",
                      marginTop: "1em",
                      marginBottom: "4em",
                    }}
                    onClick={() => {
                      updateCurrentList({
                        ...selectedList,
                        defaultValue: localState.selectedListItem?.value || "",
                      });
                    }}
                  >
                    {selectedList.defaultValue ===
                    localState.selectedListItem?.value
                      ? "IS DEFAULT"
                      : "MAKE DEFAULT"}
                  </Button>
                  <div className="settings-button-bar">
                    <Button
                      disabled={!localState.selectedListItem}
                      variant="contained"
                      color="primary"
                      data-testid="custom-list-item-save-button"
                      type="submit"
                      style={{ marginRight: "0.5em" }}
                    >
                      Save
                    </Button>
                  </div>
                </ValidatorForm>
              ) : (
                <Typography style={{ fontStyle: "italic" }}>
                  No selected item
                </Typography>
              )}
            </Grid>
          </>
        )}
        <Grid item xs={12}>
          <div
            style={{
              color: "rgb(119, 119, 119)",
              fontSize: "0.85em",
            }}
          >
            <p>
              Custom lists are useful when your form needs a dropdown whose
              values are subject to frequent change. By defining a custom list,
              form administrators can easily update the content of the dropdown
              themselves. However, IMS still needs to be contacted once in order
              to add the list to the form in the first place.
            </p>
            <p>
              Each item in a custom list consist of two attibutes: a label and a
              value. The label is what the submitter of a form sees in the
              dropdown. The value is what gets stored in the database, and is
              also what supervisors and administrators see when handling the
              form. In most cases the value would be the same as the label, and
              it therefore also default to this. Addtionally, a list item can be
              assigned as the default value of the list. The list can have one
              default value at most. Omitting a default value means the first
              value in the list, when sorted alphabetically, will be the default
              value
            </p>
          </div>
        </Grid>
      </Grid>
    </Paper>
  );
};
