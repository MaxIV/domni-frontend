import {
  Button,
  Collapse,
  Grid,
  IconButton,
  TableCell,
  TableRow,
  Theme,
  Tooltip,
  Typography,
} from "@mui/material";
import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import CommentIcon from "@mui/icons-material/Comment";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import moment from "moment";
import React from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import * as API from "../api/forms";
import { formAction } from "../store/Reducer";
import { useStore } from "../store/Store";
import { getFormConfig } from "../utilities/Config";
import {
  getFormAccess,
  getLatestSupervisorAction,
  getSize,
  StatusTag,
  useWindowDimensions,
} from "../utilities/Helpers";
import { ActionMenu } from "./ActionMenu";
import CommentsWidget from "./CommentsWidget";
import Decider from "./Decider";
import { FormLink } from "./FormLink";
import { LdapPersonModal } from "./LdapPerson";
import {
  getInfoModal,
  getModalAPIErrorDispatch,
  getModalMsgDispatch,
} from "./Modal";
import SupervisorStepper from "./SupervisorStepper";
import { createFile, mapFile } from "../api/files";

const SubmittedForm = (props: System.ArchiveComponentProps) => {
  const { form, onFormChange, onFormAddedOrRemoved } = props;
  const formConfig = getFormConfig(form.formType);
  const [open, setOpen] = React.useState(props.open);
  const [showComments, setShowComments] = React.useState(false);
  const { dispatch, state } = useStore();
  const isAdmin = state.user.admin[form.formType];
  const history = useHistory();
  const { lg } = useWindowDimensions();
  const formAccess = getFormAccess(form, state.user, "SubmittedForm");
  const commentIconStyle = showComments
    ? { background: "rgba(0, 0, 0, 0.06)" }
    : {};

  const isPrint = useLocation().pathname.includes("/print/");
  const location = useLocation().pathname;
  const isArchive = location.includes("archive"); //Approval if false
  const dispatchArchiveAction = (form: System.Form) => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Archiving form",
        content:
          "Are you sure you want to archive this form? Archived forms can not be modified in any way, and are hidden by default.",
        btn1Text: "Archive",
        btn2Text: "Cancel",
        btn1OnClick: async (inputs: System.ModalInputs) => {
          try {
            const res = await API.update({
              form: { ...form, isArchived: true },
              updateType: "ARCHIVING",
            });
            onFormChange && onFormChange(res);
            dispatch(
              getInfoModal(
                "Form archived",
                `${formConfig.name} #${res._id} has been archived`
              )
            );
          } catch (e) {
            dispatch(getModalAPIErrorDispatch(e as System.APIError));
          }
        },
      },
    });
  };
  const dispatchCloneAction = (form: System.Form) => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Cloning form",
        content:
          "Are you sure you want to clone this form? Except the id, the new form will inherit all the inputs from the original form. Timestamps, events, comments and supervisor actions will be reset for the new form. The status of the form will be reset to Draft, meaning only the original submitter can see, edit and submit it.",
        btn1Text: "Clone",
        btn2Text: "Cancel",
        btn1OnClick: async (inputs: System.ModalInputs) => {
          try {
            const res = await API.cloneForm(form.formType, form._id as string);
            onFormAddedOrRemoved && onFormAddedOrRemoved();
            dispatch(
              getInfoModal(
                "Form cloned",
                `${formConfig.name} #${res._id} was successfully created`
              )
            );
          } catch (e) {
            dispatch(getModalAPIErrorDispatch(e as System.APIError));
          }
        },
      },
    });
  };
  const classes = makeStyles((theme: Theme) =>
    createStyles({
      customWidth: {
        maxWidth: "none",
      },
    })
  )();
  if (isPrint) {
    //Minimalistic print friendly version
    return (
      <TableRow>
        <TableCell style={{ borderBottom: "none" }}>
          <Grid container spacing={1}>
            <formConfig.formComponent
              match={{ params: [form._id || ""] }}
              embedded={true}
            />
          </Grid>
        </TableCell>
      </TableRow>
    );
  }
  return (
    <>
      <TableRow className={open || showComments ? "highlight-background" : ""}>
        <TableCell>
          {lg && (
            <Tooltip
              title={
                <>
                  <div
                    style={{
                      borderBottom: "1px solid #444",
                      paddingBottom: "0.5em",
                    }}
                  >
                    Click to toggle content (inline)
                  </div>

                  <div>{formConfig.renderSubmissionSummary(form, false)}</div>
                </>
              }
              placement="top"
              arrow
            >
              <IconButton
                aria-label="expand row"
                size="small"
                onClick={() => setOpen(!open)}
                data-testid={`form-${form._id}-toggle-expanded-form-button`}
              >
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
            </Tooltip>
          )}
        </TableCell>
        <TableCell>
          <IdComponent
            form={form}
            compact={!lg}
            formConfig={formConfig}
            onChange={(form) => {
              onFormChange(form);
            }}
          />
        </TableCell>
        <TableCell>
          {moment(form.submissionDate).format("YYYY-MM-DD HH:mm")}
        </TableCell>
        <TableCell>
          <LdapPersonModal
            email={form.submitterEmail}
            name={form.submitterName}
          />
        </TableCell>
        <TableCell>
          <LdapPersonModal {...getLatestSupervisorAction(form)} />
        </TableCell>
        {
          formConfig.customColumns ?
          formConfig.customColumns.map((col) => <TableCell>{col.func(form)}</TableCell>) : ""
        }
        <TableCell style={{ padding: "0.2em" }}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <div>
              <StatusTag formId={form._id} status={form.status}></StatusTag>
              <Decider form={form} formAccess={formAccess} />
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                gap: "0.5em",
              }}
            >
              {lg && form.files && form.files.length > 0 && (
                <Tooltip
                  classes={{
                    tooltip: classes.customWidth,
                  }}
                  title={
                    <>
                      {form.files &&
                        form.files.map((file) => (
                          <div key={file.name}>
                            {file.name}, {file.size}
                          </div>
                        ))}
                    </>
                  }
                  placement="top"
                  arrow
                >
                  <AttachFileIcon
                    style={{
                      alignSelf: "center",
                      color: "#aaa",
                    }}
                  />
                </Tooltip>
              )}
              {lg && isAdmin && (
                <span>
                  <Tooltip title="Toggle comments" placement="top" arrow>
                    <IconButton
                      data-testid={`form-${form._id}-toggle-comment-button`}
                      style={{ ...commentIconStyle, color: "#de9300" }}
                      onClick={() => setShowComments(!showComments)}
                      size="small"
                    >
                      <CommentIcon />
                    </IconButton>
                  </Tooltip>
                  <div
                    style={{
                      textAlign: "left",
                      display: "inline-block",
                      width: "1.2em",
                      fontSize: "0.7",
                      fontWeight: "bold",
                      color: "#de9300",
                    }}
                  >
                    {form.comments?.length || 0}
                  </div>
                </span>
              )}
              {lg && (
                <FormLink
                  formType={form.formType}
                  id={form._id}
                  editable={formAccess.canEdit}
                />
              )}
              <ActionMenu
                formId={form._id || ""}
                actions={[
                  {
                    label: "Toggle content (inline)",
                    testId: "action-menu-toggle-details",
                    onClick: () => setOpen(!open),
                  },
                  {
                    label: "View content (ext.)",
                    testId: "action-menu-view-original",
                    onClick: () => {
                      history.push(`/${formConfig.path}/form/${form._id}`);
                    },
                  },
                  {
                    label: "View print version",
                    testId: "action-menu-view-print-version",
                    onClick: () => {
                      history.push(`/${formConfig.path}/print/${form._id}`);
                    },
                  },
                  {
                    disabled: !isAdmin,
                    label: "Toggle comments",
                    testId: "action-menu-toggle-comments",
                    onClick: () => {
                      setShowComments(!showComments);
                    },
                  },
                  {
                    disabled: !isAdmin,
                    label: "Clone",
                    testId: "action-menu-clone",
                    onClick: () => {
                      if (isAdmin) {
                        dispatchCloneAction(form);
                      }
                    },
                  },
                  {
                    disabled:
                      !isAdmin || form.isArchived || form.status === "PENDING",
                    label: form.isArchived ? "Archived" : "Archive",
                    testId: "action-menu-archive",
                    onClick: () => {
                      if (
                        !(
                          !isAdmin ||
                          form.isArchived ||
                          form.status === "PENDING"
                        )
                      ) {
                        dispatchArchiveAction(form);
                      }
                    },
                  },
                  {
                    disabled: !form.files || form.files.length === 0,
                    label: "Download attachments",
                    testId: "action-menu-download-attachments",
                    onClick: () => {
                      form.files.forEach((file) => {
                        window.open("/api/files/" + file.name);
                      });
                    },
                  },
                  {
                    disabled: !formConfig.fileUpload,
                    label: "Upload additional attachments",
                    testId: "action-menu-upload-attachments",
                    onClick: () => {
                      dispatch({
                        type: "SET_MODAL_PROPS",
                        modalProps: {
                          open: true,
                          title: "Upload file",
                          content: (
                            <>
                              <input
                                style={{ display: "none" }}
                                id={`file-input-${form._id}`}
                                type="file"
                                name="file"
                                onChange={async (
                                  e: React.ChangeEvent<HTMLInputElement>
                                ) => {
                                  if (
                                    e.currentTarget &&
                                    e.currentTarget.files &&
                                    e.currentTarget.files.length > 0
                                  ) {
                                    let newFile = e.currentTarget.files[0];
                                    const jsonData = await createFile(newFile);

                                    const res = await mapFile({
                                      fileName: jsonData + "-" + newFile.name,
                                      size: getSize(newFile.size),
                                      id: form._id as string,
                                      formType: form.formType,
                                    });
                                    dispatch(
                                      getModalMsgDispatch(
                                        "File upload",
                                        res
                                          ? "The file was successfully added"
                                          : "The file could not be added"
                                      )
                                    );
                                    form.files.push({
                                      name: jsonData + "-" + newFile.name,
                                      size: getSize(newFile.size),
                                    });
                                    onFormChange && onFormChange(form);
                                  }
                                }}
                              />

                              <label htmlFor={`file-input-${form._id}`}>
                                <Button
                                  className="link"
                                  component="span"
                                  variant="text"
                                >
                                  Click here to select a file from your file
                                  system.
                                </Button>
                              </label>
                            </>
                          ),
                          btn1Text: "Cancel",
                          btn1Color: "info",

                          btn1OnClick: async (inputs: System.ModalInputs) => {},
                        },
                      });
                    },
                  },
                  {
                    label: "Edit external ID",
                    testId: "action-menu-edit-external-id",
                    disabled: !formConfig.hasExternalId || !isAdmin,
                    onClick: () => {
                      dispatch(
                        dispatchExternalIdAction(form, (form) => {
                          onFormChange(form);
                        })
                      );
                    },
                  },
                  {
                    label: "Delete",
                    testId: "action-menu-delete",
                    disabled: !isAdmin || form.status !== "REJECTED",
                    onClick: () => {
                      dispatch({
                        type: "SET_MODAL_PROPS",
                        modalProps: {
                          open: true,
                          title: "Delete Form",
                          content:
                            "Are you sure you want to delete this form? This action cannot be undone!",
                          btn1Text: "Delete",
                          btn1Color: "secondary",
                          btn2Text: "Cancel",

                          btn1OnClick: async (inputs: System.ModalInputs) => {
                            try {
                              await API.deleteForm(
                                form._id,
                                formConfig.formType
                              );
                              onFormAddedOrRemoved && onFormAddedOrRemoved();
                              dispatch(
                                getInfoModal(
                                  "Form deleted",
                                  `${formConfig.name} #${form._id} was successfully deleted`
                                )
                              );
                            } catch (e) {
                              dispatch(
                                getModalAPIErrorDispatch(e as System.APIError)
                              );
                            }
                          },
                        },
                      });
                    },
                  },
                ]}
                menuLabel={<MoreVertIcon style={{ color: "#de9300" }} />}
                dropDownTitle="Available actions"
              />
            </div>
          </div>
        </TableCell>
      </TableRow>
      <TableRow style={{ background: "#fafafa" }}>
        <TableCell colSpan={6} style={{ padding: "0" }}>
          <Collapse
            in={showComments || (!!state.commentHighlight && isArchive)}
            timeout="auto"
            unmountOnExit
          >
            <CommentsWidget
              form={form}
              onChange={(form) => {
                onFormChange(form);
              }}
            />
          </Collapse>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell className={"highlight-row"} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Typography
              style={{
                fontSize: "1.25em",
                paddingLeft: "2em",
                paddingTop: "1em",
              }}
            >
              Event timeline
            </Typography>
            <SupervisorStepper form={form} />
            <formConfig.formComponent
              embedded={true}
              match={{ params: [form._id || ""] }}
            />
            <div
              style={{
                textAlign: "right",
                paddingRight: "2.5em",
                paddingBottom: "2em",
                fontSize: "0.8em",
                borderBottom: "1px solid rgba(254, 169, 1, 0.6)",
              }}
            >
              <Tooltip
                title="Show a print friendly version of this form"
                placement="top"
                arrow
              >
                <Link
                  data-testid={`form-${form._id}-print-version-link`}
                  to={`/${formConfig.path}/print/${form._id}`}
                  className="link"
                >
                  PRINT VERSION
                </Link>
              </Tooltip>
            </div>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

/** Messy because hadling many cases:
 * Does the formType have external id? Does this specific form has a external id defined? Is the current user admin?
 */
const IdComponent = (props: {
  form: System.Form;
  formConfig: System.FormConfig;
  compact: boolean;
  onChange: (form: System.Form) => void;
}) => {
  const { dispatch, state } = useStore();
  const { form, compact } = props;
  const enableEdit = state.user.admin[form.formType] && !compact;
  if (!props.formConfig.hasExternalId) {
    return <>{form._id}</>;
  }
  const extId = !form.externalId ? (
    <span style={{ fontWeight: "bold" }}>{enableEdit ? "+" : ""}</span>
  ) : (
    form.externalId
  );
  return (
    <div
      style={{ display: "flex", alignItems: "center" }}
      data-testid={`form-${form._id}-id-container-div`}
    >
      <span>{form._id}</span>
      {!form.externalId ? (
        <span style={{ padding: "0 0.2em" }} />
      ) : (
        <span
          style={{
            fontWeight: "bold",
            color: "#aaa",
            fontSize: "1.4em",
            padding: "0 0.2em",
          }}
        >
          /
        </span>
      )}
      <Tooltip
        title={
          !enableEdit
            ? form.externalId || ""
            : form.externalId
            ? "External ID. Click to edit"
            : "Click to add an external ID"
        }
        placement="top"
        arrow
      >
        <span>
          <button
            data-testid={`form-${form._id}-external-id-button`}
            disabled={!state.user.admin[form.formType]}
            className="link"
            style={{
              maxWidth: "10em",
              whiteSpace: "nowrap",
              overflow: "hidden",
            }}
            onClick={() => {
              dispatch(dispatchExternalIdAction(props.form, props.onChange));
            }}
          >
            {extId}
          </button>
        </span>
      </Tooltip>
    </div>
  );
};
const dispatchExternalIdAction = (
  form: System.Form,
  onChange: (form: System.Form) => void
): formAction => {
  return {
    type: "SET_MODAL_PROPS",
    modalProps: {
      open: true,
      title: "External ID",
      content: (
        <span style={{ fontSize: "0.8em", fontStyle: "italic", color: "#777" }}>
          External ids lets you map submission in Domni with associated data in
          another system. Submission are also searchable by external id in the
          default search filter.
        </span>
      ),
      btn1Text: "Save",
      btn2Text: "Cancel",
      btn1OnClick: async (inputs: System.ModalInputs) => {
        const updatedForm = await API.update({
          form: { ...form, externalId: inputs.id.value },
          updateType: "EXTERNAL_ID",
        });
        onChange(updatedForm);
      },
      inputs: {
        id: {
          type: "text",
          label: "External ID",
          value: form.externalId || "",
        },
      },
    },
  };
};
export default SubmittedForm;
