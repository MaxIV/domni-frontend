import React from "react";
import { Tooltip } from "@mui/material";
const TaggedTitle = (props: {
  title: string;
  tag: string;
  tagTooltip: string;
}) => {
  return (
    <>
      {props.title}{" "}
      <Tooltip title={props.tagTooltip} placement="top-start">
        <span className="consolas-label">{props.tag}</span>
      </Tooltip>
    </>
  );
};
export default TaggedTitle;
