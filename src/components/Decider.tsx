import React from "react";
import { Button, Tooltip } from "@mui/material";
import { useStore } from "../store/Store";
import { useLocation, Link, Redirect } from "react-router-dom";
import * as Config from "../utilities/Config";
import * as api from "../api/forms";
import {
  getSortedSupervisorActions,
  isCurrentSupervisor,
  pushComment,
  createComment,
} from "../utilities/Helpers";
import { actionType } from "../pages/Submit";
import { getModalAPIErrorDispatch } from "./Modal";

interface Props {
  form: System.Form;
  formAccess: System.FormAccess;
}
interface SupervisorFormUpdate {
  form: System.Form;
  status: System.FormStatus;
  newSupervisor: System.Supervisor;
  comment: string;
  notify: boolean;
}

const Decider = (props: Props) => {
  const { dispatch, state } = useStore();
  const { form, formAccess } = props;
  const [redirect, setRedirect] = React.useState("");
  const location = useLocation().pathname;
  const formConfig = Config.getFormConfig(form.formType);
  const isArchive = location.includes("archive"); //Approval if false

  const update = async (supervisorFormUpdate: SupervisorFormUpdate) => {
    const { form, status, newSupervisor, comment, notify } =
      supervisorFormUpdate;
    const isAdmin = state.user.admin[form.formType];
    const actions = getSortedSupervisorActions(form);
    const isCurrentSuper = isCurrentSupervisor(form, state.user);
    //Updating latest and adding new action - we want to add an action if a new supervisor has been assigned
    //We also want to add an action if the person processing this is not the assigned supervisor (i.e. admin).
    //If so, we set the current action as SUPPLANTED and create a new one for the admin and her action
    actions[actions.length - 1] = {
      eventType: "SupervisorAction",
      comment: isCurrentSuper ? comment : "Processed by " + state.user.name,
      notify: isCurrentSuper ? notify : false,
      action: isCurrentSuper ? status : "SUPPLANTED",
      timestamp: actions[actions.length - 1].timestamp, //keep old value
      decisionDate: new Date(),
      name: actions[actions.length - 1].name, //keep old value
      email: actions[actions.length - 1].email, //keep old value
      role: actions[actions.length - 1].role, //keep old value
    };
    if (!isCurrentSuper) {
      const future = new Date();
      future.setSeconds(future.getSeconds() + 1);
      actions.push({
        eventType: "SupervisorAction",
        action: status,
        comment,
        notify,
        timestamp: new Date(),
        decisionDate: future,
        name: state.user.name,
        email: state.user.email,
        role: isAdmin ? "Administrator" : "Supervisor",
      });
    }
    if (newSupervisor) {
      const future = new Date();
      future.setSeconds(future.getSeconds() + 2);
      actions.push({
        eventType: "SupervisorAction",
        comment: "",
        notify: true,
        action: "PENDING",
        timestamp: future,
        decisionDate: undefined,
        name: newSupervisor.name,
        email: newSupervisor.email,
        role: "Supervisor",
      });
    } else {
      form.status = status; //finalize
    }
    const comments = pushComment(
      form,
      comment,
      newSupervisor
        ? "REASSIGNMENT COMMENT"
        : status === "APPROVED"
        ? "APPROVAL COMMENT"
        : "REJECTION COMMENT",
      state
    );
    try {
      const result = await api.update({
        form: { ...form, supervisorActions: actions, comments },
        updateType: !!newSupervisor
          ? "SUPERVISOR_REASSIGNMENT"
          : status === "REJECTED"
          ? "REJECTION"
          : "APPROVAL",
        comment: comment
          ? createComment(form, comment, "REASSIGNMENT COMMENT", state)
          : undefined,
      });
      const redirectMsg: actionType = newSupervisor
        ? "reassigned"
        : status === "APPROVED"
        ? "approved"
        : "rejected";
      setRedirect(`/${formConfig.path}/submit/?action=${redirectMsg}`);
      //Remove from pending supervisor cases if approving or rejecting
      if (status === "APPROVED" || "REJECTED") {
        dispatch({
          type: "SET_PENDING_SUPERVISOR_CASES",
          pendingSupervisorCases: {
            ...state.pendingSupervisorCases,
            [result.formType]: state.pendingSupervisorCases[
              result.formType
            ].filter((c) => c._id !== result._id),
          },
        });
      } else {
        //update the form in the cache if not removed
        const array = state.pendingSupervisorCases[result.formType].slice(0);
        array.forEach((form, index) => {
          if (form._id === result._id) {
            array[index] = result;
          }
        });

        dispatch({
          type: "SET_PENDING_SUPERVISOR_CASES",
          pendingSupervisorCases: {
            ...state.pendingSupervisorCases,
            [result.formType]: array,
          },
        });
      }
    } catch (err) {
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
      return false;
    }
  };

  const isApproved = form.status === "APPROVED";
  if (redirect) {
    return <Redirect to={redirect} />;
  }
  ////provide no actions if the form is archived, and print that
  if (form.isArchived) {
    return (
      <Tooltip
        title="This submission is archived and cannot be edited"
        placement="top-start"
      >
        <div className="archived-tag">Archived</div>
      </Tooltip>
    );
  }
  //provide no actions if the form is not processable)
  if (!formAccess.canResolve) {
    return null;
  }
  if (!formConfig.requiresApproval && form.status === "APPROVED") {
    return null; //can't recall forms that don't require approval
  }
  //if in archive view, just link to the approval view
  if (isArchive) {
    return (
      <>
        <Link
          to={location.replace("archive", "approval") + form._id}
          className="link"
          data-testid={`form-${form._id}-${
            isApproved ? "recall" : "resolve"
          }-button`}
        >
          <Button
            variant="contained"
            size="small"
            color="info"
            style={{ margin: "auto 0.2em", fontSize: "0.8em", minWidth: "6em" }}
          >
            {isApproved ? "Recall" : "Resolve"}
          </Button>
        </Link>{" "}
      </>
    );
  }
  if (!formConfig.requiresApproval) {
    return (
      <Button
        data-testid={`form-${form._id}-process-button`}
        variant="contained"
        color="primary"
        size="small"
        style={{ margin: "auto 0.2em", fontSize: "0.8em", minWidth: "6em" }}
        onClick={() =>
          dispatch({
            type: "SET_MODAL_PROPS",
            modalProps: {
              open: true,
              title: `${formConfig.name} processing`,
              content: `Are you sure you want to set this ${formConfig.name} as processed?`,
              btn1Text: "OK",
              btn1OnClick: (
                inputs: System.ModalInputs,
                selectedSupervisor: System.Supervisor
              ) =>
                update({
                  form,
                  status: "APPROVED",
                  newSupervisor: selectedSupervisor,
                  comment: inputs.comment.value,
                  notify: false,
                }),
              inputs: {
                comment: {
                  type: "text",
                  label: "Optional comment",
                  value: "",
                },
              },
              btn2Text: "Cancel",
            },
          })
        }
      >
        Update
      </Button>
    );
  }
  return (
    <>
      {/* Approval */}
      {!isApproved && (
        <Tooltip
          title="Click here to to perform a direct (finalized), or indirect (reassignment) approval"
          placement="top-start"
        >
          <Button
            data-testid={`form-${form._id}-approve-button`}
            variant="contained"
            color="primary"
            size="small"
            style={{ margin: "auto 0.2em", fontSize: "0.8em", minWidth: "6em" }}
            onClick={() =>
              dispatch({
                type: "SET_MODAL_PROPS",
                modalProps: {
                  open: true,
                  size: "sm",
                  title: `${formConfig.name} approval`,
                  content:
                    formConfig.approvalMessage ||
                    `You can either finalize the approval now, in which case the original submitter gets notified, or assign an additional supervisor who also needs to review this ${formConfig.name} before it gets finalized.`,
                  btn1Text: "Approve",
                  supervisorSelector: {
                    formType: formConfig.formType,
                    label: formConfig.approvalMessage
                      ? ""
                      : "Does this form require the approval by an additional supervisor?",
                    alias: "No, finalize my decision",
                    allowEmptyValue: true,
                  },
                  btn1OnClick: (
                    inputs: System.ModalInputs,
                    selectedSupervisor: System.Supervisor
                  ) =>
                    update({
                      form,
                      status: "APPROVED",
                      newSupervisor: selectedSupervisor,
                      comment: inputs.comment.value,
                      notify: inputs.notifyMe.value.toLowerCase() === "true",
                    }),
                  inputs: {
                    comment: {
                      type: "text",
                      label: "Approval comment (Optional)",
                      value: "",
                    },
                    notifyMe: {
                      type: "checkbox",
                      label:
                        "Notify me about any additional changes made to this form",
                      value: "false",
                    },
                  },
                  btn2Text: "Cancel",
                  validate: (_inputs, supervisor) => {
                    // Lack of a supervisor here indicates final approval. Execute final approval validation if required by the form.
                    if (!supervisor && formConfig.finalApprovalValidation) {
                      return formConfig.finalApprovalValidation(form);
                    }
                    return {
                      status: "SUCCESS",
                    };
                  },
                },
              })
            }
          >
            Approve
          </Button>
        </Tooltip>
      )}
      {/* Recall or reject */}
      <Button
        data-testid={`form-${form._id}-reject-button`}
        variant="contained"
        color="secondary"
        size="small"
        style={{ fontSize: "0.8em", minWidth: "6em" }}
        onClick={() =>
          dispatch({
            type: "SET_MODAL_PROPS",
            modalProps: {
              open: true,
              size: "sm",
              title: isApproved
                ? `${formConfig.name} approval recall`
                : `${formConfig.name} rejection`,
              content: isApproved
                ? `Are you sure you want to recall the approval of this ${formConfig.name}? This will mark the form as Rejected. The original submitter can edit and resubmit the form.`
                : `Are you sure you want to reject this ${formConfig.name}? The original submitter will be notificed by mail along with your rejection comment. They will also have the option to correct the form and resubmit it`,
              btn1Text: isApproved ? "Recall approval" : "Reject",
              btn1Color: "secondary",
              btn2Text: "Cancel",
              btn1OnClick: (
                inputs: System.ModalInputs,
                selectedSupervisor: System.Supervisor
              ) =>
                update({
                  form,
                  status: "REJECTED",
                  newSupervisor: selectedSupervisor,
                  comment: inputs.comment.value,
                  notify: false,
                }),
              inputs: {
                comment: {
                  type: "text",
                  label: isApproved
                    ? "Approval recall comment (optional)"
                    : "Rejection comment (optional)",
                  value: "",
                },
              },
            },
          })
        }
      >
        {isApproved ? "Recall approval" : "Reject"}
      </Button>
    </>
  );
};

export default Decider;
