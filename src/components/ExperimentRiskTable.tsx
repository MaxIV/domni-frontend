import { Grid, TextField } from "@mui/material";
import React from "react";
import { useWindowDimensions } from "../utilities/Helpers";

type Props = {
  updateFormData: (form: System.RiskAssessmentForm) => void;
  formData: System.RiskAssessmentForm;
  embedded: boolean;
};

export const ExperimentRiskTable = (props: Props) => {
  const { formData, updateFormData } = props;
  const { lg } = useWindowDimensions();
  return (
    <div
      style={{
        borderBottom: "none",
        background: "#f0f0f0",
        paddingBottom: "1em",
        borderRadius: "0 0 0.2em 0.2em",
      }}
    >
      {lg && (
        <Grid
          container
          style={{
            background: "#777",
            color: "#fff",
            padding: "0em",
            borderRadius: "0.2em 0.2em 0 0",
          }}
          item
          xs={12}
        >
          <Grid item lg={3} style={{ padding: "1em" }}>
            Details
          </Grid>
          <Grid item lg={3} style={{ padding: "1em" }}>
            Risks and hazards
          </Grid>
          <Grid item lg={3} style={{ padding: "1em" }}>
            Suggested mitigation
          </Grid>
          <Grid item lg={3} style={{ padding: "1em" }}>
            Actions in the event of emergency
          </Grid>
        </Grid>
      )}
      {(
        Object.keys(
          formData.experimentDetails
        ) as System.ExperimentDetailLocation[]
      ).map((key) => {
        const { details, risks, mitigation, emergencyAction } =
          formData.experimentDetails[key];
        return (
          <Grid container key={key} item xs={12}>
            <Grid item xs={12}>
              <div
                style={{
                  fontSize: lg ? "0.8em" : "1em",
                  padding: "1em",
                  paddingBottom: "0",
                  fontStyle: lg ? "italic" : "normal",
                  textAlign: lg ? "left" : "center",
                }}
              >
                {key === "lab"
                  ? "At the Chemical/Bio/Preparation Lab (Specify)"
                  : key === "beamline"
                  ? "At the beamline"
                  : "In other places"}
              </div>
            </Grid>
            {!lg && (
              <Grid
                item
                xs={2}
                style={{
                  textAlign: "center",
                  paddingTop: "0.5em",
                }}
              >
                Details
              </Grid>
            )}
            <Grid
              item
              xs={10}
              lg={3}
              key={key + "details"}
              style={{
                padding: "0.5em",
              }}
            >
              {" "}
              <TextField
                fullWidth={true}
                style={{ background: "white" }}
                value={details}
                onChange={(event) =>
                  updateFormData({
                    ...formData,
                    experimentDetails: {
                      ...formData.experimentDetails,
                      [key]: {
                        ...formData.experimentDetails[key],
                        details: event.target.value,
                      },
                    },
                  })
                }
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 6}
              />
            </Grid>
            {!lg && (
              <Grid
                item
                xs={2}
                style={{
                  textAlign: "center",
                  paddingTop: "0.5em",
                }}
              >
                Risks and hazards
              </Grid>
            )}
            <Grid
              item
              xs={10}
              lg={3}
              key={key + "risks"}
              style={{
                padding: "0.5em",
              }}
            >
              {" "}
              <TextField
                fullWidth={true}
                style={{ background: "white" }}
                value={risks}
                onChange={(event) =>
                  updateFormData({
                    ...formData,
                    experimentDetails: {
                      ...formData.experimentDetails,
                      [key]: {
                        ...formData.experimentDetails[key],
                        risks: event.target.value,
                      },
                    },
                  })
                }
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 6}
              />
            </Grid>
            {!lg && (
              <Grid
                item
                xs={2}
                style={{
                  textAlign: "center",
                  paddingTop: "0.5em",
                }}
              >
                Suggested mitigation
              </Grid>
            )}
            <Grid
              item
              xs={10}
              lg={3}
              key={key + "mitigation"}
              style={{
                padding: "0.5em",
              }}
            >
              {" "}
              <TextField
                fullWidth={true}
                style={{ background: "white" }}
                value={mitigation}
                onChange={(event) =>
                  updateFormData({
                    ...formData,
                    experimentDetails: {
                      ...formData.experimentDetails,
                      [key]: {
                        ...formData.experimentDetails[key],
                        mitigation: event.target.value,
                      },
                    },
                  })
                }
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 6}
              />
            </Grid>
            {!lg && (
              <Grid
                item
                xs={2}
                style={{
                  textAlign: "center",
                  paddingTop: "0.5em",
                }}
              >
                Action in the event of emergency
              </Grid>
            )}
            <Grid
              item
              xs={10}
              lg={3}
              key={key + "emergencyAction"}
              style={{
                padding: "0.5em",
              }}
            >
              {" "}
              <TextField
                fullWidth={true}
                style={{ background: "white" }}
                value={emergencyAction}
                onChange={(event) =>
                  updateFormData({
                    ...formData,
                    experimentDetails: {
                      ...formData.experimentDetails,
                      [key]: {
                        ...formData.experimentDetails[key],
                        emergencyAction: event.target.value,
                      },
                    },
                  })
                }
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 6}
              />
            </Grid>
          </Grid>
        );
      })}
    </div>
  );
};
