import React from "react";
import { Button, Grid, Typography } from "@mui/material";
import { FileLink, getSize } from "../utilities/Helpers";
import { createFile } from "../api/files";

type Props = {
  handleSetFiles: (arg0: { name: string; size: string }) => void;
  files: { name: string; size: React.ReactNode }[];
  handleClearFiles:
    | ((event: React.FormEvent<HTMLButtonElement>) => void)
    | undefined;
  helpText: string | JSX.Element;
  disabled: boolean;
};

const FormGridFileUpload = (props: Props) => {
  const handleFiles = async (event: { target: { files: any } }) => {
    if (event.target.files.length > 0) {
      let newFile = event.target.files[0];
      const jsonData = await createFile(newFile);
      const fileToAdd = {
        name: jsonData + "-" + newFile.name,
        size: getSize(newFile.size),
      };
      props.handleSetFiles(fileToAdd);
    }
  };

  return (
    <Grid item xs={12} style={{ marginTop: "1em" }}>
      <Typography className="form-heading">Files</Typography>
      <Typography style={{ paddingBottom: "1em" }}>
        {!props.disabled && <small>{props.helpText}</small>}
      </Typography>
      <Typography>
        Uploaded {props.files.length}{" "}
        {props.files.length === 1 ? "file" : "files"}:
      </Typography>
      <ul>
        {props.files.map((item: { name: string; size: React.ReactNode }) => {
          return (
            <li key={item.name}>
              <div key={item.name}>{FileLink(item)}</div>
            </li>
          );
        })}
      </ul>
      {!props.disabled && (
        <>
          <input
            style={{ display: "none" }}
            id="fileInput"
            type="file"
            name="file"
            onChange={handleFiles}
          />
          <label htmlFor="fileInput" style={{ marginRight: "1em" }}>
            <Button color="info" component="span" variant="contained">
              Upload a file
            </Button>
          </label>

          <Button
            onClick={props.handleClearFiles}
            variant="contained"
            color="secondary"
          >
            Clear files
          </Button>
        </>
      )}
    </Grid>
  );
};

export default FormGridFileUpload;
