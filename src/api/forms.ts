import * as config from "../utilities/Config";

export const cloneForm = async (
  formType: System.FormType,
  id: string
): Promise<System.Form> => {
  const resp = await fetch(`/api/forms/clone`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ formType, id }),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `The form could not be cloned`,
    };
    throw error;
  }
  return await resp.json();
};
export const createForm = async (
  formData: System.Form
): Promise<System.Form> => {
  const formConfig = config.getFormConfig(formData.formType);
  const resp = await fetch(`/api/forms/`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(formData),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `Error submitting the ${formConfig.name}`,
    };
    throw error;
  }
  return await resp.json();
};
export const emailData = async (simpleForm: System.SimpleForm) => {
  const resp = await fetch(`/api/email/`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ simpleForm }),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      msg: "The form could not be submitted. Please try again or contact IMS directly if the problem persists",
      title: `Error submitting request`,
    };
    throw error;
  }
};

export const update = async (
  formUpdate: System.FormUpdate
): Promise<System.Form> => {
  const formConfig = config.getFormConfig(formUpdate.form.formType);
  const process =
    formUpdate.updateType === "APPROVAL" ||
    formUpdate.updateType === "REJECTION" ||
    formUpdate.updateType === "SUPERVISOR_REASSIGNMENT"
      ? "process/"
      : "";
  const resp = await fetch(`/api/forms/${process}`, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(formUpdate),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      title: resp.status === 403 ? "Forbidden" : "",
      msg:
        resp.status === 403
          ? `Only the designated supervisor or ${formConfig.name} administrators can update this submission`
          : "",
      code: resp.status,
    };
    throw error;
  }
  return await resp.json();
};

interface SupervisorCaseParams {
  status?: System.FormStatus;
  formType?: System.FormType;
}
export const getSupervisorCases = async (
  params: SupervisorCaseParams
): Promise<{ [key in System.FormType]: System.Form[] }> => {
  const resp = await fetch(
    `/api/forms/supervisorCases/?status=${params.status || ""}&formType=${
      params.formType || ""
    }`
  );
  if (!resp.ok) {
    const error: System.APIError = {
      title: resp.status === 403 ? "Forbidden" : "",
      msg:
        resp.status === 403
          ? `You are not authorized to access this resource`
          : "",
      code: resp.status,
    };
    throw error;
  }
  return await resp.json();
};

export const deleteAllDrafts = async () => {
  const resp = await fetch(`/api/forms/userDrafts/`, {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `The drafts could not be deleted`,
      msg:
        resp.status === 403 ? `You are not authorized to delete this form` : "",
    };
    throw error;
  }
  return await resp.json();
};
export const deleteForm = async (
  id: string | undefined,
  formType: System.FormType
) => {
  if (!id) {
    return;
  }
  const resp = await fetch(`/api/forms/`, {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ id, formType }),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `Form could not be deleted`,
      msg:
        resp.status === 403 ? `You are not authorized to delete this form` : "",
    };
    throw error;
  }
  return await resp.json();
};
export const getUserDrafts = async (): Promise<
  { [key in System.FormType]: System.Form[] }
> => {
  const resp = await fetch(`/api/forms/userDrafts/`);
  if (!resp.ok) {
    const error: System.APIError = {
      title: resp.status === 403 ? "Forbidden" : "",
      msg:
        resp.status === 403
          ? `You are not authorized to access this resource`
          : "",
      code: resp.status,
    };
    throw error;
  }
  return await resp.json();
};

export const getUserCases = async (
  fromDate: Date,
  toDate: Date
): Promise<{ [key in System.FormType]: System.Form[] }> => {
  const resp = await fetch(
    `/api/forms/userCases/?fromDate=${fromDate.toISOString()}&toDate=${toDate.toISOString()}`
  );
  if (!resp.ok) {
    const error: System.APIError = {
      title: resp.status === 403 ? "Forbidden" : "",
      msg:
        resp.status === 403
          ? `You are not authorized to access this resource`
          : "",
      code: resp.status,
    };
    throw error;
  }
  return await resp.json();
};

export const getForm = async (
  id: string,
  formType: System.FormType
): Promise<System.Form> => {
  const resp = await fetch(`/api/forms/?id=${id}&formType=${formType}`);
  if (!resp.ok) {
    const error: System.APIError = {
      title: resp.status === 403 ? "Forbidden" : "",
      msg:
        resp.status === 403
          ? `You are not authorized to access this resource`
          : "",
      code: resp.status,
    };
    throw error;
  }
  return await resp.json();
};
export const search = async (
  formConfig: System.FormConfig,
  searchParams: System.ArchiveSearchParams,
  signal: AbortSignal,
  paginationInfo?: System.PaginationInfo
): Promise<System.SearchResults> => {
  const urlParams = new URLSearchParams({
    fromDate: searchParams.fromDate.toISOString(),
    toDate: searchParams.toDate.toISOString(),
  });
  if (searchParams.query) {
    urlParams.append("query", searchParams.query);
  }

  if (searchParams.type !== "DEFAULT") {
    urlParams.append("type", searchParams.type);
  }
  if (searchParams.status !== "Any") {
    urlParams.append("status", searchParams.status);
  }
  if (searchParams.customField) {
    urlParams.append("customField", searchParams.customField);
  }
  if (searchParams.includeArchived === true) {
    urlParams.append("includeArchived", "true");
  }

  if (paginationInfo && paginationInfo.page !== 1) {
    urlParams.append("page", paginationInfo.page.toString());
  }
  if (paginationInfo && paginationInfo.limit !== 10) {
    urlParams.append("limit", paginationInfo.limit.toString());
  }

  const query = `/api/forms/search?${urlParams}&formType=${formConfig.formType}`;
  let toThrow;
  try {
    const resp = await fetch(query, { signal });
    if (!resp.ok) {
      const error: System.APIError = {
        title: resp.status === 403 ? "Forbidden" : "",
        msg:
          resp.status === 403
            ? `You are not authorized to access this resource`
            : "",
        code: resp.status,
        wasAborted: false,
      };
      toThrow = error;
    } else {
      return await resp.json();
    }
  } catch (err) {
    if ((err as Error).name === "AbortError") {
      const error: System.APIError = {
        title: "Aborted",
        msg: "Aborted",
        code: 0,
        wasAborted: true,
      };
      throw error;
    } else {
      const error: System.APIError = {
        title: "Network error",
        msg: "Unexpected network error",
        code: 0,
      };
      throw error;
    }
  }
  throw toThrow;
};
