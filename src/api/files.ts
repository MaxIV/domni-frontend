export const createFile = async (file: File): Promise<Number> => {
  const fileData = new FormData();
  fileData.append("file", file);

  const resp = await fetch("/api/files", {
    method: "POST",
    body: fileData,
  });
  if (!resp.ok) {
    const error: System.APIError = {
      title: "Error uploading file",
      code: resp.status,
    };
    throw error;
  }
  return await resp.json();
};

export const mapFile = async (data: {
  fileName: string;
  size: string;
  id: string;
  formType: System.FormType;
}) => {
  const resp = await fetch("/api/forms/file", {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return resp.ok;
};
