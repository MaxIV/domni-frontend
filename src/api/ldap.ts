export const ldapSearch = async (
  request: System.LDAPRequest
): Promise<System.LDAPPerson[]> => {
  const resp = await fetch(`/api/ldap/`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(request),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `LDAP search failed`,
    };
    throw error;
  }
  return await resp.json();
};

export const update = async (
  person: System.LDAPPersonUpdate
): Promise<System.LDAPPerson> => {
  const resp = await fetch(`/api/ldap/`, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(person),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `LDAP search failed`,
    };
    throw error;
  }
  return resp.json();
};

export const getLdapPerson = async (
  email: string,
  formType: System.FormType
): Promise<System.LDAPPerson> => {
  const resp = await fetch(`/api/ldap/person?email=${email}`, {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `LDAP search failed`,
    };
    throw error;
  }
  return resp.json();
};
export const ldapLoadAll = async (): Promise<System.LDAPPerson[]> => {
  const resp = await fetch(`/api/ldap/`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `LDAP search failed`,
    };
    throw error;
  }
  return await resp.json();
};
