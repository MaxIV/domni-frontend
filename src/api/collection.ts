export const getCollection = async (
  form: System.FormType | undefined,
  collectionType: System.CollectionType,
  signal: AbortSignal
): Promise<System.List[] | System.Supervisor[] | System.Project[]> => {
  const resp = form
    ? await fetch(
        `/api/collection/?collectionType=${collectionType}&form=${form}`,
        { signal }
      )
    : await fetch(`/api/collection/?collectionType=${collectionType}`, {
        signal,
      });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `Error fetching ${collectionType}`,
    };
    throw error;
  }
  return await resp.json();
};

export const createCollection = async (
  collectionType: System.CollectionType,
  collection: System.Collection
): Promise<System.Collection> => {
  const { _id, ...collectionWithoutId } = collection;
  const resp = await fetch(`/api/collection/`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ collection: collectionWithoutId, collectionType }),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `Error creating ${collectionType}`,
    };
    throw error;
  }
  return await resp.json();
};

export const deleteCollection = async (
  collectionType: System.CollectionType,
  collectionId: string,
  formType: System.FormType
): Promise<System.List | System.Supervisor | System.Project> => {
  const resp = await fetch(`/api/collection/`, {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ _id: collectionId, formType, collectionType }),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `Error deleting ${collectionType.toLowerCase()}`,
    };
    throw error;
  }
  return await resp.json();
};

export const updateCollection = async (
  collectionType: System.CollectionType,
  collection: System.Collection
): Promise<System.Collection> => {
  const resp = await fetch(`/api/collection/`, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ collection, collectionType }),
  });
  if (!resp.ok) {
    const error: System.APIError = {
      code: resp.status,
      title: `Error updating ${collectionType}`,
    };
    throw error;
  }
  return await resp.json();
};
