export const getDbData = async (): Promise<System.DbData> => {
  const resp = await fetch(`/api/util/dbData/`);
  if (!resp.ok) {
    return Promise.resolve({ type: "PRODUCTION" });
  }
  return (await resp.json()) as System.DbData;
};
