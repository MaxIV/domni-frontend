import {} from "react";
declare global {
  export namespace System {
    export interface IState {
      isLoading: boolean;
      modalProps: System.ModalProps;
      commentHighlight: string;
      user: System.User;
      dbData: System.DbData;
      ldapPeople: System.LDAPPerson[];
      supervisors: {
        [key in System.FormType]: System.Supervisor[];
      };
      projects: {
        [key in System.FormType]: System.Project[];
      };
      lists: {
        [key in System.FormType]: System.List[];
      };
      pendingSupervisorCases: { [key in System.FormType]: System.Form[] };
    }

    type EventRole = "Submitter" | "Supervisor" | "Administrator" | "System";
    type FormVisibility = "PUBLIC" | "STAFF";
    type CollectionType = "LIST" | "PROJECT" | "SUPERVISOR";
    type FormType =
      | "PURCHASE_APPLICATION"
      | "TRAVEL"
      | "EMPLOYMENT_REQUEST"
      | "GRANT_INITIATIVE"
      | "ADDITIONAL_FUNDING"
      | "HOTEL_BOOKING"
      | "CATERING_REQUEST"
      | "ADD_FORM"
      | "RISK_ASSESSMENT"
      | "SAFETY_TEST";
    type FormStatus =
      | "UNSAVED"
      | "DRAFT"
      | "PENDING"
      | "APPROVED"
      | "REJECTED"
      | "DELETED";
    type FormStatusAction = FormStatus | "SUPPLANTED";
    type FormUpdateType =
      | "DRAFT_SUBMITTAL"
      | "RESUBMITTAL"
      | "EDIT"
      | "COMMENT"
      | "SUPERVISOR_REASSIGNMENT"
      | "APPROVAL"
      | "REJECTION"
      | "EXTERNAL_ID"
      | "DELETE"
      | "ATTACHMENT"
      | "ARCHIVING"
      | "POSTPONE_REMINDER";
    type BudgetType =
      | "IN_BUDGET"
      | "OUT_OF_BUDGET"
      | "REPLACEMENT"
      | "TEMPORARY_REPLACEMENT"
      | "REOPENING";
    type EventType = "Comment" | "SupervisorAction" | "Edit" | "SystemEvent";
    type SearchableLdapAttribute =
      | "fullName"
      | "firstName"
      | "title"
      | "office"
      | "phone"
      | "username"
      | "email";
    type SafetyClassificationType =
      | "Explosive"
      | "Flammable"
      | "Oxidizing"
      | "Gas under Pressure"
      | "Corrosive"
      | "Acute toxicity"
      | "Health Hazard"
      | "Serious health hazard"
      | "Dangerous for the environment"
      | "Radioactive";
    type CommentSource =
      | "REGULAR COMMENT"
      | "APPROVAL COMMENT"
      | "REJECTION COMMENT"
      | "REASSIGNMENT COMMENT"
      | "EDIT COMMENT"
      | "CLONE COMMENT";
    type Department =
      | "Finance"
      | "Administration"
      | "KITS"
      | "Safety"
      | "Misc.";
    type LDAPAttribute = keyof LDAPPerson;
    type HotelPaymentType =
      | "Guest to pay at the reception"
      | "MAX IV payment"
      | "Online payment"
      | "";
    type ValidationResult = ValidationSuccess | ValidationFailure;
    type ValidationSuccess = {
      status: "SUCCESS";
    };
    type ValidationFailure = {
      status: "FAILURE";
      errorElement: JSX.Element;
    };

    interface HasId {
      _id: string;
    }
    interface selectOption {
      value: string | number;
      label?: string;
      disabled?: boolean;
    }
    interface APIError {
      title?: string;
      msg?: string;
      code: number;
      wasAborted?: boolean;
    }
    type DbType = "TEST" | "DEVELOPMENT" | "PRODUCTION";
    interface DbData {
      type: DbType;
    }

    /**Specified as a modal prop in order to render additional inputs in the modal. The value of any input defined will be accessible through the button click handlers */
    interface ModalInput {
      type: "text" | "checkbox" | "date";
      label: string;
      value: string;
    }
    interface ModalInputs {
      [id: string]: System.ModalInput;
    }
    interface ModalProps {
      open: boolean;
      title: string;
      content: JSX.Element | string;
      btn1Text: string;
      btn1Color?: "info" | "primary" | "secondary" | undefined;
      btn1OnClick?: (
        inputs: { [id: string]: System.ModalInput },
        selectedSupervisor: System.Supervisor | Undefined
      ) => void;
      btn2Text?: string;
      btn2Color?: "info" | "primary" | "secondary" | undefined;
      btn2OnClick?: (
        inputs: { [id: string]: System.ModalInput },
        selectedSupervisor: System.Supervisor | Undefined
      ) => void;
      inputs?: System.ModalInputs;
      size?: "xs" | "sm" | "md" | "lg" | "xl";
      supervisorSelector?: {
        formType: System.FormType;
        label?: string;
        alias?: string;
        allowEmptyValue: boolean;
      };
      validate?: (
        inputs: System.ModalInputs,
        supervisor: Supervisor | undefined
      ) => ValidationResult;
    }

    interface JWT {
      name: string;
      username: string;
      email: string;
      groups: string[];
    }
    interface WindowDimensions {
      width: number;
      height: number;
      xs: boolean;
      sm: boolean;
      md: boolean;
      lg: boolean;
      xl: boolean;
    }
    interface LDAPPerson {
      fullName: string;
      firstName: string;
      title: string;
      office: string;
      phone: string;
      memberOf: string[];
      username: string;
      email: string;
      jpeg: string;
      DN: string;
    }
    interface LDAPPersonUpdate {
      fullName: string;
      title: string;
      office: string;
      phone: string;
      email: string;
      DN: string;
    }

    interface ProjectsCategorized {
      [key: string]: System.Project[];
    }

    type ArchiveSearchType = "DEFAULT" | "COMMENT" | "CUSTOM_FIELD";
    interface ArchiveSearchParams {
      query: string;
      type: ArchiveSearchType;
      status: System.FormStatus | "Any";
      fromDate: Date;
      toDate: Date;
      customField: string;
      includeArchived: boolean;
    }

    interface User {
      email: string;
      name: string;
      phone: string;
      username: string;
      groups: string[];
      admin: {
        [key in System.FormType]: boolean;
      };
    }
    interface FormAccess {
      readAccess: "NONE" | "BASIC" | "DEFAULT" | "FULL";
      canEdit: boolean;
      canResolve: boolean;
      canDelete: boolean;
      isStaff: boolean;
      isSubmitter: boolean;
      isSupervisor: boolean;
      isAdmin: boolean;
    }
    interface FormConfigs {
      forms: {
        [key in System.FormType]: System.FormConfig;
      };
    }
    interface FormConfig {
      /**Specifies which users (AD username) are admins of this formType*/
      adminUsers: string[];
      /**Specifies which AD group(s) are admins of this formType*/
      adminGroups: string[];
      /**Imposes limitations on form (submission) visibility. Public is availble to all with max iv account */
      visibility: FormVisibility; //got a hunch we'll need more in the future, that's why it's not a boolean
      /**If false, entire approval flow is skipped. More details in /docs*/
      requiresApproval: boolean;
      /**If true, admins can edit submitted forms of this formtype. More details in /docs*/
      canEditAdmin: boolean;
      /**If true, supervisors can edit submitted forms of this formtype. More details in /docs*/
      canEditSupervisor: boolean;
      /**If true, submitters cannot view form details of submitted form (canRead === "BASIC") */
      hideApprovedForms: boolean;
      /**Unique id of this formType. Primary way of referring to a form programatically.*/
      formType: System.FormType;
      /** If true, approval and archive is skipped. More details in /docs*/
      isSimple: boolean;
      /**If true, enabled assigning external IDs to forms. More details in /docs*/
      hasExternalId: boolean;
      /**Name of the material ui icon of this formType (as seen on index page)*/
      icon: string;
      /**Name of this formType, appears in numerous places*/
      name: string;
      /**Short description of this formType (as seen on index page)*/
      description: string;
      /**Url fragment (but not API endpoint) used for this formType*/
      path: string;
      /**Used for formType categorization/grouping in views and menus.*/
      department: Department;
      /**The react component used to render the submittable version of this form (inputs, checkboxes etc.) */
      formComponent: (props: FormProps) => JSX.Element;
      /**An optional function performing validation on a form before final approval. Relevant for forms that are edited after initial submission */
      finalApprovalValidation?(form: Form): ValidationResult;
      /**Short description of of this form as seen in at the top of the form view.  */
      helpComponent?: string | JSX.Element;
      /**If defined, enable file upload for the form, with given string or JSX as a description */
      fileUpload?: string | JSX.Element;
      /**An optional React component containing detailed help on the formType for the end user on a separate page. If missing, link will be omitted */
      additionalHelpComponent?: string | JSX.Element;
      /**An optional submit message shown after submitting a form. Defaults to a generic submit message */
      submitMessage?: string | JSX.Element;
      /**An optional text describing the options in the approval popup. Default to general description of reassignment */
      approvalMessage?: string | JSX.Element;
      /**
       * A string that describes a submitted form. Enabled end users to easily identify a specific submission. Ideally using just 1 form field that is short and unique for each submission by that user
       */
      renderSubmissionSummary: (
        form: Form,
        singleLine: boolean
      ) => string | JSX.Element;
      /**Optional list of searchable fields unique for this formType. value refers to the mongodb collection field name*/
      customSearchFields?: { value: string; label: string }[];
      /**If false, this the form view of this component does not show up in the production environment. Archive and setting view are still available to admin though, to allow them to configure settings */
      isInProduction: boolean;
      /**If set, this component will be used on the /print page instad of the formComponent.*/
      printComponent?: (props: {form: Form}) => JSX.Element;
      customColumns?: {name: string, func: (form: Form) => JSX.Element}[];
    }
    interface ArchiveComponentProps {
      form: Form;
      open: boolean;
      children?: any;
      onFormAddedOrRemoved?: () => void;
      onFormChange: (form: Form) => void;
    }
    //Props for the specific forms
    interface FormProps {
      match: { params: string[] };
      embedded: boolean;
    }

    interface URLParamProps {
      match: { params: string[] };
    }
    /**Props for the generic form component in Form.tsx*/
    interface FormTemplateProps<T extends System.Form> {
      /**The default data for an empty form */
      emptyForm: T;
      FormInputs: (props: System.FormInputsProps<T>) => JSX.Element;
      formType: System.FormType;
      id: string;
      /** If true, this form is being displayed inline in archive or approval*/
      embedded: boolean;
      /**
       * determines if the submit button should be enabled. return "" of submittable, info msg o.w.
       */
      submittable?: (fetchedForm: T) => string;
      /**
       * Allow processing of form prior after loading an existing form from the db, e.g. setting local checkbox state
       */
      afterFormFetch?: (fetchedForm: T) => void;
      /**
       * callback once user has been properly loaded
       */
      afterUserSet?: (params: {
        formAccess: FormAccess;
        formData: T;
        user: User;
        updateFormData: (form: T) => void;
      }) => void;
      beforeFormSave?: (form: T, isDraft?: boolean) => T | null; //Allow form validation/resolution prior to the db call for create or update in submit or saveDraft. If null is returned, no further action is taken (save aborted)
    }
    /**
     * Properties provided by `Form.tsx` to the specific form implementation's `formInput`
     */
    interface FormInputsProps<T extends Form> {
      /** Ideal for updating "simple" formData properties. Will set formData.name = value, assuming that the input attribute 'name' has the value `name`. Also works with checkboxes where `checked` is used instead. */
      handleInputChange: (
        event: React.ChangeEvent<{
          name: string;
          value: string;
          type: string;
          checked?: boolean;
        }>
      ) => void;
      /** update function used when the formData state update is more complex, e.g. when an input change results in multiple formData changes, or in case of nested changes */
      updateFormData: (form: T) => void;
      formData: T;
      state: IState;
      formAccess: System.FormAccess;
      /** If true, this form is being displayed inline in archive or approval*/
      embedded: boolean;
    }
    interface PaginationInfo {
      totalDocs: number;
      page: number;
      limit: number;
    }
    interface SearchResults {
      docs: System.Form[];
      totalDocs: number;
      hasNextPage: boolean;
      hasPrevPage: boolean;
      limit: number;
      nextPage: number | null;
      page: number;
    }
    interface withId {
      _id: string;
    }
    //MODELS
    interface Counter {
      _id?: string;
      sequenceValue: number;
    }
    interface Collection {
      _id: string;
      name: string;
      form: System.FormType;
    }
    interface Supervisor extends Collection {
      projects: string[];
      email: string;
    }

    interface ListItem {
      label: string;
      value: string;
      _id?: string;
    }
    interface List extends Collection {
      items: ListItem[];
      defaultValue: string;
      description: string;
    }

    interface Project extends Collection {
      project: string;
      category: string;
    }

    interface LDAPRequest {
      searchQuery: string;
      attribute: System.LDAPAttribute;
    }
    interface FormUpdate {
      form: System.Form;
      updateType: FormUpdateType;
      disableNotify?: boolean; //if true, supervisors will not be notified even if they checked "notify me...". Currently only used for comments
      comment?: Comment;
      edit?: Edit;
      //systemEvent or supervisorAction are not needed (yet)
    }

    interface Event {
      _id?: string;
      eventType: EventType;
      timestamp: Date;
      role: EventRole;
    }

    interface SystemEvent extends Event {
      action: string;
      agent: string;
      role: string;
      description: string;
      color: string;
    }
    //unfortunately misnamed, refers to approval, resassignment and rejection (i.e. 'decision') actions by super OR admins
    //Does NOT refer to deletion, edits, ext id, additional uploads, comments or any other action that supervisors might perform
    //Correct name would have been SupervisorDecision
    interface SupervisorAction extends Event {
      action: FormStatusAction;
      decisionDate: Date | undefined;
      name: string;
      email: string;
      comment: string;
      notify: boolean;
    }
    interface Edit extends Event {
      name: string;
      email: string;
    }

    interface Comment extends Event {
      updated: Date;
      source: CommentSource;
      msg: string;
      author: string;
      status: Status;
      supervisor: string;
    }

    type ScheduledEventType = "SAFETY_TEST_REMINDER";
    type ScheduledEventStatus = "NEW" | "DONE" | "ERROR";
    type ScheduledEventPayload = SafetyTestReminderEventPayload;

    interface SafetyTestReminderEventPayload {
      userEmail: string;
    }

    interface ScheduledEvent {
      type: ScheduledEventType;
      status: ScheduledEventStatus;
      timestamp: Date;
      payload: ScheduledEventPayload;
    }

    interface SimpleForm {
      _id?: string;
      formType: string;
      emailSubject: string;
      emailRecipient: string;
      data: any;
    }
    /**A form but excluding admin fields as well as form specific fields */
    interface BasicForm {
      _id?: string;
      formType: System.FormType;
      submitterName: string;
      submitterEmail: string;
      submissionDate: Date;
      status: System.FormStatus;
      supervisorActions: SupervisorAction[];
    }

    interface Form {
      _id?: string;
      externalId?: string;
      formType: System.FormType;
      submitterName: string;
      submitterEmail: string;
      submissionDate: Date;
      files: Array<{ name: string; size: string }>;
      status: System.FormStatus;
      edits?: Edit[];
      supervisorActions: SupervisorAction[];
      comments?: Comment[];
      systemEvents?: SystemEvent[];
      postponedUntil?: Date | undefined | null;
      isArchived: boolean;
    }

    interface HotelForm extends Form {
      submitterPhoneNumber: string;
      guestName: string;
      guestEmail: string;
      guestPhoneNumber: string;
      checkInDate: Date | undefined;
      checkOutDate: Date | undefined;
      lateArrival: boolean;
      breakfast: boolean;
      paymentType: HotelPaymentType;
      activityNumber: string;
      additionalMsg: string;
    }

    interface TravelForm extends Form {
      project: string;
      destination: string;
      travelPurpose: string;
      startDate: Date | undefined;
      endDate: Date | undefined;
      expenses: {
        travel: number;
        accommodation: number;
        conferenceFee: number;
        subsistence: number;
      };
      totalExpenses: number;
    }
    interface PurchaseForm extends Form {
      project: string;
      motivation: string;
      bidder1: string;
      bidder2: string;
      bidder3: string;
      supplierName: string;
      frameworkAgrmt: boolean;
      description: string;
      product: string;
      comment: string;
      amount: number;
      environment: string;
    }
    interface EmploymentRequest extends Form {
      position: string;
      group: string;
      team: string;
      employmentType: "Permanent" | "Temporary" | "";
      extent: number;
      estStartDate: Date | undefined;
      estEndDate: Date | undefined;
      estMonthlySalary: number;
      contactedOfficeCoordinator: boolean;
      rejectionAction: string;
      budgetType: BudgetType;
      budgetTypeDetail: string;
      budgetTypeDetail2: string;
      externallyFundedProject: string;
      externallyFundedPercentage: number;
      primulaGroup: string;
    }
    interface GrantInitiativeForm extends Form {
      responsible: string;
      coordinator: string;
      otherParties: string;
      collaborators: string;
      fundingAgency: string;
      projectName: string;
      projectDescription: string;
      beamlineAccessType: string;
      officeSpaceCount: number;
      pooledResource: string;
      ownership: "Yes" | "No" | "N/A";
      otherResources: string;
      strategyAlignment: string;
      startingYear: number;
      people: {
        firstName: string;
        lastName: string;
        inKind: boolean;
        years: { salaryPercentage: number; nbrMonths: number }[];
      }[];
      funding: {
        salaries: number[];
        investments: number[];
        travel: number[];
        others: number[];
        indirectCost: number[];
      };
      cofunding: {
        salaries: number[];
        others: number[];
        indirectCost: number[];
      };
    }
    interface AdditionalFundingForm extends Form {
      responsibleDirector: string;
      applicant: string;
      organizationalUnitType: "Beamline" | "Group";
      organizationalUnitName: string;
      totalAmountRequested: number;
      investmentDescription: string;
      investmentBackground: string;
      contingencyAmountRemaining: number;
      totalAmountApproved: number;
      fundingSource:
        | ""
        | "Upkeep"
        | "DM Ex contingency"
        | "DM Ex contingency (out of budget)";
    }
    interface CateringRequest extends Form {
      deliveryPoint: string;
      deliveryDate: Date | undefined;
      nameOfTheMeeting: string; // No longer used, expect for old forms
      purpose: string;
      activity: string;
      orders: {
        deliveryTime: Date | null;
        servingType: string;
        quantity: number;
      }[];
      otherRequests: string;
      participants: string;
    }
    interface ChemicalSample {
      name: string;
      cas: string;
      aggregationState:
        | "Solid/Powder"
        | "Solid/Piece"
        | "Single"
        | "Liquid"
        | "Gas";
      quantity: string;
      safetyClassifications: SafetyClassificationType[];
    }
    interface NanoSample {
      name: string;
      source: "Commercial" | "Natural" | "Other";
      sourceDetail: string;
      quantity: string;
      aggregationState:
        | "Powder"
        | "Aerosol"
        | "In Suspension/Liquid"
        | "On Solid Matrix";
      sizeDistribution: string;
      length: string;
      density: string;
      class: 0 | 1 | 2 | 3;
    }
    interface BioSample {
      category:
        | "Proteins"
        | "Lipids"
        | "Antibodies"
        | "Bacteria"
        | "Viral components"
        | "Mammalian cells"
        | "Human or animal tissue"
        | "Other";
      name: string;
      source: "Commercial" | "Laboratory made" | "Animal";
      sourceDetail: string;
      aggregationState: string;
      quantity: string;
    }
    interface RiskEquipment {
      mitigations: string;
      checks: string[];
    }
    type RiskType =
      | "vessles"
      | "heater"
      | "cryostat"
      | "magnetic"
      | "electrochemical"
      | "laser"
      | "lamps"
      | "sonic"
      | "blowtorch"
      | "ribbon";
    type ExperimentDetailLocation = "lab" | "beamline" | "other";
    interface ExperimentDetail {
      details: string;
      risks: string;
      mitigation: string;
      emergencyAction: string;
    }
    interface RiskAssessmentForm extends Form {
      proposalId: string;
      title: string;
      safetyClassification: string;
      hasDates: boolean;
      startDate?: Date;
      endDate?: Date;
      beamline: string;
      contactName: string;
      contactEmail: string;
      contactPhone: string;
      proposerName: string;
      proposerEmail: string;
      proposerPhone: string;
      PIName: string;
      PIEmail: string;
      PIPhone: string;
      companyName: string;
      significantModifications: string;
      sampleRemoved: "MAX IV" | "User";
      labAccessRequired: boolean;
      customBuiltEquipmentDescription: string;
      additionalChemicalProperties: string;
      additionalNanoProperties: string;
      equipments: {
        [key in RiskType]: RiskEquipment;
      };
      chemicalSamples: ChemicalSample[];
      nanoSamples: NanoSample[];
      bioSamples: BioSample[];
      experimentDetails: {
        [key in ExperimentDetailLocation]: ExperimentDetail;
      };
      hasSignificantModifications: boolean;
      hasHazardousEquipment: boolean;
      hasOwnEquipment: boolean;
      hasChemicalSamples: boolean;
      hasNanoSamples: boolean;
      hasBioSamples: boolean;
      hasCMRCompound: boolean;
    }

    type SafetyTestAnswerCategory = "BASIC" | "RADIATION" | "CHEMICAL" | "BIO";
    interface SafetyTestFormAnswer {
      category: SafetyTestAnswerCategory;
      qId: string;
      aId: string;
    }
    interface SafetyTestForm extends Form {
      employer: string;
      contactPerson: string;
      moreThanTwoMonths: boolean;
      phoneNumber: string;
      signature: boolean;
      answers: SafetyTestFormAnswer[];
      hasRadiation: boolean;
      hasChemical: boolean;
      hasBio: boolean;
    }
  }
}
