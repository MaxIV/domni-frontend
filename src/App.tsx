import { useEffect } from "react";
import Cookies from "universal-cookie";
import { useStore } from "./store/Store";
import { BrowserRouter, Route, RouteComponentProps } from "react-router-dom";
import Header from "./components/Header";
import Settings from "./pages/Settings";
import Approval from "./pages/Approval";
import Submit from "./pages/Submit";
import { isAdmin, isInProduction } from "./utilities/Helpers";
import LandingPage from "./pages/LandingPage";
import {
  ThemeProvider,
  createTheme,
  StyledEngineProvider,
} from "@mui/material/styles";

import {
  Modal,
  getModalAPIErrorDispatch,
  getEmptyModal,
} from "./components/Modal";
import {
  getFormConfig,
  authURL,
  loginURL,
  getFormConfigs,
  getEmptyFormTypeObject,
  getFormTypes,
  COOKIE,
} from "./utilities/Config";
import Archive from "./pages/Archive";
import Help from "./pages/Help";
import Docs from "./pages/Docs";
import { getCollection } from "./api/collection";
import Print from "./pages/Print";
import Profile from "./pages/Profile";
import { getSupervisorCases } from "./api/forms";
import People from "./pages/People";
import { QuickNav } from "./components/QuickNav";
import { getDbData } from "./api/util";

const themeMUI = createTheme({
  palette: {
    primary: {
      light: "#82be00",
      main: "#6c9e00",
      dark: "#557c00",
      contrastText: "#fff",
    },
    secondary: {
      light: "#fea901",
      main: "#de9300",
      dark: "#bf7f00",
      contrastText: "#fff",
    },
    info: {
      light: "#bbb",
      main: "#999",
      dark: "#777",
      contrastText: "#fff",
    },
  },
  components: {
    //.css-4j1lg5-MuiPaper-root-MuiAppBar-root
    //Header background color which defaults to green
    MuiAppBar: {
      styleOverrides: {
        root: { backgroundColor: "#011428" },
      },
    },
    //increase max width of entire app container
    MuiContainer: {
      styleOverrides: {
        root: {
          "@media (min-width: 0px)": {
            maxWidth: "1400px",
          },
        },
      },
    },
    // .css-9v9idb-MuiButtonBase-root-MuiCheckbox-root.Mui-checked, .css-9v9idb-MuiButtonBase-root-MuiCheckbox-root.MuiCheckbox-indeterminate
    //yellow checkboxes
    MuiCheckbox: {
      styleOverrides: {
        root: {
          "&.Mui-checked": {
            color: "#de9300",
          },
        },
      },
    },
    //yellow radiobutotns
    MuiRadio: {
      styleOverrides: {
        root: {
          color: "#de9300",
          "&.Mui-checked": {
            color: "#de9300",
          },
        },
      },
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          padding: "0.3em",
          borderBottom: "1px solid #f5f5f5",
        },
        stickyHeader: {
          backgroundColor: "#eee",
          fontWeight: "bold",
          color: "#777",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          boxShadow: "none",
          textTransform: "none",
          minWidth: "auto",
          "&.link": { color: "#de9300" },
          "&:hover": { boxShadow: "none" },
        },
      },
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          fontSize: "1em",
          backgroundColor: "#011428",
        },
        arrow: {
          color: "#333",
        },
      },
    },
    //custom chips in settings
    MuiChip: {
      styleOverrides: {
        root: {
          borderRadius: "4px",
          backgroundColor: "rgb(220 169 66 / 20%)",
          color: "rgb(145 96 0)",
          height: "28px",
          margin: "0 2px",
        },

        deletable: {
          "&:focus": {
            backgroundColor: "rgb(220 169 66 / 20%)",
          },
        },
        deleteIcon: {
          color: "rgb(145 96 0 / 34%)",
          "&:hover": {
            color: "rgb(145 96 0)",
          },
          width: 18,
          height: 18,
        },
      },
    },

    //hr dividing title form body in modal
    MuiDialogTitle: {
      styleOverrides: {
        root: {
          padding: "0.5em 1em",
          borderBottom: "1px solid #eee",
          marginBottom: "1em",
        },
      },
    },
    //.css-iy580e-MuiPaper-root-MuiDrawer-paper {
    //override background color in low-res menu
    MuiDrawer: {
      styleOverrides: {
        paper: {
          background: "#011428",
        },
      },
    },

    MuiSelect: {
      styleOverrides: {
        select: {
          color: "black",
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          "&focus": { color: "#82be00" },
        },
      },
    },
    MuiListItem: {
      styleOverrides: {
        root: {
          "&.Mui-selected": { backgroundColor: "rgba(254, 169, 1, 0.2)" },
          "&.Mui-selected, &.Mui-selected:hover": {
            backgroundColor: "rgba(254, 169, 1, 0.2)",
          },
        },
        button: {
          transition: "background-color 0.2s",
          "&:hover": {
            textDecoration: "none",
            backgroundColor: "rgba(254, 169, 1, 0.1)",
          },
        },
      },
    },
  },
});

const App = () => {
  const { dispatch } = useStore();
  //register global key listener
  useEffect(() => {
    const globalListener = (e: KeyboardEvent) => {
      if (document.activeElement === document.body) {
        if (e.ctrlKey && e.key === "q") {
          dispatch({
            type: "SET_MODAL_PROPS",
            modalProps: {
              open: true,
              title: "Quick Nav",
              content: QuickNav({
                onLogout,
                onClose: () =>
                  dispatch({
                    type: "SET_MODAL_PROPS",
                    modalProps: getEmptyModal(),
                  }),
              }),
              btn1Text: "Close",
              btn1Color: "info",
              size: "sm",
            },
          });
        }
      }
    };

    document.addEventListener("keydown", globalListener, false);
    // eslint-disable-next-line
  }, []);
  //loading supervisors and projects. The global state 'loading' will remain true until this is completed
  useEffect(() => {
    const ac = new AbortController();
    const populateLists = async () => {
      try {
        const supervisorsData = (await getCollection(
          undefined,
          "SUPERVISOR",
          ac.signal
        )) as System.Supervisor[];
        const projectsData = (await getCollection(
          undefined,
          "PROJECT",
          ac.signal
        )) as System.Project[];
        const listsData = (await getCollection(
          undefined,
          "LIST",
          ac.signal
        )) as System.List[];
        const supervisors: {
          [key in System.FormType]: System.Supervisor[];
        } = getEmptyFormTypeObject<System.Supervisor[]>([]);
        supervisorsData.forEach((s) => {
          try {
            supervisors[s.form].push(s);
          } catch (err) {
            console.log("Failed to add supervisor", s);
          }
        });
        const projects: {
          [key in System.FormType]: System.Project[];
        } = getEmptyFormTypeObject<System.Project[]>([]);
        projectsData.forEach((p) => {
          try {
            projects[p.form].push(p);
          } catch (err) {
            console.log("Failed to add project", p);
          }
        });
        const lists: {
          [key in System.FormType]: System.List[];
        } = getEmptyFormTypeObject<System.List[]>([]);
        listsData.forEach((l) => {
          try {
            lists[l.form].push(l);
          } catch (err) {
            console.log("Failed to add list", l);
          }
        });
        const pendingSupervisorCases = await getSupervisorCases({
          status: "PENDING",
        });
        const dbData = await getDbData();
        dispatch({
          type: "SET_CACHE",
          projects,
          supervisors,
          lists,
          pendingSupervisorCases,
          dbData,
        });
      } catch (err) {
        dispatch(getModalAPIErrorDispatch(err as System.APIError));
      }
    };
    populateLists();
    return () => {
      ac.abort();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //on mount, parsing the jwt token. If missing or invalid, redirect to login. If valid, set global user state, including admin rights for the user
  useEffect(() => {
    const cookies = new Cookies();
    let jwt = cookies.get(COOKIE);

    async function handleJwt() {
      if (jwt) {
        const decoded = await decode(jwt);
        if (decoded !== false) {
          const admin = getEmptyFormTypeObject<boolean>(true, {
            excludeSimple: false,
          });
          getFormTypes().forEach((formType) => {
            admin[formType] = isAdmin(
              decoded.groups,
              decoded.username,
              getFormConfig(formType).formType
            );
          });
          dispatch({
            type: "SET_USER",
            user: {
              email: decoded.email,
              name: decoded.name,
              phone: decoded.phone,
              username: decoded.username,
              groups: decoded.groups,
              admin,
            },
          });
        } else {
          window.location.replace(
            `${loginURL}?redirect=${window.location.href}`
          );
        }
      } else {
        window.location.replace(`${loginURL}?redirect=${window.location.href}`);
      }
    }
    handleJwt();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const decode = async (jwtToken: any) => {
    try {
      const resp = await fetch(authURL, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ jwt: jwtToken }),
      });
      if (!resp.ok) {
        return false;
      }
      return await resp.json();
    } catch (err) {
      return false;
    }
  };
  const onLogout = () => {
    const cookies = new Cookies();
    cookies.remove(COOKIE, { path: "/", domain: ".maxiv.lu.se" });
    window.location.replace(`${loginURL}?redirect=${window.location.href}`);
  };
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={themeMUI}>
        <BrowserRouter>
          {/* Our single instance modal. Visibility and content determined by the global state MODAL_PROPS */}
          <Modal />
          <Route
            path="/"
            render={(props: RouteComponentProps) => (
              <Header onLogout={onLogout} {...props} />
            )}
          />
          <Route exact path="/" component={LandingPage} />
          {getFormConfigs({ excludeNonProd: isInProduction() }).map(
            (config) => {
              return (
                <Route
                  key={config.formType}
                  exact
                  path={`/${config.path}/form/*`}
                  render={(props: RouteComponentProps<string[]>) => (
                    <config.formComponent {...props} embedded={false} />
                  )}
                ></Route>
              );
            }
          )}
          <Route
            exact
            path="/*/settings/*"
            render={(props: RouteComponentProps<string[]>) => (
              <Settings {...props} />
            )}
          />
          <Route
            exact
            path="/*/archive/*"
            render={(props: RouteComponentProps<string[]>) => (
              <Archive {...props} />
            )}
          />
          <Route
            exact
            path={`/*/approval/*`}
            render={(props: RouteComponentProps<string[]>) => (
              <Approval {...props} />
            )}
          />
          <Route
            exact
            path={`/*/print/*`}
            render={(props: RouteComponentProps<string[]>) => (
              <Print {...props} />
            )}
          />
          <Route
            exact
            path={`/profile/*`}
            render={(props: RouteComponentProps<string[]>) => (
              <Profile {...props} />
            )}
          />
          <Route
            exact
            path={`/people*`}
            render={(props: RouteComponentProps<string[]>) => (
              <People {...props} />
            )}
          />
          <Route
            exact
            path={"/*/help/*"}
            render={(props: RouteComponentProps<string[]>) => (
              <Help {...props} />
            )}
          />
          <Route
            exact
            path={"/docs*"}
            render={(props: RouteComponentProps<string[]>) => (
              <Docs {...props} />
            )}
          />
          <Route
            path="/*/submit/*"
            render={(props: RouteComponentProps<string[]>) => (
              <Submit {...props} />
            )}
          />
        </BrowserRouter>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default App;
