import React from "react";

// FORMTYPES
export const PURCHASE: System.FormType = "PURCHASE_APPLICATION";
export const TRAVEL: System.FormType = "TRAVEL";
export const EMPLOYMENT: System.FormType = "EMPLOYMENT_REQUEST";
export const HOTEL: System.FormType = "HOTEL_BOOKING";
export const GRANT_INITIATIVE: System.FormType = "GRANT_INITIATIVE";
export const ADDITIONAL_FUNDING: System.FormType = "ADDITIONAL_FUNDING";
export const CATERING: System.FormType = "CATERING_REQUEST";
export const ADD_FORM: System.FormType = "ADD_FORM";
export const RISK_ASSESSMENT: System.FormType = "RISK_ASSESSMENT";
export const SAFETY_TEST: System.FormType = "SAFETY_TEST";

export const AD_GROUP_EST = "EST";
export const AD_GROUP_FINANCE = "Ekonomi";
export const AD_GROUP_ADMIN = "Administration";
export const AD_GROUP_IMS = "Information Management";
export const AD_GROUP_AD_ADMIN = "account-admins";
export const AD_GROUP_RADIATION_SAFETY = "Rad";

// SUBMIT MESSAGES
export const PURCHASE_SUBMIT_MSG = `You will shortly receive an email confirming that your purchase application has been submitted, where the PO number is included. An email has also been sent to the designated supervisor. You will recieve an additional email notification once they have made a decision to either approve or deny your purchase application. If your purchase application is approved you then can place the order and give the PO number to the supplier if required.`;

export const TRAVEL_SUBMIT_MSG = `You will shortly receive an email confirming that your travel approval request has been submitted. An email has also been sent to the designated supervisor. You will recieve an additional email notification once they have made a decision to either approve or deny your travel approval request.`;
export const EMPLOYMENT_SUBMIT_MSG = `You will shorty receive an email confirming that the employment request has been submitted. An email has also been sent to the designated supervisor. You will recieve an additional email notification once they have made a decision to either approve or deny your employment request.`;
export const HOTEL_SUBMIT_MSG = `Your submission has been registered. You will receive an email once your booking has been confirmed.`;
export const CATERING_SUBMIT_MSG = `Your submission has been registered.`;
export const ADD_FORM_MSG = `Thank you for your submission! We'll get back to you.`;
export const SAFETY_TEST_MSG: JSX.Element = (
  <>
    <div>
      Your Safety Test was correct and has been submitted. You will be notified
      by email as soon as the MAX IV Reception has handled the test and
      activated/updated your MAX ID card access rights.
    </div>
    <div style={{ marginTop: "1em" }}>
      Ditt säkerhetsprov var korrekt och har skickats in. Du kommer att meddelas
      via epost när receptionen på MAX IV har hanterat provet och
      aktiverat/förnyat behörigheterna på ditt tillträdeskort.
    </div>
  </>
);

// HELP COMPONENTS
export const PURCHASE_HELP_COMPONENT = (
  <>
    Additional information about purchasing and procurement can be found{" "}
    <a
      style={{ margin: "0" }}
      className="link"
      target="_blank"
      rel="noopener noreferrer"
      href="https://intranet.maxiv.lu.se/administration/procurement/"
    >
      on the intranet
    </a>
    {". "}
  </>
);

export const TRAVEL_HELP_COMPONENT = (
  <>
    Additional information about travel approval can be found{" "}
    <a
      style={{ margin: "0" }}
      className="link"
      target="_blank"
      rel="noopener noreferrer"
      href="https://intranet.maxiv.lu.se/administration/travel/travel-approval/"
    >
      on the intranet
    </a>
    .{" "}
  </>
);
export const EMPLOYMENT_HELP_COMPONENT = (
  <>
    For a quick guide visit the recruitment page on{" "}
    <a
      className="link"
      rel="noopener noreferrer"
      target="_blank"
      href="https://intranet.maxiv.lu.se/hr/recruitment-process/recruitment/"
    >
      the intranet
    </a>
    . Contact{" "}
    <a
      className="link"
      rel="noopener noreferrer"
      target="_blank"
      href="mailto:finance@maxiv.lu.se"
    >
      the MAX IV finance department
    </a>{" "}
    for more information.
  </>
);

// GENERIC MUI TOOLTIPS
export const EMAIL_TOOLTIP =
  "Your email address. This is where you will be notified when the application is approved or rejected.";
export const BUDGET_RESPONSIBLE_TOOLTIP =
  "Select the email of the person responsible for the budget. Autocompletes when budget is selected. If a person is missing, please contact the page manager.";
export const BUDGET_TOOLTIP =
  'If this is unclear or if the purchase belongs to an external project, please select "Other,\n specify" state this below, under "Comment on the budget".';
export const BUDGET_COMMENT_TOOLTIP =
  "You must specify what budget you are using for this purchase. It can for example be a post in an operations budget or budget for externally funded project. Add activity number or describe in words what budget you will use.";
export const EDIT_MODE_TOOLTIP =
  "You are currently editing an already submitted form. Clicking 'Save' will update the form with your changes, and log that you've made an edit.";
