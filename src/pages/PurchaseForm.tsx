import {
  Checkbox,
  FormControl,
  FormControlLabel,
  Grid,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React from "react";
import FrameWorkAgreement from "../components/FrameWorkAgreement";
import { ProjectSelect } from "../components/ProjectSelect";
import { SupervisorSelect } from "../components/SupervisorSelect";
import { DomniTextValidator } from "../components/TextValidator";
import { SimpleSelect } from "../components/SimpleSelect";
import * as Constants from "../constants";
import { PURCHASE } from "../constants";
import { selectSupervisor } from "../store/Reducer";
import Form from "./Form";

const emptyPurchaseForm = (): System.PurchaseForm => {
  return {
    _id: "",
    formType: PURCHASE,
    submitterName: "",
    submitterEmail: "",
    status: "UNSAVED",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name: "",
        email: "",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    submissionDate: new Date(),
    isArchived: false,
    environment: "No",
    project: "",
    motivation: "",
    bidder1: "",
    bidder2: "",
    bidder3: "",
    supplierName: "",
    frameworkAgrmt: false,
    description: "",
    product: "",
    comment: "",
    amount: 0,
  };
};
const PurchaseForm = (props: System.FormProps) => {
  const FormInputs = (props: System.FormInputsProps<System.PurchaseForm>) => {
    const { formData, updateFormData, handleInputChange, state, formAccess } =
      props;
    return (
      <>
        <Grid item xs={12}>
          <Typography className="form-heading">Budget approval</Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip title={Constants.BUDGET_TOOLTIP} placement="top-start">
            <FormControl variant="outlined" fullWidth={true}>
              <ProjectSelect
                required={true}
                formType="PURCHASE_APPLICATION"
                selectedProject={formData.project}
                onChange={(project: System.Project) => {
                  const supervisor = selectSupervisor(
                    state,
                    project.project,
                    "PURCHASE_APPLICATION"
                  );

                  if (supervisor) {
                    updateFormData({
                      ...formData,
                      project: project.project,
                      supervisorActions: [
                        {
                          ...formData.supervisorActions[0],
                          name: supervisor.name,
                          email: supervisor.email,
                        },
                      ],
                    });
                  } else {
                    updateFormData({
                      ...formData,
                      project: project.project,
                    });
                  }
                }}
              />
            </FormControl>
          </Tooltip>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip
            title={Constants.BUDGET_RESPONSIBLE_TOOLTIP}
            placement="top-start"
          >
            <FormControl variant="outlined" fullWidth={true}>
              <SupervisorSelect
                formType="PURCHASE_APPLICATION"
                selectedEmail={formData.supervisorActions[0].email}
                onChange={(supervisor) =>
                  updateFormData({
                    ...formData,
                    supervisorActions: [
                      {
                        ...formData.supervisorActions[0],
                        name: supervisor.name,
                        email: supervisor.email,
                      },
                    ],
                  })
                }
              />
            </FormControl>
          </Tooltip>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip
            title={Constants.BUDGET_COMMENT_TOOLTIP}
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              label="Comment on the budget"
              onChange={handleInputChange}
              name="comment"
              value={formData.comment}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Product information</Typography>
        </Grid>
        <Grid item sm={6} xs={12}>
          <Tooltip
            title="Enter the name of the product, for example 'Magnet'. A more detailed description can be entered below."
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              label="Product"
              onChange={handleInputChange}
              name="product"
              value={formData.product}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Tooltip
            title="Enter the estimated value of the product in SEK. "
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              onFocus={(event) => (event.target as any).select()}
              type="number"
              label="Estimated value (SEK)"
              onChange={handleInputChange}
              name="amount"
              value={formData.amount}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12}>
          <Tooltip
            title="Specify the description of the product with more detailed information such as quantity, model, size etc."
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              label="Description of product"
              onChange={handleInputChange}
              name="description"
              value={formData.description}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
              multiline={true}
              rows={props.embedded ? undefined : 3}
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Sustainability</Typography>
        </Grid>
        <Grid item xs={12}>
          <div>
            It is mandatory to state whether the environmental requirements have been 
            set during procurement. For more information, refer to the sustainability 
            plan for Lund University (in Swedish): <a target="_blank" href="https://www.medarbetarwebben.lu.se/sites/medarbetarwebben.lu.se/files/2023-02/Hallbarhetsplan-2020-2026.pdf">https://www.medarbetarwebben.lu.se/sites/medarbetarwebben.lu.se/files/2023-02/Hallbarhetsplan-2020-2026.pdf</a>
          </div>
        </Grid>
          <Tooltip
            title="Environmental requirements have been set during procurement"
            placement="top-start"
          >
            <Grid item xs={12} md={4}>
              <SimpleSelect
                options={[{ value: "No" }, { value: "Yes" }]}
                selectedValue={formData.environment}
                testid="environment-select"
                title="Environmental requirements set"
                name="environment"
                onChange={handleInputChange}
              />
            </Grid>
          </Tooltip>
        <Grid item xs={12}>
          <Typography className="form-heading">Framework Agreement</Typography>
        </Grid>
        <Grid item xs={12}>
          <Tooltip
            title="Check the box if we already have a framework agreement with the supplier. If we do,
                                    then you can make the purchase in Lupin/Proceedo. If we do not have any agreement with
                                    the supplier then you must look up 3 suppliers who are qualified for the purchase and
                                    fill them in below."
            placement="top-start"
          >
            <FormControlLabel
              className="link"
              control={
                <Checkbox
                  disabled={!formAccess.canEdit}
                  size="medium"
                  checked={formData.frameworkAgrmt}
                  value={formData.frameworkAgrmt}
                  onChange={() =>
                    updateFormData({
                      ...formData,
                      frameworkAgrmt: !formData.frameworkAgrmt,
                    })
                  }
                  name="framework"
                />
              }
              label="There is a pre-existing framework agreement"
            />
          </Tooltip>
        </Grid>
        <Grid item>
          {
            <FrameWorkAgreement
              show={!formData.frameworkAgrmt}
              onChange={handleInputChange}
              supplier={formData.supplierName}
              bidder1={formData.bidder1}
              bidder2={formData.bidder2}
              bidder3={formData.bidder3}
              motivation={formData.motivation}
            />
          }
        </Grid>
      </>
    );
  };
  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={emptyPurchaseForm()}
      formType="PURCHASE_APPLICATION"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
};

export default PurchaseForm;
