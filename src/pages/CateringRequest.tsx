import {
  Autocomplete,
  Box,
  FormControl,
  Grid,
  TextField,
  Tooltip,
} from "@mui/material";
import { SelectValidator } from "react-material-ui-form-validator";
import { DatePicker, DomniTimePicker } from "../components/DateWidgets";
import { DynamicInputTable } from "../components/DynamicInputTable";
import { SimpleSelect } from "../components/SimpleSelect";
import { CATERING } from "../constants";
import { selectList, selectSupervisor } from "../store/Reducer";
import { useStore } from "../store/Store";
import Form from "./Form";

const deliveryPoints = ["E-building dining hall", "D-building dining hall", "MAX IV Cube"];
const getEmptyCateringRequest = (
  state: System.IState
): System.CateringRequest => {
  return {
    _id: "",
    formType: CATERING,
    submitterName: "",
    submitterEmail: "",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name:
          selectSupervisor(state, "Default", CATERING)?.name ||
          "MAX IV Reception",
        email:
          selectSupervisor(state, "Default", CATERING)?.email ||
          "reception@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    status: "UNSAVED",
    submissionDate: new Date(),
    isArchived: false,
    deliveryDate: new Date(),
    deliveryPoint: "",
    nameOfTheMeeting: "",
    purpose: "",
    otherRequests: "",
    activity: "",
    participants: "",
    orders: [],
  };
};
function CateringRequest(props: System.FormProps) {
  const { state: initialState } = useStore();
  const purposeList = selectList({
    state: initialState,
    form: "CATERING_REQUEST",
    name: "Purpose",
    defaultValues: ["No purpose has been defined"],
  });
  const servingTypeList = selectList({
    state: initialState,
    form: "CATERING_REQUEST",
    name: "ServingType",
    defaultValues: ["No serving type has been defined yet"],
  });
  const FormInputs = (
    props: System.FormInputsProps<System.CateringRequest>
  ) => {
    const {
      formData,
      formAccess,
      updateFormData,
      embedded,
      handleInputChange,
    } = props;

    const isServingTypesUsed = (servingType: string) => {
      return formData.orders
        .map((order) => order.servingType)
        .includes(servingType);
    };
    const getAvailableServingTypes = () => {
      return servingTypeList.items.filter(
        (item) => !isServingTypesUsed(item.value)
      );
    };
    const backwardCompatibleFix = () => {
      if (!formData._id || formData._id >= "10015") {
        return null;
      }
      const oldData = formData as any;
      return (
        <>
          <Grid item xs={12}>
            <Box
              className="highlight-background"
              sx={{ fontStyle: "italic", padding: "1em" }}
            >
              This form was submitted as the old version of Catering Request,
              which had different fields than the current version. What follows
              is a raw dump of the incompatible data:
            </Box>
          </Grid>
          {oldData.morning && (
            <Grid item xs={12} md={4}>
              <Box sx={{ fontWeight: "bold" }}>Morning</Box>
              <div>Serving type: {oldData.morning.servingType}</div>
              <div>Quantity: {oldData.morning.quantity}</div>
              <div>Serving time:{oldData.morning.servingTime}</div>
              <div>Other requests: {oldData.morning.otherRequests}</div>
            </Grid>
          )}
          {oldData.lunch && (
            <Grid item xs={12} md={4}>
              <Box sx={{ fontWeight: "bold" }}>Lunch</Box>
              {oldData.lunch.items.map((item: any) => {
                return (
                  <Box>
                    <div>Serving type: {item.servingType}</div>
                    <div>Quantity: {item.quantity}</div>
                    <div>Other requests: {item.otherRequests}</div>
                  </Box>
                );
              })}
            </Grid>
          )}
          {oldData.afternoon && (
            <Grid item xs={12} md={4}>
              <Box sx={{ fontWeight: "bold" }}>Afternoon</Box>
              <div>Serving type: {oldData.afternoon.servingType}</div>
              <div>Quantity: {oldData.afternoon.quantity}</div>
              <div>Serving time:{oldData.afternoon.servingTime}</div>
              <div>Other requests: {oldData.afternoon.otherRequests}</div>
            </Grid>
          )}
        </>
      );
    };
    return (
      <>
        <Grid item xs={12} className="form-heading">
          Delivery Details
        </Grid>
        <Grid item md={3} sm={6} xs={12}>
          <DatePicker
            disabled={!formAccess.canEdit}
            start={{
              label: "Delivery date",
              name: "DeliveryTime",
              date: formData.deliveryDate,
              setDate: (date: Date | undefined) =>
                updateFormData({ ...formData, deliveryDate: date }),
            }}
            singleInputFullWidth={true}
          />
        </Grid>
        <Grid item md={3} sm={6} xs={12}>
          <FormControl variant="outlined" fullWidth={true}>
            <SelectValidator
              name="deliveryPoint"
              variant="outlined"
              fullWidth
              onChange={handleInputChange}
              value={formData.deliveryPoint}
              SelectProps={{
                native: true,
              }}
              validators={["required"]}
              errorMessages={["This field is required"]}
            >
              <option value="" disabled>
                Delivery Point
              </option>
              {deliveryPoints.map((point, idx) => (
                <option key={idx}>{point}</option>
              ))}
            </SelectValidator>
          </FormControl>
        </Grid>
        <Grid item md={3} sm={6} xs={12}>
          <Tooltip
            title="Select a predefined purpose or specify one of you own."
            placement="top-start"
          >
            <Autocomplete
              freeSolo
              clearOnBlur
              autoSelect
              autoHighlight
              disabled={!formAccess.canEdit}
              value={formData.purpose}
              onChange={(_, selected) => {
                let purpose: string = "";
                if (typeof selected === "string") {
                  purpose = selected;
                } else {
                  purpose = selected?.value || "";
                }
                updateFormData({
                  ...formData,
                  purpose,
                });
              }}
              options={purposeList.items}
              renderInput={(params) => (
                <TextField
                  {...params}
                  name="purpose-autocomplete"
                  label="Purpose of the meeting"
                />
              )}
            />
          </Tooltip>
        </Grid>
        <Grid item md={3} sm={6} xs={12}>
          <Tooltip
            title="The activity is used for the payment."
            placement="top-start"
          >
            <TextField
              fullWidth={true}
              name="activity"
              label="Activity"
              value={formData.activity}
              onChange={(event) =>
                updateFormData({ ...formData, activity: event.target.value })
              }
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} className="form-heading">
          Orders
        </Grid>

        {backwardCompatibleFix()}
        <Grid item xs={12}>
          <div>
            <DynamicInputTable
              disabled={!formAccess.canEdit || embedded}
              emptyMessage="No order has been added yet"
              addRowText="Add order"
              compactMode={false}
              canAddRow={getAvailableServingTypes().length > 0}
              deleteRowText="Delete this order"
              onAddRow={() =>
                updateFormData({
                  ...formData,
                  orders: [
                    ...formData.orders,
                    {
                      deliveryTime: null,
                      quantity: 0,
                      servingType: getAvailableServingTypes()[0].value,
                    },
                  ],
                })
              }
              onDeleteRow={(rowIndex) =>
                updateFormData({
                  ...formData,
                  orders: formData.orders.filter(
                    (order, index) => index !== rowIndex
                  ),
                })
              }
              data={formData.orders}
              columnOrder={["servingType", "deliveryTime", "quantity"]}
              columnDefinitions={{
                servingType: {
                  title: "Serving type",
                  Component: (props: { rowIndex: number }) => (
                    <SimpleSelect
                      variant="outlined"
                      testid={`servingType-${props.rowIndex}-select`}
                      name={`servingType-row-${props.rowIndex}`}
                      disabled={{
                        value: !formAccess.canEdit || embedded,
                      }}
                      selectedValue={
                        formData.orders[props.rowIndex].servingType
                      }
                      onChange={(event) => {
                        const newOrders = formData.orders.map(
                          (order, index) => {
                            if (index === props.rowIndex) {
                              return {
                                ...order,
                                servingType: event.target.value,
                              };
                            } else {
                              return order;
                            }
                          }
                        );
                        updateFormData({ ...formData, orders: newOrders });
                      }}
                      options={servingTypeList.items.map((item) => {
                        return {
                          ...item,
                          disabled: isServingTypesUsed(item.value),
                        };
                      })}
                    />
                  ),
                },
                deliveryTime: {
                  title: "Delivery time",
                  Component: (props: { rowIndex: number }) => (
                    <DomniTimePicker
                      disabled={!formAccess.canEdit || embedded}
                      testId={`deliveryTime-${props.rowIndex}-timePicker`}
                      label="Delivery time"
                      value={formData.orders[props.rowIndex].deliveryTime}
                      onChange={(date) => {
                        const newOrders = formData.orders.map(
                          (order, index) => {
                            if (index === props.rowIndex) {
                              return { ...order, deliveryTime: date };
                            } else {
                              return order;
                            }
                          }
                        );
                        updateFormData({ ...formData, orders: newOrders });
                      }}
                    />
                  ),
                },
                quantity: {
                  title: "Quantity",
                  Component: (props: { rowIndex: number }) => (
                    <TextField
                      fullWidth
                      data-testid={`quantity-${props.rowIndex}-input`}
                      onFocus={(event) => event.target.select()}
                      type="number"
                      variant="outlined"
                      name={`quantity-row-${props.rowIndex}`}
                      value={formData.orders[props.rowIndex].quantity}
                      onChange={(event) => {
                        const newOrders = formData.orders.map(
                          (order, index) => {
                            if (index === props.rowIndex) {
                              return {
                                ...order,
                                quantity: parseInt(event.target.value),
                              };
                            } else {
                              return order;
                            }
                          }
                        );
                        updateFormData({ ...formData, orders: newOrders });
                      }}
                    />
                  ),
                },
              }}
            />
          </div>
        </Grid>
        <Grid item xs={12} className="form-heading">
          Additional Details
        </Grid>
        <Grid item xs={12} md={6}>
          <Tooltip
            title="Optionally, general requests not relating to a specific serving"
            placement="top-start"
          >
            <TextField
              fullWidth
              label="Other Requests"
              onChange={(event) => handleInputChange(event)}
              name="otherRequests"
              value={formData.otherRequests}
              variant="outlined"
              multiline={true}
              rows={props.embedded ? undefined : 5}
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} md={6}>
          <Tooltip
            title="Please list all the people participating "
            placement="top-start"
          >
            <TextField
              fullWidth
              label="List of participants"
              onChange={(event) => handleInputChange(event)}
              name="participants"
              value={formData.participants}
              variant="outlined"
              multiline={true}
              rows={props.embedded ? undefined : 5}
            />
          </Tooltip>
        </Grid>
      </>
    );
  };

  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={getEmptyCateringRequest(initialState)}
      submittable={(form) => {
        if (form.orders.length === 0) {
          return "Your order is currently empty";
        }
        if (
          form.orders.filter(
            (form) => form.quantity === 0 || !form.deliveryTime
          ).length > 0
        ) {
          return "Please make sure every order row has a quantity and a delivery time";
        }
        return "";
      }}
      formType="CATERING_REQUEST"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
}

export default CateringRequest;
