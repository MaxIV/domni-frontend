import React, { useEffect } from "react";
import * as api from "../api/forms";
import {
  Button,
  Checkbox,
  Container,
  FormControlLabel,
  Paper,
  Tooltip,
} from "@mui/material";
import Grid from "@mui/material/Grid/Grid";
import { useState } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import { getModalAPIErrorDispatch } from "../components/Modal";
import { useStore } from "../store/Store";
import { Redirect } from "react-router-dom";
import { getFormConfig } from "../utilities/Config";
import { ADD_FORM } from "../constants";
import { DomniTextValidator } from "../components/TextValidator";

//This is a simple form
const Inquiry = (props: any) => {
  const { dispatch, state } = useStore();
  const [localState, setLocalState] = useState<{
    contactName: string;
    contactEmail: string;
    nameAndDescription: string;
    accessInstructions: string;
  }>({
    contactName: state.user.name,
    contactEmail: state.user.email,
    nameAndDescription: "",
    accessInstructions: "",
  });
  const [redirect, setRedirect] = useState<boolean>(false);
  const [checkboxState, setCheckboxState] = useState<{
    cb1: boolean;
    //cb2: boolean;
    cb3: boolean;
    cb4: boolean;
    isSimpleForm: boolean;
  }>({
    cb1: false,
    //cb2: false,
    cb3: false,
    cb4: false,
    isSimpleForm: false,
  });
  const formConfig = getFormConfig(ADD_FORM);
  useEffect(() => {
    setLocalState({
      ...localState,
      contactName: state.user.name,
      contactEmail: state.user.email,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.user]);
  const submit = async () => {
    try {
      await api.emailData({
        formType: ADD_FORM,
        data: {
          ...localState,
          isSimpleForm: checkboxState.isSimpleForm ? "Yes" : "No",
        },
        emailRecipient: "ims@maxiv.lu.se",
        emailSubject: "New form inquiry",
      });
      setRedirect(true);
    } catch (err) {
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
      return;
    }
  };
  const submitDisabled = () => {
    return (
      !checkboxState.cb1 ||
      //!checkboxState.cb2 ||
      !checkboxState.cb3 ||
      !checkboxState.cb4
    );
  };

  const handleInputChange = (
    event: React.ChangeEvent<{ name: string; value: any }>
  ) => {
    setLocalState({
      ...localState,
      [event.target.name]: event.target.value,
    });
  };
  if (redirect) {
    return <Redirect to={`/${formConfig.path}/submit/?action=submitted`} />;
  }
  return (
    <ValidatorForm
      onKeyPress={(event: any) => {
        if (
          event.which === 13 &&
          event.target.nodeName.toLowerCase() !== "textarea"
        ) {
          event.preventDefault();
        }
      }}
      onSubmit={submit}
    >
      <Container>
        <Paper elevation={2} className="paper">
          <h2>
            <span className="category-preface">Form</span>
            <span className="primary-dark-color"> Inquiry </span>
          </h2>
          <Grid container spacing={2} style={{ marginTop: "1em" }}>
            <Grid item xs={12}>
              If you have a form that you think would make a good addition to
              Domni, please tell us about it below! Also make sure you read
              through the{" "}
              <a target="_blank" className="link" href="/docs/">
                Domni introduction
              </a>{" "}
              carefully first, so that you have an good understanding of what
              Domni is and what type of forms it is intended to handle.
            </Grid>
            <Grid item xs={12} className="form-heading">
              Contact Information
            </Grid>
            <Grid item xs={12} md={6}>
              <DomniTextValidator
                fullWidth={true}
                label="Contact person name"
                onChange={handleInputChange}
                name="contactName"
                value={localState.contactName}
                validators={["required"]}
                errorMessages={["This field is required"]}
                variant="outlined"
                multiline
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <DomniTextValidator
                fullWidth={true}
                label="Contact person email"
                onChange={handleInputChange}
                name="contactEmail"
                value={localState.contactEmail}
                validators={["required"]}
                errorMessages={["This field is required"]}
                variant="outlined"
                multiline
              />
            </Grid>
            <Grid item xs={12} className="form-heading">
              Form Information
            </Grid>
            <Grid item xs={12}>
              <Tooltip
                title="Please provide a name and description of the form. Don't forget to describe how a form is handled after it has been submitted. Is there an approval process? Does it need to be archived?"
                placement="top"
                arrow
              >
                <DomniTextValidator
                  fullWidth={true}
                  label="Name and short description"
                  onChange={handleInputChange}
                  name="nameAndDescription"
                  value={localState.nameAndDescription}
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                  variant="outlined"
                  multiline={true}
                  rows={props.embedded ? undefined : 3}
                />
              </Tooltip>
            </Grid>
            <Grid item xs={12}>
              <Tooltip
                title="Please specify detailed instruction on how the current version of the form can be accessed."
                placement="top"
                arrow
              >
                <DomniTextValidator
                  fullWidth={true}
                  label="Access instructions for the current version of the form"
                  onChange={handleInputChange}
                  name="accessInstructions"
                  value={localState.accessInstructions}
                  validators={["required"]}
                  errorMessages={["This field is required"]}
                  variant="outlined"
                  multiline={true}
                  rows={props.embedded ? undefined : 3}
                />
              </Tooltip>
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                className="link"
                control={
                  <Checkbox
                    size="medium"
                    value={checkboxState.isSimpleForm}
                    checked={checkboxState.isSimpleForm}
                    onChange={() =>
                      setCheckboxState({
                        ...checkboxState,
                        isSimpleForm: !checkboxState.isSimpleForm,
                      })
                    }
                  />
                }
                label="This can be implemented as a Simple Form"
              />
            </Grid>
            <Grid item xs={12}>
              Please verify that the following statements hold true for the
              proposed form:
            </Grid>
            <Grid item xs={12} style={{ padding: "0em 2em" }}>
              <FormControlLabel
                className="link"
                control={
                  <Checkbox
                    size="medium"
                    value={checkboxState.cb1}
                    checked={checkboxState.cb1}
                    onChange={() =>
                      setCheckboxState({
                        ...checkboxState,
                        cb1: !checkboxState.cb1,
                      })
                    }
                  />
                }
                label="It only needs to be accessed by people who have a MAX IV account."
              />
            </Grid>
            <Grid item xs={12} style={{ padding: "0em 2em" }}>
              <FormControlLabel
                className="link"
                control={
                  <Checkbox
                    size="medium"
                    value={checkboxState.cb3}
                    checked={checkboxState.cb3}
                    onChange={() =>
                      setCheckboxState({
                        ...checkboxState,
                        cb3: !checkboxState.cb3,
                      })
                    }
                  />
                }
                label="Its content, with exception to information that relates to supervisors and projects, is not subject to frequent change (e.g. on a monthly basis)."
              />
            </Grid>
            <Grid item xs={12}>
              And also that:
            </Grid>
            <Grid item xs={12} style={{ padding: "0em 2em" }}>
              <FormControlLabel
                className="link"
                control={
                  <Checkbox
                    size="medium"
                    value={checkboxState.cb4}
                    checked={checkboxState.cb4}
                    onChange={() =>
                      setCheckboxState({
                        ...checkboxState,
                        cb4: !checkboxState.cb4,
                      })
                    }
                  />
                }
                label="I've read the Domni introduction."
              />
            </Grid>
            <Grid
              style={{
                marginTop: "2em",
                display: "flex",
                justifyContent: "space-between",
              }}
              item
              xs={12}
            >
              <span
                style={{
                  fontSize: "0.8em",
                  fontStyle: "italic",
                  color: "#777",
                  alignSelf: "center",
                }}
              >
                {" "}
                Domni is written and maintained by the{" "}
                <a className="link" href="mailto:ims@maxiv.lu.se">
                  IMS
                </a>{" "}
                group at MAX IV. Source code available{" "}
                <a
                  className="link"
                  href="https://gitlab.maxiv.lu.se/kits-ims/domni-frontend"
                >
                  here
                </a>{" "}
                and{" "}
                <a
                  className="link"
                  href="https://gitlab.maxiv.lu.se/kits-ims/domni-backend"
                >
                  here
                </a>
              </span>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                disabled={submitDisabled()}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </ValidatorForm>
  );
};
export default Inquiry;
