import {
  Container,
  Grid,
  IconButton,
  InputBase,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import DomniLoader from "../components/DomniLoader";
import { useWindowDimensions } from "../utilities/Helpers";
import AppsIcon from "@mui/icons-material/Apps";
import queryString from "query-string";
import MenuIcon from "@mui/icons-material/Menu";
import LinkIcon from "@mui/icons-material/Link";
import SearchIcon from "@mui/icons-material/Search";
import { Pagination } from "@mui/material";
import { PaginationItem } from "@mui/lab";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import React, { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import { ldapLoadAll } from "../api/ldap";
import { getFlair, LdapPersonCard } from "../components/LdapPerson";
import { SimpleSelect } from "../components/SimpleSelect";
import { useStore } from "../store/Store";
import {
  getAdminDescriptionArray,
  Highlight,
  possessive,
} from "../utilities/Helpers";
import "./Settings.css";

interface State {
  sortBy: System.SearchableLdapAttribute;
  filter: string;
  isLoading: boolean;
  page: number;
  pageSize: number;
  viewMode: "grid" | "table";
  filteredAndSortedPeople: System.LDAPPerson[];
}

const People = (props: RouteComponentProps & System.URLParamProps) => {
  const { state, dispatch } = useStore();
  const { xl, md } = useWindowDimensions();
  const params = queryString.parse(props.location.search);
  const defaultFilter = (params.filter || "") as string;
  const defaultPage = (params.page || "0") as string;
  const defaultSortBy = (params.sortBy ||
    "fullName") as System.SearchableLdapAttribute;
  const defaultPageSize = (params.pageSize || "12") as string;
  //default to grid, unless following a link specifying table mode, UNLESS resolution is too low for table mode
  const defaultViewMode = (!md ? "grid" : params.viewMode || "grid") as
    | "grid"
    | "table";
  const [localState, setLocalState] = useState<State>({
    sortBy: defaultSortBy,
    filter: defaultFilter,
    isLoading: true,
    page: parseInt(defaultPage),
    pageSize: parseInt(defaultPageSize),
    viewMode: defaultViewMode,
    filteredAndSortedPeople: [],
  });
  const {
    sortBy,
    filter,
    isLoading,
    page,
    pageSize,
    filteredAndSortedPeople,
    viewMode,
  } = localState;
  const filterOpacity = !!filter ? { opacity: "1" } : {};
  const loadPeople = async () => {
    if (state.ldapPeople.length > 0) {
      return;
    } else {
      const result = await ldapLoadAll();
      dispatch({ type: "SET_LDAP_PEOPLE", ldapPeople: result });
    }
  };
  const filterStyle = {
    ...filterOpacity,
    flexGrow: !md ? 1 : 0,
  };
  useEffect(() => {
    loadPeople();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    setLocalState({
      ...localState,
      isLoading: state.ldapPeople.length === 0,
      filteredAndSortedPeople: state.ldapPeople
        .filter((person) => {
          if (!localState.filter) {
            return true;
          }
          const lowerFilter = filter.toLowerCase();
          return (
            person.fullName?.toLowerCase().includes(lowerFilter) ||
            person.email?.toLowerCase().includes(lowerFilter) ||
            person.phone?.toLowerCase().includes(lowerFilter) ||
            person.office?.toLowerCase().includes(lowerFilter) ||
            person.title?.toLowerCase().includes(lowerFilter) ||
            person.username?.toLowerCase().includes(lowerFilter) ||
            getFlair(person).toLowerCase().includes(lowerFilter)
          );
        })
        .sort((a, b) => {
          if (!a[localState.sortBy]) {
            return b[localState.sortBy] ? 1 : 0;
          }
          return a[sortBy].localeCompare(b[sortBy]);
        }),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.ldapPeople, localState.sortBy, localState.filter]);
  const useStyles = makeStyles({
    root: {
      color: "#aaa",
    },
    icon: {
      color: "#aaa",
    },
    outlined: {
      borderColor: "#aaa",
    },
  });
  const classes = useStyles();
  if (isLoading) {
    return (
      <Container>
        <h2 style={{ marginTop: "1.5em" }}>
          {md && <span className="category-preface">Domni</span>}
          <span className="primary-dark-color"> People</span>
        </h2>
        <Paper elevation={2} className="paper">
          <DomniLoader show={true} />
        </Paper>
      </Container>
    );
  }
  return (
    <Container style={{ marginBottom: "6em" }}>
      <Grid container spacing={2} style={{ marginTop: "2em" }}>
        <Grid item xs={12}>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              flexWrap: "wrap",
              marginBottom: "0.5em",
            }}
          >
            <div
              style={
                md
                  ? { display: "flex", marginBottom: "0.5em" }
                  : {
                      display: "flex",
                      marginBottom: "0.5em",
                      flexGrow: 1,
                      justifyContent: "space-between",
                    }
              }
            >
              <h2>
                {md && <span className="category-preface">Domni</span>}
                <span className="primary-dark-color"> People</span>
              </h2>
              <SimpleSelect
                disabled={{
                  value: false,
                }}
                options={[
                  { label: "Name", value: "fullName" },
                  { label: "Email", value: "email" },
                  { label: "Phone", value: "phone" },
                  { label: "Title", value: "title" },
                  { label: "Office", value: "office" },
                  { label: "Username", value: "username" },
                ]}
                selectedValue={sortBy}
                fullWidth={false}
                style={{
                  width: "7em",
                  marginLeft: "1em",
                }}
                testid="people-sortby-select"
                variant="standard"
                title="Sort by"
                name="sortBy"
                onChange={(event) => {
                  setLocalState({
                    ...localState,
                    sortBy: event.target.value,
                  });
                }}
              />
            </div>
            <div
              style={
                md
                  ? { display: "flex", marginBottom: "0.5em" }
                  : {
                      display: "flex",
                      marginBottom: "0.5em",
                      flexGrow: 1,
                      justifyContent: "space-between",
                    }
              }
            >
              <a
                href="/api/files/phoneBook.vcf"
                className="link"
                style={{
                  alignSelf: "center",
                  margin: "0em 1em",
                  fontSize: "0.8em",
                  textAlign: "center",
                }}
                download
              >
                <div>DOWNLOAD</div>
                <div> PHONEBOOK</div>
              </a>
              {md && (
                <Tooltip
                  title="Toggle between table and grid view"
                  placement="top"
                  arrow
                >
                  <ToggleButtonGroup
                    value={viewMode}
                    exclusive
                    onChange={(_, value) =>
                      setLocalState({ ...localState, viewMode: value })
                    }
                    aria-label="text alignment"
                  >
                    <ToggleButton
                      title="Grid view"
                      value="grid"
                      aria-label="grid"
                    >
                      <AppsIcon />
                    </ToggleButton>
                    <ToggleButton
                      title="Table view"
                      value="table"
                      aria-label="table"
                    >
                      <MenuIcon />
                    </ToggleButton>
                  </ToggleButtonGroup>
                </Tooltip>
              )}
              {md && (
                <Tooltip
                  title="Save this search result to the URL"
                  placement="top"
                  arrow
                >
                  <IconButton
                    onClick={() => {
                      props.history.push({
                        pathname: "/people/",
                        search: `?filter=${filter}&sortBy=${sortBy}&page=${page}&pageSize=${pageSize}&viewMode=${viewMode}`,
                      });
                    }}
                    data-testid="people-filter-anchor-button"
                    aria-label="anchor"
                    size="large"
                  >
                    <LinkIcon />
                  </IconButton>
                </Tooltip>
              )}
              <Tooltip
                title="Filter by name, email, title, office, phone number or username"
                placement="top"
                arrow
              >
                <Paper className="search-input" style={filterStyle}>
                  <InputBase
                    value={filter}
                    data-testid="people-filter-input"
                    onChange={(e) =>
                      setLocalState({
                        ...localState,
                        filter: e.target.value,
                        page: 0,
                      })
                    }
                    placeholder="Filter"
                  />
                  <SearchIcon />
                </Paper>
              </Tooltip>
            </div>
          </div>
        </Grid>
        {viewMode === "grid" ? (
          filteredAndSortedPeople
            .slice(page * pageSize, page * pageSize + pageSize)
            .map((person) => (
              <Grid key={person.username} item xs={12} md={6} lg={4}>
                <LdapPersonCard
                  className="ldap-person-card-container"
                  person={person}
                  highlight={filter}
                />
              </Grid>
            ))
        ) : (
          <Grid item xs={12}>
            <TableContainer component={Paper}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell />
                    <TableCell>NAME</TableCell>
                    <TableCell>TITLE</TableCell>
                    <TableCell>PHONE</TableCell>
                    <TableCell>EMAIL</TableCell>
                    <TableCell>OFFICE</TableCell>
                    <TableCell>USERNAME</TableCell>
                    <TableCell>DOMNI ROLES</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {filteredAndSortedPeople
                    .slice(page * pageSize, page * pageSize + pageSize)
                    .map((person) => (
                      <TableRow key={person.username}>
                        <TableCell>
                          {" "}
                          <button
                            onClick={() =>
                              dispatch({
                                type: "SET_MODAL_PROPS",
                                modalProps: {
                                  open: true,
                                  title: "",
                                  content: (
                                    <img src={person.jpeg} alt="profile" />
                                  ),
                                  btn1Text: "Close",
                                  btn1Color: "info",
                                },
                              })
                            }
                          >
                            {person.jpeg && (
                              <Tooltip
                                title={`Click to view ${possessive(
                                  person.firstName
                                )} profile picture`}
                                placement="top"
                                arrow
                              >
                                <AccountBoxIcon className="link" />
                              </Tooltip>
                            )}
                          </button>
                        </TableCell>
                        <TableCell>
                          <Highlight query={filter} msg={person.fullName} />
                        </TableCell>
                        <TableCell>
                          <Highlight query={filter} msg={person.title} />
                        </TableCell>
                        <TableCell>
                          <Highlight query={filter} msg={person.phone} />
                        </TableCell>
                        <TableCell>
                          <Highlight query={filter} msg={person.email} />
                        </TableCell>
                        <TableCell>
                          <Highlight query={filter} msg={person.office} />
                        </TableCell>
                        <TableCell>
                          <Highlight query={filter} msg={person.username} />
                        </TableCell>
                        <TableCell>
                          {getAdminDescriptionArray(person).map(
                            (role, index) => {
                              return (
                                <span key={role.toString()}>
                                  {index !== 0 ? ", " : " "} {role}
                                </span>
                              );
                            }
                          )}
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        )}
      </Grid>
      <div
        style={{
          position: "fixed",
          bottom: "0px",
          left: "0px",
          right: "0px",
          background: "#011428f0",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexWrap: "wrap",
          }}
        >
          <div style={{ padding: "0.5em" }}>
            <Pagination
              count={Math.ceil(filteredAndSortedPeople.length / pageSize)}
              page={page + 1}
              color="secondary"
              variant="outlined"
              siblingCount={xl ? 2 : md ? 1 : 0}
              boundaryCount={xl ? 2 : 1}
              renderItem={(item) => (
                <PaginationItem
                  {...item}
                  classes={{
                    root: classes.root,
                    icon: classes.icon,
                    outlined: classes.outlined,
                  }}
                />
              )}
              onChange={(e, value) =>
                setLocalState({ ...localState, page: value - 1 })
              }
            />
          </div>
          <div
            style={{ display: "flex", alignItems: "center", padding: "0.5em" }}
          >
            <SimpleSelect
              disabled={{
                value: false,
              }}
              options={[
                { label: "12", value: 12 },
                { label: "60", value: 60 },
                { label: "All", value: 1000 },
              ]}
              selectedValue={pageSize}
              fullWidth={false}
              style={{ marginRight: "1em" }}
              selectStyle={{
                width: "5em",
                color: "#ccc",
              }}
              variant="standard"
              darkTheme={true}
              title="Page size"
              name="pageSize"
              onChange={(event) => {
                setLocalState({
                  ...localState,
                  pageSize: event.target.value,
                  page: 0,
                });
              }}
            />
            <span
              style={{
                color: "#ccc",
                fontSize: "0.8em",
                letterSpacing: "1px",
              }}
            >
              {filteredAndSortedPeople.length}{" "}
              {filteredAndSortedPeople.length === 1 ? "PERSON" : "PEOPLE"}
            </span>
          </div>
        </div>
      </div>
    </Container>
  );
};
export default People;
