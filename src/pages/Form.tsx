import { Container, Grid, Paper, Tooltip } from "@mui/material";
import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import { ValidatorForm } from "react-material-ui-form-validator";
import { Link, Redirect } from "react-router-dom";
import * as api from "../api/forms";
import FormGridFileUpload from "../components/FormGridFileUpload";
import { FormGridHeader } from "../components/FormGridHeader";
import { FormGridSubmitterDetails } from "../components/FormGridSubmitterDetails";
import DomniLoader from "../components/DomniLoader";
import {
  getModalAPIErrorDispatch,
  getModalEditSuccessDispatch,
  getModalMsgDispatch,
} from "../components/Modal";
import { useStore } from "../store/Store";
import * as Config from "../utilities/Config";
import {
  getEarliestSupervisorAction,
  getFormAccess,
  isStaff,
} from "../utilities/Helpers";

export function Form<T extends System.Form>(
  props: System.FormTemplateProps<T>
) {
  const { dispatch, state } = useStore();
  const {
    emptyForm,
    FormInputs,
    formType,
    id,
    afterFormFetch,
    beforeFormSave,
    afterUserSet,
    submittable,
  } = props;
  const [formData, setFormData] = useState<T>(emptyForm);
  const [redirect, setRedirect] = useState<string>("");
  const [formAccess, setFormAccess] = useState<System.FormAccess>(
    getFormAccess(formData, state.user, "Form - default state")
  );
  //indicates that a form is being fetched from the db
  const [formIsLoading, setFormIsLoading] = useState<boolean>(true);
  const formConfig = Config.getFormConfig(formType);
  useEffect(() => {
    const fetchForm = async () => {
      if (id) {
        try {
          const result = await api.getForm(id, formConfig.formType);
          if (result) {
            const form = result as T;
            setFormData(form);
            setFormIsLoading(false);
            setFormAccess(
              getFormAccess(
                form,
                state.user,
                "Form - after fetching by id param"
              )
            );
            if (afterFormFetch) {
              afterFormFetch(form);
            }
            return;
          }
        } catch (err) {
          dispatch(getModalAPIErrorDispatch(err as System.APIError));
          setRedirect(`/${formConfig.path}/form/`);
          return;
        }
      } else {
        setFormData({ ...emptyForm });
        setFormIsLoading(false);
        setFormAccess(
          getFormAccess(
            { ...emptyForm, submitterEmail: state.user.email },
            state.user,
            "Form - with no param id"
          )
        );
      }
    };
    if (!redirect || redirect.endsWith(id)) {
      if (redirect.endsWith(id)) {
        //special case for redirect to the same page, which happens when we save a draft for the first time which just updates the url with an form id
        setRedirect("");
      }
      if (state.user.email) {
        fetchForm();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, formConfig, state.user.email]);

  useEffect(() => {
    if (formData.status === "UNSAVED") {
      const newFormData = {
        ...formData,
        submitterName: state.user.name,
        submitterEmail: state.user.email,
      };
      setFormData(newFormData);
      setFormAccess(
        getFormAccess(newFormData, state.user, "user change on unsaved")
      );
    } else {
      setFormAccess(
        getFormAccess(formData, state.user, "Form - user changed on saved")
      );
    }
    if (state.user.email && afterUserSet) {
      afterUserSet({
        formAccess: getFormAccess(
          formData,
          state.user,
          "Form - passed to afterUserSet"
        ),
        formData: formData,
        user: state.user,
        updateFormData,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.user]);

  const handleInputChange = (
    event: React.ChangeEvent<{
      name: string;
      type: string;
      value: string;
      checked?: boolean;
    }>
  ) => {
    //this is not the typescript way, but typescript is really hard and annoying sometimes
    const value =
      event.target.type === "checkbox"
        ? event.target.checked || false
        : event.target.value;

    setFormData({ ...formData, [event.target.name]: value });
  };

  function handleSetFiles(newFile: { name: string; size: string }) {
    setFormData({ ...formData, files: [...formData.files, newFile] });
  }

  const handleClearFiles = () => {
    setFormData({ ...formData, files: [] });
  };
  const updateFormData = (newFormData: T) => {
    setFormData(newFormData);
  };

  const saveDraft = async () => {
    const resolvedData = beforeFormSave
      ? beforeFormSave(formData, true)
      : formData;
    if (!resolvedData) {
      return;
    }
    const isUnsaved = formData.status === "UNSAVED";
    resolvedData.status = "DRAFT";
    if (isUnsaved) {
      const result = await api.createForm(resolvedData);
      setRedirect(`/${formConfig.path}/form/${result._id}`);
      return;
    }
    await api.update({
      form: resolvedData,
      updateType: "EDIT",
    });
    dispatch(
      getModalMsgDispatch("Saved!", "Your draft was successfully saved.")
    );
  };

  const submit = async (event: React.FormEvent<Element>) => {
    const resolvedData = beforeFormSave ? beforeFormSave(formData) : formData;
    if (!resolvedData) {
      event.preventDefault();
      return;
    }
    if (formData.status === "UNSAVED") {
      try {
        resolvedData.status = "PENDING";
        api.createForm(resolvedData);
        setRedirect(`/${formConfig.path}/submit/?action=submitted`);
        return;
      } catch (err) {
        dispatch(getModalAPIErrorDispatch(err as System.APIError));
        return;
      }
    }
    try {
      //3 cases - draft submittal, admin edit or submitter resubmitting after rejection.
      const isDraftSubmittal = resolvedData.status === "DRAFT";
      const isEdit = resolvedData.status === "PENDING";
      const isResubmittal = resolvedData.status === "REJECTED";
      const currentDate = new Date();
      const edit: System.Edit | undefined =
        isEdit || isResubmittal
          ? {
              name: state.user.name,
              email: state.user.email,
              timestamp: currentDate,
              eventType: "Edit" as System.EventType,
              role: isResubmittal
                ? "Submitter"
                : state.user.admin[formData.formType]
                ? "Administrator"
                : "Supervisor",
            }
          : undefined;
      //If it's a resubmittal, we need to add a new pending supervisoraction slightly in the future (past the edit)
      //Note - the supervisor should go back to the original supervisor, so we get the ealiest supervisor!
      if (isResubmittal) {
        const future = new Date();
        future.setSeconds(future.getSeconds() + 1);
        const earliestSupervisor = getEarliestSupervisorAction(resolvedData);
        resolvedData.supervisorActions.push({
          eventType: "SupervisorAction",
          name: earliestSupervisor.name,
          email: earliestSupervisor.email,
          action: "PENDING",
          timestamp: future,
          decisionDate: undefined,
          comment: "",
          notify: true,
          role: earliestSupervisor.role || "Supervisor",
        });
      }
      await api.update({
        form: resolvedData,
        edit,
        updateType: isDraftSubmittal
          ? "DRAFT_SUBMITTAL"
          : isEdit
          ? "EDIT"
          : "RESUBMITTAL",
      });
      if (isDraftSubmittal || isResubmittal) {
        setRedirect(`/${formConfig.path}/submit/?action=submitted`);
        return;
      } else {
        dispatch(
          getModalEditSuccessDispatch(() => {
            if (isEdit) {
              setRedirect(`/${formConfig.path}/approval/${resolvedData._id}`);
            }
          })
        );
      }
    } catch (err) {
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
    }
  };
  // To prevent ui flickering, we don't render a form until after basic stuff has been loaded
  if (
    state.isLoading ||
    formIsLoading ||
    (!state.user.name && !state.user.email)
  ) {
    return <DomniLoader show={true} />;
  }
  if (redirect) {
    return <Redirect to={redirect} />;
  }
  if (formConfig.visibility !== "PUBLIC" && !isStaff(state.user)) {
    return <Container>
        <Paper
          elevation={2}
          className="paper"
          style={{ marginTop: "2em", marginBottom: "0.5em" }}
        >
        This form is not for you. If you are a visitor who got lost trying to find the safety test, go to <a href="https://duo.maxiv.lu.se/duo/">duo</a> instead!
        </Paper>
      </Container>
  }
  if (formAccess.readAccess === "BASIC") {
    return (
      <div
        style={{ textAlign: "center", paddingTop: "1em", fontSize: "1.25em" }}
      >
        You are not authorized to read the content of this form
      </div>
    );
  }
  if (props.embedded === true) {
    return (
      <fieldset disabled={true}>
        <ValidatorForm className="domni-form" onSubmit={() => {}}>
          <Container>
            <Grid container spacing={2}>
              <FormInputs
                handleInputChange={handleInputChange}
                updateFormData={updateFormData}
                formData={formData}
                state={state}
                embedded={props.embedded}
                formAccess={{ ...formAccess, canEdit: false }} //Never allow write on embedded
              />
              {formConfig.fileUpload && (
                <FormGridFileUpload
                  files={formData.files}
                  handleSetFiles={handleSetFiles}
                  handleClearFiles={handleClearFiles}
                  helpText={formConfig.fileUpload}
                  disabled={true}
                />
              )}
            </Grid>
          </Container>
        </ValidatorForm>
      </fieldset>
    );
  }
  const submitWarning: string = submittable ? submittable(formData) : "";
  return (
    <fieldset
      data-testid="form-fieldset-disabled-form"
      disabled={!formAccess.canEdit}
    >
      <ValidatorForm
        className="domni-form"
        onKeyPress={(event: any) => {
          if (
            event.which === 13 &&
            event.target.nodeName.toLowerCase() !== "textarea"
          ) {
            event.preventDefault();
          }
        }}
        onSubmit={submit}
      >
        <Container>
          <Paper elevation={2} className="paper">
            <Grid container spacing={2}>
              <FormGridHeader
                form={formData}
                helpText={formConfig.helpComponent}
                formAccess={formAccess}
              />

              <FormGridSubmitterDetails form={formData} />
              <FormInputs
                handleInputChange={handleInputChange}
                updateFormData={updateFormData}
                formData={formData}
                state={state}
                embedded={props.embedded}
                formAccess={formAccess}
              />
              {formConfig.fileUpload && (
                <FormGridFileUpload
                  files={formData.files}
                  handleSetFiles={handleSetFiles}
                  handleClearFiles={handleClearFiles}
                  helpText={formConfig.fileUpload}
                  disabled={!formAccess.canEdit}
                />
              )}
              {formAccess.canEdit && (
                <Grid
                  style={{
                    marginTop: "1em",
                    paddingTop: "2em",
                    display: "flex",
                    borderTop: "1px solid #eee",
                    justifyContent: "space-between",
                  }}
                  item
                  xs={12}
                >
                  {formData.status === "UNSAVED" ||
                  formData.status === "DRAFT" ? (
                    <Tooltip
                      title="Drafts are not visible to supervisors or administrators. You can find your saved drafts in the profile view"
                      placement="top-start"
                    >
                      <Button
                        color="info"
                        data-testid="form-save-draft-btn"
                        variant="contained"
                        onClick={saveDraft}
                      >
                        Save draft
                      </Button>
                    </Tooltip>
                  ) : (
                    <div />
                  )}
                  <Tooltip title={submitWarning} placement="top-start">
                    <span>
                      <Button
                        data-testid="form-submit-btn"
                        type="submit"
                        color="primary"
                        variant="contained"
                        disabled={!!submitWarning}
                      >
                        {formData.status === "UNSAVED" ||
                        formData.status === "DRAFT"
                          ? "Submit"
                          : formData.status === "REJECTED"
                          ? "Update"
                          : "Save"}
                      </Button>
                    </span>
                  </Tooltip>
                  {formData.status === "REJECTED" && !formAccess.canEdit && (
                    <Link to={`/${formConfig.path}/archive/`} className="link">
                      <Button variant="contained" color="secondary">
                        Cancel
                      </Button>
                    </Link>
                  )}
                </Grid>
              )}
            </Grid>
          </Paper>
        </Container>
      </ValidatorForm>
    </fieldset>
  );
}

export default Form;
