import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Container,
  useMediaQuery,
  Grid,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { useStore } from "../store/Store";
import { getModalAPIErrorDispatch } from "../components/Modal";
import { getFormConfigByPath } from "../utilities/Config";
import * as api from "../api/forms";
import SubmittedForm from "../components/SubmittedForm";
import { getFormAccess } from "../utilities/Helpers";

const Approval = (props: { match: { params: string[] } }) => {
  const { dispatch, state } = useStore();
  const largeScreen = useMediaQuery("(min-width:960px)");
  const [form, setForm] = useState<System.Form | undefined>(undefined);
  const urlParam = props.match.params[1] as string;
  const formConfig = getFormConfigByPath(props.match.params);

  const search = async () => {
    if (!urlParam) {
      dispatch(
        getModalAPIErrorDispatch({
          title: "Invalid link",
          msg: "This approval link is not valid.",
          code: 400,
        })
      );
      return;
    }
    try {
      const result = await api.getForm(urlParam, formConfig.formType);
      if (!result) {
        dispatch(
          getModalAPIErrorDispatch({
            title: "Not found",
            msg: "The requested form does not exist",
            code: 404,
          })
        );
        return;
      }
      const formAccess = getFormAccess(
        result,
        state.user,
        "Approval - after fetching form"
      );
      if (!formAccess.isAdmin && !formAccess.isSupervisor) {
        dispatch(
          getModalAPIErrorDispatch({
            title: "Forbidden",
            msg: `Only the designated supervisor or ${formConfig.name} administrators can process this submission`,
            code: 404,
          })
        );
        return;
      } else {
        setForm(result);
      }
    } catch (err) {
      console.log("ERROR", err);
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
    }
  };

  useEffect(() => {
    if (state.user.email) {
      search();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.user]);
  return (
    <Container>
      <Grid container>
        <Grid item xs={12}>
          <h2 style={{ marginTop: "1em" }}>
            <span className="category-preface">Approval</span>
            <span className="primary-dark-color">{formConfig.name} </span>
          </h2>
          <Typography style={{ fontSize: "0.9em", fontStyle: "italic" }}>
            Please review the form below and then use the correponding buttons
            to either approve or reject the submission. Note that by clicking on
            Approve you will also have the option to reassign the submission to
            an additional supervisor if further review and approval is required.{" "}
          </Typography>
        </Grid>
      </Grid>

      {!form ? (
        <Grid style={{ fontSize: "1.2em" }}></Grid>
      ) : (
        <Grid>
          <TableContainer
            component={Paper}
            style={{ overflow: "visible", marginTop: "1em" }}
            className="tableContainer"
          >
            <Table stickyHeader size={largeScreen ? "medium" : "small"}>
              <TableHead>
                <TableRow>
                  <TableCell />
                  <TableCell>ID</TableCell>
                  <TableCell>SUBMITTED</TableCell>
                  <TableCell>SUBMITTER</TableCell>
                  <TableCell>SUPERVISOR</TableCell>
                  <TableCell>STATUS</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <SubmittedForm
                  key={form._id}
                  form={form}
                  open={true}
                  onFormChange={(form: System.Form) => {
                    setForm(form);
                  }}
                />
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      )}
      <div style={{ padding: "4em" }} />
    </Container>
  );
};

export default Approval;
