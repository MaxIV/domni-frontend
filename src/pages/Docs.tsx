import React from "react";
import {
  Button,
  Container,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import Logo from "../components/Logo";
import TaggedTitle from "../components/TaggedTitle";
import { DomniLink } from "../components/DomniLink";

const Docs = (props: any) => {
  return (
    <Container>
      <Paper elevation={2} className="paper">
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <h2>
            <span className="category-preface">Domni</span>
            <span className="primary-dark-color"> Introduction </span>
          </h2>
          <Logo style={{ marginTop: "-1em", marginRight: "-0.5em" }} />
        </div>
        <div
          style={{ marginTop: "0.5em", fontSize: "0.9em", fontStyle: "italic" }}
        >
          This document describes the features of Domni primarily from a form
          administrators point of view. It acts both as a manual for
          administrative users as well as a guide and checklist when evaluating
          and testing a new form being implemented. Domni users with other roles
          (Submitter or Supervisor) are expected to recieve all necessary
          instructions on the form page. If you are administrating an external
          form and are considering having it migrated to Domni, this
          documentation might also help you determine if this migration is
          suitable and/or feasible. Any additional questions about the
          possibilities or limitations can be sent to{" "}
          <DomniLink href="mailto:ims@maxiv.lu.se">IMS</DomniLink>.
        </div>
        <h5>Content</h5>
        <Grid container style={{ padding: "1em 2em", background: "#fafafa" }}>
          <TOCElement name="Overview" />
          <TOCElement name="Supervisors" />
          <TOCElement name="Projects" />
          <TOCElement name="Custom Lists" id="customLists" />
          <TOCElement name="Administrators" />
          <TOCElement name="Emails" />
          <TOCElement name="Submission Lifecycle" id="lifeCycle" />
          <TOCElement name="Configurable features" id="configurableFeatures" />
          <TOCElement
            name="Further Customizability"
            id="furtherCustomizability"
          />
          <TOCElement
            name=" Adding new form types"
            id="limitationsConsiderations"
          />
          <TOCElement name="API" />
          <TOCElement name="Terminology" />
        </Grid>
        <h5 id="overview">Overview</h5>
        <div>
          The purpose of Domni is to collect many of the forms used at MAX IV in
          a single location, and at the same time making them easy to use,
          manage, configure, store and retrieve. For a less complex and much
          more quicky implemented version of Domni forms, see{" "}
          <button className="link" onClick={() => scrollIntoView("simpleForm")}>
            Simple form
          </button>
          .
        </div>
        <div style={{ marginTop: "0.5em" }}>
          As a form is submitted it is assigned a supervisor, which can either
          be selected by the submitter, or pre-defined for a specific form type.
          Supervisors are notified via email about the form they have been
          assigned to and are asked to handle the submitted request in some way.
          Depending on the configuration, this can involve a number of different
          actions including approval, rejection, editing or commenting on the
          form, as well as supervisor reassignment. These actions, along with
          the forms themselves, are stored in a central database that is
          searchable by the users defined as administrative users of the
          specific form type. Administrators also have some possibility to
          configure some aspects of the form content themselves through a
          settings view, including the list of available supervisors.
        </div>
        <div style={{ marginTop: "0.5em" }}>
          There is also a profile view where users can get an overview of all
          forms they're involved in, whether it's in their role as form
          submitter (regular user), supervisor, or administrator. This view
          lists:
          <ul>
            <li>All current drafts</li>
            <li>A searchable archive of all forms the user has submitted</li>
            <li>
              A list of all pending forms where the user has been assigned as as
              supervisor
            </li>
            <li>A list of all form types that the user administrates</li>
            <li>
              A list of all form types where the user is an available supervisor
            </li>
          </ul>
        </div>
        <div style={{ marginTop: "0.5em" }}>
          Note that Domni is located on the DMZ. This means it's accessible by
          anyone from outside of MAX IV. However, use of this application{" "}
          <i>always</i> requires that the user authenticates using their MAX IV
          credentials. Domni is only available to users who are either MAX IV
          staff or members of <i>DUO active</i>. Upon request, a given form type
          can be completely hidden from DUO active users.
        </div>
        <h5 id="supervisors">Supervisors</h5>
        <div>
          <i>Supervisors</i> have a name and an email. Typically, for forms that
          require approval, the supervisor gets notified via email after the
          submission, and are provided a link for handling the application. An
          email is then again sent to the original submitter informing them
          about the decision. In case of rejection, they will have the option to
          correct the form and resubmit it. Supervisors can also be associated
          with zero, one or multiple <i>Projects</i> (see below). In most cases,
          this means that if both a supervisor and project dropdown exist in a
          form, the corresponding supervisor is automatically selected upon the
          selection of a project.
        </div>
        <div style={{ marginTop: "0.5em" }}>
          If a supervisor does not resolve or reassign a submission they've been
          assigned, a reminder email is sent out every three days. This
          notification can be postponed in the profile view.
        </div>
        <div style={{ marginTop: "0.5em" }}>
          A supervisor can also choose to reassign the role of supervisor upon
          approval. This is a feature useful for those decisions that require
          the approval of multiple individuals in succession. If requested,
          there's also the option to allow supervisors to edit already submitted
          forms. These edits are logged.
        </div>
        <h5 id="projects">Projects</h5>
        <div>
          <i>Projects</i> provides a way of allowing form administrators to
          configure an additional type of drop-down available in the form. Most
          forms ask the submitter to select a value from a predefined list of
          values, whether they're actually called <i>Projects</i>, <i>Groups</i>
          , <i>Teams</i> or something else. Projects consists of a name and a
          category. The name is what's being selected in the drop-down, and the
          category determines how the projects are sorted and grouped in the
          dropdown.
        </div>
        <h5 id="customLists">Custom Lists</h5>
        <div>
          <i>Custom lists</i> is the most general way in which an administrator
          can manage the content of a form. Any dropdown select in the form can,
          upon request, be mapping to a custom list. The available values in the
          dropdown will then correspond to the <i>items</i> in the custom list,
          which can be created, edited or deleted by any admin. If you have a
          dropdown in which values are expected to change over time, such as a
          list of all beamlines at MAX IV, you should request that it's
          implemented as a custom list.
        </div>
        <div style={{ marginTop: "0.5em" }}>
          When designing a new form, try to identify which inputs, if any,
          should correspond to Supervisors, Projects and Custom lists. Note that
          the features above described as "typical" are optional, and other
          features could also be developed. Also note that while <i>Project</i>{" "}
          and <i>Supervisor</i> are their default names, and always how they are
          referred to under Settings, both of them might be called something
          else in a specific form types.
        </div>
        <h5 id="administrators">Administrators</h5>
        <div>
          Administrators are assigned on a form basis. Each form has has a list
          of AD groups as well as a list of users which are assigned as
          administrators. If a logged in user belongs to any of the designated
          administrator groups, och her username is present in the username
          admin list, this user is an administrator. In addition to the various
          forms, as well as the approval view available to supervisors,
          administrators also have access to an archive view and a settings
          view. The archive view allows them to view, edit and approve all
          submitted forms. The settings view allow them to define the lists of
          available supervisors and projects.
        </div>
        <h5 id="emails">Emails generated by Domni</h5>
        <div>
          Various actions in the application trigger the sending of email to the
          involved parties. In the test environment, all these emails end up in
          the{" "}
          <DomniLink href="https://mailcatcher.maxiv.lu.se">
            MAX IV mailcatcher
          </DomniLink>{" "}
          rather than being sent to the actual recipient. Please make a habit of
          reviewing emails being sent as you're testing the application. Most of
          these are generic in the sense that they're the same across all
          different form types, which just a few placeholders referencing the
          current form type or supervisor. However, if it's important that
          certain information is included, we can make an exception and add some
          custom text for a specific form. The following is a list of all emails
          sent out by the system along with a short description. Note that not
          all of these applies to all form types.
        </div>
        <TableContainer style={{ marginTop: "2em" }}>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Description</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell style={{ width: "20em" }}>
                  Submission confirmation
                </TableCell>
                <TableCell>
                  Sent out to the submitter right after submitting a form. It
                  confirms the submission and informs the submitter that they
                  will be notified when a decision has been made.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Submission notification</TableCell>
                <TableCell>
                  Sent out to the assigned supervisor right after a submission
                  has been made. Contains a link for handling the form.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Edit notification</TableCell>
                <TableCell>
                  Sent to all <i>listening</i> supervisors upon edit, except the
                  editor themselves. Listening supervisors includes the current
                  supervisor as well as any previous supervisor that opted in to
                  be informed about future changes to the form at the point of
                  reassigning it.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Reassignment notification</TableCell>
                <TableCell>
                  Sent to the new supervisor upon supervisor reassignment. It
                  let's them know they've been assigned and provides a link to
                  handling the form.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Decision notifications - listener</TableCell>
                <TableCell>
                  Sent to all listening supervisors upon rejection or final
                  approval, except the decision maker themselves. Not sent in
                  partial approval during supervisor reassignment.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Decision notification - submitter</TableCell>
                <TableCell>
                  Sent to the submitter upon upon rejection or final approval.
                  Not sent in partial approval during supervisor reassignment.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Submission reminder</TableCell>
                <TableCell>
                  A reminder sent out to the current supervisor every three days
                  as long as they have not taken any actions on a form that has
                  been assigned to them.
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <div>
          <h5 id="lifeCycle">The Lifecycle of a Domni Form Submission</h5>
          <div>
            The below image describes the lifecycle of a form in domni. For each
            stage, it shows the available statuses of that stage, and as well as
            actions available to different user roles. Click on the image to
            download a full-res png version.
          </div>
        </div>
        <div style={{ textAlign: "center", margin: "1em -2em" }}>
          <DomniLink href={process.env.PUBLIC_URL + "/form-lifecycle.png"}>
            <img
              style={{ width: "100%" }}
              alt="domni form lifecycle"
              src={process.env.PUBLIC_URL + "/form-lifecycle.png"}
            />
          </DomniLink>
        </div>
        <div>
          In addition to the actions depicted in the flowchart above,
          administrators can perform a couple other actions on submission.
          Available options typically vary between form types, but may include
          commenting, assigning external id, archiving, downloading file
          attachments, exporting one or many submission as json or csv, or
          cloning of a submission.
        </div>
        <h5 id="configurableFeatures">
          <TaggedTitle
            title="Configurable Form Type Features"
            tag="ADMIN"
            tagTooltip="This segment addressed configuration of form types in Domni and is primarily intended for administrators"
          />
        </h5>
        For every form type all of the following features must be defined as
        either enabled or disabled.
        <div className="subtitle">EDITABLE</div>
        <div>
          If a form is editable form administrators will have an option to edit
          a submitted form that currently has the <i>Pending</i> status.{" "}
          <i>Editable</i> should only be enabled if administrators need to be
          able to correct submitted information, or if a form has input that
          only an administrator should be allowed to specify. Editable can also
          be enabled for supervisors.
        </div>
        <div className="subtitle">REQUIRES APPROVAL</div>
        <div>
          If a form doesn't require approval it is automatically considered
          approved as soon as it has been submitted. While there will still be
          an option to mark the form as processed in the archive view, there is
          no option to approve or deny, or to reassign the form to a new
          supervisor. <i>Requires approval</i> should only be disabled if the
          form never requires approval.
        </div>
        <div className="subtitle">HAS EXTERNAL ID</div>
        <div>
          If there is an external system that also manages the same type of
          information as Domni, this system might have their own ID for each
          submission that needs to be associated with the form in Domni. By
          enabling <i>Has External ID</i>, this ID can be manually specified by
          administrators in the archive view. This also makes external ID
          searchable in Domni using the default search filter.{" "}
          <i>Has External ID</i> should only be enabled if some or all forms
          need to be easily mapped to related information in some other system.
        </div>
        <div className="subtitle" id="simpleForm">
          IS SIMPLE
        </div>
        <div>
          For forms that require neither an approval process, a browsable
          archive, supervisors, projects or custom lists, a more simple version
          can be implemented in a fraction of the time it would take to make a
          regular form. <i>Simple forms</i> still support all the form
          components, input validation and complexity found in regular forms.
          However, once a simple form has been submitted, it can no longer be
          administrered or even viewed in Domni. Instead, all the content of the
          form is sent as an email to a pre-defined form administrator, and it's
          up to this administrator to manage the request from this point
          forward. <i>Is Simple</i> should only be enabled if the form requires
          neither an approval process nor archiving.
        </div>
        <h5 id="furtherCustomizability">
          <TaggedTitle
            title="Further Customizability"
            tag="DEV"
            tagTooltip="This segment addressed technical aspects of Domni, and is primarily intended for developers"
          />
        </h5>
        <div>
          In addition to the features above, and the form design itself, each
          from type can be customized through various{" "}
          <DomniLink href="https://en.wikipedia.org/wiki/Hooking">
            hooks
          </DomniLink>{" "}
          that can override or extend the default form handling features:
        </div>
        <div>
          <code>onSubmit</code>- for adding a custom handling of form submit
          events{" "}
        </div>
        <div>
          <code>onEdit</code>- for adding a custom handling of administrator
          edit events{" "}
        </div>
        <div>
          <code>onReassign</code>- for adding a custom handling of supervisor
          reassignment events{" "}
        </div>
        <div>
          <code>onDecision</code>- for adding a custom handling of final
          decision events (approval or rejection){" "}
        </div>
        <div>
          <code>onComment</code> - for adding a custom handling of comment
          events{" "}
        </div>
        <div>
          <code>finalApprovalValidation</code> - for adding custom form
          validation at the point of final approval
        </div>
        <div>
          <code>approvalMessage</code> - for adding a custom approval message
        </div>
        <div>
          <code>submitMessage</code> - for adding a custom submit message
        </div>
        <div>
          <code>additionalHelpComponent</code> - for adding a dedicated help
          page
        </div>
        <div>
          <code>customSearchFields</code> - for adding form type specific fields
          in the archive search filter.
        </div>
        <h5 id="limitationsConsiderations">
          <TaggedTitle
            title="New Form Types - Limitations and Considerations"
            tag="ADMIN"
            tagTooltip="This segment addressed the introduction of new form types in Domni and is primarily intended for administrators"
          />
        </h5>
        <div>
          Below is a list of things to consider when evaluating layout and
          features of a form. Note that <i>(number)</i> refers to the
          corresponding number in the screenshot of the Purchase Application
          form below the text.
        </div>
        <ul>
          <li>
            Does the form need a short descriptive text (1) in addition to the
            help link? Keep in mind that this could be the very first time the
            user visits Domni.
          </li>
          <li>
            Does the help text (2) contain all the information needed, as well
            as links to all relevant resources?
          </li>
          <li>
            Does the text shown to the user after submitting (3) the form have
            all the information needed?
          </li>
          <li>
            For each input field:
            <ul>
              <li>
                Consider the name (4), does it explain to the user what we're
                expecting from them?{" "}
              </li>
              <li>
                Does the input require an explanation? If so, we can add that as
                a tooltip (5).
              </li>
              <li>
                Does the input require validation (6)? For example, is the input
                mandatory?
              </li>
              <li> Does the grouping of inputs (7) make sense?</li>
              <li>Does the titles of input groups (8) make sense? </li>
              <li>
                Are there some inputs whose visibility or ability to edit only
                makes sense if another input has a certain value (9)?
              </li>
              <li>
                {" "}
                Is it possible that some users would like to include one or
                several file attachments with their submission (10)?
              </li>
              <li>
                What type should each input widget be? Free text? Number?
                Checkbox? Radion buttons? Select/Dropdown?
              </li>
            </ul>
          </li>
        </ul>
        <div
          style={{
            textAlign: "center",
            margin: "1em 0em",
          }}
        >
          <img
            style={{ width: "100%" }}
            alt="screenshot of purchase application"
            src={process.env.PUBLIC_URL + "/purch-app-screenshot.png"}
          />
        </div>
        <h5 id="api">
          <TaggedTitle
            title="Domni API"
            tag="DEV"
            tagTooltip="This segment addressed technical aspects of Domni, and is primarily intended for developers"
          />
        </h5>
        <div>
          Domni has an open API for handling of communication between its
          frontend and backend, where authentication and authorization are
          performed with a jwt cookie. The same API can be used externally to
          perform <code>CRUD</code> operations on forms, projects, supervisors
          and files. To simplify this, the jwt can also be specified as a part
          of the body (in case of http <code>POST</code>, <code>PUT</code> and{" "}
          <code>DELETE</code>), or as a query param (in case of http{" "}
          <code>GET</code>). To acquire a jwt, visit{" "}
          <DomniLink href="https://auth.maxiv.lu.se">
            auth.maxiv.lu.se
          </DomniLink>
          . Below is a short description of some available endpoints in Domni.
          More detailed information can be added if requested from{" "}
          <DomniLink href="mailto:ims@maxiv.lu.se">IMS</DomniLink>.
        </div>
        <div style={{ marginTop: "2em", marginBottom: "0.2em" }}>
          Base URLs: <code>https://domni.maxiv.lu.se/api/</code> and{" "}
          <code>https://domni-test.maxiv.lu.se/api/</code>. All non-primitive
          datatypes listed below are declared as global TypeScript types or
          interfaces in{" "}
          <DomniLink href="https://gitlab.maxiv.lu.se/kits-ims/domni-backend/-/blob/master/src/types/global.d.ts">
            /src/types/global.d.ts
          </DomniLink>
          .
        </div>
        <TableContainer>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell>Endpoint</TableCell>
                <TableCell>Verb</TableCell>
                <TableCell>Parameters</TableCell>
                <TableCell>Description</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>
                  <code>forms/search</code>
                </TableCell>
                <TableCell>
                  <code>GET</code>
                </TableCell>
                <TableCell>
                  <code>searchQuery:string</code>
                  <br />
                  <code>formType:FormType </code>
                  <br />
                  <code>[page:int=1]</code>
                  <br />
                  <code>[limit:int=10]</code>
                </TableCell>
                <TableCell>
                  Return all forms of type <code>formType</code> where{" "}
                  <code>searchQuery</code> partially matches the name or email
                  of the submitter or supervisor, or exactly matches the form's
                  id or status. Only return forms for which the authenticated
                  user is a supervisor or administrator.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <code>forms</code>
                </TableCell>
                <TableCell>
                  <code>POST</code>
                </TableCell>
                <TableCell>
                  <code>form:Form</code>
                </TableCell>
                <TableCell>
                  Creates a new form. Any authenticated user can submit a new
                  form.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <code>forms</code>
                </TableCell>
                <TableCell>
                  <code>PUT</code>
                </TableCell>
                <TableCell>
                  <code>{`form:Partial<Form>`}</code>
                  <br />
                  <code>[isNewSupervisor:boolean=false]</code>
                  <br />
                  <code>[isEdit:boolean=false]</code>
                  <br />
                  <code>[isComment:boolean=false]</code>
                </TableCell>
                <TableCell>
                  Performs a partial update of a form. <code>form._id</code> is
                  required. Any other attribute in <code>form</code> will
                  overwrite the corresponding attribute in the database. The
                  boolean flags indicates what type of update it is. This
                  determines if specific logic for that update type should be
                  triggered. Usually this just involves notifying supervisors
                  and other listeners via email, but varies between forms. The
                  authenticated user needs to be an administrator or a
                  supervisor of the form being updated.
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <code>{`files/<filename>`}</code>
                </TableCell>
                <TableCell>
                  <code>GET</code>
                </TableCell>
                <TableCell>-</TableCell>
                <TableCell>
                  Return any publicly available file. This includes profile
                  pictures (<i>[username].jpeg</i> ) and the phonebook (
                  <i>phoneBook.vcf</i>){" "}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <code>ldap</code>
                </TableCell>
                <TableCell>
                  <code>POST</code>
                </TableCell>
                <TableCell>
                  <code>ldapRequest:LDAPRequest</code>
                </TableCell>
                <TableCell>
                  Return a list of all <code>LDAPPerson</code> matching the{" "}
                  <code>LDAPRequest</code>.
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <h5 id="terminology" style={{ marginBottom: "0.5em" }}>
          Terminology
        </h5>
        <TableContainer>
          <Table stickyHeader>
            <TableHead>{row("term", "Description")}</TableHead>
            <TableBody>
              {row(
                <div style={{ whiteSpace: "nowrap" }}>
                  <i>(Form) submission</i>
                </div>,
                <>
                  An instance of a <i>form type</i> that has been submitted by a{" "}
                  <i>submitter</i>.
                </>
              )}
              {row(
                <i>(Form) type</i>,
                <>
                  The template for forms of a specific type, e.g.{" "}
                  <i>Purchase Application</i>. Each form type has multiple{" "}
                  <i>submission</i>. Form types (with the exception of{" "}
                  <i>Simple form</i>) also have an archive and a settings view
                  associated with them.
                </>
              )}
              {row(
                <i>Submitter</i>,
                <>
                  A domni user role. Submitter are "regular" users, who only
                  uses Domni to make form <i>submission</i>.
                </>
              )}
              {row(
                <i>Supervisor</i>,
                <>
                  A domni user role. Supervisors <i>resolve</i> submissions. See{" "}
                  <button
                    className="link"
                    onClick={() => scrollIntoView("supervisors")}
                  >
                    Supervisors
                  </button>
                  .
                </>
              )}
              {row(
                "Administrator",
                <>
                  A domni user role. Administrators can supplant{" "}
                  <i>supervisors</i>, but also have access to archiving and
                  configuration options for specific form types. See{" "}
                  <button
                    className="link"
                    onClick={() => scrollIntoView("administrators")}
                  >
                    Administrators
                  </button>
                  .
                </>
              )}
              {row(
                <i>Draft</i>,
                <>
                  A <i>submission status</i>. <i>Drafts</i> are not yet
                  submitted, and only visible to the <i>submitter</i>
                </>
              )}
              {row(
                <i>Pending</i>,
                <>
                  A <i>submission status</i>. Pending submission are awaiting a
                  decision from the designated <i>supervisor</i>
                </>
              )}
              {row(
                <i>Approved</i>,
                <>
                  A <i>submission status</i>. Indicates that the submission has
                  recieved a final approved by the designated <i>supervisor</i>{" "}
                  or by an <i>administrator</i>. Approved submissions can be{" "}
                  <i>recalled</i>.
                </>
              )}
              {row(
                <i>Rejected</i>,
                <>
                  A <i>submission status</i>. Indicates that the submission has
                  been rejected by the designated <i>supervisor</i> or by an{" "}
                  <i>administrator</i>. Rejected submission can be{" "}
                  <i>resubmitted</i>.
                </>
              )}
              {row(
                <i>Archived</i>,
                <>
                  An action peformed on approved or rejected <i>submission</i>.
                  <i>Archived</i> submission are hidden by default, and cannot
                  be
                  <i>edited</i> or <i>resubmitted</i>.
                </>
              )}
              {row(
                <div style={{ whiteSpace: "nowrap", fontStyle: "italic" }}>
                  (submission) Editing
                </div>,
                <>
                  Refers to chaning the content (field values) of a{" "}
                  <i>submission</i>. Other actions, such as <i>resolution</i>,{" "}
                  <i>reassignment</i> or <i>commenting</i> are not considered
                  edits.
                </>
              )}
              {row(
                <i>Resubmission</i>,
                <>
                  Upon <i>rejection</i> of a <i>submission</i>, the{" "}
                  <i>submitter</i> is notified and requested to <i>edit</i> and
                  then resubmit the submission. This will reset the status to{" "}
                  <i>pending</i>.
                </>
              )}
              {row(
                <i>Recalling</i>,
                <>
                  An action peformed on approved submission. Changes the status
                  to <i>rejected</i>, allowing the <i>submitter</i> to edit and
                  resubmit it.
                </>
              )}
              {row(
                <i>Resolvning</i>,
                <>
                  The action of <i>approving</i> or <i>rejecting</i> a{" "}
                  <i>submission</i>.
                </>
              )}
              {row(
                <i>Approving</i>,
                <>
                  Refers to either of the actions available when approving a{" "}
                  <i>submission</i> - <i>reassignment</i> or a{" "}
                  <i>final approval</i>. The terminology{" "}
                  <i>(supervisor) reassignment</i> / <i>final approval</i> can
                  be used to avoid disambiguity.
                </>
              )}
              {row(
                <i>Reassigning</i>,
                <>
                  Deferring the <i>resolution</i> of a form to another{" "}
                  <i>supervisor</i>.
                </>
              )}
              {row(
                <i>Comment</i>,
                <>
                  Comments made by <i>supervisors</i> or <i>administrators</i>{" "}
                  on a <i>submission</i>. Comments are only visible in the
                  archive and approval view.
                </>
              )}
              {row(
                <i>External ID</i>,
                <>
                  An optional additional id associated with a specific
                  <i>submission</i>, distincts from the one generated by Domni.
                </>
              )}
              {row(
                <i>Cloning</i>,
                <>
                  The action of using an existing <i>submission</i> as a
                  template for a new submission <i>draft</i> associated with the
                  original <i>submitter</i>.
                </>
              )}
              {row(
                <i>Projects</i>,
                <>
                  A predefined list of items in a dropdown available for some{" "}
                  <i>form types</i>. Can be configured by <i>administrators</i>,
                  including the association of each item with a{" "}
                  <i>supervisor</i>. See{" "}
                  <button
                    className="link"
                    onClick={() => scrollIntoView("projects")}
                  >
                    Projects
                  </button>
                  .
                </>
              )}
              {row(
                <i>Custom lists</i>,
                <>
                  Dynamic lists of items in a dropdown available for some{" "}
                  <i>form types</i>. Can be created and configured by{" "}
                  <i>administrators</i>. See{" "}
                  <button
                    className="link"
                    onClick={() => scrollIntoView("customLists")}
                  >
                    Custom Lists
                  </button>
                  .
                </>
              )}
              {row(
                <i>Simple form</i>,
                <>
                  Refers to <i>form types</i> where all of the resolution,
                  reassignment and archiving features have been omitted in favor
                  of a very quick implementation of the form type.
                </>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <div
          style={{
            fontSize: "0.8em",
            marginTop: "3em",
            fontStyle: "italic",
            color: "#777",
            textAlign: "right",
          }}
        >
          Domni is written and maintained by the{" "}
          <DomniLink href="mailto:ims@maxiv.lu.se">IMS</DomniLink> group at MAX
          IV. Source code available{" "}
          <DomniLink href="https://gitlab.maxiv.lu.se/kits-ims/domni-frontend">
            here
          </DomniLink>{" "}
          and{" "}
          <DomniLink href="https://gitlab.maxiv.lu.se/kits-ims/domni-backend">
            here
          </DomniLink>
        </div>
      </Paper>
    </Container>
  );
};
const row = (...columnValues: Array<string | JSX.Element>) => {
  return (
    <TableRow>
      {columnValues.map((col) => (
        <TableCell>{col}</TableCell>
      ))}
    </TableRow>
  );
};
const TOCElement = (props: { id?: string; name: string }) => {
  return (
    <Grid item xs={12} sm={6} md={4}>
      <Button
        variant="text"
        className="link"
        style={{ letterSpacing: "1px" }}
        onClick={() => scrollIntoView(props.id || props.name.toLowerCase())}
      >
        {props.name}
      </Button>
    </Grid>
  );
};
const scrollIntoView = (id: string) => {
  const el: HTMLElement | null = document.querySelector(`#${id}`);
  if (el) {
    window.scroll({
      top: el.offsetTop - 80,
      left: 0,
      behavior: "smooth",
    });
    // el.scrollIntoView({
    //   behavior: "smooth",
    //   block: "center",
    // });
  }
};
export default Docs;
