import {
  Checkbox,
  Collapse,
  FormControlLabel,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import TextField from "@mui/material/TextField/TextField";
import { AddCircle } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import React, { useState } from "react";
import { SimpleSelect } from "../components/SimpleSelect";
import { DomniTextValidator } from "../components/TextValidator";
import * as Constants from "../constants";
import { GrayBlock } from "../utilities/Helpers";
import Form from "./Form";

const getEmptyFunding = () => {
  return {
    salaries: [0, 0, 0, 0, 0],
    investments: [0, 0, 0, 0, 0],
    travel: [0, 0, 0, 0, 0],
    others: [0, 0, 0, 0, 0],
    indirectCost: [0, 0, 0, 0, 0],
  };
};
const getEmptyCofunding = () => {
  return {
    salaries: [0, 0, 0, 0, 0],
    others: [0, 0, 0, 0, 0],
    indirectCost: [0, 0, 0, 0, 0],
  };
};

const getEmptyPerson = () => {
  return {
    inKind: false,
    firstName: "",
    lastName: "",
    years: [
      { salaryPercentage: 0, nbrMonths: 0 },
      { salaryPercentage: 0, nbrMonths: 0 },
      { salaryPercentage: 0, nbrMonths: 0 },
      { salaryPercentage: 0, nbrMonths: 0 },
      { salaryPercentage: 0, nbrMonths: 0 },
    ],
  };
};
const getEmptyGrantInitiativeForm = (): System.GrantInitiativeForm => {
  return {
    _id: "",
    formType: Constants.GRANT_INITIATIVE,
    submitterName: "",
    submitterEmail: "",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name: "MAX IV Finance",
        email: "finance@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    submissionDate: new Date(),
    isArchived: false,
    status: "UNSAVED",
    responsible: "",
    coordinator: "",
    otherParties: "",
    collaborators: "",
    fundingAgency: "",
    projectName: "",
    projectDescription: "",
    beamlineAccessType: "",
    officeSpaceCount: 0,
    pooledResource: "",
    ownership: "Yes",
    otherResources: "",
    strategyAlignment: "",
    startingYear: new Date().getFullYear(),
    people: [getEmptyPerson()],
    funding: getEmptyFunding(),
    cofunding: getEmptyCofunding(),
  };
};
function GrantInitiativeForm(props: System.FormProps) {
  const FormInputs = (
    props: System.FormInputsProps<System.GrantInitiativeForm>
  ) => {
    const { formData, updateFormData, handleInputChange, formAccess } = props;
    const [checkboxState, setCheckboxState] = useState<{
      beamlineAccessType: boolean;
      pooledResource: boolean;
      fundingTable: boolean;
      cofundingTable: boolean;
    }>({
      pooledResource: !!formData.pooledResource,
      beamlineAccessType: !!formData.beamlineAccessType,
      fundingTable: fundingSum(formData, "funding") > 0 || !formData._id,
      cofundingTable: fundingSum(formData, "cofunding") > 0 || !formData._id,
    });
    return (
      <>
        <Grid item xs={12}>
          <Typography className="form-heading">General Information</Typography>
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Name of project"
            onChange={handleInputChange}
            name="projectName"
            value={formData.projectName || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Grant responsible at MAX IV"
            onChange={handleInputChange}
            name="responsible"
            value={formData.responsible || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Coordinator/main applicant of grant"
            onChange={handleInputChange}
            name="coordinator"
            value={formData.coordinator || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Others involved at MAX IV"
            onChange={handleInputChange}
            name="otherParties"
            value={formData.otherParties || ""}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Collaborators/partners"
            onChange={handleInputChange}
            name="collaborators"
            value={formData.collaborators || ""}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Funding agency and call"
            onChange={handleInputChange}
            name="fundingAgency"
            value={formData.fundingAgency || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <DomniTextValidator
            fullWidth={true}
            label="Description of project"
            onChange={handleInputChange}
            name="projectDescription"
            value={formData.projectDescription || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
            multiline={true}
            rows={props.embedded ? undefined : 8}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">
            Resources needed from MAX IV
          </Typography>
        </Grid>

        <Grid item xs={12} md={6}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                data-testid="beamlineAccessType-checkbox"
                size="medium"
                disabled={!formAccess.canEdit}
                checked={checkboxState.beamlineAccessType}
                value={checkboxState.beamlineAccessType}
                onChange={() => {
                  if (checkboxState.beamlineAccessType) {
                    //clear beamlineAccessType
                    updateFormData({
                      ...formData,
                      beamlineAccessType: "",
                    });
                  }
                  setCheckboxState({
                    ...checkboxState,
                    beamlineAccessType: !checkboxState.beamlineAccessType,
                  });
                }}
              />
            }
            label="Beamline time"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                data-testid="pooledResource-checkbox"
                disabled={!formAccess.canEdit}
                checked={checkboxState.pooledResource}
                value={checkboxState.pooledResource}
                onChange={() => {
                  if (checkboxState.pooledResource) {
                    //clear pooledResource
                    updateFormData({
                      ...formData,
                      pooledResource: "",
                    });
                  }
                  setCheckboxState({
                    ...checkboxState,
                    pooledResource: !checkboxState.pooledResource,
                  });
                }}
              />
            }
            label="Pooled resources (such as engineering, KITS and similar)"
          />
        </Grid>
        <Grid item xs={12} md={6}>
          {checkboxState.beamlineAccessType ? (
            <DomniTextValidator
              disabled={!checkboxState.beamlineAccessType}
              name="beamlineAccessType"
              label="Describe type of access"
              value={formData.beamlineAccessType || ""}
              onChange={handleInputChange}
              fullWidth={true}
              variant="outlined"
              multiline={true}
              rows={props.embedded ? undefined : 5}
            />
          ) : (
            <GrayBlock height="9.3em" />
          )}
        </Grid>
        <Grid item xs={12} md={6}>
          {checkboxState.pooledResource ? (
            <DomniTextValidator
              disabled={!checkboxState.pooledResource}
              style={{
                opacity: checkboxState.pooledResource ? "100%" : "30%",
              }}
              name="pooledResource"
              label="Describe required resources"
              value={formData.pooledResource || ""}
              onChange={handleInputChange}
              fullWidth={true}
              variant="outlined"
              multiline={true}
              rows={props.embedded ? undefined : 5}
            />
          ) : (
            <GrayBlock height="9.3em" />
          )}
        </Grid>
        <Tooltip
          title="Ownership/right of use of equipment and methods developed that is part of the project"
          placement="top-start"
        >
          <Grid item xs={12} md={4}>
            <SimpleSelect
              disabled={{
                value: !formAccess.canEdit,
              }}
              options={[{ value: "Yes" }, { value: "No" }, { value: "N/A" }]}
              selectedValue={formData.ownership}
              testid="ownership-select"
              title="Ownership"
              name="ownership"
              onChange={(event) => {
                handleInputChange(event);
              }}
            />
          </Grid>
        </Tooltip>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Number of office spaces"
            type="number"
            onChange={handleInputChange}
            name="officeSpaceCount"
            value={formData.officeSpaceCount || 0}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Other"
            onChange={handleInputChange}
            name="otherResources"
            value={formData.otherResources || ""}
            variant="outlined"
          />
        </Grid>

        <Grid item xs={12}>
          <Typography className="form-heading">
            Five year budget tables
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={checkboxState.fundingTable}
                value={checkboxState.fundingTable}
                onChange={() => {
                  if (checkboxState.fundingTable) {
                    //clear funding
                    updateFormData({
                      ...formData,
                      funding: getEmptyFunding(),
                    });
                  }
                  setCheckboxState({
                    ...checkboxState,
                    fundingTable: !checkboxState.fundingTable,
                  });
                }}
              />
            }
            label="Will MAX IV be a partial or full recipient of the grant?"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse
            style={{ padding: "1em", paddingTop: "0em" }}
            in={checkboxState.fundingTable}
          >
            <div
              style={{
                fontStyle: "italic",
                color: "#777",
                fontSize: "0.9em",
              }}
            >
              Please only specify the amounts which applies to MAX IV
            </div>
            <TableContainer>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ fontWeight: "normal" }}>
                      Funding type \ Year
                    </TableCell>
                    {[0, 1, 2, 3, 4].map((i) => (
                      <TableCell
                        key={i}
                        style={{
                          fontWeight: "normal",
                          textAlign: "right",
                          paddingRight: "1em",
                        }}
                      >
                        {formData.startingYear + i}
                      </TableCell>
                    ))}
                    <TableCell style={{ textAlign: "right" }}>
                      <b>Total per type</b>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>Salaries</TableCell>
                    {formData.funding.salaries.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`funding-salaries-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const salaries = [...formData.funding.salaries];
                            salaries[index] = parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              funding: {
                                ...formData.funding,
                                salaries,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.funding.salaries.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Investments</TableCell>
                    {formData.funding.investments.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`funding-investments-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const investments = [
                              ...formData.funding.investments,
                            ];
                            investments[index] =
                              parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              funding: {
                                ...formData.funding,
                                investments,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.funding.investments.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Travel</TableCell>
                    {formData.funding.travel.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`funding-travel-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const travel = [...formData.funding.travel];
                            travel[index] = parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              funding: {
                                ...formData.funding,
                                travel,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.funding.travel.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Others</TableCell>
                    {formData.funding.others.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`funding-others-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const others = [...formData.funding.others];
                            others[index] = parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              funding: {
                                ...formData.funding,
                                others,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.funding.others.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <Tooltip
                        title="Learn more about indirect costs (opens in new window)"
                        placement="left"
                        arrow
                      >
                        <a
                          className="link"
                          target="_blank"
                          rel="noopener noreferrer"
                          tabIndex={-1}
                          href="https://intranet.maxiv.lu.se/administration/finance/accounting/indirecta-kostnader/"
                        >
                          Indirect costs
                        </a>
                      </Tooltip>
                    </TableCell>
                    {formData.funding.indirectCost.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`funding-indirectCosts-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const indirectCost = [
                              ...formData.funding.indirectCost,
                            ];
                            indirectCost[index] =
                              parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              funding: {
                                ...formData.funding,
                                indirectCost,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.funding.indirectCost.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell style={{ fontWeight: "bold" }}>
                      Total per year
                    </TableCell>
                    {[0, 1, 2, 3, 4].map((index) =>
                      getTotalTabelCell(formData, "funding", "1.5em", index)
                    )}
                    <TableCell
                      style={{
                        fontWeight: "bold",
                        textAlign: "right",
                        padding: "1.5em",
                        paddingRight: "0.5em",
                      }}
                    >
                      {fundingSum(formData, "funding")}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <div className="table-total">
              Grand total <b>{fundingSum(formData, "funding")}</b> SEK
            </div>
          </Collapse>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={checkboxState.cofundingTable}
                value={checkboxState.cofundingTable}
                onChange={() => {
                  if (checkboxState.cofundingTable) {
                    //clear cofunding
                    updateFormData({
                      ...formData,
                      cofunding: getEmptyCofunding(),
                    });
                  }
                  setCheckboxState({
                    ...checkboxState,
                    cofundingTable: !checkboxState.cofundingTable,
                  });
                }}
              />
            }
            label="Will the project require any co-funding from MAX IV?"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse
            style={{ padding: "1em", paddingTop: "0em" }}
            in={checkboxState.cofundingTable}
          >
            <div
              style={{
                fontStyle: "italic",
                color: "#777",
                fontSize: "0.9em",
              }}
            >
              Please fill in the costs that needs to be co-funded by MAX IV
            </div>
            <TableContainer>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ fontWeight: "normal" }}>
                      Cofunding type \ Year
                    </TableCell>
                    {[0, 1, 2, 3, 4].map((i) => (
                      <TableCell
                        key={i}
                        style={{
                          fontWeight: "normal",
                          textAlign: "right",
                          paddingRight: "1em",
                        }}
                      >
                        {formData.startingYear + i}
                      </TableCell>
                    ))}
                    <TableCell style={{ textAlign: "right" }}>
                      <b>Total per type</b>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>Salaries in-kind</TableCell>
                    {formData.cofunding.salaries.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`cofunding-salaries-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const salaries = [...formData.cofunding.salaries];
                            salaries[index] = parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              cofunding: {
                                ...formData.cofunding,
                                salaries,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.cofunding.salaries.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Others</TableCell>
                    {formData.cofunding.others.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`cofunding-others-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const others = [...formData.cofunding.others];
                            others[index] = parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              cofunding: {
                                ...formData.cofunding,
                                others,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.cofunding.others.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>
                      <Tooltip
                        title="Learn more about indirect costs (opens in new window)"
                        placement="left"
                        arrow
                      >
                        <a
                          className="link"
                          target="_blank"
                          rel="noopener noreferrer"
                          tabIndex={-1}
                          href="https://intranet.maxiv.lu.se/administration/finance/accounting/indirecta-kostnader/"
                        >
                          Indirect costs
                        </a>
                      </Tooltip>
                    </TableCell>
                    {formData.cofunding.indirectCost.map((salary, index) => (
                      <TableCell style={{ textAlign: "right" }} key={index}>
                        <TextField
                          type="number"
                          inputProps={{ style: { textAlign: "right" } }}
                          style={{ width: "6em" }}
                          variant="outlined"
                          size="small"
                          data-testid={`cofunding-indirectCosts-${index}`}
                          onFocus={(event) => (event.target as any).select()}
                          value={salary}
                          onChange={(e) => {
                            const indirectCost = [
                              ...formData.cofunding.indirectCost,
                            ];
                            indirectCost[index] =
                              parseInt(e.target.value, 10) || 0;
                            updateFormData({
                              ...formData,
                              cofunding: {
                                ...formData.cofunding,
                                indirectCost,
                              },
                            });
                          }}
                        />
                      </TableCell>
                    ))}
                    <TableCell
                      style={{ fontWeight: "bold", textAlign: "right" }}
                    >
                      {formData.cofunding.indirectCost.reduce(
                        (prev: number, curr: number) => prev + curr
                      )}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell style={{ fontWeight: "bold" }}>
                      Total per year
                    </TableCell>
                    {[0, 1, 2, 3, 4].map((index) =>
                      getTotalTabelCell(formData, "cofunding", "1.5em", index)
                    )}
                    <TableCell
                      style={{
                        fontWeight: "bold",
                        textAlign: "right",
                        padding: "1.5em",
                        paddingRight: "0.5em",
                      }}
                    >
                      {fundingSum(formData, "cofunding")}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <div className="table-total">
              Grand total <b>{fundingSum(formData, "cofunding")}</b> SEK
            </div>
          </Collapse>
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">
            Salary Specification | Positions{" "}
            {formAccess.canEdit && (
              <Tooltip title="Add a new position" placement="top" arrow>
                <IconButton
                  onClick={() => {
                    const copy = { ...formData };
                    copy.people.push(getEmptyPerson());
                    updateFormData(copy);
                  }}
                  aria-label="add"
                  size="large"
                >
                  <AddCircle />
                </IconButton>
              </Tooltip>
            )}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          {formData.people.map((person, personIndex) => {
            return (
              <Paper
                key={personIndex}
                className="paper"
                style={{ marginTop: "0em", paddingTop: "1em" }}
                elevation={2}
              >
                <div
                  style={{
                    fontWeight: "bold",
                    color: "#777",
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <span style={{ alignSelf: "center" }}>
                    POSITION {personIndex + 1 + " "}
                  </span>
                  {formAccess.canEdit && (
                    <Tooltip title="Remove this position" placement="top" arrow>
                      <IconButton
                        onClick={() => {
                          const copy = { ...formData };
                          copy.people.splice(personIndex, 1);
                          updateFormData(copy);
                        }}
                        aria-label="delete"
                        size="large"
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  )}
                </div>
                <Grid container spacing={2} style={{ padding: "2em 0em" }}>
                  <Grid item xs={12} md={6} lg={4}>
                    <DomniTextValidator
                      fullWidth={true}
                      label="First name"
                      onChange={(
                        e: React.ChangeEvent<{
                          name?: string | undefined;
                          value: string;
                        }>
                      ) => {
                        const copy = { ...formData };
                        copy.people[personIndex].firstName = e.target.value;
                        updateFormData(copy);
                      }}
                      testId={`person-${personIndex}-firstName`}
                      name="firstName"
                      value={person.firstName || ""}
                      validators={["required"]}
                      errorMessages={["This field is required"]}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={12} md={6} lg={4}>
                    <DomniTextValidator
                      fullWidth={true}
                      label="Last name"
                      onChange={(
                        e: React.ChangeEvent<{
                          name?: string | undefined;
                          value: string;
                        }>
                      ) => {
                        const copy = { ...formData };
                        copy.people[personIndex].lastName = e.target.value;
                        updateFormData(copy);
                      }}
                      testId={`person-${personIndex}-lastName`}
                      name="lastName"
                      value={person.lastName || ""}
                      validators={["required"]}
                      errorMessages={["This field is required"]}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={12} lg={4}>
                    <FormControlLabel
                      className="link"
                      control={
                        <Checkbox
                          size="medium"
                          value={person.inKind}
                          disabled={!formAccess.canEdit}
                          checked={person.inKind}
                          onChange={(e) => {
                            const copy = { ...formData };
                            copy.people[personIndex].inKind =
                              !copy.people[personIndex].inKind;
                            updateFormData(copy);
                          }}
                        />
                      }
                      label="This is an in-kind position"
                    />
                  </Grid>
                </Grid>
                <TableContainer>
                  <Table style={{ padding: "1em" }}>
                    <TableHead>
                      <TableRow>
                        <TableCell>Type \ Year</TableCell>
                        {[0, 1, 2, 3, 4].map((i) => (
                          <TableCell
                            key={i}
                            style={{
                              fontWeight: "normal",
                              textAlign: "right",
                            }}
                          >
                            {formData.startingYear + i}
                          </TableCell>
                        ))}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell style={{ width: "10em" }}>
                          <div
                            style={{
                              display: "block",
                            }}
                          >
                            Percentage working
                          </div>
                          <div
                            style={{
                              display: "block",
                              paddingTop: "2em",
                            }}
                          >
                            No. months working
                          </div>
                        </TableCell>
                        {person.years.map((year, yearIndex) => {
                          return (
                            <TableCell
                              key={yearIndex}
                              style={{ textAlign: "right" }}
                            >
                              <Tooltip
                                title={"Provide a value between 0 and 100"}
                                placement="top"
                                arrow
                              >
                                <DomniTextValidator
                                  style={{
                                    display: "block",
                                    marginBottom: "1em",
                                    width: "6em",
                                    marginLeft: "auto",
                                    marginRight: "0",
                                  }}
                                  inputProps={{
                                    style: { textAlign: "right" },
                                  }}
                                  type="number"
                                  size="small"
                                  onFocus={(event) =>
                                    (event.target as any).select()
                                  }
                                  name={yearIndex + "_percentage"}
                                  value={year.salaryPercentage}
                                  validators={["minNumber:0", "maxNumber:100"]}
                                  errorMessages=""
                                  onChange={(
                                    e: React.ChangeEvent<{
                                      name?: string | undefined;
                                      value: string;
                                    }>
                                  ) => {
                                    const copy = { ...formData };
                                    copy.people[personIndex].years[
                                      yearIndex
                                    ].salaryPercentage =
                                      parseInt(e.target.value) || 0;
                                    updateFormData(copy);
                                  }}
                                />
                              </Tooltip>
                              <Tooltip
                                title={"Provide a value between 0 and 12"}
                                placement="top"
                                arrow
                              >
                                <DomniTextValidator
                                  style={{
                                    display: "block",
                                    marginBottom: "1em",
                                    width: "6em",
                                    marginLeft: "auto",
                                    marginRight: "0",
                                  }}
                                  type="number"
                                  size="small"
                                  inputProps={{
                                    style: { textAlign: "right" },
                                  }}
                                  onFocus={(event) =>
                                    (event.target as any).select()
                                  }
                                  name={yearIndex + "_months"}
                                  value={year.nbrMonths}
                                  validators={["minNumber:0", "maxNumber:12"]}
                                  errorMessages=""
                                  onChange={(
                                    e: React.ChangeEvent<{
                                      name?: string | undefined;
                                      value: string;
                                    }>
                                  ) => {
                                    const copy = { ...formData };
                                    copy.people[personIndex].years[
                                      yearIndex
                                    ].nbrMonths = parseInt(e.target.value) || 0;
                                    updateFormData(copy);
                                  }}
                                />
                              </Tooltip>
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </Paper>
            );
          })}
        </Grid>
        <Grid item xs={12}>
          <Tooltip
            title="Describe how this project align with the MAX IV strategy"
            placement="top"
            arrow
          >
            <DomniTextValidator
              fullWidth={true}
              label="MAX IV Strategy alignment"
              onChange={handleInputChange}
              name="strategyAlignment"
              value={formData.strategyAlignment || ""}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
              multiline={true}
              rows={props.embedded ? undefined : 5}
            />
          </Tooltip>
        </Grid>
      </>
    );
  };
  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={getEmptyGrantInitiativeForm()}
      formType="GRANT_INITIATIVE"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
}

export const fundingSum = (
  form: System.GrantInitiativeForm,
  type: "funding" | "cofunding"
) => {
  let sum = 0;
  //OMG typescript please
  (
    Object.keys(form[type]) as ("salaries" | "others" | "indirectCost")[]
  ).forEach((key) => {
    sum += form[type][key].reduce((prev: number, curr: number) => prev + curr);
  });
  return sum;
};
export const getTotalTabelCell = (
  form: System.GrantInitiativeForm,
  type: "funding" | "cofunding",
  padding: string,
  i: number
) => {
  let sum = 0;
  switch (type) {
    case "funding":
      sum =
        form.funding.salaries[i] +
        form.funding.investments[i] +
        form.funding.travel[i] +
        form.funding.others[i] +
        form.funding.indirectCost[i];
      break;
    case "cofunding":
      sum =
        form.cofunding.salaries[i] +
        form.cofunding.others[i] +
        form.cofunding.indirectCost[i];
      break;
  }
  return (
    <TableCell
      key={i}
      style={{
        fontWeight: "bold",
        textAlign: "right",
        padding,
      }}
    >
      {sum}
    </TableCell>
  );
};

export default GrantInitiativeForm;
