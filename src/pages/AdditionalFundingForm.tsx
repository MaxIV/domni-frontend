import {
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import { SelectValidator } from "react-material-ui-form-validator";
import { DomniTextValidator } from "../components/TextValidator";
import { ADDITIONAL_FUNDING } from "../constants";
import { selectSupervisors } from "../store/Reducer";
import { useStore } from "../store/Store";
import Form from "./Form";

const beamlines = [
  "Balder",
  "BioMAX",
  "BLOCH",
  "CoSAXS",
  "DanMAX",
  "FemtoMAX",
  "FinEstBeaMS",
  "FlexPES",
  "HIPPIE",
  "MaxPEEM",
  "NanoMAX",
  "SPECIES",
  "Veritas",
];

const getEmptyAdditionalFundingForm = (): System.AdditionalFundingForm => {
  return {
    _id: "",
    formType: ADDITIONAL_FUNDING,
    submitterName: "",
    submitterEmail: "",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name: "Julia Hansson",
        email: "julia.hansson@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    status: "UNSAVED",
    submissionDate: new Date(),
    isArchived: false,
    responsibleDirector: "",
    applicant: "",
    organizationalUnitType: "Beamline",
    organizationalUnitName: "",
    totalAmountRequested: 0,
    investmentDescription: "",
    investmentBackground: "",
    contingencyAmountRemaining: 0,
    totalAmountApproved: 0,
    fundingSource: "",
  };
};

const useStyles = makeStyles((theme) => ({
  organizationalUnitType: {
    display: "flex",
    [(theme as any).breakpoints.up("md")]: {
      justifyContent: "flex-end",
    },
  },
}));

export default function AdditionalFundingForm(props: System.FormProps) {
  const classes = useStyles();
  const { state } = useStore();
  const supervisors = selectSupervisors(state, ADDITIONAL_FUNDING);

  const FormInputs = (
    props: System.FormInputsProps<System.AdditionalFundingForm>
  ) => {
    const { formData, updateFormData, handleInputChange, formAccess } = props;
    return (
      <>
        <Grid item xs={12}>
          <Typography className="form-heading">Applicant Details</Typography>
        </Grid>
        <Grid item xs={12} md={4} lg={5}>
          <Tooltip
            title="Only required to fill in if applicant is not same as submitter"
            placement="top-start"
          >
            <DomniTextValidator
              label="Applicant name"
              name="applicant"
              value={formData.applicant}
              onChange={handleInputChange}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <RadioGroup
            row
            className={classes.organizationalUnitType}
            name="organizationalUnitType"
            value={formData.organizationalUnitType}
            onChange={(event) => {
              const organizationalUnitType = event.target.value as
                | "Beamline"
                | "Group";
              updateFormData({
                ...formData,
                organizationalUnitType,
                organizationalUnitName: "",
              });
            }}
          >
            <FormControlLabel
              value="Beamline"
              control={<Radio disabled={!formAccess.canEdit} />}
              label="Beamline"
            />
            <FormControlLabel
              value="Group"
              control={<Radio disabled={!formAccess.canEdit} />}
              label="Group"
            />
          </RadioGroup>
        </Grid>
        <Grid item xs={12} md={4}>
          {formData.organizationalUnitType === "Beamline" ? (
            <SelectValidator
              fullWidth
              name="organizationalUnitName"
              value={formData.organizationalUnitName}
              variant="outlined"
              onChange={handleInputChange}
              SelectProps={{
                native: true,
              }}
              validators={["required"]}
              errorMessages={["This field is required"]}
            >
              <option value="" disabled>
                Beamline
              </option>
              {beamlines.map((beamline, index) => (
                <option key={index} value={beamline}>
                  {beamline}
                </option>
              ))}
            </SelectValidator>
          ) : (
            <DomniTextValidator
              fullWidth={true}
              label="Group"
              name="organizationalUnitName"
              value={formData.organizationalUnitName}
              onChange={handleInputChange}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
            />
          )}
        </Grid>
        <Grid item xs={12}>
          <div className="form-heading">Funding Requested</div>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <DomniTextValidator
            fullWidth={true}
            type="number"
            label="Total amount requested (SEK)"
            name="totalAmountRequested"
            value={formData.totalAmountRequested}
            onFocus={(event) => (event.target as any).select()}
            onChange={handleInputChange}
            validators={["required", "minNumber:1"]}
            errorMessages={[
              "This field is required",
              "The amount must be greater than zero",
            ]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <SelectValidator
            fullWidth
            name="responsibleDirector"
            variant="outlined"
            onChange={handleInputChange}
            value={formData.responsibleDirector}
            SelectProps={{
              native: true,
            }}
            validators={["required"]}
            errorMessages={["This field is required"]}
          >
            <option value="" disabled>
              Responsible director
            </option>
            {supervisors.map((supervisor, index) => (
              <option key={index} value={supervisor.name}>
                {supervisor.name}
              </option>
            ))}
          </SelectValidator>
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">
            Purchase / Investment
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography>
            <small>
              Include the specifics of the purchase / investment in the text
              boxes below or upload a file containing the requested information.
            </small>
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth={true}
            label="Description of the purchase / investment"
            onChange={handleInputChange}
            name="investmentDescription"
            value={formData.investmentDescription}
            variant="outlined"
            multiline={true}
            rows={props.embedded ? undefined : 5}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth={true}
            label="Background and detailed cost breakdown"
            onChange={handleInputChange}
            name="investmentBackground"
            value={formData.investmentBackground}
            variant="outlined"
            multiline={true}
            rows={props.embedded ? undefined : 5}
          />
        </Grid>
        {(formAccess.isSupervisor || formAccess.isAdmin) && (
          <Grid item>
            <Grid container className="supervisor-form-section">
              <Grid item xs={12} className="admin-segment-grid-item">
                <div className="admin-segment-form-heading">
                  Input from Finance Team
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                md={7}
                lg={5}
                className="admin-segment-grid-item"
              >
                <DomniTextValidator
                  fullWidth={true}
                  type="number"
                  label="Remaining budget Directors contingency after transaction (SEK)"
                  name="contingencyAmountRemaining"
                  value={formData.contingencyAmountRemaining}
                  onFocus={(event) => (event.target as any).select()}
                  onChange={handleInputChange}
                  validators={["required", "minNumber:0"]}
                  errorMessages={[
                    "This field is required",
                    "The amount must be zero or greater",
                  ]}
                  variant="outlined"
                />
              </Grid>

              <Grid item xs={12} className="admin-segment-grid-item">
                <div className="admin-segment-form-heading ">
                  Decision by Directors
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                md={7}
                lg={5}
                className="admin-segment-grid-item"
              >
                <DomniTextValidator
                  fullWidth={true}
                  type="number"
                  label="Total amount approved (SEK)"
                  name="totalAmountApproved"
                  value={formData.totalAmountApproved}
                  onFocus={(event) => (event.target as any).select()}
                  onChange={handleInputChange}
                  validators={["required", "minNumber:0"]}
                  errorMessages={[
                    "This field is required",
                    "The amount must be zero or greater",
                  ]}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} className="admin-segment-grid-item">
                <Typography>Funding Source</Typography>
                <RadioGroup
                  row
                  name="fundingSource"
                  value={formData.fundingSource}
                  onChange={handleInputChange}
                >
                  <FormControlLabel
                    value="Upkeep"
                    control={<Radio />}
                    label="Upkeep"
                  />
                  <FormControlLabel
                    value="DM Ex contingency"
                    control={<Radio />}
                    label="Directors contingency"
                  />
                  <FormControlLabel
                    value="DM Ex contingency (out of budget)"
                    control={<Radio />}
                    label="Directors contingency (out of budget)"
                  />
                </RadioGroup>
              </Grid>
            </Grid>
          </Grid>
        )}
      </>
    );
  };
  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={getEmptyAdditionalFundingForm()}
      formType="ADDITIONAL_FUNDING"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
}
