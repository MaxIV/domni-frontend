import {
  Checkbox,
  Collapse,
  FormControlLabel,
  Grid,
  TextField,
  Tooltip,
  Typography,
  Select,
  MenuItem,
  IconButton,
  Autocomplete,
} from "@mui/material";
import React, { useState } from "react";
import AddCircle from "@mui/icons-material/AddCircle";
import { DynamicInputTable } from "../components/DynamicInputTable";
import { EquipmentRiskTable } from "../components/EquipmentRiskTable";
import { ExperimentRiskTable } from "../components/ExperimentRiskTable";
import { SimpleSelect } from "../components/SimpleSelect";
import * as Constants from "../constants";
import { selectList, selectSupervisor } from "../store/Reducer";
import { useStore } from "../store/Store";
import Form from "./Form";
import { StaffSuggestion } from "../components/StaffSuggestion";
import { GrayBlock } from "../utilities/Helpers";
import { DomniTextValidator } from "../components/TextValidator";
import { DatePicker } from "../components/DateWidgets";

const emptyRiskAssessmentForm = (
  state: System.IState
): System.RiskAssessmentForm => {
  return {
    _id: "",
    formType: Constants.RISK_ASSESSMENT,
    submissionDate: new Date(),
    submitterName: "",
    submitterEmail: "",
    status: "UNSAVED",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name:
          selectSupervisor(state, "Default", Constants.RISK_ASSESSMENT)?.name ||
          "MAX IV Safety group",
        email:
          selectSupervisor(state, "Default", Constants.RISK_ASSESSMENT)
            ?.email || "est@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    isArchived: false,
    proposalId: "",
    title: "",
    safetyClassification: "",
    hasDates: true,
    startDate: new Date(),
    endDate: new Date(),
    beamline: "",
    contactName: "",
    contactEmail: "",
    contactPhone: "",
    proposerName: "",
    proposerEmail: "",
    proposerPhone: "",
    PIName: "",
    PIEmail: "",
    PIPhone: "",
    companyName: "",
    significantModifications: "",
    sampleRemoved: "MAX IV",
    labAccessRequired: false,
    additionalChemicalProperties: "",
    additionalNanoProperties: "",
    equipments: {
      vessles: { mitigations: "", checks: [] },
      heater: { mitigations: "", checks: [] },
      cryostat: { mitigations: "", checks: [] },
      magnetic: { mitigations: "", checks: [] },
      electrochemical: { mitigations: "", checks: [] },
      laser: { mitigations: "", checks: [] },
      lamps: { mitigations: "", checks: [] },
      sonic: { mitigations: "", checks: [] },
      blowtorch: { mitigations: "", checks: [] },
      ribbon: { mitigations: "", checks: [] },
    },
    customBuiltEquipmentDescription: "",
    chemicalSamples: [],
    nanoSamples: [],
    bioSamples: [],
    experimentDetails: {
      lab: { details: "", risks: "", mitigation: "", emergencyAction: "" },
      beamline: { details: "", risks: "", mitigation: "", emergencyAction: "" },
      other: { details: "", risks: "", mitigation: "", emergencyAction: "" },
    },
    hasSignificantModifications: false,
    hasHazardousEquipment: false,
    hasOwnEquipment: false,
    hasChemicalSamples: false,
    hasNanoSamples: false,
    hasBioSamples: false,
    hasCMRCompound: false,
  };
};
const RiskAssessment = (props: System.FormProps) => {
  const { state: initialState } = useStore();

  const FormInputs = (
    props: System.FormInputsProps<System.RiskAssessmentForm>
  ) => {
    const {
      formData,
      updateFormData,
      handleInputChange,
      formAccess,
      embedded,
    } = props;
    const beamlineList = selectList({
      state: initialState,
      form: "RISK_ASSESSMENT",
      name: "Beamlines",
      defaultValues: [
        "No beamline has been defined. Please contact est@maxiv.lu.se",
      ],
    });
    const handleBioChange = (
      column: keyof System.BioSample,
      row: number,
      value: string
    ) => {
      const updatedBioSamples = formData.bioSamples.map((_, index) => {
        if (index === row) {
          return { ...formData.bioSamples[index], [column]: value };
        }
        return formData.bioSamples[index];
      });
      updateFormData({
        ...formData,
        bioSamples: updatedBioSamples,
      });
    };
    const handleChemicalChange = (
      column: keyof System.ChemicalSample,
      row: number,
      value: string | string[]
    ) => {
      const updatedChemicalSamples = formData.chemicalSamples.map(
        (_, index) => {
          if (index === row) {
            return { ...formData.chemicalSamples[index], [column]: value };
          }
          return formData.chemicalSamples[index];
        }
      );
      updateFormData({
        ...formData,
        chemicalSamples: updatedChemicalSamples,
      });
    };
    const handleNanoChange = (
      column: keyof System.NanoSample,
      row: number,
      value: string
    ) => {
      const updatedNanoSamples = formData.nanoSamples.map((_, index) => {
        if (index === row) {
          return { ...formData.nanoSamples[index], [column]: value };
        }
        return formData.nanoSamples[index];
      });
      updateFormData({
        ...formData,
        nanoSamples: updatedNanoSamples,
      });
    };
    return (
      <>
        {formAccess.isAdmin && (
          <Grid item style={{ margin: "1em 0" }} xs={12}>
            <Grid container className="supervisor-form-section">
              <Grid item xs={12}>
                <div className="admin-segment-form-heading">
                  Admin Evaluation
                </div>
                <small>
                  This section is only visible to Risk Assessment administrators
                </small>
              </Grid>
              <Grid item xs={12} sm={3} className="admin-segment-grid-item">
                <SimpleSelect
                  disabled={{ value: !formAccess.canEdit }}
                  options={[
                    { value: "unspecified", label: "Unspecified" },
                    { value: "green", label: "Green" },
                    { value: "yellow", label: "Yellow" },
                    { value: "red", label: "Red" },
                  ]}
                  selectedValue={formData.safetyClassification}
                  title="Safety Classification"
                  name="safetyClassification"
                  testid="safety-classification"
                  onChange={(event) => handleInputChange(event)}
                />
              </Grid>
              <Grid item xs={12} sm={4} style={{ alignSelf: "center" }}>
                <small>
                  The safety classification is also visible when hovering over a
                  submission in the archive view
                </small>
              </Grid>
            </Grid>
          </Grid>
        )}
        <Grid item xs={12}>
          <Typography className="form-heading">Basic Information</Typography>
        </Grid>
        <Grid item sm={3} xs={12}>
          <Tooltip
            title="The identification number of this proposal in the MAX IV Digital User Office"
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              name="proposalId"
              type="number"
              label="DUO Proposal ID"
              value={formData.proposalId}
              onChange={handleInputChange}
              validators={[
                "required",
                "minNumber:20000000",
                "maxNumber:29999999",
              ]}
              errorMessages={[
                "You have to provide a proposal ID",
                "This is not a valid proposal id",
                "This is not a valid proposal id",
              ]}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item sm={3} xs={12}>
          <Tooltip
            title="The beamline or area where this experiment will be performed"
            placement="top-start"
          >
            <Autocomplete
              freeSolo
              clearOnBlur
              autoSelect
              autoHighlight
              disabled={!formAccess.canEdit}
              value={formData.beamline}
              onChange={(_, selected) => {
                let beamline: string = "";
                if (typeof selected === "string") {
                  beamline = selected;
                } else {
                  beamline = selected?.value || "";
                }
                updateFormData({
                  ...formData,
                  beamline,
                });
              }}
              options={beamlineList.items}
              renderInput={(params) => (
                <TextField {...params} label="Beamline or area" />
              )}
            />
          </Tooltip>
        </Grid>
        <Grid item sm={6} xs={12}>
          <Tooltip
            title="The title of the experiment or method"
            placement="top-start"
          >
            <TextField
              fullWidth={true}
              name="title"
              label="Title"
              value={formData.title}
              onChange={(event) => handleInputChange(event)}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Tooltip
          title="The planned start and end date of the experiment"
          placement="top-start"
        >
          <Grid item sm={6} xs={12}>
            {formData.hasDates ? (
              <DatePicker
                disabled={!formAccess.canEdit}
                start={{
                  label: "Start date",
                  name: "startDate",
                  date: formData.startDate,
                  setDate: (date: Date | undefined) =>
                    updateFormData({ ...formData, startDate: date }),
                }}
                end={{
                  label: "End date",
                  name: "endDate",
                  date: formData.endDate,
                  setDate: (date: Date | undefined) =>
                    updateFormData({ ...formData, endDate: date }),
                }}
              />
            ) : (
              <GrayBlock />
            )}
          </Grid>
        </Tooltip>
        <Grid item sm={4} xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={!formData.hasDates}
                name="hasDates"
                onChange={(e) => {
                  const checked = e.target.checked;
                  updateFormData({
                    ...formData,
                    hasDates: !checked,
                  });
                }}
              />
            }
            label="The dates have not yet been scheduled"
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Main Proposer</Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="proposerName"
            label="Full name"
            value={formData.proposerName}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="proposerEmail"
            label="Email"
            value={formData.proposerEmail}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="proposerPhone"
            label="Phone number"
            value={formData.proposerPhone}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">
            Leading Principal Investigator On-Site
          </Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="PIName"
            label="Full name"
            value={formData.PIName}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="PIEmail"
            label="Email"
            value={formData.PIEmail}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="PIPhone"
            label="Phone number"
            value={formData.PIPhone}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid
          item
          xs={12}
          style={{
            display: "flex",
            // justifyContent: "space-between",
            alignItems: "flex-end",
            marginTop: "1em",
          }}
        >
          <Typography style={{ fontSize: "1.25em" }}>
            Local MAX IV Contact
          </Typography>
          <StaffSuggestion
            size="small"
            placeholder={"Search"}
            onChange={(value: System.LDAPPerson | null) => {
              updateFormData({
                ...formData,
                contactName: value ? value.fullName : "",
                contactEmail: value ? value.email : "",
                contactPhone: value ? value.phone : "",
              });
            }}
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="contactName"
            label="Full name"
            value={formData.contactName}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="contactEmail"
            label="Email"
            value={formData.contactEmail}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <TextField
            fullWidth={true}
            name="contactPhone"
            label="Phone number"
            value={formData.contactPhone}
            onChange={(event) => handleInputChange(event)}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Tooltip
            title="If this is an industrial user, please provide a company name"
            placement="top-start"
          >
            <TextField
              fullWidth={true}
              name="companyName"
              label="Company name"
              value={formData.companyName}
              onChange={(event) => handleInputChange(event)}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        {/* SIGNIFICANT MODITICATIONS AND CRM*/}
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasSignificantModifications}
                name="hasSignificantModifications"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="Significant modifications have been made to the proposal description from the original proposal"
          />
        </Grid>
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <Collapse in={formData.hasSignificantModifications}>
            <div style={{ borderLeft: "4px solid  #de930038", padding: "1em" }}>
              <small>
                Please give a brief outline of the significant changes between
                the original proposal and the current state e.g. addition of new
                samples, new equipment, change of the experimental environmental
                conditions etc.
              </small>
              <TextField
                fullWidth={true}
                name="significantModifications"
                value={formData.significantModifications}
                onChange={(event) => handleInputChange(event)}
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 3}
              />
            </div>
          </Collapse>
        </Grid>

        {/* HAZARDOUS EQUIPMENT */}
        <Grid item xs={12}>
          <Typography className="form-heading">Experiment Equipment</Typography>
        </Grid>
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasHazardousEquipment}
                name="hasHazardousEquipment"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="The experiment will involve the use of hazardous equipment at MAX IV"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse in={formData.hasHazardousEquipment}>
            <EquipmentRiskTable
              formData={formData}
              updateFormData={updateFormData}
              disabled={!formAccess.canEdit || embedded}
              embedded={embedded}
            />
          </Collapse>
        </Grid>
        {/* OWN EQUIPMENT */}
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasOwnEquipment}
                name="hasOwnEquipment"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="The experiment involves bringing our own equipment to MAX IV"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse in={formData.hasOwnEquipment}>
            <div
              style={{ borderLeft: "4px solid  #de930038", paddingLeft: "1em" }}
            >
              <small>
                If you plan on bringing your own equipment to be used in your
                experiment, please provide a detailed description below. We
                request that you:
                <ul>
                  <li>
                    State if the equipment is commercially available or it is
                    custom built
                  </li>
                  <li>
                    Supply a manual/technical data sheet or provide a link to
                    the equipment online if available (In English)
                  </li>
                  <li>
                    Make sure that the equipment complies with relevant
                    technical standards and be in correct operating conditions
                  </li>
                  <li>
                    Include pictures, schemes, PI&Ds or similar, as well as
                    specific equipment risk assessment if available
                  </li>
                  <li>
                    Accept that the equipment might need to be checked and
                    approved by MAX IV staff on-site before it can be used
                  </li>
                  <li>
                    Use the <i>File upload</i> feature at the end of this form
                    to attach files
                  </li>
                </ul>
              </small>
              <TextField
                fullWidth={true}
                label="Equipment description"
                name="customBuiltEquipmentDescription"
                value={formData.customBuiltEquipmentDescription}
                onChange={(event) => handleInputChange(event)}
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 6}
              />
            </div>
          </Collapse>
        </Grid>
        {/* SAMPLES - NANOMATERIALS */}
        <Grid item xs={12}>
          <Typography className="form-heading">Experiment Samples</Typography>
        </Grid>
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasNanoSamples}
                name="hasNanoSamples"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="Declaration of Nanomaterials and NanoProducts (NMPs), Nano-sized Samples, 1-100 nm"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse
            style={{
              borderLeft: "4px solid  #de930038",
              paddingLeft: "1em",
            }}
            in={formData.hasNanoSamples}
          >
            <Grid item xs={12} style={{ paddingTop: "1em" }}>
              <DynamicInputTable
                disabled={!formAccess.canEdit || embedded}
                emptyMessage="No nanomaterial sample has been added yet"
                addRowText="Add sample"
                deleteRowText="Delete this sample"
                onAddRow={() =>
                  updateFormData({
                    ...formData,
                    nanoSamples: [
                      ...formData.nanoSamples,
                      {
                        name: "New sample",
                        source: "Commercial",
                        sourceDetail: "",
                        aggregationState: "Powder",
                        quantity: "",
                        sizeDistribution: "",
                        length: "",
                        density: "",
                        class: 0,
                      },
                    ],
                  })
                }
                onDeleteRow={(rowIndex) =>
                  updateFormData({
                    ...formData,
                    nanoSamples: formData.nanoSamples.filter(
                      (_, index) => index !== rowIndex
                    ),
                  })
                }
                data={formData.nanoSamples}
                columnOrder={[
                  "name",
                  "source",
                  "sourceDetail",
                  "aggregationState",
                  "quantity",
                  "sizeDistribution",
                  "length",
                  "density",
                  "class",
                ]}
                columnDefinitions={{
                  name: {
                    title: "Name",
                    Component: (props: { rowIndex: number }) => (
                      <TextField
                        size="small"
                        onFocus={(event) => (event.target as any).select()}
                        value={formData.nanoSamples[props.rowIndex].name}
                        onChange={(event) => {
                          handleNanoChange(
                            "name",
                            props.rowIndex,
                            event.target.value
                          );
                        }}
                        variant="outlined"
                      />
                    ),
                  },
                  source: {
                    title: "Source",
                    Component: (props: { rowIndex: number }) => (
                      <SimpleSelect
                        variant="outlined"
                        size="small"
                        style={{ width: "9em" }}
                        name={`nano-samples-row-${props.rowIndex}`}
                        disabled={{ value: !formAccess.canEdit || embedded }}
                        selectedValue={
                          formData.nanoSamples[props.rowIndex].source
                        }
                        onChange={(event) => {
                          handleNanoChange(
                            "source",
                            props.rowIndex,
                            event.target.value as string
                          );
                        }}
                        options={[
                          { value: "Commercial" },
                          { value: "Natural" },
                          { value: "Prepared in a laboratory" },
                          {
                            value:
                              "Will be produced in the experiment as a product",
                          },
                          { value: "Other" },
                        ]}
                      />
                    ),
                  },
                  sourceDetail: {
                    title: "Source Details",
                    Component: (props: { rowIndex: number }) => (
                      <Tooltip
                        title={
                          formData.nanoSamples[props.rowIndex].source ===
                          "Commercial"
                            ? "Add CAS number and Upload SDS"
                            : formData.nanoSamples[props.rowIndex].source ===
                              "Natural"
                            ? "Specify"
                            : "Give details"
                        }
                        placement="top-start"
                      >
                        <TextField
                          size="small"
                          variant="outlined"
                          value={
                            formData.nanoSamples[props.rowIndex].sourceDetail
                          }
                          onChange={(event) => {
                            handleNanoChange(
                              "sourceDetail",
                              props.rowIndex,
                              event.target.value
                            );
                          }}
                        />
                      </Tooltip>
                    ),
                  },
                  aggregationState: {
                    title: "Aggregation State",
                    Component: (props: { rowIndex: number }) => (
                      <SimpleSelect
                        variant="outlined"
                        size="small"
                        style={{ width: "10em" }}
                        name={`aggregation-state-row-${props.rowIndex}`}
                        disabled={{ value: !formAccess.canEdit || embedded }}
                        selectedValue={
                          formData.nanoSamples[props.rowIndex].aggregationState
                        }
                        onChange={(event) => {
                          handleNanoChange(
                            "aggregationState",
                            props.rowIndex,
                            event.target.value as string
                          );
                        }}
                        options={[
                          { value: "Powder" },
                          { value: "Aerosol" },
                          { value: "In Suspension/Liquid" },
                          { value: " On Solid Matrix" },
                        ]}
                      />
                    ),
                  },
                  quantity: {
                    title: "Quantity",
                    Component: (props: { rowIndex: number }) => (
                      <Tooltip
                        title="E.g mass, ppm, mmol, concentration, pressure, number etc."
                        placement="top-start"
                      >
                        <TextField
                          size="small"
                          style={{ width: "5em" }}
                          value={formData.nanoSamples[props.rowIndex].quantity}
                          onChange={(event) => {
                            handleNanoChange(
                              "quantity",
                              props.rowIndex,
                              event.target.value
                            );
                          }}
                          variant="outlined"
                        />
                      </Tooltip>
                    ),
                  },
                  sizeDistribution: {
                    title: "Size",
                    Component: (props: { rowIndex: number }) => (
                      <Tooltip
                        title="Size distribution on nm"
                        placement="top-start"
                      >
                        <TextField
                          size="small"
                          style={{ width: "4em" }}
                          value={
                            formData.nanoSamples[props.rowIndex]
                              .sizeDistribution
                          }
                          onChange={(event) => {
                            handleNanoChange(
                              "sizeDistribution",
                              props.rowIndex,
                              event.target.value
                            );
                          }}
                          variant="outlined"
                        />
                      </Tooltip>
                    ),
                  },
                  length: {
                    title: "Length",
                    Component: (props: { rowIndex: number }) => (
                      <Tooltip
                        title="Length and diamter of fibrous NMP"
                        placement="top-start"
                      >
                        <TextField
                          size="small"
                          style={{ width: "4em" }}
                          value={formData.nanoSamples[props.rowIndex].length}
                          onChange={(event) => {
                            handleNanoChange(
                              "length",
                              props.rowIndex,
                              event.target.value
                            );
                          }}
                          variant="outlined"
                        />
                      </Tooltip>
                    ),
                  },
                  density: {
                    title: "Density",
                    Component: (props: { rowIndex: number }) => (
                      <Tooltip title="Density g/cm³" placement="top-start">
                        <TextField
                          size="small"
                          style={{ width: "4em" }}
                          value={formData.nanoSamples[props.rowIndex].density}
                          onChange={(event) => {
                            handleNanoChange(
                              "density",
                              props.rowIndex,
                              event.target.value
                            );
                          }}
                          variant="outlined"
                        />
                      </Tooltip>
                    ),
                  },
                  class: {
                    title: "Class",
                    Component: (props: { rowIndex: number }) => (
                      <SimpleSelect
                        variant="outlined"
                        size="small"
                        name={`nano-classes-row-${props.rowIndex}`}
                        style={{ width: "10em" }}
                        disabled={{
                          value: !formAccess.canEdit || embedded,
                        }}
                        toolTip="See the nano class guide below"
                        selectedValue={
                          formData.nanoSamples[props.rowIndex].class
                        }
                        onChange={(event) => {
                          handleNanoChange(
                            "class",
                            props.rowIndex,
                            event.target.value as string
                          );
                        }}
                        options={[
                          { value: "0", label: "No nano class" },
                          { value: "1", label: "Nano class 1" },
                          { value: "2", label: "Nano class 2" },
                          { value: "3", label: "Nano class 3" },
                        ]}
                      />
                    ),
                  },
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <div
                style={{ fontWeight: "bold", marginTop: "2em", color: "#777" }}
              >
                Additional Details
              </div>
              <TextField
                fullWidth={true}
                placeholder="Please give a brief description of any additional properties or
              information regarding the NMPs"
                name="additionalNanoProperties"
                value={formData.additionalNanoProperties}
                onChange={(event) => handleInputChange(event)}
                variant="outlined"
                multiline={true}
                rows={props.embedded ? undefined : 6}
              />
            </Grid>
            <Grid
              container
              item
              style={{
                color: "#444444",
                marginRight: "-1em",
                marginBottom: "2em",
              }}
            >
              <Grid
                style={{ display: "flex", justifyItems: "stretch" }}
                item
                xs={12}
                md={6}
                lg={3}
              >
                <div
                  style={{
                    background: "#00ff0020",
                    width: "100%",
                    borderRadius: "0.4em",
                    margin: "1em",
                    padding: "1em",
                    fontSize: "0.8em",
                  }}
                >
                  <b>No nano class</b>
                  <p>• Processes in a complete vacuum tight environment</p>
                  <p>• On Solid Matrix without any physical interaction</p>
                  <p>
                    Note: This only applies if the whole process (including
                    preparation and cleaning) is in vacuum.
                  </p>
                </div>
              </Grid>
              <Grid
                style={{ display: "flex", justifyItems: "stretch" }}
                item
                xs={12}
                md={6}
                lg={3}
              >
                <div
                  style={{
                    background: "#ffff0020",
                    width: "100%",
                    borderRadius: "0.4em",
                    margin: "1em",
                    padding: "1em",
                    fontSize: "0.8em",
                  }}
                >
                  <b>Nano class I</b>
                  <p>• NMPs in Suspension/Liquid</p>
                  <p>
                    • Activity with bare NMP attached on Solid Matrix less than
                    10 mg
                  </p>
                  <p>Mitigation Level I for NMPs will be required</p>
                </div>
              </Grid>
              <Grid
                style={{ display: "flex", justifyItems: "stretch" }}
                item
                xs={12}
                md={6}
                lg={3}
              >
                <div
                  style={{
                    background: "#ffa50020",
                    width: "100%",
                    borderRadius: "0.4em",
                    margin: "1em",
                    padding: "1em",
                    fontSize: "0.8em",
                  }}
                >
                  <b>Nano class II</b>
                  <p>• Activity with bare NMPs in Powder form less than 1 g</p>
                  <p>• NMPs can be released as Aerosols</p>
                  <p>
                    • Activity with bare NMP attached on Solid Matrix greater
                    than 10 mg
                  </p>

                  <p>Mitigation Level II for NMPs will be required</p>
                </div>
              </Grid>
              <Grid
                style={{ display: "flex", justifyItems: "stretch" }}
                item
                xs={12}
                md={6}
                lg={3}
              >
                <div
                  style={{
                    background: "#ff000020",
                    width: "100%",
                    borderRadius: "0.4em",
                    margin: "1em",
                    padding: "1em",
                    fontSize: "0.8em",
                  }}
                >
                  <b>Nano class III - Not allowed</b>
                  <p>
                    • Activity with bare NMPs in Powder form greater than 1 g
                  </p>
                  <p>
                    NMPs or processes classified as Nano III are at this time
                    prohibited at MAX IV.
                  </p>
                </div>
              </Grid>
            </Grid>
          </Collapse>
        </Grid>
        {/* SAMPLES - OTHER (GAS, CHEMICAL, RADIOACTIVE) */}
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit || embedded}
                checked={formData.hasChemicalSamples}
                name="hasChemicalSamples"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="Declaration of Samples, Chemicals, Gases, Radioactive Material"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse
            in={formData.hasChemicalSamples}
            style={{
              borderLeft: "4px solid  #de930038",
              paddingLeft: "1em",
            }}
          >
            <Grid item xs={12}>
              <Grid container style={{ marginBottom: "1em" }}>
                <Grid item xs={12}>
                  <DynamicInputTable
                    disabled={!formAccess.canEdit || embedded}
                    emptyMessage="No chemical or gas sample has been added yet"
                    addRowText="Add sample"
                    deleteRowText="Delete this sample"
                    onAddRow={() =>
                      updateFormData({
                        ...formData,
                        chemicalSamples: [
                          ...formData.chemicalSamples,
                          {
                            name: "New sample",
                            cas: "",
                            quantity: "",
                            aggregationState: "Solid/Powder",
                            safetyClassifications: [],
                          },
                        ],
                      })
                    }
                    onDeleteRow={(rowIndex) =>
                      updateFormData({
                        ...formData,
                        chemicalSamples: formData.chemicalSamples.filter(
                          (_, index) => index !== rowIndex
                        ),
                      })
                    }
                    data={formData.chemicalSamples}
                    columnOrder={[
                      "name",
                      "cas",
                      "quantity",
                      "aggregationState",
                      "safetyClassifications",
                    ]}
                    columnDefinitions={{
                      name: {
                        title: "Name",
                        Component: (props: { rowIndex: number }) => (
                          <TextField
                            fullWidth={true}
                            size="small"
                            onFocus={(event) => (event.target as any).select()}
                            value={
                              formData.chemicalSamples[props.rowIndex].name
                            }
                            onChange={(event) => {
                              handleChemicalChange(
                                "name",
                                props.rowIndex,
                                event.target.value
                              );
                            }}
                            variant="outlined"
                          />
                        ),
                      },
                      cas: {
                        title: "CAS",
                        Component: (props: { rowIndex: number }) => (
                          <TextField
                            fullWidth={true}
                            size="small"
                            value={formData.chemicalSamples[props.rowIndex].cas}
                            onChange={(event) => {
                              handleChemicalChange(
                                "cas",
                                props.rowIndex,
                                event.target.value
                              );
                            }}
                            variant="outlined"
                          />
                        ),
                      },
                      quantity: {
                        title: "Quantity",
                        Component: (props: { rowIndex: number }) => (
                          <Tooltip
                            title="E.g mass, ppm, mmol, concentration, pressure, number etc."
                            placement="top-start"
                          >
                            <TextField
                              size="small"
                              fullWidth={true}
                              value={
                                formData.chemicalSamples[props.rowIndex]
                                  .quantity
                              }
                              onChange={(event) => {
                                handleChemicalChange(
                                  "quantity",
                                  props.rowIndex,
                                  event.target.value
                                );
                              }}
                              variant="outlined"
                            />
                          </Tooltip>
                        ),
                      },
                      aggregationState: {
                        title: "Aggregation state",
                        Component: (props: { rowIndex: number }) => (
                          <SimpleSelect
                            variant="outlined"
                            size="small"
                            name={`aggregation-state-row-${props.rowIndex}`}
                            style={{ width: "10em" }}
                            disabled={{
                              value: !formAccess.canEdit || embedded,
                            }}
                            selectedValue={
                              formData.chemicalSamples[props.rowIndex]
                                .aggregationState
                            }
                            onChange={(event) => {
                              handleChemicalChange(
                                "aggregationState",
                                props.rowIndex,
                                event.target.value as string
                              );
                            }}
                            options={[
                              { value: "Solid/Powder" },
                              { value: "Solid/Piece" },
                              { value: "Single Crystal" },
                              { value: "Liquid" },
                              { value: "Gas" },
                              { value: "Thin Film" },
                            ]}
                          />
                        ),
                      },
                      safetyClassifications: {
                        title: "Safety classification",
                        isStateful: true,
                        Component: (props: { rowIndex: number }) => {
                          const menuItems: System.SafetyClassificationType[] = [
                            "Explosive",
                            "Flammable",
                            "Oxidizing",
                            "Gas under Pressure",
                            "Corrosive",
                            "Acute toxicity",
                            "Health Hazard",
                            "Serious health hazard",
                            "Dangerous for the environment",
                            "Radioactive",
                          ];
                          const [open, setOpen] = useState<boolean>(false);
                          return (
                            <div
                              style={{
                                minWidth: "12em",
                                display: "flex",
                                alignItems: "center",
                              }}
                            >
                              {formAccess.canEdit && (
                                <IconButton
                                  onClick={() => setOpen(true)}
                                  size="large"
                                >
                                  <AddCircle />
                                </IconButton>
                              )}
                              {/* Cannot be implemented as SimpleSelect because it has a cutom options rendering. This is not possible with native rendering */}
                              <Select
                                disabled={!formAccess.canEdit || embedded}
                                value=""
                                style={{ width: "0em", visibility: "hidden" }}
                                open={open}
                                onClose={() => setOpen(false)}
                                onOpen={() => setOpen(true)}
                                onChange={(e) => {
                                  handleChemicalChange(
                                    "safetyClassifications",
                                    props.rowIndex,
                                    [
                                      ...formData.chemicalSamples[
                                        props.rowIndex
                                      ].safetyClassifications,
                                      e.target.value as string,
                                    ]
                                  );
                                }}
                              >
                                {menuItems
                                  .filter(
                                    (option) =>
                                      !formData.chemicalSamples[
                                        props.rowIndex
                                      ].safetyClassifications.includes(option)
                                  )
                                  .map((option) => {
                                    return (
                                      <MenuItem key={option} value={option}>
                                        <div
                                          style={{
                                            display: "flex",
                                            justifyContent: "space-between",
                                            width: "100%",
                                            alignItems: "center",
                                          }}
                                        >
                                          <span>{option}</span>
                                          <img
                                            alt="radioactive"
                                            style={{
                                              marginLeft: "1em",
                                              height:
                                                option === "Radioactive"
                                                  ? "42px"
                                                  : "48px",
                                            }}
                                            src={
                                              process.env.PUBLIC_URL +
                                              `/${option
                                                .toLowerCase()
                                                .replaceAll(" ", "_")}.gif`
                                            }
                                          />
                                        </div>
                                      </MenuItem>
                                    );
                                  })}
                              </Select>
                              {formData.chemicalSamples[props.rowIndex]
                                .safetyClassifications.length === 0 ? (
                                <i>None</i>
                              ) : (
                                formData.chemicalSamples[
                                  props.rowIndex
                                ].safetyClassifications.map(
                                  (classification) => {
                                    return (
                                      <Tooltip
                                        key={classification}
                                        title={`${classification}${
                                          formAccess.canEdit
                                            ? ". Click to remove"
                                            : ""
                                        } `}
                                        placement="top-start"
                                      >
                                        <img
                                          alt={classification}
                                          key={classification}
                                          style={{
                                            height:
                                              classification === "Radioactive"
                                                ? "28px"
                                                : "32px",
                                          }}
                                          onClick={(e) => {
                                            if (!formAccess.canEdit) {
                                              return;
                                            }
                                            handleChemicalChange(
                                              "safetyClassifications",
                                              props.rowIndex,
                                              formData.chemicalSamples[
                                                props.rowIndex
                                              ].safetyClassifications.filter(
                                                (c) => c !== classification
                                              )
                                            );
                                          }}
                                          src={
                                            process.env.PUBLIC_URL +
                                            `/${classification
                                              .toLowerCase()
                                              .replaceAll(" ", "_")}.gif`
                                          }
                                        />
                                      </Tooltip>
                                    );
                                  }
                                )
                              )}
                            </div>
                          );
                        },
                      },
                    }}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  style={{
                    fontWeight: "bold",
                    marginTop: "2em",
                    color: "#777",
                  }}
                >
                  Additional Details
                </Grid>
                <Grid item xs={12} lg={6} style={{ paddingRight: "2em" }}>
                  <small>
                    Please give a brief description of any additional properties
                    or information regarding the sample/chemical or gas:
                    <ul>
                      <li>
                        Upload Safety Data Sheet (SDS) for each sample in English,
                        no older than 2 years.
                        The SDS shall confirm to the CLP regulation No 1272/2008.
                        If an SDS does not exist (e.g. synthesized compound/ non-commercial),
                        give a comment on this.
                      </li>
                      <li>
                        Gases: Specify details on expected Volume, Pressure and
                        purity for all gases to be used. If you need a special
                        Gas Mixture, also specify in mol% or vol% the main
                        component and what carrier gas is needed. Also comment
                        on flowrates planned to be used and if it’s continuous
                        flow or not.
                      </li>
                      <li>
                        If you foresee that the end-products are hazardous,
                        please comment on this.
                      </li>
                      <li>
                        Use the File upload feature at the end of this form to
                        attach files.
                      </li>
                    </ul>
                    <div>
                      Note: MAX IV will provide the gas or gas mixture if the
                      cost is reasonable. Users are not allowed to bring their
                      own gas cylinders without a formal approval from the EST.
                    </div>
                    <div style={{ marginTop: "0.7em" }}>
                      Note: Radioactive substances will require more
                      information. A radiation safety officer will contact you.
                    </div>
                  </small>
                </Grid>
                <Grid item xs={12} lg={6}>
                  <TextField
                    fullWidth={true}
                    name="additionalChemicalProperties"
                    placeholder="Additional details"
                    value={formData.additionalChemicalProperties}
                    onChange={(event) => handleInputChange(event)}
                    variant="outlined"
                    multiline={true}
                    rows={props.embedded ? undefined : 6}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Collapse>
        </Grid>
        {/* CMR COMPOUND*/}
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasCMRCompound}
                name="hasCMRCompound"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="One or several of the substrates or chemicals are classed as a CMR compound (Check for the following H-phrases: H340x, 350x or 360x)."
          />
        </Grid>
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <Collapse in={formData.hasCMRCompound}>
            <div style={{ borderLeft: "4px solid  #de930038", padding: "1em" }}>
              <small>
                Make sure you download and fill out the{" "}
                <a
                  href={
                    process.env.PUBLIC_URL +
                    "/[Replace_with_your_Proposal_ID]_CMR_ENG.docx"
                  }
                  className="link"
                  download
                >
                  CMR substitution investigation form
                </a>
                . Please fill in the form and upload it in the <i>Files</i>{" "}
                segment below. Replace the text in the brackets [ ] with your
                proposal ID.
              </small>
            </div>
          </Collapse>
        </Grid>
        {/* SAMPLES - BIOLOGICAL */}
        <Grid item xs={12} style={{ marginBottom: "-1em" }}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasBioSamples}
                name="hasBioSamples"
                onChange={(e) => handleInputChange(e)}
              />
            }
            label="Declaration of Biological Samples"
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse in={formData.hasBioSamples}>
            <Grid item xs={12}>
              <div
                style={{
                  borderLeft: "4px solid  #de930038",
                  paddingLeft: "1em",
                  marginBottom: "2em",
                  paddingTop: "1em",
                }}
              >
                <DynamicInputTable
                  disabled={!formAccess.canEdit || embedded}
                  emptyMessage="No biological sample has been added yet"
                  addRowText="Add sample"
                  deleteRowText="Delete this sample"
                  onAddRow={() =>
                    updateFormData({
                      ...formData,
                      bioSamples: [
                        ...formData.bioSamples,
                        {
                          name: "New sample",
                          category: "Proteins",
                          quantity: "",
                          aggregationState: "",
                          source: "Commercial",
                          sourceDetail: "",
                        },
                      ],
                    })
                  }
                  onDeleteRow={(rowIndex) =>
                    updateFormData({
                      ...formData,
                      bioSamples: formData.bioSamples.filter(
                        (sample, index) => index !== rowIndex
                      ),
                    })
                  }
                  data={formData.bioSamples}
                  columnOrder={[
                    "name",
                    "category",
                    "source",
                    "sourceDetail",
                    "aggregationState",
                    "quantity",
                  ]}
                  columnDefinitions={{
                    name: {
                      title: "Name",
                      Component: (props: { rowIndex: number }) => (
                        <TextField
                          size="small"
                          onFocus={(event) => (event.target as any).select()}
                          value={formData.bioSamples[props.rowIndex].name}
                          onChange={(event) => {
                            handleBioChange(
                              "name",
                              props.rowIndex,
                              event.target.value
                            );
                          }}
                          variant="outlined"
                        />
                      ),
                    },
                    category: {
                      title: "Category",
                      Component: (props: { rowIndex: number }) => (
                        <SimpleSelect
                          variant="outlined"
                          size="small"
                          name={`category-row-${props.rowIndex}`}
                          disabled={{
                            value: !formAccess.canEdit || embedded,
                          }}
                          selectedValue={
                            formData.bioSamples[props.rowIndex].category
                          }
                          onChange={(event) => {
                            handleBioChange(
                              "category",
                              props.rowIndex,
                              event.target.value as string
                            );
                          }}
                          options={[
                            { value: "Proteins" },
                            { value: "Lipids" },
                            { value: "Antibodies" },
                            { value: "Bacteria" },
                            { value: "Viral components" },
                            { value: "Mammalian cells" },
                            { value: "Human or animal tissue" },
                            { value: "Other" },
                          ]}
                        />
                      ),
                    },
                    source: {
                      title: "Source",
                      Component: (props: { rowIndex: number }) => (
                        <SimpleSelect
                          variant="outlined"
                          size="small"
                          name={`source-row-${props.rowIndex}`}
                          disabled={{
                            value: !formAccess.canEdit || embedded,
                          }}
                          selectedValue={
                            formData.bioSamples[props.rowIndex].source
                          }
                          onChange={(event) => {
                            handleBioChange(
                              "source",
                              props.rowIndex,
                              event.target.value as string
                            );
                          }}
                          options={[
                            {
                              value: "Commercial",
                              label: "Commercially purchased",
                            },
                            {
                              value: "Laboratory made",
                              label: "Make or prepared in a laboratory",
                            },
                            {
                              value: "Animal",
                              label: "Collected from humans or animals",
                            },
                          ]}
                        />
                      ),
                    },
                    sourceDetail: {
                      title: "Source Details",
                      Component: (props: { rowIndex: number }) => (
                        <Tooltip
                          title={
                            formData.bioSamples[props.rowIndex].source ===
                            "Commercial"
                              ? "Please specify CAS number and attach SDS or product specification sheet"
                              : formData.bioSamples[props.rowIndex].source ===
                                "Animal"
                              ? "Please provide ethical/animal permission details and sample details"
                              : "Give a brief description of sample preparation"
                          }
                          placement="top-start"
                        >
                          <TextField
                            size="small"
                            variant="outlined"
                            value={
                              formData.bioSamples[props.rowIndex].sourceDetail
                            }
                            onChange={(event) => {
                              handleBioChange(
                                "sourceDetail",
                                props.rowIndex,
                                event.target.value
                              );
                            }}
                          />
                        </Tooltip>
                      ),
                    },
                    aggregationState: {
                      title: "Aggregation State",
                      Component: (props: { rowIndex: number }) => (
                        <Tooltip
                          title="Specify the aggregation state and components of sample if mixed"
                          placement="top-start"
                        >
                          <TextField
                            size="small"
                            inputProps={{ style: { width: "12em" } }}
                            variant="outlined"
                            value={
                              formData.bioSamples[props.rowIndex]
                                .aggregationState
                            }
                            onChange={(event) => {
                              handleBioChange(
                                "aggregationState",
                                props.rowIndex,
                                event.target.value
                              );
                            }}
                          />
                        </Tooltip>
                      ),
                    },
                    quantity: {
                      title: "Quantity",
                      Component: (props: { rowIndex: number }) => (
                        <Tooltip
                          title="Quantity and concentration"
                          placement="top-start"
                        >
                          <TextField
                            size="small"
                            variant="outlined"
                            style={{ width: "5em" }}
                            value={formData.bioSamples[props.rowIndex].quantity}
                            onChange={(event) => {
                              handleBioChange(
                                "quantity",
                                props.rowIndex,
                                event.target.value
                              );
                            }}
                          />
                        </Tooltip>
                      ),
                    },
                  }}
                />
              </div>
            </Grid>
          </Collapse>
        </Grid>
        {/* SAMPLE REMOVAL */}
        <Grid
          item
          container
          sm={6}
          xs={12}
          style={{ alignContent: "space-between" }}
        >
          <Grid item xs={12}>
            <Typography className="form-heading">Sample Removal</Typography>
            <small>
              No samples or chemicals are allowed to be stored permanently at
              MAX IV. Temporary storage can be allowed if next beam time is
              within 3 months. Contact est@maxiv.lu.se if required.{" "}
              <b>Samples will be removed by:</b>
            </small>
          </Grid>
          <Grid item xs={12}>
            <SimpleSelect
              style={{ marginTop: "1em" }}
              disabled={{ value: !formAccess.canEdit }}
              options={[{ value: "The User" }, { value: "MAX IV" }]}
              selectedValue={formData.sampleRemoved}
              title="Click to select"
              name="sampleRemoved"
              onChange={(event) => handleInputChange(event)}
            />
          </Grid>
        </Grid>
        {/* LAB ACCESS */}
        <Grid
          item
          container
          sm={6}
          xs={12}
          style={{ alignContent: "space-between" }}
        >
          <Grid item xs={12}>
            <Typography className="form-heading">Lab Access</Typography>
            <small>
              Upon request, access to a chemical or biological lab is available.
              Reach out to your local contact for more info.
              <br />
              <b>I will require access to a Chemical or Biology laboratory:</b>
            </small>
          </Grid>
          <Grid item xs={12}>
            <SimpleSelect
              style={{ marginTop: "1em" }}
              disabled={{ value: !formAccess.canEdit }}
              options={[{ value: "Yes" }, { value: "No" }]}
              selectedValue={formData.labAccessRequired ? "Yes" : "No"}
              title="Click to select"
              name="labAccessRequired"
              onChange={(event) =>
                updateFormData({
                  ...formData,
                  labAccessRequired: event.target.value === "Yes",
                })
              }
            />
          </Grid>
        </Grid>
        {/* DETAILS TABLE */}
        <Grid item xs={12}>
          <Typography className="form-heading">Experiment Details</Typography>
          <small>
            Please provide a detailed outline of how you plan your work with the
            experiment/method. Highlight the Risks/Hazards from the suggested
            actions/process and your suggestion of how you plan to mitigate
            them. The EST will review your information and further
            clarifications might be requested if required.
          </small>
        </Grid>
        <Grid item xs={12}>
          <ExperimentRiskTable
            formData={formData}
            embedded={props.embedded}
            updateFormData={updateFormData}
          />
        </Grid>
      </>
    );
  };
  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={emptyRiskAssessmentForm(initialState)}
      formType="RISK_ASSESSMENT"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
};
export default RiskAssessment;
