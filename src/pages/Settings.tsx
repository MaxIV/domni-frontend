import React, { useEffect } from "react";
import AddCircle from "@mui/icons-material/AddCircle";
import ContactMailIcon from "@mui/icons-material/ContactMail";

import {
  Container,
  Paper,
  Typography,
  Chip,
  FormControl,
  Button,
  Grid,
  IconButton,
  Tooltip,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  createCollection,
  updateCollection,
  deleteCollection,
} from ".././api/collection";
import { ValidatorForm } from "react-material-ui-form-validator";
import { useStore } from "../store/Store";
import "./Settings.css";
import { getFormTypeByPath, getFormConfig } from "../utilities/Config";
import {
  selectSupervisors,
  selectProjects,
  selectProjectCategories,
  selectFirstProjectCategory,
  formAction,
} from "../store/Reducer";
import { getModalAPIErrorDispatch } from "../components/Modal";
import { SimpleSelect } from "../components/SimpleSelect";
import { SelectableListWidget } from "../components/SelectableListWidget";
import {
  createSimpleValidationResult,
  validateMaxIVEmail,
} from "../utilities/Helpers";
import { CustomLists } from "../components/CustomLists";
import { ldapSearch } from "../api/ldap";
import { LdapPersonModalContent } from "../components/LdapPerson";
import { RouteComponentProps } from "react-router-dom";
import { DomniTextValidator } from "../components/TextValidator";

interface State {
  selectedSupervisor: System.Supervisor | undefined;
  selectedProject: System.Project | undefined;
  formType: System.FormType;
}
const Settings = (props: RouteComponentProps<string[]>) => {
  const [localState, setLocalState] = React.useState<State>({
    selectedSupervisor: undefined,
    selectedProject: undefined,
    formType: getFormTypeByPath(props.match.params),
  });
  const { dispatch, state } = useStore();
  const { formType } = localState;
  const projects = selectProjects(state, formType);
  const supervisors = selectSupervisors(state, formType);
  const formConfig = getFormConfig(localState.formType);
  useEffect(() => {
    const newFormType = getFormTypeByPath(props.match.params);
    const selectedSupervisor = selectSupervisors(state, newFormType)[0];
    const selectedProject = selectProjects(state, newFormType)[0];
    setLocalState({
      ...localState,
      selectedSupervisor,
      selectedProject,
      formType: newFormType,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.match.params[0], state.isLoading]);

  const handleInputChange = (event: any, type: string) => {
    if (type === "supervisor") {
      if (!localState.selectedSupervisor) {
        return;
      }
      setLocalState({
        ...localState,
        selectedSupervisor: {
          ...localState.selectedSupervisor,
          [event.target.name]: event.target.value,
        },
      });
    } else if (type === "project") {
      if (!localState.selectedProject) {
        return;
      }
      setLocalState({
        ...localState,
        selectedProject: {
          ...localState.selectedProject,
          [event.target.name]: event.target.value,
        },
      });
    }
  };

  const handleChipDelete = (itemToDelete: String) => () => {
    if (!localState.selectedSupervisor || !formConfig.requiresApproval) {
      return;
    }
    setLocalState({
      ...localState,
      selectedSupervisor: {
        ...localState.selectedSupervisor,
        projects: [
          ...localState.selectedSupervisor.projects.filter(
            (project) => project !== itemToDelete
          ),
        ],
      },
    });
  };
  const saveCollection = async (
    collectionType: System.CollectionType,
    collection: System.Supervisor | System.Project | System.List,
    dispatchValue: formAction
  ) => {
    try {
      await updateCollection(collectionType, collection);
      dispatch(dispatchValue);
    } catch (err) {
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
      return;
    }
  };

  const removeCollection = async (
    collectionType: System.CollectionType,
    collectionId: string,
    dispatchValue: formAction,
    onConfirm: () => void
  ) => {
    if (!collectionId) {
      return;
    }
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Confirm deletion",
        content:
          "Are you sure you want to delete this " +
          collectionType.toLowerCase() +
          "?",
        btn1Text: "Delete",
        btn1Color: "secondary",
        btn2Text: "Cancel",

        btn1OnClick: async (inputs: System.ModalInputs) => {
          try {
            await deleteCollection(collectionType, collectionId, formType);
            dispatch(dispatchValue);
            onConfirm();
          } catch (err) {
            dispatch(getModalAPIErrorDispatch(err as System.APIError));
            return;
          }
        },
      },
    });
  };

  const addSupervisor = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "New Supervisor",
        content: "",
        btn1Text: "Create",
        btn2Text: "Cancel",
        validate: (inputs) => {
          const errorMessage =
            !inputs.name.value || !validateMaxIVEmail(inputs.email.value)
              ? "Supervisors require both name and a valid MAX IV email"
              : "";
          return createSimpleValidationResult(errorMessage);
        },
        btn1OnClick: async (inputs: System.ModalInputs) => {
          const newSupervisor: System.Supervisor = {
            _id: "",
            name: inputs.name.value,
            email: inputs.email.value,
            form: localState.formType,
            projects: [],
          };
          try {
            const superwithId = (await createCollection(
              "SUPERVISOR",
              newSupervisor
            )) as System.Supervisor;
            setLocalState({
              ...localState,
              selectedSupervisor: superwithId,
            });
            dispatch({
              type: "SET_SUPERVISORS",
              formType,
              supervisors: [...supervisors, superwithId],
            });
          } catch (err) {
            dispatch(getModalAPIErrorDispatch(err as System.APIError));
            return;
          }
        },
        inputs: {
          name: {
            type: "text",
            label: "Name",
            value: "",
          },
          email: {
            type: "text",
            label: "Email",
            value: "",
          },
        },
      },
    });
  };

  const showLDAPModal = async () => {
    const name = localState.selectedSupervisor?.name;
    if (!name) {
      return;
    }
    try {
      const result = await ldapSearch({
        searchQuery: name,
        attribute: "fullName",
      });
      dispatch({
        type: "SET_MODAL_PROPS",
        modalProps: {
          open: true,
          title: "",
          content: LdapPersonModalContent(result),
          btn1Text: "Close",
          btn1Color: "info",
          size: "sm",
        },
      });
    } catch (error) {}
  };

  const addProject = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "New project",
        content: "",
        btn1Text: "Create",
        btn2Text: "Cancel",
        validate: (inputs) => {
          const errorMessage = !inputs.name.value ? "Name is required" : "";
          return createSimpleValidationResult(errorMessage);
        },
        btn1OnClick: async (inputs: System.ModalInputs) => {
          const newProject: System.Project = {
            _id: "",
            project: inputs.name.value,
            name: inputs.name.value,
            form: localState.formType,
            category: selectFirstProjectCategory(state, formType),
          };
          try {
            const projectWithId = (await createCollection(
              "PROJECT",
              newProject
            )) as System.Project;
            setLocalState({
              ...localState,
              selectedProject: projectWithId,
            });
            dispatch({
              type: "SET_PROJECTS",
              formType,
              projects: [...projects, projectWithId],
            });
          } catch (err) {
            dispatch(getModalAPIErrorDispatch(err as System.APIError));
            return;
          }
        },
        inputs: {
          name: {
            type: "text",
            label: "Name",
            value: "",
          },
        },
      },
    });
  };
  const saveSupervisor = async (event: any) => {
    const { selectedSupervisor } = localState;
    if (!selectedSupervisor) {
      return;
    }
    saveCollection("SUPERVISOR", selectedSupervisor, {
      type: "SET_SUPERVISORS",
      formType,
      supervisors: [
        ...supervisors.filter((s) => s._id !== selectedSupervisor._id),
        selectedSupervisor,
      ],
    });
  };

  const saveProject = async () => {
    const { selectedProject } = localState;
    if (!selectedProject) {
      return;
    }
    //We're eventually removing the field project and replacing it with name
    selectedProject.name = selectedProject.project;
    saveCollection("PROJECT", selectedProject, {
      type: "SET_PROJECTS",
      formType,
      projects: [
        ...projects.filter((s) => s._id !== selectedProject._id),
        selectedProject,
      ],
    });
  };

  const removeSupervisor = async () => {
    const { selectedSupervisor } = localState;
    if (!selectedSupervisor) {
      return;
    }
    const newSupervisors = supervisors.filter(
      (s) => s._id !== selectedSupervisor._id
    );
    removeCollection(
      "SUPERVISOR",
      selectedSupervisor._id,
      {
        type: "SET_SUPERVISORS",
        formType,
        supervisors: newSupervisors,
      },
      () => {
        setLocalState({
          ...localState,
          selectedSupervisor: newSupervisors[0],
        });
      }
    );
  };

  const removeProject = async () => {
    const { selectedProject } = localState;
    if (!selectedProject) {
      return;
    }
    const newProjects = projects.filter((p) => p._id !== selectedProject._id);
    removeCollection(
      "PROJECT",
      selectedProject._id,
      {
        type: "SET_PROJECTS",
        formType,
        projects: newProjects,
      },
      () =>
        setLocalState({
          ...localState,
          selectedProject: newProjects[0],
        })
    );
  };
  if (!state.user.admin[formType]) {
    return (
      <Container>
        <Paper
          elevation={2}
          className="paper"
          style={{ marginTop: "2em", marginBottom: "0.5em" }}
        >
          Only {formConfig.name} administrators can access this resource
        </Paper>
      </Container>
    );
  }
  return (
    <Container className="settings-container">
      <h2 style={{ marginTop: "1.5em" }}>
        <span className="category-preface"> Settings</span>
        <span className="primary-dark-color"> {formConfig.name}</span>
      </h2>
      <Typography>Manage projects, supervisors and custom lists.</Typography>
      <Paper
        elevation={2}
        className="paper"
        style={{ marginTop: "2em", marginBottom: "0.5em" }}
      >
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                fontSize: "1.5em",
                marginTop: "-0.5em",
                marginBottom: "0.5em",
                borderBottom: "1px solid rgb(238, 238, 238)",
              }}
            >
              Supervisors
              <Tooltip title="Create a new supervisor" placement="top" arrow>
                <IconButton
                  data-testid="settings-new-supervisor-button"
                  onClick={addSupervisor}
                  aria-label="add"
                  size="large"
                >
                  <AddCircle />
                </IconButton>
              </Tooltip>
            </div>
          </Grid>
          <Grid item xs={12} sm={6}>
            <SelectableListWidget
              getId={(item) => item._id}
              items={supervisors}
              isSelected={(supervisor: System.Supervisor) =>
                supervisor._id === localState?.selectedSupervisor?._id
              }
              onSelect={(item) =>
                setLocalState({ ...localState, selectedSupervisor: item })
              }
              getLabel={(item) => item.name}
              getAddtionalLabel={(item) => "" + item.projects.length}
              data-testid={"settings-supervisor-list"}
            />
          </Grid>
          <Grid
            item
            xs={12}
            sm={6}
            style={{ padding: "0em", paddingLeft: "1em" }}
          >
            <>
              {!localState.selectedSupervisor ? (
                <Typography
                  className={"small-heading"}
                  style={{ margin: "1em" }}
                >
                  No Supervisor selected
                </Typography>
              ) : (
                <ValidatorForm
                  className="settings-flex-column"
                  onSubmit={saveSupervisor}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      marginBottom: "1em",
                      alignItems: "center",
                    }}
                  >
                    <Typography className={"small-heading"}>
                      Supervisor information
                    </Typography>
                    <IconButton
                      onClick={removeSupervisor}
                      data-testid="settings-delete-supervisor-button"
                      aria-label="delete"
                      size="large"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      marginBottom: "1em",
                    }}
                  >
                    <DomniTextValidator
                      containerStyle={{ flexGrow: 1 }}
                      value={localState.selectedSupervisor?.name}
                      testId="settings-supervisor-name-input"
                      name="name"
                      label="Name"
                      onChange={(event) =>
                        handleInputChange(event, "supervisor")
                      }
                      validators={["required"]}
                      errorMessages={["This field is required"]}
                      variant="outlined"
                      fullWidth={true}
                    />
                    <Tooltip title="Show matching people" placement="top" arrow>
                      <IconButton
                        onClick={showLDAPModal}
                        aria-label="add"
                        size="large"
                      >
                        <ContactMailIcon />
                      </IconButton>
                    </Tooltip>
                  </div>
                  <DomniTextValidator
                    style={{ marginBottom: "1em" }}
                    value={localState.selectedSupervisor?.email}
                    name="email"
                    label="Email"
                    testId="settings-supervisor-email-input"
                    fullWidth={true}
                    onChange={(event) => handleInputChange(event, "supervisor")}
                    validators={["required"]}
                    errorMessages={["This field is required"]}
                    variant="outlined"
                  />
                  <FormControl
                    style={{ marginBottom: "1em" }}
                    variant="outlined"
                  >
                    <SimpleSelect
                      name="supervisorProject"
                      testid="settings-supervisor-project-select"
                      onChange={(event) =>
                        setLocalState({
                          ...localState,
                          selectedSupervisor: {
                            ...(localState.selectedSupervisor as System.Supervisor),
                            projects: [
                              ...(localState.selectedSupervisor
                                ?.projects as string[]),
                              event.target.value as string,
                            ],
                          },
                        })
                      }
                      selectedValue={""}
                      options={[
                        { label: "Add project", value: "", disabled: true },
                      ].concat(
                        projects
                          .filter(
                            (project) =>
                              !localState.selectedSupervisor?.projects.includes(
                                project.project
                              )
                          )
                          .map((project) => {
                            return {
                              label: project.project,
                              value: project.project,
                              disabled: false,
                            };
                          })
                      )}
                    />
                  </FormControl>
                  <div
                    className="settings-flex-chip"
                    data-testid="settings-supervisor-project-chips-div"
                  >
                    <Typography style={{ marginTop: "0.5em" }}>
                      {localState.selectedSupervisor.projects.length === 0 ? (
                        <i>
                          {localState.selectedSupervisor.name} is not the
                          supervisor of any project
                        </i>
                      ) : (
                        "Current projects:"
                      )}
                    </Typography>

                    {localState.selectedSupervisor.projects.map((project) => (
                      <span key={project}>
                        <Chip
                          label={project}
                          onDelete={handleChipDelete(project)}
                          className="settings-chip"
                        />
                      </span>
                    ))}
                  </div>
                  <div className="settings-button-bar">
                    <Button
                      data-testid="settings-supervisor-save-button"
                      variant="contained"
                      color="primary"
                      style={{ marginRight: "0.5em" }}
                      type="submit"
                    >
                      Save
                    </Button>
                  </div>
                </ValidatorForm>
              )}
            </>
          </Grid>
          <Grid item xs={12}>
            <div
              style={{
                textAlign: "center",
                color: "rgb(119, 119, 119)",
                fontSize: "0.85em",
                paddingTop: "1.5em",
                marginTop: "0.5em",
                borderTop: "1px solid #eee",
              }}
            >
              {formConfig.requiresApproval ? (
                <>
                  Select a supervisor from the list to edit or delete.
                  Alternatively, create a new supervisor using the plus button.
                </>
              ) : (
                <>
                  Set a name and email of the administrator of the{" "}
                  {formConfig.name} form.
                </>
              )}
            </div>
          </Grid>
        </Grid>
      </Paper>

      {formConfig.requiresApproval && (
        <>
          <Paper
            className="paper"
            elevation={2}
            style={{ marginTop: "3em", marginBottom: "0.5em" }}
          >
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    fontSize: "1.5em",
                    marginTop: "-0.5em",
                    marginBottom: "0.5em",
                    borderBottom: "1px solid rgb(238, 238, 238)",
                  }}
                >
                  Projects
                  <Tooltip title="Create a new project" placement="top" arrow>
                    <IconButton
                      data-testid="settings-new-project-button"
                      onClick={addProject}
                      aria-label="add"
                      size="large"
                    >
                      <AddCircle />
                    </IconButton>
                  </Tooltip>
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <SelectableListWidget
                  getId={(item) => item._id}
                  sort={(a, b) => a.category.localeCompare(b.category)}
                  items={projects}
                  isSelected={(project: System.Project) =>
                    project._id === localState?.selectedProject?._id
                  }
                  onSelect={(item) =>
                    setLocalState({ ...localState, selectedProject: item })
                  }
                  getLabel={(item) => item.project}
                  getAddtionalLabel={(item) => "" + item.category}
                  data-testid={"settings-project-list"}
                />
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                style={{ padding: "0", paddingLeft: "1em" }}
              >
                {!localState.selectedProject ? (
                  <Typography
                    className={"small-heading"}
                    style={{ margin: "1em" }}
                  >
                    No Project selected
                  </Typography>
                ) : (
                  <ValidatorForm
                    className="settings-flex-column"
                    onSubmit={saveProject}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        marginBottom: "1em",
                        alignItems: "center",
                      }}
                    >
                      <Typography className={"small-heading"}>
                        Project information
                      </Typography>
                      <IconButton
                        data-testid="settings-delete-project-button"
                        onClick={removeProject}
                        aria-label="delete"
                        size="large"
                      >
                        <DeleteIcon />
                      </IconButton>
                    </div>
                    <DomniTextValidator
                      style={{ marginBottom: "2em" }}
                      value={localState.selectedProject?.project}
                      name="project"
                      label="Project"
                      onChange={(event) => handleInputChange(event, "project")}
                      validators={["required"]}
                      errorMessages={["This field is required"]}
                      fullWidth={true}
                      variant="outlined"
                      testId="settings-project-project-input"
                    />
                    <SimpleSelect
                      disabled={{ value: false }}
                      options={selectProjectCategories(state, formType).map(
                        (category) => {
                          return { value: category };
                        }
                      )}
                      testid="project-categories-select"
                      name="selectedProject"
                      selectedValue={localState.selectedProject?.category}
                      style={{ marginBottom: "3em" }}
                      title="Project Category"
                      onChange={(event) =>
                        setLocalState({
                          ...localState,
                          selectedProject: {
                            ...(localState.selectedProject as System.Project),
                            category: event.target.value as string,
                          },
                        })
                      }
                    />
                    <div className="settings-button-bar">
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        style={{ marginRight: "0.5em" }}
                        data-testid="settings-project-save-button"
                      >
                        Save
                      </Button>
                    </div>
                  </ValidatorForm>
                )}
              </Grid>
              <Grid item xs={12}>
                <div
                  style={{
                    textAlign: "center",
                    color: "rgb(119, 119, 119)",
                    fontSize: "0.85em",
                    paddingTop: "1.5em",
                    marginTop: "0.5em",
                    borderTop: "1px solid #eee",
                  }}
                >
                  Select a project from the list to edit or delete.
                  Alternatively, create a new project by clicking the button
                  below.
                </div>
              </Grid>
            </Grid>
          </Paper>
        </>
      )}
      {!state.isLoading && <CustomLists formType={formType} />}
      <div style={{ padding: "4em" }} />
    </Container>
  );
};
export default Settings;
