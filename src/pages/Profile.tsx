import {
  Button,
  Container,
  Grid,
  IconButton,
  InputBase,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
} from "@mui/material";
import BackupIcon from "@mui/icons-material/Backup";
import DeleteIcon from "@mui/icons-material/Delete";
import SearchIcon from "@mui/icons-material/Search";
import AlarmOff from "@mui/icons-material/AlarmOff";
import AlarmOn from "@mui/icons-material/AlarmOn";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import { createFile, mapFile } from "../api/files";
import * as API from "../api/forms";
import DomniLoader from "../components/DomniLoader";
import DomniTimeAgo from "../components/DomniTimeAgo";
import { LdapPersonModal } from "../components/LdapPerson";
import {
  getModalAPIErrorDispatch,
  getModalMsgDispatch,
} from "../components/Modal";
import { selectAdminForms, selectSupervisorForms } from "../store/Reducer";
import { useStore } from "../store/Store";
import { formConfigs } from "../utilities/Config";
import {
  getFormAccess,
  getLatestSupervisorAction,
  getSize,
  isStaff,
  StatusTag,
} from "../utilities/Helpers";
import "./Settings.css";
import { YearPicker } from "../components/DateWidgets";

interface State {
  userCases: System.Form[];
  userDrafts: System.Form[];
  selectedYear: number;
  loadingCases: boolean;
  rowsPerPage: number;
  currentPage: number;
  supervisorFilter: string;
  submissionFilter: string;
}
const Profile = (props: RouteComponentProps) => {
  const { state, dispatch } = useStore();
  const adminFormsTypes = selectAdminForms(state);
  const supervisorFormsTypes = selectSupervisorForms(state);
  const [localState, setLocalState] = useState<State>({
    userCases: [],
    userDrafts: [],
    rowsPerPage: 10,
    currentPage: 0,
    selectedYear: new Date().getFullYear(),
    loadingCases: true,
    supervisorFilter: "",
    submissionFilter: "",
  });
  const supervisorFilterOpacity = !!localState.supervisorFilter
    ? { opacity: "1" }
    : {};
  const submissionFilterOpacity = !!localState.submissionFilter
    ? { opacity: "1" }
    : {};
  useEffect(() => {
    search();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.user, state.isLoading, localState.selectedYear]);
  const search = async () => {
    const fromDate = moment(
      `${localState.selectedYear}-01-01T00:00:00`
    ).toDate();
    const toDate = moment(
      `${localState.selectedYear + 1}-01-01T00:00:00`
    ).toDate();
    const userCasesResp = await API.getUserCases(fromDate, toDate);
    const userCases: System.Form[] = [];
    (Object.keys(userCasesResp) as System.FormType[]).forEach((formType) => {
      userCasesResp[formType].forEach((form) => userCases.push(form));
    });
    userCases.sort((a, b) => (a.submissionDate < b.submissionDate ? 1 : -1));

    const userDrafts: System.Form[] = [];
    const userDraftsResp = await API.getUserDrafts();
    (Object.keys(userDraftsResp) as System.FormType[]).forEach((formType) => {
      userDraftsResp[formType].forEach((form) => userDrafts.push(form));
    });
    userDrafts.sort((a, b) => ((a._id || "") < (b._id || "") ? 1 : -1));
    setLocalState({
      ...localState,
      userCases,
      userDrafts,
      loadingCases: false,
      currentPage: 0,
    });
  };
  if (state.isLoading) {
    return (
      <Container>
        <h2 style={{ margin: "1em 0.5em 0em" }}>
          <span className="category-preface">Profile</span>
          <span className="primary-dark-color"> {state.user.name}</span>
        </h2>
        <div style={{ marginTop: "-0.5em" }}>{state.user.email}</div>
        <Paper
          elevation={2}
          className="paper"
          style={{ marginTop: "2em", marginBottom: "0.5em" }}
        >
          <DomniLoader show={true} />
        </Paper>
      </Container>
    );
  }
  const supervisorForms: System.Form[] = [];
  (Object.keys(state.pendingSupervisorCases) as System.FormType[]).forEach(
    (formType) => {
      state.pendingSupervisorCases[formType].forEach((form) =>
        supervisorForms.push(form)
      );
    }
  );
  supervisorForms.sort((a, b) =>
    a.submissionDate > b.submissionDate ? -1 : 1
  );

  const deleteAllDrafts = async () => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Delete drafts",
        content: "Are you sure you want to delete all your drafts?",
        btn1Text: "Delete",
        btn1Color: "secondary",
        btn2Text: "Cancel",
        btn1OnClick: async (inputs: System.ModalInputs) => {
          try {
            await API.deleteAllDrafts();
            setLocalState({
              ...localState,
              userDrafts: [],
            });
          } catch (error) {
            dispatch(getModalAPIErrorDispatch(error as System.APIError));
          }
        },
      },
    });
  };
  const deleteForm = async (form: System.Form) => {
    dispatch({
      type: "SET_MODAL_PROPS",
      modalProps: {
        open: true,
        title: "Delete draft",
        content: "Are you sure you want to delete this draft?",
        btn1Text: "Delete",
        btn2Text: "Cancel",
        btn1OnClick: async (inputs: System.ModalInputs) => {
          try {
            await API.deleteForm(form._id, form.formType);
            setLocalState({
              ...localState,
              userDrafts: localState.userDrafts.filter(
                (draft) => draft._id !== form._id
              ),
            });
          } catch (error) {
            dispatch(getModalAPIErrorDispatch(error as System.APIError));
          }
        },
      },
    });
  };
  const handleReminder = async (form: System.Form) => {
    const now = new Date();
    if (moment(form.postponedUntil || now) > moment(now)) {
      const newForm = await API.update({
        form: { ...form, postponedUntil: null },
        updateType: "POSTPONE_REMINDER",
      });
      dispatch(
        getModalMsgDispatch(
          "Cancelled!",
          "The reminder postponement for this submission has been cancelled!"
        )
      );
      dispatch({
        type: "UPDATE_PENDING_SUPERVISOR_CASE",
        pendingSupervisorCase: newForm,
      });
    } else {
      const oneMonthFromNow = new Date();
      oneMonthFromNow.setMonth(oneMonthFromNow.getMonth() + 1);

      dispatch({
        type: "SET_MODAL_PROPS",
        modalProps: {
          open: true,
          title: "Postpone reminders",
          content: "",
          btn1Text: "Postpone",
          btn2Text: "Cancel",
          btn2Color: "info",
          validate: (inputs) => {
            if (!(new Date(inputs.date.value) > new Date())) {
              return {
                status: "FAILURE",
                errorElement: <>The date has to be set to the future</>,
              };
            } else {
              return { status: "SUCCESS" };
            }
          },
          btn1OnClick: async (inputs: System.ModalInputs) => {
            try {
              const newForm = await API.update({
                form: { ...form, postponedUntil: new Date(inputs.date.value) },
                updateType: "POSTPONE_REMINDER",
              });
              dispatch(
                getModalMsgDispatch(
                  "Postponed!",
                  "Your reminders for this submission has been postponed!"
                )
              );
              dispatch({
                type: "UPDATE_PENDING_SUPERVISOR_CASE",
                pendingSupervisorCase: newForm,
              });
            } catch (err) {
              dispatch(getModalAPIErrorDispatch(err as System.APIError));
              return;
            }
          },
          inputs: {
            date: {
              type: "date",
              label: "Postpone reminders until",
              value: oneMonthFromNow.toString(),
            },
          },
        },
      });
    }
  };
  const renderRole = (title: string, desc: string) => {
    return (
      <div style={{ padding: "0.3em 0" }}>
        <span style={{ fontWeight: "bold", color: "#777" }}>{title}</span>{" "}
        {desc}
      </div>
    );
  };
  return (
    <Container>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h2 style={{ margin: "1em 0.5em 0em" }}>
            <span className="category-preface">Profile</span>
            <span className="primary-dark-color"> {state.user.name}</span>
          </h2>
          <div
            data-testid="profile-email-div"
            style={{ marginLeft: "1em", color: "#777" }}
          >
            {state.user.email}
          </div>
        </Grid>

        {/* User Drafts */}
        {(localState.userDrafts.length > 0 || localState.loadingCases) && (
          <Grid item xs={12} style={{ margin: "1em" }}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <span style={{ fontSize: "1.5em" }}>Drafts</span>
              <Button
                data-testid="profile-delete-drafts-btn"
                variant="text"
                color="secondary"
                style={{
                  fontSize: "0.8em",
                }}
                onClick={deleteAllDrafts}
              >
                DISCARD ALL DRAFTS
              </Button>
            </div>
            <Paper style={{ margin: "0.5em 0 2em 0", padding: "1em 2em" }}>
              {localState.loadingCases ? (
                <DomniLoader testid="profile-drafts-domniloader" show={true} />
              ) : (
                localState.userDrafts.map((form) => (
                  <div
                    key={form._id + form.formType}
                    data-testid={`profile-draft-${
                      formConfigs.forms[form.formType].formType
                    }-${form._id}-div`}
                    style={{
                      alignItems: "center",
                      borderBottom: "1px solid #f5f5f5",
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <Link
                      to={`/${formConfigs.forms[form.formType].path}/form/${
                        form._id
                      }`}
                      className="link"
                      style={{ whiteSpace: "nowrap" }}
                    >
                      {form._id} - {formConfigs.forms[form.formType].name}
                    </Link>
                    <div
                      style={{
                        fontStyle: "italic",
                        color: "#777",
                        maxHeight: "1.5em",
                        overflow: "hidden",
                        flex: "1",
                        paddingLeft: "1em",
                      }}
                    >
                      {formConfigs.forms[form.formType].renderSubmissionSummary(
                        form,
                        true
                      ) || "-"}
                    </div>
                    <div style={{ whiteSpace: "nowrap", paddingLeft: "0.5em" }}>
                      <Tooltip title="Discard this draft" placement="top" arrow>
                        <IconButton
                          onClick={() => {
                            deleteForm(form);
                          }}
                          aria-label="delete"
                          size="large"
                        >
                          <DeleteIcon fontSize="small" />
                        </IconButton>
                      </Tooltip>
                    </div>
                  </div>
                ))
              )}
            </Paper>
          </Grid>
        )}
        {/* User Cases */}
        <Grid item xs={12} style={{ margin: "0em 1em" }}>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "baseline",
                marginBottom: "1em",
              }}
            >
              <span style={{ fontSize: "1.5em", alignSelf: "flex-end" }}>
                Your submitted cases
              </span>
              <YearPicker
                value={localState.selectedYear}
                testid="profile-submitted-cases-year-picker"
                style={{
                  minWidth: "5em",
                  marginLeft: "1em",
                  alignSelf: "flex-end",
                }}
                onSelect={(year) => {
                  setLocalState({
                    ...localState,
                    selectedYear: year,
                  });
                }}
              />
            </div>
            <div style={{ marginBottom: "1em" }}>
              <Tooltip
                title="ID, form type, status or supervisor"
                placement="top"
                arrow
              >
                <Paper className="search-input" style={submissionFilterOpacity}>
                  <InputBase
                    data-testid="profile-submitted-cases-filter-input"
                    value={localState.submissionFilter}
                    onChange={(e) =>
                      setLocalState({
                        ...localState,
                        submissionFilter: e.target.value,
                        currentPage: 0,
                      })
                    }
                    placeholder="Filter"
                  />
                  <SearchIcon style={{ paddingRight: "0.3em" }} />
                </Paper>
              </Tooltip>
            </div>
          </div>
          {localState.userCases.length === 0 && !localState.loadingCases ? (
            <Paper elevation={2} className="paper" style={{ margin: "0" }}>
              <i>You have no submitted cases for {localState.selectedYear}</i>
            </Paper>
          ) : (
            <div>
              <TableContainer component={Paper}>
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell>ID</TableCell>
                      <TableCell>FORM TYPE</TableCell>
                      <TableCell>FORM</TableCell>
                      <TableCell>SUBMITTED</TableCell>
                      <TableCell style={{ textAlign: "center" }}>
                        STATUS
                      </TableCell>
                      <TableCell>SUPERVISOR</TableCell>
                      <TableCell></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {localState.loadingCases && (
                      <TableRow>
                        <TableCell colSpan={6}>
                          <DomniLoader
                            testid="profile-submitted-cases-domniloader"
                            show={true}
                          />
                        </TableCell>
                      </TableRow>
                    )}

                    {localState.userCases
                      .filter((form) => {
                        const filter =
                          localState.submissionFilter.toLowerCase();
                        if (!filter) {
                          return true;
                        }
                        return (
                          form._id?.includes(filter) ||
                          form.externalId?.includes(filter) ||
                          form.formType.toLowerCase().includes(filter) ||
                          form.status.toLowerCase().includes(filter) ||
                          getLatestSupervisorAction(form)
                            .name.toLowerCase()
                            .includes(filter)
                        );
                      })
                      .slice(
                        localState.rowsPerPage * localState.currentPage,
                        localState.rowsPerPage * localState.currentPage +
                          localState.rowsPerPage
                      )
                      .map((form: System.Form) => {
                        return (
                          <TableRow
                            data-testid={`profile-submitted-cases-${form.formType}-${form._id}-row`}
                            key={form._id + form.formType}
                          >
                            <TableCell>
                              <div
                                data-testid={`profile-submitted-cases-${form.formType}-${form._id}-id`}
                                style={{
                                  maxWidth: "10em",
                                  whiteSpace: "nowrap",
                                  overflow: "hidden",
                                }}
                              >
                                <Tooltip
                                  title={
                                    form.externalId
                                      ? "External ID: " + form.externalId
                                      : ""
                                  }
                                  placement="left"
                                  arrow
                                >
                                  {getFormAccess(form, state.user, "Profile")
                                    .readAccess !== "BASIC" ? (
                                    <Link
                                      to={`/${
                                        formConfigs.forms[form.formType].path
                                      }/form/${form._id}`}
                                      className="link"
                                    >
                                      {form._id}{" "}
                                      {form.externalId
                                        ? `/ ${form.externalId}`
                                        : ""}
                                    </Link>
                                  ) : (
                                    <>
                                      {" "}
                                      {form._id}{" "}
                                      {form.externalId
                                        ? `/ ${form.externalId}`
                                        : ""}
                                    </>
                                  )}
                                </Tooltip>
                              </div>
                            </TableCell>
                            <TableCell
                              data-testid={`profile-submitted-cases-${form.formType}-${form._id}-formtype-name`}
                            >
                              {formConfigs.forms[form.formType].name}
                            </TableCell>
                            <TableCell>
                              <Tooltip
                                title={formConfigs.forms[
                                  form.formType
                                ].renderSubmissionSummary(form, true)}
                                placement="top-start"
                              >
                                <div
                                  data-testid={`profile-submitted-cases-${form.formType}-${form._id}-summary`}
                                  style={{
                                    fontStyle: "italic",
                                    color: "#777",
                                    maxWidth: "20em",
                                    maxHeight: "1.5em",
                                    overflow: "hidden",
                                  }}
                                >
                                  {formConfigs.forms[
                                    form.formType
                                  ].renderSubmissionSummary(form, true)}
                                </div>
                              </Tooltip>
                            </TableCell>
                            <TableCell>
                              <DomniTimeAgo datetime={form.submissionDate} />
                            </TableCell>
                            <TableCell style={{ textAlign: "center" }}>
                              {" "}
                              <StatusTag
                                testid={`profile-submitted-cases-${form.formType}-${form._id}-status`}
                                status={form.status}
                              ></StatusTag>
                            </TableCell>
                            <TableCell>
                              <LdapPersonModal
                                testid={`profile-submitted-cases-${form.formType}-${form._id}-supervisor`}
                                {...getLatestSupervisorAction(form)}
                              />
                            </TableCell>
                            <TableCell>
                              {form.status === "APPROVED" &&
                                !form.isArchived &&
                                formConfigs.forms[form.formType].fileUpload && (
                                  <>
                                    <input
                                      style={{ display: "none" }}
                                      id={`file-input-${form._id}`}
                                      type="file"
                                      name="file"
                                      onChange={async (
                                        e: React.ChangeEvent<HTMLInputElement>
                                      ) => {
                                        if (
                                          e.currentTarget &&
                                          e.currentTarget.files &&
                                          e.currentTarget.files.length > 0
                                        ) {
                                          let newFile =
                                            e.currentTarget.files[0];
                                          const jsonData = await createFile(
                                            newFile
                                          );

                                          const res = await mapFile({
                                            fileName:
                                              jsonData + "-" + newFile.name,
                                            size: getSize(newFile.size),
                                            id: form._id as string,
                                            formType: form.formType,
                                          });
                                          dispatch(
                                            getModalMsgDispatch(
                                              "File upload",
                                              res
                                                ? "The file was successfully added"
                                                : "The file could not be added"
                                            )
                                          );
                                        }
                                      }}
                                    />
                                    <Tooltip
                                      title="Click here to upload supplementary files or documents to this already finalized submission. The current supervisor will also be notified."
                                      placement="top"
                                      arrow
                                    >
                                      <label htmlFor={`file-input-${form._id}`}>
                                        <Button component="span" variant="text">
                                          <BackupIcon
                                            fontSize="small"
                                            style={{ color: "rgba(0,0,0,0.5" }}
                                          />
                                        </Button>
                                      </label>
                                    </Tooltip>
                                  </>
                                )}
                            </TableCell>
                          </TableRow>
                        );
                      })}
                  </TableBody>
                </Table>
                <TablePagination
                  rowsPerPageOptions={[10, 100]}
                  component="div"
                  count={localState.userCases.length}
                  rowsPerPage={localState.rowsPerPage}
                  page={localState.currentPage}
                  onPageChange={(event, page) =>
                    setLocalState({ ...localState, currentPage: page })
                  }
                  onRowsPerPageChange={(e) =>
                    setLocalState({
                      ...localState,
                      rowsPerPage: parseInt(e.target.value, 10),
                    })
                  }
                />
              </TableContainer>
            </div>
          )}
        </Grid>
        <Grid item xs={12}>
          <Paper
            elevation={2}
            className="paper"
            style={{
              margin: "1em",
              padding: "1em 3em",
            }}
          >
            <div
              style={{
                fontSize: "1em",
                borderBottom: "1px solid rgb(238, 238, 238)",
                marginBottom: "0.5em",
                fontWeight: "bold",
                color: "#777",
                textTransform: "uppercase",
              }}
            >
              Your roles
            </div>
            <div
              style={{
                flex: "1",
                paddingBottom: "1em",
                marginBottom: "0.5em",
                borderBottom: "1px solid #eee",
              }}
              data-testid={`profile-roles-div`}
            >
              {isStaff(state.user)
                ? renderRole(
                    "MAX IV Staff",
                    "lets you fill out and submit most standard forms available in Domni. Determined by membership of the AD group Staff"
                  )
                : renderRole(
                    "External users",
                    "have limited access to Domni forms. All users who are not members of the AD group Staff are considered external users."
                  )}
              {supervisorFormsTypes.length > 0 &&
                renderRole(
                  "Form Supervisors",
                  "are users who are available as supervisors of one or several form types. Supervisors can approve, reject or reassign submission they've been assigned."
                )}
              {adminFormsTypes.length > 0 &&
                renderRole(
                  "Form Administrators",
                  "are users who have been designated as an administrator of one or several form types in Domni. Administrators can, in addition to handle all submissions of that form type,  configure available supervisors and other other form type properties"
                )}
            </div>
            <div
              style={{
                color: "rgb(119, 119, 119)",
                fontSize: "0.85em",
              }}
            >
              Roles are determined based on the information stored about you in
              AD
            </div>
          </Paper>
        </Grid>
        <Grid
          item
          xs={12}
          style={{
            display: "flex",
            justifyContent: "space-between",
            flexWrap: "wrap",
          }}
        >
          <Paper
            elevation={2}
            className="paper"
            style={{
              flex: "1 1 auto",
              margin: "1em",
              padding: "1em 3em",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                fontSize: "1em",
                borderBottom: "1px solid rgb(238, 238, 238)",
                marginBottom: "0.5em",
                fontWeight: "bold",
                color: "#777",
                textTransform: "uppercase",
              }}
            >
              Administered forms
            </div>
            <div
              style={{
                flex: "1",
                paddingBottom: "1em",
                marginBottom: "0.5em",
                borderBottom: "1px solid #eee",
              }}
            >
              {adminFormsTypes.length > 0 ? (
                adminFormsTypes.map((form) => <div key={form}>{form}</div>)
              ) : (
                <i>You are not the administrator of any form type</i>
              )}
            </div>
            <div
              style={{
                color: "rgb(119, 119, 119)",
                fontSize: "0.85em",
              }}
            >
              These are form types where you have full admin access.
            </div>
          </Paper>
          <Paper
            elevation={2}
            className="paper"
            style={{
              flex: "1 1 auto",
              margin: "1em",
              padding: "1em 3em",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
            }}
          >
            <div
              style={{
                fontSize: "1em",
                borderBottom: "1px solid rgb(238, 238, 238)",
                marginBottom: "0.5em",
                fontWeight: "bold",
                color: "#777",
                textTransform: "uppercase",
              }}
            >
              Supervised forms
            </div>
            <div
              style={{
                flex: "1",
                paddingBottom: "1em",
                marginBottom: "0.5em",
                borderBottom: "1px solid #eee",
              }}
            >
              {supervisorFormsTypes.length > 0 ? (
                supervisorFormsTypes.map((form) => <div key={form}>{form}</div>)
              ) : (
                <i>You are not a supervisor in any form type</i>
              )}
            </div>
            <div
              style={{
                color: "rgb(119, 119, 119)",
                fontSize: "0.85em",
              }}
            >
              These are form types where you are avaliable as a supervisor
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} style={{ margin: "0em 1em" }}>
          <div
            style={{
              display: "flex",
              marginBottom: "1em",
              justifyContent: "space-between",
              flexWrap: "wrap",
            }}
          >
            <div
              style={{
                fontSize: "1.5em",
                alignSelf: "flex-end",
              }}
            >
              Your pending supervisor cases
            </div>
            <Tooltip
              title="ID, form type, submitter or current supervisor"
              placement="top"
              arrow
            >
              <Paper className="search-input" style={supervisorFilterOpacity}>
                <InputBase
                  value={localState.supervisorFilter}
                  onChange={(e) =>
                    setLocalState({
                      ...localState,
                      supervisorFilter: e.target.value,
                    })
                  }
                  placeholder="Filter"
                />
                <SearchIcon style={{ paddingRight: "0.3em" }} />
              </Paper>
            </Tooltip>
          </div>
          {/* Supervisor cases */}
          {supervisorForms.length === 0 && !localState.loadingCases ? (
            <Paper elevation={2} className="paper" style={{ margin: "0" }}>
              <i>You have no pending cases</i>
            </Paper>
          ) : (
            <TableContainer component={Paper}>
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell>FORM TYPE</TableCell>
                    <TableCell>FORM</TableCell>
                    <TableCell>SUBMITTED</TableCell>
                    <TableCell>SUBMITTER</TableCell>
                    <TableCell>CURRENT SUPERVISOR</TableCell>
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {localState.loadingCases && (
                    <TableRow>
                      <TableCell colSpan={6}>
                        <DomniLoader show={true} />
                      </TableCell>
                    </TableRow>
                  )}
                  {supervisorForms
                    .filter((form) => {
                      const filter = localState.supervisorFilter.toLowerCase();
                      if (!filter) {
                        return true;
                      }
                      return (
                        form._id?.includes(filter) ||
                        form.formType.toLowerCase().includes(filter) ||
                        form.submitterName?.toLowerCase().includes(filter) ||
                        getLatestSupervisorAction(form)
                          .name.toLowerCase()
                          .includes(filter)
                      );
                    })
                    .map((form) => {
                      const isCurrentSupervisor =
                        getLatestSupervisorAction(form).email.toLowerCase() ===
                        state.user.email.toLowerCase();
                      const now = new Date();
                      const isPostponed =
                        moment(form.postponedUntil || now) > moment(now);
                      const reminderTooltip = isPostponed
                        ? `No new reminders will be sent out before ${moment(
                            form.postponedUntil
                          ).format("YYYY-MM-DD")}. Click to resume reminders.`
                        : `Currently sending out regular reminders (every 3 days). Click to postpone.`;
                      return (
                        <TableRow
                          key={form._id + form.formType}
                          className={
                            isCurrentSupervisor ? "highlight-background" : ""
                          }
                          data-testid={`profile-pending-supervisor-cases-${form.formType}-${form._id}-row`}
                        >
                          <TableCell>
                            <Link
                              data-testid={`profile-pending-supervisor-cases-${form.formType}-${form._id}-id`}
                              to={`/${
                                formConfigs.forms[form.formType].path
                              }/approval/${form._id}`}
                              className="link"
                            >
                              {form._id}
                            </Link>
                          </TableCell>
                          <TableCell>
                            {formConfigs.forms[form.formType].name}
                          </TableCell>
                          <TableCell>
                            <div
                              data-testid={`profile-pending-supervisor-cases-${form.formType}-${form._id}-summary`}
                              style={{
                                fontStyle: "italic",
                                color: "#777",
                                maxWidth: "20em",
                                maxHeight: "1.5em",
                                overflow: "hidden",
                              }}
                            >
                              {formConfigs.forms[
                                form.formType
                              ].renderSubmissionSummary(form, true)}
                            </div>
                          </TableCell>
                          <TableCell>
                            <DomniTimeAgo datetime={form.submissionDate} />
                          </TableCell>
                          <TableCell>{form.submitterName}</TableCell>
                          <TableCell>
                            {isCurrentSupervisor ? (
                              <i>You</i>
                            ) : (
                              <LdapPersonModal
                                {...getLatestSupervisorAction(form)}
                              />
                            )}
                          </TableCell>
                          <TableCell>
                            {isCurrentSupervisor && (
                              <Tooltip
                                title={reminderTooltip}
                                placement="top"
                                arrow
                              >
                                <Button
                                  onClick={() => handleReminder(form)}
                                  component="span"
                                  variant="text"
                                >
                                  {isPostponed ? (
                                    <AlarmOff
                                      fontSize="small"
                                      style={{ color: "rgba(0,0,0,0.5" }}
                                    />
                                  ) : (
                                    <AlarmOn
                                      fontSize="small"
                                      style={{ color: "rgba(0,0,0,0.5" }}
                                    />
                                  )}
                                </Button>
                              </Tooltip>
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
              <div
                style={{
                  color: "rgb(119, 119, 119)",
                  fontSize: "0.85em",
                  padding: "1em",
                  borderTop: "1px solid #ddd",
                }}
              >
                A yellow hightlight indicates that a form is currently assigned
                to you. Click on the ID to manage these submission.
              </div>
            </TableContainer>
          )}
        </Grid>
      </Grid>

      <div style={{ padding: "4em" }} />
    </Container>
  );
};

export default Profile;
