import {
  FormControl,
  Grid,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React from "react";
import { DatePicker } from "../components/DateWidgets";
import { ProjectSelect } from "../components/ProjectSelect";
import { SupervisorSelect } from "../components/SupervisorSelect";
import { DomniTextValidator } from "../components/TextValidator";
import * as Constants from "../constants";
import { TRAVEL } from "../constants";
import { selectSupervisor } from "../store/Reducer";
import Form from "./Form";

const emptyTravelForm = (): System.TravelForm => {
  const endDate = new Date();
  endDate.setDate(endDate.getDate() + 7);
  return {
    _id: "",
    formType: TRAVEL,
    submitterName: "",
    submitterEmail: "",
    destination: "",
    travelPurpose: "",
    project: "",
    startDate: new Date(),
    endDate,
    expenses: {
      travel: 0,
      accommodation: 0,
      conferenceFee: 0,
      subsistence: 0,
    },
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name: "",
        email: "",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    totalExpenses: 0,
    files: [],
    submissionDate: new Date(),
    isArchived: false,
    status: "UNSAVED",
  };
};
function TravelForm(props: System.FormProps) {
  const handleExpensesChange = (
    event: React.ChangeEvent<{ name: string; value: any }>,
    formData: System.TravelForm,
    updateFormData: (form: System.TravelForm) => void
  ) => {
    const newFormData = {
      ...formData,
      expenses: {
        ...formData.expenses,
        [event.target.name]: parseInt(event.target.value) || 0,
      },
    };
    updateFormData({
      ...newFormData,
      totalExpenses: Object.entries(newFormData.expenses)
        .map((entry) => entry[1])
        .reduce((prev, curr) => prev + curr),
    });
  };
  const FormInputs = (props: System.FormInputsProps<System.TravelForm>) => {
    const { formData, formAccess, updateFormData, handleInputChange, state } =
      props;
    return (
      <>
        <Grid item xs={12}>
          <Typography className="form-heading">Destination Details</Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip
            title="Enter the destination to which you are traveling."
            placement="top-start"
          >
            <DomniTextValidator
              label="Destination"
              onChange={handleInputChange}
              name="destination"
              value={formData.destination}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item sm={8} xs={12}>
          <Tooltip
            title="Enter the purpose of the requested travel."
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              label="Purpose of travel"
              onChange={handleInputChange}
              name="travelPurpose"
              value={formData.travelPurpose}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
              multiline
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Dates</Typography>
        </Grid>
        <Grid item xs={12} sm={8} md={6}>
          <DatePicker
            disabled={!formAccess.canEdit}
            start={{
              label: "Departure date",
              name: "startDate",
              date: formData.startDate,
              setDate: (date: Date | undefined) =>
                updateFormData({ ...formData, startDate: date }),
            }}
            end={{
              label: "Return date",
              name: "endDate",
              date: formData.endDate,
              setDate: (date: Date | undefined) =>
                updateFormData({ ...formData, endDate: date }),
            }}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography className="form-heading">
            Travel Expenses (SEK)
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={2}>
          <Tooltip
            title="Enter the approximate amount required for travel (in SEK)."
            placement="top-start"
          >
            <DomniTextValidator
              name="travel"
              label="TRAVEL"
              value={formData.expenses.travel}
              onChange={(
                event: React.ChangeEvent<{ name: string; value: any }>
              ) => handleExpensesChange(event, formData, updateFormData)}
              onFocus={(event) => (event.target as any).select()}
              fullWidth={true}
              type="number"
              validators={["minNumber:0"]}
              errorMessages="Cannot be negative"
              variant="outlined"
            />
          </Tooltip>
        </Grid>

        <Grid item xs={12} sm={6} md={2}>
          <Tooltip
            title="Enter the approximate amount required for accommodation (in SEK)."
            placement="top-start"
          >
            <DomniTextValidator
              name="accommodation"
              label="ACCOMMODATION"
              value={formData.expenses.accommodation}
              onChange={(
                event: React.ChangeEvent<{ name: string; value: any }>
              ) => handleExpensesChange(event, formData, updateFormData)}
              onFocus={(event) => (event.target as any).select()}
              fullWidth={true}
              type="number"
              validators={["minNumber:0"]}
              errorMessages="Cannot be negative"
              variant="outlined"
            />
          </Tooltip>
        </Grid>

        <Grid item xs={12} sm={6} md={2}>
          <Tooltip
            title="Enter the approximate amount required for conference or participation fees (in SEK)."
            placement="top-start"
          >
            <DomniTextValidator
              name="conferenceFee"
              label="CONFERENCE FEE"
              value={formData.expenses.conferenceFee}
              onChange={(
                event: React.ChangeEvent<{ name: string; value: any }>
              ) => handleExpensesChange(event, formData, updateFormData)}
              onFocus={(event) => (event.target as any).select()}
              fullWidth={true}
              type="number"
              validators={["minNumber:0"]}
              errorMessages="Cannot be negative"
              variant="outlined"
            />
          </Tooltip>
        </Grid>

        <Grid item xs={12} sm={6} md={2}>
          <Tooltip
            title="Enter the approximate amount required for subsistence (in SEK)."
            placement="top-start"
          >
            <DomniTextValidator
              name="subsistence"
              label="SUBSISTENCE"
              value={formData.expenses.subsistence}
              onChange={(
                event: React.ChangeEvent<{ name: string; value: any }>
              ) => handleExpensesChange(event, formData, updateFormData)}
              onFocus={(event) => (event.target as any).select()}
              fullWidth={true}
              type="number"
              validators={["minNumber:0"]}
              errorMessages="Cannot be negative"
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} md={4}>
          <TextField
            fullWidth={true}
            name="totalExpenses"
            label="TOTAL EXPENSES"
            style={{ width: "100%" }}
            value={formData.totalExpenses}
            variant="outlined"
            disabled={true}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Budget approval</Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip title={Constants.BUDGET_TOOLTIP} placement="top-start">
            <FormControl variant="outlined" fullWidth={true}>
              <ProjectSelect
                required={true}
                formType="TRAVEL"
                selectedProject={formData.project}
                onChange={(project: System.Project) => {
                  const supervisor = selectSupervisor(
                    state,
                    project.project,
                    "TRAVEL"
                  );
                  if (supervisor) {
                    updateFormData({
                      ...formData,
                      project: project.project,
                      supervisorActions: [
                        {
                          ...formData.supervisorActions[0],
                          name: supervisor.name,
                          email: supervisor.email,
                        },
                      ],
                    });
                  } else {
                    updateFormData({ ...formData, project: project.project });
                  }
                }}
              />
            </FormControl>
          </Tooltip>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip
            title={Constants.BUDGET_RESPONSIBLE_TOOLTIP}
            placement="top-start"
          >
            <FormControl variant="outlined" fullWidth={true}>
              <SupervisorSelect
                formType="TRAVEL"
                selectedEmail={formData.supervisorActions[0].email}
                onChange={(supervisor) =>
                  updateFormData({
                    ...formData,
                    supervisorActions: [
                      {
                        ...formData.supervisorActions[0],
                        name: supervisor.name,
                        email: supervisor.email,
                      },
                    ],
                  })
                }
              />
            </FormControl>
          </Tooltip>
        </Grid>
      </>
    );
  };

  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={emptyTravelForm()}
      formType="TRAVEL"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
}

export default TravelForm;
