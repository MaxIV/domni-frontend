import {
  Checkbox,
  FormControlLabel,
  Grid,
  Tooltip,
  Typography,
} from "@mui/material";
import { DatePicker } from "../components/DateWidgets";
import { SimpleSelect } from "../components/SimpleSelect";
import { DomniTextValidator } from "../components/TextValidator";
import { HOTEL } from "../constants";
import { selectSupervisor } from "../store/Reducer";
import { useStore } from "../store/Store";
import Form from "./Form";

const getEmptyHotelForm = (state: System.IState): System.HotelForm => {
  return {
    _id: "",
    formType: HOTEL,
    submitterName: "",
    submitterEmail: "",
    guestName: "",
    guestEmail: "",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name:
          selectSupervisor(state, "Default", HOTEL)?.name || "MAX IV Reception",
        email:
          selectSupervisor(state, "Default", HOTEL)?.email ||
          "reception@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    status: "UNSAVED",
    submissionDate: new Date(),
    isArchived: false,
    submitterPhoneNumber: "",
    guestPhoneNumber: "",
    checkInDate: new Date(),
    checkOutDate: new Date(),
    breakfast: false,
    lateArrival: false,
    paymentType: "",
    activityNumber: "",
    additionalMsg: "",
  };
};
const HotelForm = (props: System.FormProps) => {
  const { state: initialState } = useStore();
  const FormInputs = (props: System.FormInputsProps<System.HotelForm>) => {
    const { formData, updateFormData, handleInputChange, formAccess } = props;
    return (
      <>
        <Grid item xs={12} sm={6}>
          <DomniTextValidator
            fullWidth={true}
            label="Submitter phone number"
            onChange={handleInputChange}
            name="submitterPhoneNumber"
            value={formData.submitterPhoneNumber || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Guest Details</Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Guest name"
            onChange={handleInputChange}
            name="guestName"
            value={formData.guestName || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Guest email"
            onChange={handleInputChange}
            name="guestEmail"
            value={formData.guestEmail || ""}
            validators={["required", "isEmail"]}
            errorMessages={["This field is required", "Email is not valid"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={4}>
          <DomniTextValidator
            fullWidth={true}
            label="Guest phone number"
            onChange={handleInputChange}
            name="guestPhoneNumber"
            value={formData.guestPhoneNumber || ""}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Dates</Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <DatePicker
            disabled={!formAccess.canEdit}
            start={{
              label: "Check-in date",
              name: "checkInDate",
              date: formData.checkInDate,
              setDate: (date: Date | undefined) =>
                updateFormData({ ...formData, checkInDate: date }),
            }}
            end={{
              label: "Check-out date",
              name: "checkOutDate",
              date: formData.checkOutDate,
              setDate: (date: Date | undefined) =>
                updateFormData({ ...formData, checkOutDate: date }),
            }}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Tooltip
            title={
              "The reception at MAX IV is open weekdays between 08:00-16:00. Arrival at other times are considered late arrivals, in which case the key will be left in a safety box and the guest will receive the information."
            }
            placement="top-start"
          >
            <FormControlLabel
              style={{ marginTop: "0.5em" }}
              className="link"
              control={
                <Checkbox
                  size="medium"
                  disabled={!formAccess.canEdit}
                  checked={formData.lateArrival}
                  onChange={() =>
                    updateFormData({
                      ...formData,
                      lateArrival: !formData.lateArrival,
                    })
                  }
                  name="lateArrival"
                />
              }
              label="Late arrival"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Tooltip
            title={"Check the box if your guest wants to have breakfast."}
            placement="top-start"
          >
            <FormControlLabel
              style={{ marginTop: "0.5em" }}
              className="link"
              control={
                <Checkbox
                  size="medium"
                  disabled={!formAccess.canEdit}
                  checked={formData.breakfast}
                  onChange={() =>
                    updateFormData({
                      ...formData,
                      breakfast: !formData.breakfast,
                    })
                  }
                  name="breakfast"
                />
              }
              label="Breakfast"
            />
          </Tooltip>
        </Grid>
        <Grid item xs={12} className="form-heading">
          Additional information
        </Grid>
        <Grid item xs={12} sm={6}>
          <SimpleSelect
            disabled={{
              value: !formAccess.canEdit,
            }}
            options={[
              { value: "Guest to pay at the reception" },
              { value: "MAX IV payment" },
              { value: "Online payment" },
            ]}
            selectedValue={
              formData.paymentType || "Guest to pay at the reception"
            }
            title="Payment Type"
            name="paymentType"
            onChange={(event) => {
              handleInputChange(event);
            }}
          />
        </Grid>
        {formData.paymentType === "MAX IV payment" ? (
          <Grid item xs={12} sm={6}>
            <DomniTextValidator
              fullWidth={true}
              label="Project/activity"
              onChange={handleInputChange}
              placeholder="Please specify the paying project/activity"
              name="activityNumber"
              validators={["required"]}
              errorMessages={["This field is required"]}
              value={formData.activityNumber || ""}
              variant="outlined"
            />
          </Grid>
        ) : null}
        {formData.paymentType === "Online payment" ? (
          <Grid item xs={12}>
            <Typography paragraph>
              To make an online payment please click{" "}
              <a
                href="https://luccp.adm.lu.se/"
                target="_blank"
                className="link"
                rel="noopener noreferrer"
              >
                here{" "}
              </a>
              and provide the information needed.
              <br />
              Please state{" "}
              <mark className="highlight-background">
                MAX IV Guest House
              </mark>{" "}
              in <mark className="highlight-background">Event/Händelse</mark>{" "}
              and on{" "}
              <mark className="highlight-background">
                Remittance information/betalningsinformation
              </mark>
              . On{" "}
              <mark className="highlight-background">Other information</mark>{" "}
              please state the{" "}
              <mark className="highlight-background">guest name</mark>.
              <br />
              As soon as our finance department has received your payment, we
              will email you a copy of your receipt. This could take a few days.
            </Typography>
          </Grid>
        ) : null}
        <Grid item xs={12}>
          <DomniTextValidator
            fullWidth={true}
            label="Additional message or request"
            onChange={handleInputChange}
            name="additionalMsg"
            value={formData.additionalMsg || ""}
            variant="outlined"
            multiline={true}
            rows={props.embedded ? undefined : 3}
          />
        </Grid>
      </>
    );
  };

  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={getEmptyHotelForm(initialState)}
      formType="HOTEL_BOOKING"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
};

export default HotelForm;
