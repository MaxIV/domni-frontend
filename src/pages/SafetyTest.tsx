import {
  Checkbox,
  Collapse,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useStore } from "../store/Store";
import { selectSupervisor } from "../store/Reducer";
import { SimpleSelect } from "../components/SimpleSelect";
import { SAFETY_TEST } from "../constants";
import Form from "./Form";
import { getModalMsgDispatch } from "../components/Modal";
import { createSet, isStaff } from "../utilities/Helpers";
import { LanguageIcon } from "../components/LanguageIcon";
import { DomniLink } from "../components/DomniLink";

const SUB_I_GENERAL = 0;
const SUB_I_RADIATION = 3;
const SUB_I_CHEMICAL = 6;
const SUB_I_ENVIRONMENTAL = 10;

const emptySafetyTestForm = (state: System.IState): System.SafetyTestForm => {
  return {
    _id: "",
    formType: SAFETY_TEST,
    submitterName: "",
    submitterEmail: "",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name:
          selectSupervisor(state, "Default", SAFETY_TEST)?.name ||
          "MAX IV Reception",
        email:
          selectSupervisor(state, "Default", SAFETY_TEST)?.email ||
          "reception@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    submissionDate: new Date(),
    status: "UNSAVED",
    isArchived: false,
    employer: isStaff(state.user) ? "MAX IV" : "",
    contactPerson: "",
    moreThanTwoMonths: false,
    phoneNumber: state.user.phone,
    signature: false,
    answers: [
      ...basicQuestions.map((_, index) => {
        return {
          category: "BASIC" as System.SafetyTestAnswerCategory,
          qId: index.toString(),
          aId: "",
        };
      }),
      ...radiationQuestions.map((_, index) => {
        return {
          category: "RADIATION" as System.SafetyTestAnswerCategory,
          qId: index.toString(),
          aId: "",
        };
      }),
      ...chemicalQuestions.map((_, index) => {
        return {
          category: "CHEMICAL" as System.SafetyTestAnswerCategory,
          qId: index.toString(),
          aId: "",
        };
      }),
      ...bioQuestions.map((_, index) => {
        return {
          category: "BIO" as System.SafetyTestAnswerCategory,
          qId: index.toString(),
          aId: "",
        };
      }),
    ],
    hasRadiation: false,
    hasChemical: false,
    hasBio: false,
  };
};

function SafetyTestForm(props: System.FormProps) {
  const { state: initialState } = useStore();
  const { dispatch } = useStore();
  const [lang, setLang] = useState<"en" | "sv">("en");
  const i18n = (en: string, sv: string) => {
    return lang === "en" ? en : sv;
  };
  const getBasicSubCategory = (i: number) => {
    return i < SUB_I_RADIATION
      ? i18n("General and fire safety", "Allmän säkerhet och brandsäkerhet")
      : i < SUB_I_CHEMICAL
        ? i18n(
            "Radiation safety for supervised areas",
            "Strålsäkerhet för skyddat område"
          )
        : i < SUB_I_ENVIRONMENTAL
          ? i18n("Basic chemical safety", "Grundnivå kemikaliesäkerhet")
          : i18n(
              "Recycling and waste handling",
              "Återvinning och hantering av farligt avfall"
            );
  };

  const maybeSubtitle = (i: number, question: Question) => {
    if (question.category !== "BASIC") {
      return null;
    }
    if (i !== 0 && i !== 3 && i !== 6 && i !== 10) {
      return null;
    }
    const title = getBasicSubCategory(i);
    return (
      <Grid
        item
        xs={12}
        style={{
          fontWeight: "bold",
          color: "#646464",
          paddingTop: "1em",
          paddingBottom: "0em",
        }}
      >
        {title}
      </Grid>
    );
  };
  const isErrornous = (
    answer: System.SafetyTestFormAnswer,
    hasRadiation: boolean,
    hasChemical: boolean,
    hasBio: boolean
  ): { error: boolean; msg: string } => {
    switch (answer.category) {
      case "BASIC":
        return {
          error:
            basicQuestions[
              parseInt(answer.qId)
            ].correctOptionIndex.toString() !== answer.aId,
          msg:
            i18n("Basic access", "Grundläggande tillträde") +
            " - " +
            getBasicSubCategory(parseInt(answer.qId)),
        };
      case "RADIATION":
        if (!hasRadiation) {
          return {
            error: false,
            msg: "",
          };
        }
        return {
          error:
            radiationQuestions[
              parseInt(answer.qId)
            ].correctOptionIndex.toString() !== answer.aId,
          msg: i18n(
            "Radiation safety for controlled areas",
            "Strålsäkerhet för Kontrollerat Område"
          ),
        };
      case "CHEMICAL":
        if (!hasChemical) {
          return { error: false, msg: "" };
        }
        return {
          error:
            chemicalQuestions[
              parseInt(answer.qId)
            ].correctOptionIndex.toString() !== answer.aId,
          msg: i18n("Extended Chemical Safety", "Utökad Kemikaliesäkerhet"),
        };
      case "BIO":
        if (!hasBio) {
          return { error: false, msg: "" };
        }
        return {
          error:
            bioQuestions[parseInt(answer.qId)].correctOptionIndex.toString() !==
            answer.aId,
          msg: i18n("Biosafety", "Biosäkerhet"),
        };
    }
  };
  const FormInputs = (props: System.FormInputsProps<System.SafetyTestForm>) => {
    const { formData, formAccess, updateFormData, handleInputChange } = props;

    const renderQuestion = (
      question: Question,
      questionIndex: number,
      required: boolean
    ) => {
      const qId = questionIndex.toString();
      const answer = formData.answers.filter(
        (answer) => answer.category === question.category && answer.qId === qId
      )[0];
      return (
        <React.Fragment key={questionIndex}>
          {maybeSubtitle(questionIndex, question)}
          <Grid item xs={12}>
            <FormControl
              style={{
                width: "100%",
                backgroundColor: "#f0f0f0",
                borderRadius: "0.3em",
              }}
            >
              <div
                style={{
                  padding: "1em",
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div>
                  <FormLabel>
                    {questionIndex + getIndexOffset(question.category) + 1}.{" "}
                    {question.description[lang]}
                  </FormLabel>
                  <RadioGroup
                    name={qId + "-" + question.category}
                    value={answer.aId}
                    onChange={(event) => {
                      updateFormData({
                        ...formData,
                        answers: formData.answers.map((answer) => {
                          if (
                            answer.qId === qId &&
                            answer.category === question.category
                          ) {
                            return { ...answer, aId: event.target.value };
                          }
                          return answer;
                        }),
                      });
                    }}
                  >
                    {question.options[lang].map((option, index) => {
                      return (
                        <FormControlLabel
                          key={option}
                          value={index.toString()}
                          control={
                            <Radio
                              required={required}
                              disabled={!formAccess.canEdit}
                              data-testid={`safety-test-${question.category}-question-${questionIndex}-option-${index}`}
                            />
                          }
                          label={option}
                        />
                      );
                    })}
                  </RadioGroup>
                </div>
                {question.image && (
                  <img
                    alt={question.image}
                    style={{ height: "150px" }}
                    src={process.env.PUBLIC_URL + "/" + question.image}
                  />
                )}
              </div>
            </FormControl>
          </Grid>
        </React.Fragment>
      );
    };

    return (
      <>
        <>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth={true}
              label="Employer"
              onChange={(event) => handleInputChange(event)}
              name="employer"
              value={formData.employer}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField
              fullWidth={true}
              label="Phone number"
              onChange={(event) => handleInputChange(event)}
              name="phoneNumber"
              value={formData.phoneNumber}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            I estimate that I will spend more than 2 months at MAX IV during the
            coming 12 months
          </Grid>
          <Grid item xs={12} md={2}>
            <SimpleSelect
              disabled={{
                value: formAccess.isStaff,
                msg: "This field only applies to contractors",
              }}
              options={[{ value: "Yes" }, { value: "No" }]}
              selectedValue={
                formAccess.isStaff
                  ? ""
                  : formData.moreThanTwoMonths
                    ? "Yes"
                    : "No"
              }
              title="Click to select"
              testid="more-than-two-months"
              name="moreThanTwoMonths"
              onChange={(event) =>
                updateFormData({
                  ...formData,
                  moreThanTwoMonths: event.target.value === "Yes",
                })
              }
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Tooltip
              title={
                formAccess.isStaff
                  ? "This field only applies to contractors"
                  : ""
              }
              placement="top-start"
            >
              <TextField
                fullWidth={true}
                disabled={formAccess.isStaff}
                label="MAX IV contact person"
                onChange={(event) => handleInputChange(event)}
                name="contactPerson"
                value={formData.contactPerson}
                variant="outlined"
              />
            </Tooltip>
          </Grid>
        </>
        {/* basic access */}
        <Grid
          item
          xs={12}
          style={{
            paddingBottom: "0",
            marginTop: "3em",
            paddingTop: "2em",
            borderTop: "2px dashed #ddd",
          }}
        ></Grid>
        <Grid item xs={10}>
          <Typography className="form-heading">
            {i18n(
              "Safety Test - Basic access",
              "Säkerhetsprov - Grundläggande tillträde"
            )}
          </Typography>
        </Grid>
        <Grid item xs={2}>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <LanguageIcon
              lang={lang}
              testId={"safety-test-language"}
              onClick={() => setLang(lang === "sv" ? "en" : "sv")}
            />
          </div>
        </Grid>
        <Grid item xs={12} style={{ marginTop: "-0.5em" }}>
          <small>
            {i18n(
              "This part of the test is mandatory in order to get access to the MAX IV facility. ",
              "Denna del av provet är obligatisk för att få grundläggande tillträde till MAX IV. "
            )}
            {i18n(
              "Please study General and fire safety, Radiation safety for supervised areas, Basic chemical safety and Recycling and waste handling. ",
              "Vänligen studera Allmän säkerhet och brandsäkerhet, Strålsäkerhet för skyddat område, Grundnivå kemikaliesäkerhet och Återvinning och hantering av farligt avfall. "
            )}
            {i18n(
              "By answering the questions you confirm that you have studied the required safety information.",
              "Genom att svara på följande frågor bekräftar du att du har studerat den nödvändiga säkerhetsinformationen."
            )}
          </small>
        </Grid>

        {basicQuestions.map((question, index) => {
          return renderQuestion(question, index, true);
        })}

        {/* Radiation safety for controlled areas */}
        <Grid item xs={12}>
          <Typography className="form-heading">
            {i18n(
              "Safety Test - Radiation safety for controlled areas",
              "Säkerhetsprov -  Strålsäkerhet för kontrollerat område"
            )}
          </Typography>
          <small>
            {i18n(
              "This part of the test is required to get access to controlled areas. This includes accelerator areas as well as beamline hutches. By marking the checkbox you also confirm that you have studied the Radiation safety for controlled areas information at MAX IV.",
              "Denna del av testet är obligatoriskt för att utföra arbetsuppgifter i kontrollerade områden (acceleratorområden och/eller strålrörshytter). Genom att checka för kryssrutan så bekräftar du också att du har studerat Strålsäkerhet för kontrollerade områden."
            )}
          </small>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasRadiation}
                onChange={(event) => handleInputChange(event)}
                data-testid="safety-test-check-radiation"
                name="hasRadiation"
              />
            }
            label={i18n(
              "I have been assigned tasks that require access to controlled areas",
              "Jag har tilldelats arbetsuppgifter som kräver åtkomst till kontrollerade områden"
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse in={formData.hasRadiation}>
            <div
              style={{
                borderLeft: "6px solid  #de930038",
                paddingLeft: "0.5em",
              }}
            >
              <Grid container spacing={2}>
                {radiationQuestions.map((question, index) => {
                  return renderQuestion(question, index, formData.hasRadiation);
                })}
              </Grid>
            </div>
          </Collapse>
        </Grid>
        {/* Extended Chemical Safety */}
        <Grid item xs={12}>
          <Typography className="form-heading">
            {i18n(
              "Safety Test - Extended chemical safety",
              "Säkerhetsprov -  Utökad kemikaliesäkerhet"
            )}
          </Typography>
          <small>
            {i18n(
              "This part of the test is required if you will be engaging in hazardous chemical work or if you will bring chemical hazards to the facility. By marking the checkbox you also confirm that you have studied the Extended Chemical Safety information for MAX IV.",
              "Denna del av testet är obligatoriskt om du kommer arbeta med farliga kemikalier eller ta med sådana till anläggningen. Genom att checka för kryssrutan så bekräftar du också att du har studerat Utökad kemikaliesäkerhet."
            )}
          </small>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasChemical}
                onChange={(event) => handleInputChange(event)}
                data-testid="safety-test-check-chemical"
                name="hasChemical"
              />
            }
            label={i18n(
              "I will bring or work with hazardous chemicals in the facility",
              "Jag kommer medföra eller arbeta med farliga kemikalier på anläggningen"
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse in={formData.hasChemical}>
            <div
              style={{
                borderLeft: "6px solid  #de930038",
                paddingLeft: "0.5em",
              }}
            >
              <Grid container spacing={2}>
                {chemicalQuestions.map((question, index) => {
                  return renderQuestion(question, index, formData.hasChemical);
                })}
              </Grid>
            </div>
          </Collapse>
        </Grid>

        {/* Biosafety */}
        <Grid item xs={12}>
          <Typography className="form-heading">
            {i18n("Safety Test - Biosafety", "Säkerhetsprov -  Biosäkerhet")}
          </Typography>
          <small>
            {i18n(
              "This part of the test is required if you will be bringing biological samples to the facility, or prepare biological samples in the bio lab or the preparation labs. By marking the checkbox you also confirm that you have studied the Biosafety information for MAX IV.",
              "Denna del av testet är obligatoriskt om du kommer ta med biologiska prover till anläggningen eller arbeta med biologiska prover i biolaboratoriet/prepplabben. Genom att checka för kryssrutan så bekräftar du också att du har studerat Biosäkerhet."
            )}
          </small>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                size="medium"
                disabled={!formAccess.canEdit}
                checked={formData.hasBio}
                onChange={(event) => handleInputChange(event)}
                data-testid="safety-test-check-bio"
                name="hasBio"
              />
            }
            label={i18n(
              "I will bring or work with biological samples in the facility",
              "Jag kommer medföra eller arbeta med biologiska prover på anläggningen"
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <Collapse in={formData.hasBio}>
            <div
              style={{
                borderLeft: "6px solid  #de930038",
                paddingLeft: "0.5em",
              }}
            >
              <Grid container spacing={2}>
                {bioQuestions.map((question, index) => {
                  return renderQuestion(question, index, formData.hasBio);
                })}
              </Grid>
            </div>
          </Collapse>
        </Grid>

        {/* footer */}
        <Grid item xs={12}>
          <Typography className="form-heading">
            {i18n("Certification", "Intyg")}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                disabled={!formAccess.canEdit}
                size="medium"
                value={formData.signature}
                checked={formData.signature}
                data-testid="safety-test-check-signature"
                inputProps={{
                  name: "signature",
                }}
                onChange={() =>
                  updateFormData({
                    ...formData,
                    signature: !formData.signature,
                  })
                }
              />
            }
            label={i18n(
              "I hereby certify that I am at least 18 years old and that I have reviewed the safety information as ticked above and completed the corresponding parts of the safety test",
              "Jag intygar härmed att jag är minst 18 år gammal och att jag har tagit del av ovan kryssad säkerhetsinformation, och att jag har genomfört motsvarande delar av säkerhetsprovet"
            )}
          />
        </Grid>
      </>
    );
  };

  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={emptySafetyTestForm(initialState)}
      formType="SAFETY_TEST"
      afterUserSet={(props) => {
        const employer = props.formAccess.isStaff ? "MAX IV" : "";
        props.updateFormData({
          ...props.formData,
          employer,
          phoneNumber: props.user.phone,
        });
      }}
      id={props.match.params[0] || ""}
      embedded={props.embedded}
      beforeFormSave={(form, isDraft) => {
        if (isDraft) {
          return form;
        }
        let errors: string[] = [];
        form.answers.forEach((answer) => {
          const isError = isErrornous(
            answer,
            form.hasRadiation,
            form.hasChemical,
            form.hasBio
          );
          if (isError.error) {
            errors.push(isError.msg);
          }
        });
        if (errors.length > 0) {
          const errorMessages = (
            <div data-testid="safety-test-validation">
              <div>
                {i18n(
                  "You have one or more errors in the following parts:",
                  "Du har svarat fel på en eller flera frågor i följande avsnitt:"
                )}
              </div>
              <ul>
                {createSet(errors).map((error) => {
                  return <li key={error}>{error}</li>;
                })}
              </ul>
              <div>
                {i18n(
                  "Please review the relevant information before correcting and resubmitting the test.",
                  "Vänligen studera de relevanta delarna igen innan du korrigerar testet och skickar in det."
                )}
              </div>
            </div>
          );
          dispatch(
            getModalMsgDispatch(
              i18n("Test results", "Provresultat"),
              errorMessages
            )
          );
          return null;
        } else {
          return form;
        }
      }}
      submittable={(form) => {
        return form.signature
          ? ""
          : i18n(
              "Please make sure you have certified the information provided",
              "Du behöver signera formuläret innan det kan skickas in"
            );
      }}
    ></Form>
  );
}

const getIndexOffset = (category: System.SafetyTestAnswerCategory) => {
  switch (category) {
    case "BASIC":
      return 0;
    case "RADIATION":
      return 13;
    case "CHEMICAL":
      return 20;
    case "BIO":
      return 24;
  }
};
interface Question {
  description: { en: string; sv: string };
  options: { en: string[]; sv: string[] };
  correctOptionIndex: number;
  image?: string;
  category: System.SafetyTestAnswerCategory;
}

//basic: question 13
const basicQuestions: Question[] = [
  {
    description: {
      en: "Where is the assembly point after evacuation?",
      sv: "Var finns återsamlingsplatsen efter utrymning?",
    },
    options: {
      en: [
        "The area south of the start building",
        "The inner yard",
        "Along A building wall close to D building entrance",
      ],
      sv: [
        "Området söder om startbyggnaden",
        "Innergården",
        "Längs A-byggnadens vägg nära entrén till D-byggnaden",
      ],
    },
    correctOptionIndex: 2,
    category: "BASIC",
  },
  {
    description: {
      en: "Which of the following statements is true?",
      sv: "Vilket påstående stämmer?",
    },
    options: {
      en: [
        "It is okay to block an evacuation path if an alternative is available",
        "Safety equipment is not necessary if you know what you are doing",
        "Hazardous work may not be performed as solitary work",
      ],
      sv: [
        "Man får blockera en utrymningsväg om alternativ finns att tillgå",
        "Säkerhetsutrustning är onödig om man kan sitt arbete",
        "Riskfyllda arbetsuppgifter får inte utföras som ensamarbete",
      ],
    },
    correctOptionIndex: 2,
    category: "BASIC",
  },
  {
    description: {
      en: "Are you required to report occupational incidents and injuries?",
      sv: "Är du skyldig att rapportera tillbud och arbetsskador?",
    },
    options: {
      en: [
        "No, not at all",
        "Only injuries need to be reported",
        "Yes, all occupational incidents and injuries need to be reported",
      ],
      sv: [
        "Nej, inte alls",
        "Enbart arbetsskador behöver rapporteras",
        "Ja, alla tillbud och arbetsskador behöver rapporteras",
      ],
    },
    correctOptionIndex: 2,
    category: "BASIC",
  },
  {
    description: {
      en: "In which areas are you required to wear a personal TL-dosimeter (TLD), if you have been assigned one?",
      sv: "Inom vilka områden skall du alltid bära en personlig TL-dosimeter (TLD), om du har tilldelats en?",
    },
    options: {
      en: [" All controlled areas", "All supervised and controlled areas"],
      sv: [
        "Samtliga kontrollerade områden",
        "Samtliga skyddade och kontrollerade områden",
      ],
    },
    correctOptionIndex: 1,
    category: "BASIC",
  },
  {
    description: {
      en: "Are you allowed to temporarily move shielding material (e.g. lead, concrete, iron) without prior permission from the radiation safety team if you know that the accelerator/beamline will not be started?",
      sv: "Är det tillåtet att tillfälligt flytta strålskydd (t.ex. bestående av bly, betong, järn) utan strålsäkerhetsteamets tillstånd om du vet att acceleratorn/strålröret inte kommer att startas?",
    },
    options: {
      en: [
        "Yes",
        "No, since I need a radiological work permit I contact the radiation safety team",
      ],
      sv: [
        "Ja",
        "Nej, eftersom jag behöver ett radiologiskt arbetstillstånd kontaktar jag strålsäkerhetsteamet",
      ],
    },
    correctOptionIndex: 1,
    category: "BASIC",
  },
  {
    description: {
      en: "A door to a beamline hutch is open, who is allowed to enter?",
      sv: "En dörr in till en strålrörshytt är öppen, vem får lov att gå in?",
    },
    options: {
      en: [
        " Everyone",
        "Those who have been assigned tasks that require access to controlled areas and have taken the radiation safety training for controlled areas (and wear an electronic dosimeter)",
      ],
      sv: [
        "Alla",
        "De som tilldelats arbetsuppgifter som kräver tillträde till kontrollerade områden och har genomgått strålsäkerhetsutbildningen för kontrollerade områden (samt bär en elektronisk dosimeter)",
      ],
    },
    correctOptionIndex: 1,
    category: "BASIC",
  },
  {
    description: {
      en: "I need to be able to recognize where hazardous areas are located when walking around the facility",
      sv: "Jag behöver känna till vilka områden som är farliga när jag rör mig inom anläggningen",
    },
    options: { en: ["Yes", "No"], sv: ["Ja", "Nej"] },
    correctOptionIndex: 0,
    category: "BASIC",
  },
  {
    description: {
      en: "Are you allowed to move gas bottles around after completing the basic chemical safety introduction?",
      sv: " Får du flytta gasflaskor runt anläggningen efter att ha läst grundnivå inom kemikaliesäkerhet?",
    },
    options: { en: ["Yes", "No"], sv: ["Ja", "Nej"] },
    correctOptionIndex: 1,
    category: "BASIC",
  },
  {
    description: {
      en: "What does this hazardous pictogram mean?",
      sv: "Vad betyder följande faropiktogram?",
    },
    options: {
      en: ["Corrosive", "Acute toxicity", "Health hazard"],
      sv: ["Frätande", "Akut toxiskt", "Hälsofarligt"],
    },
    correctOptionIndex: 1,
    image: "acute_toxicity.gif",
    category: "BASIC",
  },
  {
    description: {
      en: "What does this hazardous pictogram mean?",
      sv: "Vad betyder följande faropiktogram?",
    },
    options: {
      en: ["Corrosive", "Bat storage", "Gas under pressure"],
      sv: ["Frätande", "Klubbförvaring", "Gas under tryck"],
    },
    correctOptionIndex: 2,
    image: "gas_under_pressure.gif",
    category: "BASIC",
  },
  {
    description: {
      en: "Are you allowed to eat or drink wherever you want in the facility?",
      sv: "Får du äta och dricka var som helst inom anläggningen?",
    },
    options: {
      en: ["Yes, it's allowed", "No it's not allowed"],
      sv: ["Jajamen", "Nej, inte överallt"],
    },
    correctOptionIndex: 1,
    category: "BASIC",
  },
  {
    description: {
      en: " Are you allowed to dispose combustible waste and corrugated paper in the same waste container?",
      sv: "Får man slänga brännbart avfall och wellpapp i samma avfallscontainer?",
    },
    options: {
      en: [
        "Yes, corrugated paper is combustible and can be disposed in the same waste container as other combustible waste",
        "No, separate waste containers must be used for combustible waste and for corrugated paper",
      ],
      sv: [
        "Ja, wellpapp är brännbart och kan slängas i samma avfallscontainer som annat brännbart avfall",
        "Nej, separata avfallscontainrar måste användas för brännbart avfall och för wellpapp",
      ],
    },
    correctOptionIndex: 1,
    category: "BASIC",
  },
  {
    description: {
      en: "Are you allowed to throw containers with residual chemicals in the recycling bins?",
      sv: "Får man slänga behållare som innehåller kemikalierester i återvinningstunnor?",
    },
    options: {
      en: [
        "No, containers with residual chemicals are treated as hazardous waste",
        "Yes, residual chemicals in a container are such a small amount that it doesn’t cause any hazard",
      ],
      sv: [
        "Nej, behållare med kemikalierester ska behandlas som farligt avfall",
        "Ja, kemikalierester i en behållare är så små mängder att det inte utgör någon fara",
      ],
    },
    correctOptionIndex: 0,
    category: "BASIC",
  },
];

//Radiation safety for controlled areas: 7
const radiationQuestions: Question[] = [
  {
    description: {
      en: "Within which areas are you required to wear an electronic dosimeter?",
      sv: "Inom vilka områden skall elektronisk dosimeter alltid bäras?",
    },
    options: {
      en: [
        "All controlled areas and when working in the immediate vicinity of the klystrons",
        "All supervised and controlled areas",
      ],
      sv: [
        "Samtliga kontrollerade områden samt vid arbete i klystronernas omedelbara närhet",
        "Samtliga skyddade och kontrollerade områden",
      ],
    },
    correctOptionIndex: 0,
    category: "RADIATION",
  },
  {
    description: {
      en: "The accelerators cannot be turned on unless everyone is logged out. From which areas is logging out necessary?",
      sv: "Start av acceleratorerna kan endast ske om alla loggat ut. Från vilka områden krävs utloggning?",
    },
    options: {
      en: [
        "The linac tunnel, the 1.5 and 3 GeV ring tunnels and the SPF",
        "All supervised and controlled areas",
      ],
      sv: [
        "Linactunneln, 1,5 GeV- och 3 GeV-ringtunneln samt SPF:en",
        "Samtliga skyddade och kontrollerade områden",
      ],
    },
    correctOptionIndex: 0,
    category: "RADIATION",
  },
  {
    description: {
      en: "Access to a controlled area is only allowed when",
      sv: "Tillträde till kontrollerat område får endast ske då",
    },
    options: {
      en: [
        "The green light above the door is off",
        "The green light above the door is lit",
      ],
      sv: [
        " Den gröna lampan ovanför dörren är släckt",
        "Den gröna lampan ovanför dörren är tänd",
      ],
    },
    correctOptionIndex: 1,
    category: "RADIATION",
  },
  {
    description: {
      en: "What is your first action on an electronic dosimeter alarm?",
      sv: "Vad gör du först om den elektroniska dosimetern ger larm?",
    },
    options: {
      en: ["Contact radiation safety on-call", "Exit the area"],
      sv: ["Kontaktar strålsäkerhetsjouren", "Avlägsnar dig från området"],
    },
    correctOptionIndex: 1,
    category: "RADIATION",
  },
  {
    description: {
      en: "Are you allowed to remain in a controlled area while its warning lights or sirens are active? It is assumed that you are not presently performing a search of the area.",
      sv: "Får du under några omständigheter stanna kvar i ett kontrollerat område då dess varningsljus eller -ljud är aktiva? Det antas att du inte för tillfället söker av området.",
    },
    options: { en: ["Yes", "No"], sv: ["Ja", "Nej"] },
    correctOptionIndex: 1,
    category: "RADIATION",
  },
  {
    description: {
      en: "Which of the following statements is true regarding visitors in controlled areas?",
      sv: "Vilket påstående stämmer gällande besökare i kontrollerade områden?",
    },
    options: {
      en: [
        "I am not allowed to bring visitors younger than 18 years old",
        "For a visitor group it is enough that one person in the group carry an electronic dosimeter",
      ],
      sv: [
        " Jag får inte lov att ta med besökare som är yngre än 18 år",
        "I en besöksgrupp räcker det med att en person i gruppen bär en elektronisk dosimeter",
      ],
    },
    correctOptionIndex: 0,
    category: "RADIATION",
  },
  {
    description: {
      en: " How should you proceed if you want to bring out an item from the 3 GeV ring, e.g. lighting equipment?",
      sv: "Vad ska du göra om du vill ta ut ett föremål från 3 GeV-ringtunneln, t.ex. en lysrörsarmatur?",
    },
    options: {
      en: [
        "Recycle the equipment since it was more than two meters from the electron path",
        "Contact the radiation safety team and, unless otherwise instructed, label the item and leave it in the dedicated container at one of the main access points to the ring",
      ],
      sv: [
        "Återvinna föremålet eftersom det var mer än två meter från elektronstrålgången",
        "Kontakta strålsäkerhetsteamet och, om du inte får andra instruktioner, fyll i och fäst en informationslapp på föremålet och lägg det i den avsedda behållaren vid en av huvudingångarna till ringen",
      ],
    },
    correctOptionIndex: 1,
    category: "RADIATION",
  },
];
//extended chemical safety 4
const chemicalQuestions: Question[] = [
  {
    description: {
      en: "Are you allowed to enter the chemical laboratories after finishing the extended safety training?",
      sv: "Får du tillträde till kemikalielabben efter genomgång av utökad kemikaliesäkerhet?",
    },
    options: {
      en: [
        "Yes, I have the correct training after that",
        "No, a special training is needed to access the chemical laboratories",
      ],
      sv: [
        "Ja, då har jag tillräcklig kunskap",
        "Nej, ytterligare träning krävs för tillträde till kemikalielabben",
      ],
    },
    correctOptionIndex: 1,
    category: "CHEMICAL",
  },
  {
    description: {
      en: "You need to dismantle some equipment containing a Beryllium window. How do you proceed?",
      sv: "Du behöver nermontera utrustning innehållande ett Berylliumfönster. Hur går du vidare?",
    },
    options: {
      en: [
        "I start dismantling it immediately",
        "As I need a work permit, I contact the Chemical Safety group before any dismantling",
      ],
      sv: [
        " Jag sätter igång",
        "Eftersom jag behöver ett arbetstillstånd kontaktar jag kemikaliesäkerhetsgruppen",
      ],
    },
    correctOptionIndex: 1,
    category: "CHEMICAL",
  },
  {
    description: {
      en: "Are you allowed to purchase a new hazardous substrate prior to making a risk assessment and approval by the Chemical Safety group?",
      sv: "Får du lov att beställa en ny farlig substans innan genomförd riskbedömning och godkännande från kemikaliesäkerhetsgruppen?",
    },
    options: { en: ["Yes", "No"], sv: ["Ja", "Nej"] },
    correctOptionIndex: 1,
    category: "CHEMICAL",
  },
  {
    description: {
      en: "You transfer some ethanol to a new container that you will use the next two days. Which is the correct way to label the new container?",
      sv: " Du flyttar lite Etanol över i en ny behållare för användning de närmaste dagarna. Hur ska behållaren märkas?",
    },
    options: {
      en: [
        "I write my name and date on it",
        "I don’t label it because I will use it the next two days",
        "I label it with the correct pictogram, write Ethanol on it and my contact details",
      ],
      sv: [
        "Jag skriver mitt namn och datum på den",
        "Den behöver inte märkas, jag ska snart använda den",
        "Jag märker den med rätt piktogram, skriver Etanol på den samt mina kontaktuppgifter",
      ],
    },
    correctOptionIndex: 2,
    category: "CHEMICAL",
  },
];
//biosafety 2
const bioQuestions: Question[] = [
  {
    description: {
      en: "Are you allowed to perform major manipulation of biological samples at the preparation labs?",
      sv: "Är det tillåtet att utföra större manipulation av biologiska prover i preplabben?",
    },
    options: {
      en: ["Yes", "No, it has to be done in the Biolab"],
      sv: ["Ja", "Nej, det måste göras i Biolabb"],
    },
    correctOptionIndex: 1,
    category: "BIO",
  },
  {
    description: {
      en: "You need the access to the biological laboratory, how do you proceed?",
      sv: "Du behöver tillträde till Biolabb, hur går du tillväga?",
    },
    options: {
      en: [
        "I ask the local contact or someone working in the laboratory to open the door for me",
        "As the door is open, I just enter",
        "I need to contact the Biolab manager regarding my requirements well in advance",
      ],
      sv: [
        "Jag ber Local Contact eller någon i labbet att öppna dörren till mig",
        "Jag går in eftersom dörren är öppen",
        "Jag behöver kontakta Biolabb Manager om mina krav i god tid innan jag behöver tillträde",
      ],
    },
    correctOptionIndex: 2,
    category: "BIO",
  },
];
export default SafetyTestForm;
