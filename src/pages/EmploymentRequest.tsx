import {
  Checkbox,
  Collapse,
  FormControl,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { SelectValidator } from "react-material-ui-form-validator";
import { DatePicker } from "../components/DateWidgets";
import { ProjectSelect } from "../components/ProjectSelect";
import { DomniTextValidator } from "../components/TextValidator";
import { EMPLOYMENT } from "../constants";
import { selectList } from "../store/Reducer";
import { useStore } from "../store/Store";
import { GrayBlock } from "../utilities/Helpers";
import Form from "./Form";

const defaultEndDate = () => {
  const endDate = new Date();
  endDate.setFullYear(endDate.getFullYear() + 3);
  return endDate;
};

const emptyEmploymentForm = (): System.EmploymentRequest => {
  return {
    _id: "",
    formType: EMPLOYMENT,
    submitterName: "",
    submitterEmail: "",
    supervisorActions: [
      {
        eventType: "SupervisorAction",
        name: "Julia Hansson",
        email: "julia.hansson@maxiv.lu.se",
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: true,
        role: "Supervisor",
      },
    ],
    files: [],
    status: "UNSAVED",
    submissionDate: new Date(),
    isArchived: false,
    position: "",
    group: "",
    team: "",
    employmentType: "",
    extent: 100,
    estStartDate: new Date(),
    estEndDate: defaultEndDate(),
    estMonthlySalary: 0,
    budgetType: "OUT_OF_BUDGET",
    contactedOfficeCoordinator: false,
    budgetTypeDetail: "",
    budgetTypeDetail2: "",
    externallyFundedProject: "",
    externallyFundedPercentage: 0,
    rejectionAction: "",
    primulaGroup: "",
  };
};

function EmploymentRequest(props: System.FormProps) {
  const { state } = useStore();

  const FormInputs = (
    props: System.FormInputsProps<System.EmploymentRequest>
  ) => {
    const { formData, updateFormData, handleInputChange, formAccess } = props;
    const [checkboxState, setCheckboxState] = useState<{
      externallyFundedProject: boolean;
    }>({
      externallyFundedProject: !!formData.externallyFundedProject,
    });
    const primulaGroupList = selectList({
      state,
      form: EMPLOYMENT,
      name: "Primula group",
      additionalValues: props.embedded
        ? [formData.externallyFundedProject]
        : [],
    });
    const operationsBudgetList = selectList({
      state,
      form: EMPLOYMENT,
      name: "Operations budgets",
      defaultValues: ["No positions available"],
      additionalValues: props.embedded ? [formData.budgetTypeDetail] : [],
    });
    const beamlineProjectBudgetList = selectList({
      state,
      form: EMPLOYMENT,
      name: "Beamline project budget",
      defaultValues: ["MicroMAX", "ForMAX", "DataSTaMP"],
      additionalValues: props.embedded ? [formData.budgetTypeDetail2] : [],
    });
    return (
      <>
        <Grid item xs={12}>
          <Typography className="form-heading">
            Organizational details
          </Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <Tooltip
            title="The position/role of the requested employment"
            placement="top-start"
          >
            <DomniTextValidator
              fullWidth={true}
              name="position"
              label="Position/Role"
              value={formData.position || ""}
              onChange={handleInputChange}
              validators={["required"]}
              errorMessages={["This field is required"]}
              variant="outlined"
            />
          </Tooltip>
        </Grid>
        <Grid item sm={4} xs={12}>
          <DomniTextValidator
            fullWidth={true}
            name="group"
            label="Group"
            value={formData.group || ""}
            onChange={handleInputChange}
            validators={["required"]}
            errorMessages={["This field is required"]}
            variant="outlined"
          />
        </Grid>

        <Grid item sm={4} xs={12}>
          <DomniTextValidator
            fullWidth={true}
            name="team"
            label="Team/Beamline"
            value={formData.team || ""}
            onChange={handleInputChange}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Employment details</Typography>
        </Grid>
        <Grid item sm={4} xs={12}>
          <FormControl variant="outlined" fullWidth={true}>
            <SelectValidator
              fullWidth={true}
              name="employmentType"
              variant="outlined"
              onChange={(
                event: React.ChangeEvent<{ name: string; value: any }>
              ) => {
                updateFormData({
                  ...formData,
                  employmentType: event.target.value,
                  estEndDate:
                    event.target.value === "Permanent"
                      ? undefined
                      : formData.estEndDate || defaultEndDate(),
                });
              }}
              value={formData.employmentType || ""}
              SelectProps={{
                native: true,
              }}
              validators={["required"]}
              errorMessages={["This field is required"]}
            >
              <option value="" disabled>
                Employment type
              </option>
              <option>Permanent</option>
              <option>Temporary</option>
            </SelectValidator>
          </FormControl>
        </Grid>
        <Grid item sm={4} xs={12}>
          <DomniTextValidator
            name="extent"
            label="Extent (%)"
            value={formData.extent || ""}
            onChange={handleInputChange}
            onFocus={(event) => (event.target as any).select()}
            fullWidth={true}
            type="number"
            validators={["minNumber:0", "maxNumber:100"]}
            errorMessages="Has to be between 0-100%"
            variant="outlined"
          />
        </Grid>
        <Grid item sm={4} xs={12}>
          <DomniTextValidator
            name="estMonthlySalary"
            label="Estimated monthly salary"
            value={formData.estMonthlySalary || 0}
            onChange={handleInputChange}
            onFocus={(event) => (event.target as any).select()}
            fullWidth={true}
            type="number"
            validators={["minNumber:0"]}
            errorMessages="Invalid value"
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <Typography className="form-heading">Employment period</Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <DatePicker
            disabled={!formAccess.canEdit}
            start={{
              label: "Estimated start date",
              name: "estStartDate",
              date: formData.estStartDate,
              setDate: (date: Date | undefined) =>
                updateFormData({ ...formData, estStartDate: date }),
            }}
            end={{
              label: "Estimated end date",
              name: "estEndDate",
              date: formData.estEndDate,
              hidden: formData.employmentType === "Permanent",
              setDate: (date: Date | undefined) => {
                updateFormData({ ...formData, estEndDate: date });
              },
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormControlLabel
            className="link"
            control={
              <Checkbox
                disabled={!formAccess.canEdit}
                size="medium"
                value={formData.contactedOfficeCoordinator}
                checked={formData.contactedOfficeCoordinator}
                inputProps={{
                  name: "contactedOfficeCoordinatorCheck",
                }}
                onChange={() =>
                  updateFormData({
                    ...formData,
                    contactedOfficeCoordinator:
                      !formData.contactedOfficeCoordinator,
                  })
                }
              />
            }
            label=" I have contacted the Office Coordinator regarding office spaces."
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth={true}
            label="What are your actions if this recruitment is not approved?"
            onChange={(event) => handleInputChange(event)}
            name="rejectionAction"
            value={formData.rejectionAction}
            variant="outlined"
            multiline={true}
            rows={props.embedded ? undefined : 3}
            style={{ marginTop: "1em" }}
          />
        </Grid>
        <Grid item>
          <Grid container className="supervisor-form-section">
            <Grid item xs={12} className="admin-segment-grid-item">
              <div className="admin-segment-form-heading">Finance Details</div>
              <small>
                This section requires input and/or approval from Finance
              </small>
            </Grid>
            <Grid item xs={12} sm={4} className="admin-segment-grid-item">
              <span style={{ color: "#de9300", fontSize: "1.1em" }}>
                Budget Type ❯
              </span>
              <FormControl component="fieldset">
                <RadioGroup
                  value={formData.budgetType}
                  name="budgetType"
                  onChange={(
                    event: React.ChangeEvent<{ name: string; value: any }>
                  ) => {
                    updateFormData({
                      ...formData,
                      budgetType: event.target.value,
                      budgetTypeDetail:
                        event.target.value === "IN_BUDGET"
                          ? operationsBudgetList.defaultValue ||
                            operationsBudgetList.items[0].value
                          : "",
                      budgetTypeDetail2:
                        event.target.value === "IN_BUDGET"
                          ? beamlineProjectBudgetList.defaultValue ||
                            beamlineProjectBudgetList.items[0].value
                          : "",
                    });
                  }}
                >
                  <FormControlLabel
                    value="OUT_OF_BUDGET"
                    control={<Radio disabled={!formAccess.canEdit} />}
                    label="Out of budget"
                  />
                  <FormControlLabel
                    value="IN_BUDGET"
                    control={<Radio disabled={!formAccess.canEdit} />}
                    label="In budget"
                  />
                  <FormControlLabel
                    value="REPLACEMENT"
                    control={<Radio disabled={!formAccess.canEdit} />}
                    label="Replacement recruitment"
                  />
                  <FormControlLabel
                    value="TEMPORARY_REPLACEMENT"
                    control={<Radio disabled={!formAccess.canEdit} />}
                    label="Temporary replacement, leave of absence"
                  />
                  <FormControlLabel
                    value="REOPENING"
                    control={<Radio disabled={!formAccess.canEdit} />}
                    label="Reopening of position"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={12}
              sm={8}
              className="admin-segment-grid-item"
              style={{ borderLeft: "1px solid orange", paddingLeft: "1em" }}
            >
              <Typography style={{ color: "#de9300", fontSize: "1.1em" }}>
                {translateBudgetType(formData.budgetType)}
              </Typography>
              <Collapse in={formData.budgetType === "OUT_OF_BUDGET"}>
                <div style={{ margin: "0.5em 0" }}>
                  This employment has not been budgeted
                </div>
              </Collapse>
              <Collapse in={formData.budgetType === "IN_BUDGET"}>
                <>
                  <div style={{ margin: "0.5em 0" }}>Operations budget:</div>
                  <SelectValidator
                    disabled={operationsBudgetList.items.length === 0}
                    fullWidth={true}
                    inputProps={{
                      "data-testid": "budgetTypeDetail",
                    }}
                    name="budgetTypeDetail"
                    variant="outlined"
                    onChange={handleInputChange}
                    value={formData.budgetTypeDetail || ""}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    {operationsBudgetList.items.map((item) => (
                      <option key={item.value} value={item.value}>
                        {item.label}
                      </option>
                    ))}
                  </SelectValidator>
                  <div style={{ marginTop: "1em" }}>
                    Beamline Project budget:
                  </div>
                  <SelectValidator
                    fullWidth={true}
                    name="budgetTypeDetail2"
                    variant="outlined"
                    onChange={handleInputChange}
                    value={formData.budgetTypeDetail2 || ""}
                    inputProps={{
                      "data-testid": "budgetTypeDetail2",
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    {beamlineProjectBudgetList.items.map((pos) => (
                      <option key={pos.value} value={pos.value}>
                        {pos.label}
                      </option>
                    ))}
                  </SelectValidator>
                  {beamlineProjectBudgetList.items.length === 0 && (
                    <div>
                      <div style={{ margin: "0.5em 0" }}>
                        Position list has not yet been finalized. Please
                        describe it with free text in the meantime:
                      </div>
                      <TextField
                        fullWidth={true}
                        label="Budgeted position"
                        onChange={(event) => handleInputChange(event)}
                        name="budgetTypeDetail"
                        data-testid="budgetTypeDetailFreeText"
                        value={formData.budgetTypeDetail || ""}
                        variant="outlined"
                      />
                    </div>
                  )}
                </>
              </Collapse>
              <Collapse
                in={
                  formData.budgetType === "REPLACEMENT" ||
                  formData.budgetType === "TEMPORARY_REPLACEMENT"
                }
              >
                <>
                  <div style={{ margin: "0.5em 0" }}>
                    Please provide the full name of the person being replaced.
                  </div>
                  <TextField
                    fullWidth={true}
                    label="Person replaced"
                    onChange={(event) => handleInputChange(event)}
                    name="budgetTypeDetail"
                    data-testid="budgetTypeDetailReplacement"
                    value={formData.budgetTypeDetail || ""}
                    variant="outlined"
                  />
                </>
              </Collapse>
              <Collapse in={formData.budgetType === "REOPENING"}>
                <>
                  <div>
                    Please provide both the ID and PA number of the old
                    employment request that is being reopened.
                  </div>
                  <div
                    style={{
                      fontSize: "0.8em",
                      fontStyle: "italic",
                      color: "#777",
                      marginBottom: "0.5em",
                    }}
                  >
                    (If you dont know these, please leave it empty and Finance
                    will try to figure it out.)
                  </div>
                  <TextField
                    fullWidth={true}
                    label="ID Number"
                    onChange={(event) => handleInputChange(event)}
                    name="budgetTypeDetail"
                    data-testid="budgetTypeDetailReopeningID"
                    value={formData.budgetTypeDetail || ""}
                    variant="outlined"
                  />
                  <div style={{ paddingTop: "1em" }} />
                  <TextField
                    fullWidth={true}
                    label="PA Number"
                    onChange={(event) => handleInputChange(event)}
                    name="budgetTypeDetail2"
                    data-testid="budgetTypeDetailReopeningPA"
                    value={formData.budgetTypeDetail2 || ""}
                    variant="outlined"
                  />
                </>
              </Collapse>
            </Grid>

            <Grid
              item
              xs={12}
              className="admin-segment-grid-item"
              style={{ paddingBottom: "0em" }}
            >
              <FormControlLabel
                className="link"
                control={
                  <Checkbox
                    size="medium"
                    disabled={!formAccess.canEdit}
                    checked={checkboxState.externallyFundedProject}
                    inputProps={{
                      name: "externallyFundedProject",
                    }}
                    onChange={() => {
                      if (checkboxState.externallyFundedProject) {
                        //clear project and percentage
                        updateFormData({
                          ...formData,
                          externallyFundedProject: "",
                          externallyFundedPercentage: 0,
                        });
                      }
                      setCheckboxState({
                        ...checkboxState,
                        externallyFundedProject:
                          !checkboxState.externallyFundedProject,
                      });
                    }}
                  />
                }
                label="This is an externally funded project"
              />
            </Grid>
            <Grid item sm={8} xs={12} className="admin-segment-grid-item">
              {checkboxState.externallyFundedProject ? (
                <FormControl variant="outlined" fullWidth={true}>
                  <ProjectSelect
                    required={false}
                    projectAlias="Externally funded projects"
                    formType={EMPLOYMENT}
                    selectedProject={formData.externallyFundedProject}
                    onChange={(project: System.Project) => {
                      updateFormData({
                        ...formData,
                        externallyFundedProject: project.project,
                      });
                    }}
                  />
                </FormControl>
              ) : (
                <GrayBlock />
              )}
            </Grid>
            <Grid item sm={4} xs={12} className="admin-segment-grid-item">
              {checkboxState.externallyFundedProject ? (
                <DomniTextValidator
                  type="number"
                  name="externallyFundedPercentage"
                  label="Percentage of salary funded by project"
                  value={formData.externallyFundedPercentage || 0}
                  onChange={handleInputChange}
                  onFocus={(event) => (event.target as any).select()}
                  fullWidth={true}
                  validators={["minNumber:0", "maxNumber:100"]}
                  errorMessages="Has to be between 0-100%"
                  variant="outlined"
                />
              ) : (
                <GrayBlock />
              )}
            </Grid>
            <Grid item xs={12} className="admin-segment-grid-item">
              <SelectValidator
                disabled={primulaGroupList.items.length === 0}
                fullWidth={true}
                name="primulaGroup"
                variant="outlined"
                onChange={handleInputChange}
                value={formData.primulaGroup || ""}
                SelectProps={{
                  native: true,
                }}
              >
                <option value="" disabled>
                  Choose correct Primula group in this list!
                </option>
                {primulaGroupList.items.map((item) => (
                  <option key={item.value} value={item.value}>
                    {item.label}
                  </option>
                ))}
              </SelectValidator>
            </Grid>
          </Grid>
        </Grid>
      </>
    );
  };

  return (
    <Form
      FormInputs={FormInputs}
      emptyForm={emptyEmploymentForm()}
      formType="EMPLOYMENT_REQUEST"
      id={props.match.params[0] || ""}
      embedded={props.embedded}
    ></Form>
  );
}

const translateBudgetType = (budgetType: System.BudgetType) => {
  switch (budgetType) {
    case "IN_BUDGET":
      return "In budget";
    case "OUT_OF_BUDGET":
      return "Out of budget";
    case "REPLACEMENT":
      return "Replacement recruitment";
    case "TEMPORARY_REPLACEMENT":
      return "Temporary replacement, leave of absence";
    case "REOPENING":
      return "Reopening of position";
  }
};
export default EmploymentRequest;
