import React from "react";
import { Container, Paper, Typography } from "@mui/material";
import { getFormConfigByPath } from "../utilities/Config";

const Help = (props: any) => {
  const formConfig = getFormConfigByPath(props.match.params);
  return (
    <Container>
      <Paper elevation={2} className="paper">
        <h2 style={{ marginBottom: "1em" }}>
          <span className="category-preface">Help</span>
          <span className="primary-dark-color"> {formConfig.name}</span>
        </h2>
        <div>
          {formConfig.additionalHelpComponent ? (
            formConfig.additionalHelpComponent
          ) : (
            <Typography>
              No help text has been provided for this form. Please contact{" "}
              {formConfig.department} for help.
            </Typography>
          )}
          <div>
            An overview of how Domni works can be found{" "}
            <a
              style={{ margin: "0" }}
              rel="noopener noreferrer"
              target="_blank"
              href="/docs/"
              className="link"
            >
              {" "}
              here
            </a>
            . For feedback, suggestions or bug reports, please contact{" "}
            <a
              style={{ margin: "0" }}
              rel="noopener noreferrer"
              target="_blank"
              href="mailto:ims@maxiv.lu.se"
              className="link"
            >
              {" "}
              IMS.
            </a>
          </div>
        </div>
      </Paper>
    </Container>
  );
};
export default Help;
