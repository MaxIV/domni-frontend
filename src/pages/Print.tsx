import { useEffect, useState } from "react";
import { useStore } from "../store/Store";
import { getModalAPIErrorDispatch } from "../components/Modal";
import { getFormConfigByPath } from "../utilities/Config";
import * as api from "../api/forms";
import { formatDate, getLatestSupervisorAction } from "../utilities/Helpers";
import SubmittedForm from "../components/SubmittedForm";

const Print = (props: any) => {
  const { dispatch } = useStore();
  const [form, setForm] = useState<System.Form | undefined>(undefined);
  const searchQuery = props.match.params[1] as string;
  const formConfig = getFormConfigByPath(props.match.params);

  const search = async () => {
    if (!searchQuery) {
      dispatch(
        getModalAPIErrorDispatch({
          title: "Invalid link",
          msg: "This print link is not valid.",
          code: 400,
        })
      );
      return;
    }
    try {
      const result = await api.getForm(searchQuery, formConfig.formType);
      if (!result) {
        dispatch(
          getModalAPIErrorDispatch({
            title: "Not found",
            msg: "The requested form does not exist",
            code: 404,
          })
        );
        return;
      }
      setForm(result);
    } catch (err) {
      dispatch(getModalAPIErrorDispatch(err as System.APIError));
    }
  };

  useEffect(() => {
    search();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!form) {
    return null;
  }
  return (
    <>
      <>
        {form.formType.toString() !== "RISK_ASSESSMENT" ? (
          <div style={{ marginLeft: "0.5em", fontSize:"smaller"}}>
            {formConfig.name} {form._id}
            <div style={{ textTransform: "capitalize" }}>
              {formatDate(form.submissionDate)} Submitted by {form.submitterName}
            </div>
            <div style={{ textTransform: "capitalize" }}>
              {formatDate(getLatestSupervisorAction(form).decisionDate)}{" "}
              {form.status.toLowerCase()} by {getLatestSupervisorAction(form).name}
            </div>
          </div>
        ) : null}
      </>
      {
        formConfig.printComponent ?
        <formConfig.printComponent form={form} /> :
        <SubmittedForm key={form._id} form={form} open={true} onFormChange={form => {}} />
      }
    </>
  );
};

export default Print;
