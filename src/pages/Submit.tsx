import React from "react";
import { Container, Paper } from "@mui/material";
import * as Config from "../utilities/Config";
import queryString from "query-string";
export type actionType =
  | "approved"
  | "rejected"
  | "reassigned"
  | "submitted"
  | "not found";

const Submit = (props: any) => {
  const formConfig = Config.getFormConfigByPath(props.match.params);
  const action =
    (queryString.parse(props.match.params[1] || props.location.search)
      .action as actionType) || "not found";

  return (
    <Container>
      <Paper
        style={{ marginTop: "2em", padding: "2em", paddingTop: "1em" }}
        elevation={2}
      >
        <h2 style={{ marginBottom: "0.5em" }} data-testid="submit-title-h2">
          <span
            className="category-preface"
            style={{ textTransform: "capitalize" }}
          >
            {action}
          </span>
          <span className="primary-dark-color">{formConfig.name} </span>
        </h2>
        <div data-testid="submit-body-div">{body(formConfig, action)}</div>
      </Paper>
    </Container>
  );
};
const body = (formConfig: System.FormConfig, action: actionType) => {
  switch (action) {
    case "approved":
      if (formConfig.requiresApproval) {
        return (
          <>
            Your approval has been registered and an email has been sent to the
            original submitter.
          </>
        );
      } else {
        return <>Your update has been registered.</>;
      }
    case "rejected":
      return (
        <>
          Your rejection has been registered and an email has been sent to the
          original submitter.
        </>
      );
    case "reassigned":
      return (
        <>
          Your approval has been registered and an email has been sent to the
          new supervisor.
        </>
      );
    case "submitted":
      return formConfig.submitMessage ? (
        formConfig.submitMessage
      ) : (
        <>
          Thank you for your submission! You'll be contacted as soon as it has
          been processed.
        </>
      );
    case "not found":
      return (
        <>
          This page is no longer valid. Click{" "}
          <a href="/" className="link">
            here
          </a>{" "}
          to go back to the domni start page.
        </>
      );
  }
};

export default Submit;
