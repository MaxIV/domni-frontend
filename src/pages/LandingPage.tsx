import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useStore } from "../store/Store";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { Container, Grid, ButtonBase, InputBase, Paper } from "@mui/material";
import "./LandingPage.css";
import SearchIcon from "@mui/icons-material/Search";
import { getFormConfigs } from "../utilities/Config";
import Icon from "@mui/material/Icon";
import { isInProduction, isStaff } from "../utilities/Helpers";
import Tooltip from "@mui/material/Tooltip/Tooltip";
import { useWindowDimensions } from "../utilities/Helpers";

const LandingPage = () => {
  const { state } = useStore();
  const [searchTerm, setSearchTerm] = useState<string>("");
  const { md } = useWindowDimensions();
  const inputStyle: React.CSSProperties = {};
  if (!!searchTerm) {
    inputStyle.opacity = "1";
  }
  if (!md) {
    inputStyle.width = "100%";
  }
  return (
    <Container>
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          marginTop: "2em",
          marginBottom: "2em",
          justifyContent: "space-between",
          alignItems: "flex-end",
        }}
      >
        <div>
          <h2
            style={{
              textAlign: "left",
              display: "inline-block",
              whiteSpace: "nowrap",
            }}
          >
            {md && <span className="category-preface">Domni</span>}
            <span className="primary-dark-color">Available forms</span>
          </h2>
          <div
            style={{
              color: "#777",
              fontSize: "0.85em",
              marginTop: "-0.5em",
            }}
          >
            Domni is written and maintained by the{" "}
            <a className="link" href="mailto:ims@maxiv.lu.se">
              IMS
            </a>{" "}
            group at MAX IV.{" "}
            <Link to={`/docs/`} className="link">
              More info
            </Link>
            .
          </div>
        </div>

        <Tooltip
          title="Name, description or department (e.g. Finance or Administration)"
          placement="top"
          arrow
        >
          <Paper className="search-input" style={inputStyle}>
            <InputBase
              value={searchTerm}
              onChange={(e) => setSearchTerm(e.target.value)}
              placeholder="Filter"
            />
            <SearchIcon style={{ paddingRight: "0.3em" }} />
          </Paper>
        </Tooltip>
      </div>
      <Grid container spacing={4} className="landing-page-card-container">
        {getFormConfigs({
          excludeNonProd: isInProduction(),
          visibility: isStaff(state.user) ? "STAFF" : "PUBLIC",
        }).map((formConfig) => {
          return (
            <FormCard
              searchTerm={searchTerm}
              key={formConfig.formType}
              formConfig={formConfig}
            />
          );
        })}
      </Grid>
    </Container>
  );
};
const FormCard = (props: {
  searchTerm: string;
  formConfig: System.FormConfig;
}) => {
  const { formConfig } = props;
  //hack for now, add config if more forms need unique card render in future.
  const cardClassName =
    formConfig.formType === "ADD_FORM"
      ? "landing-page-add-card"
      : "landing-page-card";
  const hide =
    !(formConfig.formType === "ADD_FORM") &&
    props.searchTerm &&
    !formConfig.name.toLowerCase().includes(props.searchTerm.toLowerCase()) &&
    !formConfig.department
      .toLowerCase()
      .includes(props.searchTerm.toLowerCase()) &&
    !formConfig.description
      .toLowerCase()
      .includes(props.searchTerm.toLowerCase());
  if (hide) {
    return null;
  }
  return (
    <Grid
      key={formConfig.name}
      item
      xs={12}
      sm={6}
      md={4}
      lg={3}
      className="landing-page-container"
    >
      <Link
        id={formConfig.formType}
        to={`/${formConfig.path}/form/`}
        className="link"
      >
        <ButtonBase
          data-testid={`landing-page-${formConfig.formType}-btn`}
          style={{ width: "100%" }}
        >
          <Card className={cardClassName}>
            <CardContent>
              <div>
                <Icon className="landing-page-icon">{formConfig.icon}</Icon>
              </div>
              <div className={"landing-page-card-title"}>{formConfig.name}</div>
              <div className={"landing-page-card-description"}>
                {formConfig.description}
              </div>
            </CardContent>
          </Card>
        </ButtonBase>
      </Link>
    </Grid>
  );
};
export default LandingPage;
