import {
  Button,
  Checkbox,
  Container,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  InputBase,
  Paper,
  Radio,
  RadioGroup,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";

import Collapse from "@mui/material/Collapse/Collapse";
import IconButton from "@mui/material/IconButton/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import SettingsIcon from "@mui/icons-material/Settings";
import moment from "moment";
import { parse } from "json2csv";
import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import * as api from "../api/forms";
import DomniLoader from "../components/DomniLoader";
import { getModalAPIErrorDispatch } from "../components/Modal";
import { SimpleSelect } from "../components/SimpleSelect";
import SubmittedForm from "../components/SubmittedForm";
import { useStore } from "../store/Store";
import { getFormConfigByPath } from "../utilities/Config";
import "./Archive.css";
import { DatePicker, YearAndWeekPicker } from "../components/DateWidgets";

interface State {
  forms: Array<System.Form>;
  paginationInfo: System.PaginationInfo;
  formConfig: System.FormConfig;
  showSearchSettings: boolean;
  searchParams: System.ArchiveSearchParams;
  abortController: AbortController;
}
export const getDefaultSearchParams = (
  query: string
): System.ArchiveSearchParams => {
  return {
    query,
    type: "DEFAULT",
    status: "Any",
    fromDate: moment("2020-01-01T00:00:00").toDate(),
    toDate: new Date(),
    customField: "",
    includeArchived: false,
  };
};
const Archive = (props: any) => {
  const { dispatch } = useStore();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [localState, setLocalState] = React.useState<State>({
    abortController: new AbortController(),
    forms: [],
    paginationInfo: {
      totalDocs: 0,
      page: 1,
      limit: 10,
    },
    formConfig: getFormConfigByPath(props.match.params),
    showSearchSettings: false,
    searchParams: getDefaultSearchParams(""),
  });
  const {
    forms,
    searchParams,
    paginationInfo,
    formConfig,
    showSearchSettings,
    abortController,
  } = localState;
  useEffect(() => {
    setLoading(false);
    setLocalState({
      ...localState,
      searchParams: getDefaultSearchParams(""),
      paginationInfo: {
        totalDocs: 0,
        page: 1,
        limit: 10,
      },
      formConfig: getFormConfigByPath(props.match.params),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.match.params[0]]);

  //Set loading to false if search results changes
  useEffect(() => {
    setLoading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [localState.forms]);

  //perform a search if limit page or formconfig changes
  useEffect(() => {
    //Abort any ongoing search, it would no longer be relevant, and can put us in a perpetual update loop
    abortController.abort();
    search();
    return () => {
      abortController.abort();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paginationInfo.limit, paginationInfo.page, formConfig]);

  const updateForm = (form: System.Form) => {
    const index = localState.forms.findIndex((f) => f._id === form._id);
    const newForms = [...localState.forms];
    newForms[index] = form;
    setLocalState({ ...localState, forms: newForms });
  };
  const search = async () => {
    if (loading) {
      return;
    }
    try {
      const ac = new AbortController();
      setLoading(true);
      const result = await api.search(
        formConfig,
        searchParams,
        ac.signal,
        paginationInfo
      );
      setLocalState({
        ...localState,
        abortController: ac,
        paginationInfo: {
          ...paginationInfo,
          totalDocs: result.totalDocs,
        },
        forms: result.docs.sort((a, b) =>
          (a._id || "") < (b._id || "") ? 1 : -1
        ),
      });
      dispatch({
        type: "SET_COMMENT_HIGHLIGHT",
        value: searchParams.type === "COMMENT" ? searchParams.query : "",
      });
    } catch (err) {
      if ((err as System.APIError).wasAborted) {
        //Take no action on aborted, isn't actually an error from users perspective
      } else {
        dispatch(getModalAPIErrorDispatch(err as System.APIError));
        setLoading(false);
      }
    }
  };

  const handleChangeRowsPerPage = async (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setLocalState({
      ...localState,
      paginationInfo: {
        ...paginationInfo,
        limit: +event.target.value,
        page: 1,
      },
    });
  };

  const handleChangePage = async (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,
    newPage: number
  ) => {
    setLocalState({
      ...localState,
      paginationInfo: {
        ...paginationInfo,
        page: newPage + 1,
      },
    });
  };
  const onClickSearchSettingButton = async () => {
    setLocalState({
      ...localState,
      showSearchSettings: !localState.showSearchSettings,
    });
  };
  const onClickSearchButton = async () => {
    if (localState.paginationInfo.page === 1) {
      search();
    } else {
      setLocalState({
        ...localState,
        paginationInfo: {
          ...paginationInfo,
          page: 1,
        },
      });
    }
  };

  const handleInputChange = (event: { target: { value: string } }) => {
    setLocalState({
      ...localState,
      searchParams: { ...localState.searchParams, query: event.target.value },
    });
  };
  const opacityStyle = !!searchParams.query ? { opacity: "1" } : {};
  const csvData =
    forms.length === 0
      ? ""
      : parse(
          forms.map((form) => {
            const {
              _id,
              comments,
              edits,
              supervisorActions,
              systemEvents,
              files,
              ...rest
            } = form;
            const transformed: any = {};
            Object.keys(rest).forEach((key) => {
              const prop = (rest as any)[key];
              if (!Array.isArray(prop) && typeof prop !== "object") {
                transformed[key] = prop;
              }
            });
            return transformed;
          })
        );
  return (
    <Container>
      <Grid container>
        <Grid item xs={12} md={8}>
          <h2 style={{ marginTop: "1em" }}>
            <span className="category-preface">Archive</span>
            <span className="primary-dark-color">{formConfig.name} </span>
          </h2>
          <Typography>
            Click{" "}
            <Link to={`/${formConfig.path}/settings/`} className="link">
              here
            </Link>{" "}
            to configure settings for the {formConfig.name} form.
          </Typography>
        </Grid>
        <Grid
          item
          xs={12}
          md={4}
          style={{
            alignSelf: "flex-end",
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Paper className="archive-search-input" style={opacityStyle}>
            <div style={{ flexGrow: 1, display: "flex" }}>
              <Tooltip
                title="Show advanced search settings"
                placement="top"
                arrow
              >
                <IconButton
                  data-testid="archive-advanced-search-toggle-button"
                  type="submit"
                  aria-label="settings"
                  onClick={onClickSearchSettingButton}
                  size="large"
                >
                  <SettingsIcon
                    style={showSearchSettings ? { color: "#de9300" } : {}}
                  />
                </IconButton>
              </Tooltip>
              <Tooltip
                title={
                  searchParams.type === "COMMENT"
                    ? "Search by word in a comment"
                    : searchParams.type === "DEFAULT"
                    ? "Search by ID, submitter or supervisor (name or email)"
                    : `Search by ${searchParams.customField}`
                }
                placement="top"
                arrow
              >
                <InputBase
                  value={searchParams.query}
                  onChange={handleInputChange}
                  placeholder="Search"
                  style={{ paddingLeft: "0.5em", flexGrow: 1 }}
                  onKeyUp={(e) => e.keyCode === 13 && onClickSearchButton()}
                />
              </Tooltip>
            </div>
            <IconButton
              type="submit"
              aria-label="search"
              onClick={onClickSearchButton}
              size="large"
            >
              <SearchIcon />
            </IconButton>
          </Paper>
        </Grid>
      </Grid>
      {/* Search settings */}
      <Grid item xs={12}>
        <Collapse
          in={localState.showSearchSettings}
          timeout="auto"
          unmountOnExit
        >
          <div className="archive-settings-outer">
            <FormControl
              component="fieldset"
              fullWidth
              style={{
                borderBottom: "1px solid #eee",
                marginBottom: "1em",
                paddingBottom: "1em",
              }}
            >
              <FormLabel style={{ fontSize: "0.75em" }} component="legend">
                Search type
              </FormLabel>
              <RadioGroup
                value={searchParams.type}
                style={{ flexDirection: "row" }}
                name="searchType"
                onChange={(e) =>
                  setLocalState({
                    ...localState,
                    searchParams: {
                      ...localState.searchParams,
                      type: e.target.value as
                        | "DEFAULT"
                        | "COMMENT"
                        | "CUSTOM_FIELD",
                      customField:
                        e.target.value === "CUSTOM_FIELD"
                          ? (formConfig.customSearchFields &&
                              formConfig.customSearchFields[0].value) ||
                            ""
                          : "",
                    },
                  })
                }
              >
                <div>
                  <Tooltip
                    title="Search by ID, submitter or supervisors (name or email)"
                    placement="top"
                    arrow
                  >
                    <FormControlLabel
                      value="DEFAULT"
                      control={
                        <Radio
                          inputProps={{
                            //@ts-ignore
                            "data-testid":
                              "archive-advanced-search-default-radio",
                          }}
                        />
                      }
                      label="Default (ID, submitter or supervisor)"
                      data-testid="archive-advanced-search-default-radio"
                    />
                  </Tooltip>
                </div>
                <div>
                  <Tooltip
                    title="Search by word in a comment"
                    placement="top"
                    arrow
                  >
                    <FormControlLabel
                      value="COMMENT"
                      control={
                        <Radio
                          inputProps={{
                            //@ts-ignore
                            "data-testid":
                              "archive-advanced-search-comment-radio",
                          }}
                        />
                      }
                      label="Comment (whole word search)"
                      data-testid="archive-advanced-search-comment-radio"
                    />
                  </Tooltip>
                </div>
                {formConfig.customSearchFields && (
                  <div>
                    <Tooltip
                      title={`Search by a field specific to ${formConfig.name}`}
                      placement="top"
                      arrow
                    >
                      <FormControlLabel
                        value="CUSTOM_FIELD"
                        control={
                          <Radio
                            inputProps={{
                              //@ts-ignore
                              "data-testid":
                                "archive-advanced-search-custom-field-radio",
                            }}
                          />
                        }
                        label={`Specific ${formConfig.name} field`}
                      />
                    </Tooltip>
                  </div>
                )}
              </RadioGroup>
            </FormControl>
            <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                <DatePicker
                  disabled={false}
                  start={{
                    label: "Forms newer than",
                    name: "fromDate",
                    date: searchParams.fromDate,
                    tooltip: `Week ${moment(searchParams.fromDate).isoWeek()}`,
                    setDate: (date: Date | undefined) =>
                      setLocalState({
                        ...localState,
                        searchParams: {
                          ...localState.searchParams,
                          fromDate: date || new Date(),
                        },
                      }),
                  }}
                  end={{
                    label: "Forms no newer than",
                    name: "toDate",
                    date: searchParams.toDate,
                    tooltip: `Week ${moment(searchParams.toDate).isoWeek()}`,
                    setDate: (date: Date | undefined) =>
                      setLocalState({
                        ...localState,
                        searchParams: {
                          ...localState.searchParams,
                          toDate: date || new Date(),
                        },
                      }),
                  }}
                  inputVariant="standard"
                />
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                style={{
                  display: "flex",
                  alignItems: "baseline",
                }}
              >
                <small style={{ marginRight: "1em" }}>
                  Set date interval by week and year:{" "}
                </small>
                <YearAndWeekPicker
                  buttonTooltip="Click to update the date interval with the selected week and year"
                  onSelect={(fromDate, toDate) => {
                    setLocalState({
                      ...localState,
                      searchParams: {
                        ...localState.searchParams,
                        fromDate,
                        toDate,
                      },
                    });
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={4} md={3}>
                <SimpleSelect
                  disabled={{
                    value: false,
                  }}
                  options={[
                    { label: "Any", value: "Any" },
                    { label: "Pending", value: "PENDING" },
                    { label: "Approved", value: "APPROVED" },
                    { label: "Rejected", value: "REJECTED" },
                  ]}
                  selectedValue={searchParams.status}
                  testid="archive-advanced-search-status-select"
                  variant="standard"
                  title="Status"
                  name="Status"
                  onChange={(event) => {
                    setLocalState({
                      ...localState,
                      searchParams: {
                        ...localState.searchParams,
                        status: event.target.value as System.FormStatus | "Any",
                      },
                    });
                  }}
                />
              </Grid>
              {formConfig.customSearchFields &&
                searchParams.type === "CUSTOM_FIELD" && (
                  <Tooltip
                    placement="top-start"
                    title={`Replaces the default string matching (ID or people) with this field`}
                  >
                    <Grid item xs={12} sm={4} md={3}>
                      <SimpleSelect
                        disabled={{
                          value: false,
                        }}
                        options={formConfig.customSearchFields}
                        selectedValue={searchParams.customField || ""}
                        variant="standard"
                        testid="archive-advanced-search-custom-field-select"
                        title={`${formConfig.name} field`}
                        name="customField"
                        onChange={(event) => {
                          setLocalState({
                            ...localState,
                            searchParams: {
                              ...localState.searchParams,
                              customField: event.target.value,
                            },
                          });
                        }}
                      />
                    </Grid>
                  </Tooltip>
                )}
              <Grid item xs={12} sm={4} md={3}>
                <FormControlLabel
                  className="link"
                  control={
                    <Checkbox
                      size="medium"
                      checked={localState.searchParams.includeArchived}
                      onChange={(event) => {
                        setLocalState({
                          ...localState,
                          searchParams: {
                            ...localState.searchParams,
                            includeArchived: event.target.checked,
                          },
                        });
                      }}
                    />
                  }
                  label="Include archived submission"
                />
              </Grid>
            </Grid>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                marginTop: "1em",
                paddingTop: "1em",
                borderTop: "1px solid #eee",
              }}
            >
              <div>
                <InputBase
                  style={{
                    color: "#888",
                    margin: "0em 0.5em",
                    background: "#fafafa",
                    borderRadius: "0.3em",
                    padding: "0em 0.5em",
                  }}
                  data-testid="archive-advanced-search-search-input"
                  onKeyUp={(e) => e.keyCode === 13 && onClickSearchButton()}
                  value={localState.searchParams.query}
                  onChange={(e) =>
                    setLocalState({
                      ...localState,
                      searchParams: {
                        ...localState.searchParams,
                        query: e.target.value,
                      },
                    })
                  }
                />
                <Button
                  data-testid="archive-advanced-search-search-button"
                  className="link"
                  onClick={onClickSearchButton}
                >
                  SEARCH
                </Button>
              </div>
            </div>
          </div>
        </Collapse>
      </Grid>
      {forms.length === 0 ? (
        <Paper
          style={{
            padding: "2em",
            marginTop: "1em",
            fontSize: "1em",
            textAlign: "center",
          }}
          elevation={2}
        >
          {loading ? (
            <DomniLoader show={true} />
          ) : (
            <span data-testid="archive-no-results-span">
              No matching {formConfig.name} found
            </span>
          )}
        </Paper>
      ) : (
        <Grid container spacing={0} style={{ padding: "1em 0em" }}>
          <Grid item sm={12}>
            <TableContainer
              component={Paper}
              className="archive-table-container"
            >
              <Table stickyHeader>
                <TableHead>
                  <TableRow>
                    <TableCell />
                    <TableCell>ID</TableCell>
                    <TableCell>SUBMITTED</TableCell>
                    <TableCell>SUBMITTER</TableCell>
                    <TableCell>SUPERVISOR</TableCell>
                    {
                      formConfig.customColumns ?
                      formConfig.customColumns.map(col => <TableCell>{col.name}</TableCell>) : ""
                    }
                    <TableCell>STATUS</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {forms.map((form) => {
                    return (
                      <SubmittedForm
                        key={form._id}
                        form={form}
                        open={false}
                        onFormChange={(form: System.Form) => updateForm(form)}
                        onFormAddedOrRemoved={() => search()}
                      />
                    );
                  })}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={paginationInfo.totalDocs}
                rowsPerPage={paginationInfo.limit}
                page={paginationInfo.page - 1}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </TableContainer>
          </Grid>
        </Grid>
      )}
      {csvData && (
        <Grid item xs={12} style={{ fontSize: "0.8em", marginTop: "-1em" }}>
          Export current table content as{" "}
          <a
            className="link"
            href={`data:text/plain;charset=utf-8, ${encodeURIComponent(
              csvData
            )}`}
            download={`${forms.length}-${formConfig.path}-archive.csv`}
          >
            csv
          </a>{" "}
          <a
            className="link"
            href={`data:text/plain;charset=utf-8, ${encodeURIComponent(
              JSON.stringify(forms)
            )}`}
            download={`${forms.length}-${formConfig.path}-archive.json`}
          >
            json
          </a>
        </Grid>
      )}
      <div style={{ padding: "4em" }} />
    </Container>
  );
};

export default Archive;
