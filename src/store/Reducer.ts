import {
  getEmptyFormTypeObject,
  getFormConfig,
  getFormConfigs,
  getFormTypes,
} from "../utilities/Config";
import { isInProduction } from "../utilities/Helpers";

export interface setDbData {
  type: "SET_DB_DATA";
  dbData: System.DbData;
}
export interface setLdapPeopleAction {
  type: "SET_LDAP_PEOPLE";
  ldapPeople: System.LDAPPerson[];
}
export interface setLdapPersonAction {
  type: "SET_LDAP_PERSON";
  ldapPerson: System.LDAPPerson;
}
export interface setUserAction {
  type: "SET_USER";
  user: System.User;
}
export interface setCommentHighlight {
  type: "SET_COMMENT_HIGHLIGHT";
  value: string;
}
export interface setCache {
  type: "SET_CACHE";
  supervisors: {
    [key in System.FormType]: System.Supervisor[];
  };
  projects: {
    [key in System.FormType]: System.Project[];
  };
  lists: {
    [key in System.FormType]: System.List[];
  };
  pendingSupervisorCases: {
    [key in System.FormType]: System.Form[];
  };
  dbData: System.DbData;
}
export interface setSupervisorsAction {
  type: "SET_SUPERVISORS";
  formType: System.FormType;
  supervisors: System.Supervisor[];
}
export interface setListsAction {
  type: "SET_LISTS";
  formType: System.FormType;
  lists: System.List[];
}
export interface setProjectsAction {
  type: "SET_PROJECTS";
  formType: System.FormType;
  projects: System.Project[];
}

export interface setPendingSupervisorCases {
  type: "SET_PENDING_SUPERVISOR_CASES";
  pendingSupervisorCases: {
    [key in System.FormType]: System.Form[];
  };
}
export interface updatePendingSupervisorCase {
  type: "UPDATE_PENDING_SUPERVISOR_CASE";
  pendingSupervisorCase: System.Form;
}

export interface setModalPropsAction {
  type: "SET_MODAL_PROPS";
  modalProps: System.ModalProps;
}
export type formAction =
  | setUserAction
  | setProjectsAction
  | setListsAction
  | setSupervisorsAction
  | setCache
  | setModalPropsAction
  | setCommentHighlight
  | setPendingSupervisorCases
  | updatePendingSupervisorCase
  | setLdapPeopleAction
  | setLdapPersonAction
  | setDbData;

export const initialState: System.IState = {
  isLoading: true,
  modalProps: {
    open: false,
    title: "",
    content: "",
    btn1Text: "",
    btn1Color: undefined,
    btn1OnClick: undefined,
    btn2Text: "",
    btn2Color: undefined,
    btn2OnClick: undefined,
    inputs: undefined,
  },
  ldapPeople: [],
  dbData: { type: "TEST" },
  commentHighlight: "",
  supervisors: getEmptyFormTypeObject<System.Supervisor[]>([]),
  projects: getEmptyFormTypeObject<System.Project[]>([]),
  lists: getEmptyFormTypeObject<System.List[]>([]),
  user: {
    email: "",
    name: "",
    phone: "",
    username: "",
    groups: [],
    admin: getEmptyFormTypeObject<boolean>(true, { excludeNonProd: true }),
  },
  pendingSupervisorCases: getEmptyFormTypeObject<System.Form[]>([]),
};

export const reducer = (state: System.IState, action: formAction) => {
  //Log every action while we're in development mode
  if (!isInProduction()) {
    const { type, ...rest } = action;
    console.log(type + ": ", rest);
  }
  switch (action.type) {
    case "SET_DB_DATA":
      return { ...state, dbData: action.dbData };
    case "SET_USER":
      return { ...state, user: action.user };
    case "SET_MODAL_PROPS":
      return { ...state, modalProps: action.modalProps };
    case "SET_SUPERVISORS":
      return {
        ...state,
        supervisors: {
          ...state.supervisors,
          [action.formType]: action.supervisors,
        },
      };
    case "SET_PROJECTS":
      return {
        ...state,
        projects: { ...state.projects, [action.formType]: action.projects },
      };
    case "SET_LISTS":
      return {
        ...state,
        lists: { ...state.lists, [action.formType]: action.lists },
      };
    case "SET_CACHE":
      return {
        ...state,
        supervisors: action.supervisors,
        projects: action.projects,
        lists: action.lists,
        pendingSupervisorCases: action.pendingSupervisorCases,
        isLoading: false,
        dbData: action.dbData,
      };
    case "SET_COMMENT_HIGHLIGHT":
      return {
        ...state,
        commentHighlight: action.value,
      };
    case "SET_PENDING_SUPERVISOR_CASES":
      return {
        ...state,
        pendingSupervisorCases: action.pendingSupervisorCases,
      };
    case "UPDATE_PENDING_SUPERVISOR_CASE":
      const cases = state.pendingSupervisorCases[
        action.pendingSupervisorCase.formType
      ].map((form) => {
        if (form._id !== action.pendingSupervisorCase._id) {
          return form;
        } else {
          return action.pendingSupervisorCase;
        }
      });
      return {
        ...state,
        pendingSupervisorCases: {
          ...state.pendingSupervisorCases,
          [action.pendingSupervisorCase.formType]: cases,
        },
      };
    case "SET_LDAP_PEOPLE":
      return {
        ...state,
        ldapPeople: action.ldapPeople,
      };
    case "SET_LDAP_PERSON":
      return {
        ...state,
        ldapPeople: state.ldapPeople.map((person) => {
          if (person.DN === action.ldapPerson.DN) {
            return action.ldapPerson;
          }
          return person;
        }),
      };
    default:
      return state;
  }
};
// A selector is a function that accepts a global state as an argument and returns data that is derived from that state. Selectors are useful to derived data that is complex and prone to mistakes/bugs when fetching it.

/** Get the supervisor for a given formType and project */
export const selectSupervisor = (
  state: System.IState,
  project: string,
  form: System.FormType
): System.Supervisor | undefined => {
  const matches = state.supervisors[form].filter((s) =>
    s.projects.includes(project)
  );
  return matches.length > 0 ? matches[0] : undefined;
};

/** Get all supervisors for a given formType, sorted  alphabetically */
export const selectSupervisors = (
  state: System.IState,
  form: System.FormType
): System.Supervisor[] => {
  return state.supervisors[form].sort((a, b) => {
    return a.name > b.name ? 1 : -1;
  });
};
/** Get all lists for a given formType, sorted alphabetically by name*/
export const selectLists = (
  state: System.IState,
  form: System.FormType
): System.List[] => {
  return state.lists[form].sort((a, b) => {
    return a.name > b.name ? 1 : -1;
  });
};

export interface SelectListParams {
  state: System.IState;
  form: System.FormType;
  name: string;
  defaultValues?: string[]; //values in case the list doesn't exist or can't be found
  //values that will be appended to the db list. Often contains the current value for embedded forms, to ensure that the value is displayed even if it has been removed from the list since the initial submission
  additionalValues?: string[];
}
/** Get a specific list for a given formType, based on its name. If not found, returns either a blank List (empy item array) or defaultValues (if specified)*/
export const selectList = (params: SelectListParams): System.List => {
  const { state, form, name, defaultValues, additionalValues } = params;
  const list = state.lists[form].filter((form) => form.name === name)[0];
  if (list) {
    if (additionalValues && additionalValues?.length > 0) {
      return {
        ...list,
        items: list.items.concat(
          additionalValues.map((value) => {
            return { label: value, value };
          })
        ),
      };
    } else {
      return list;
    }
  }
  return defaultValues
    ? {
        _id: name,
        name,
        description: name,
        items: defaultValues.map((value) => {
          return { value, label: value };
        }),
        defaultValue: defaultValues[0],
        form,
      }
    : {
        _id: name,
        name,
        description: name,
        items: [],
        defaultValue: "",
        form,
      };
};
/** Get all projects for a given formType, sorted alphabetically by 1) category, 2) project */
export const selectProjects = (
  state: System.IState,
  form: System.FormType
): System.Project[] => {
  return state.projects[form].sort((a, b) => {
    return a.category > b.category
      ? 1
      : a.category === b.category
      ? a.project > b.project
        ? 1
        : -1
      : -1;
  });
};

/** Get a list of all project categories for the given formtype */
export const selectProjectCategories = (
  state: System.IState,
  form: System.FormType
): string[] => {
  return state.projects[form]
    .map((project) => project.category)
    .filter((v, i, a) => a.indexOf(v) === i && v !== "");
};

/** Get the first (by same order as return in selectProjectCateories) project category for the given form type. Suitable for giving a default value */
export const selectFirstProjectCategory = (
  state: System.IState,
  form: System.FormType
): string => {
  return state.projects[form].length > 0
    ? state.projects[form][0].category
    : "";
};
/** Get a mapping between each category and a list of all projects in that category, for the given formtype */
export const selectCategorizedProjects = (
  state: System.IState,
  form: System.FormType
): System.ProjectsCategorized => {
  const map: System.ProjectsCategorized = {};
  state.projects[form].forEach((project) => {
    if (!map[project.category]) {
      map[project.category] = [];
    }
    map[project.category].push(project);
  });
  return map;
};

/** Returns a list of names of all forms the current user is admin of */
export const selectAdminForms = (state: System.IState) => {
  if (!state.user.email) {
    return [];
  }
  return getFormConfigs()
    .filter((formConfig) => state.user.admin[formConfig.formType])
    .map((formConfig) => formConfig.name);
};

/** Returns a list of names of all forms the current user is a supervisor of */
export const selectSupervisorForms = (state: System.IState): string[] => {
  if (!state.user.email) {
    return [];
  }
  return getFormTypes()
    .filter(
      (formType) =>
        state.supervisors[formType].filter(
          (supervisor) =>
            supervisor.email.toLowerCase() === state.user.email.toLowerCase()
        ).length > 0
    )
    .map((formType) => getFormConfig(formType).name);
};
