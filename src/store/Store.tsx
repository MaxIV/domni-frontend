import React, { Dispatch, useContext } from "react";
import { formAction, initialState, reducer } from "./Reducer";

interface IContextProps {
  state: System.IState;
  dispatch: Dispatch<formAction>;
}

const store = React.createContext({} as IContextProps);

export const StoreProvider = (props: any) => {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  const value = { state, dispatch };

  //If not in prod, expose store
  if (process.env.REACT_APP_MODE !== "production") {
    (window as any).store = state;
  }
  return <store.Provider value={value}>{props.children}</store.Provider>;
};

export const useStore = () => useContext(store);
