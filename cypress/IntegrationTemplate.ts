//TODO - find and replace 'Template' with config.name, config.url, config.path etc.
describe("Template", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Logs in and submits a form", () => {
    cy.login("user");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "TEMPLATE",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the Template form
    cy.get("#TEMPLATE").click();
    cy.url().should("include", "template/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("user").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("user").email);
    //TODO fill out fields
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "template/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //TODO fill out more fields
    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  //Log in as admin
  it("Can be processed by an administrator", () => {
    cy.login("administrator", "template/approval/10000");
    cy.waitForCache();
    //verify in approval view
    cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
    cy.getByTestId("form-10000-process-button").should("not.be.disabled");
    //navigate to form view through action menu and then back
    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-view-original").click();
    cy.getByTestId("form-fieldset-disabled-form").should("be.disabled");
    cy.getByTestId("form-header-status-label").should("contain", "PENDING");
    cy.getByTestId("form-title-h2").should("contain", "Template 10000");
    cy.go("back");
    //approve the submission
    cy.getByTestId("form-10000-process-button").click();
    cy.getByTestId("modal-button-1").click();
    cy.getByTestId("submit-title-h2").should("contain", "approved");
    cy.getByTestId("submit-title-h2").should("contain", "Template");
    cy.getByTestId("submit-body-div").should(
      "contain",
      "Your update has been registered."
    );
    //go to and verify in archive view
    // cy.navigateTo("TEMPLATE", "archive");
    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-toggle-details").click();
    cy.getByTestId("form-10000-step-3-div").contains("approved");
    //TODO check for any hook side effects added to the stepper (reminder mail, external reg, etc.)
  });
});
