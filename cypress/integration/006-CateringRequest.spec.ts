describe("Catering Request", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });

  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "CATERING_REQUEST",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the Catering request form
    cy.get("#CATERING_REQUEST").click();
    cy.url().should("include", "catering-request/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);
    cy.get("[name='DeliveryTime']").clear().type("2023-01-21");
    cy.get("[name='deliveryPoint']").select("E-building dining hall");
    cy.get("[name='purpose-autocomplete']").type("No purpose has been defined");
    cy.get("[name='activity']").type("activity");

    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "catering-request/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //continue
    cy.getByTestId("dynamicInputTable-new-row-button").click();
    cy.getByTestId("servingType-0-select").select("Lunch1");
    cy.getByTestId("deliveryTime-0-timePicker").type("12:00");
    cy.getByTestId("quantity-0-input").type("10");

    cy.getByTestId("dynamicInputTable-new-row-button").click();
    cy.getByTestId("deliveryTime-1-timePicker").type("15:00");
    cy.getByTestId("quantity-1-input").type("10");

    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });
  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 1);
    cy.get("table tr").eq(1).click().contains("Catering Request #10000");
    cy.wait(1000);
    cy.get("table tr").eq(2).click().contains("Catering Request #10000");
    cy.wait(1000);
  });
  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      cy.login("administrator", "catering-request/approval/10000");
      cy.waitForCache();
      //Status is pending
      cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
      //Event timeline step 1 is "Submitted" by "domni staff" ("Submitter")
      cy.getByTestId("form-10000-step-1-div")
        .should("contain", "submitted")
        .should("contain", Cypress.env("staff").name)
        .should("contain", "Submitter");

      //Event timeline step 2 is "Pending" by "MAX IV Reception" ("Supervisor")
      cy.getByTestId("form-10000-step-2-div")
        .should("contain", "pending")
        .should("contain", "MAX IV Reception")
        .should("contain", "Supervisor");
      //Catering specific fields:
      cy.get("[name='DeliveryTime']")
        .should("be.disabled")
        .should("have.value", "2023-01-21");
      cy.get("[name='deliveryPoint']")
        .should("be.disabled")
        .should("have.value", "E-building dining hall");
      cy.get("[name='purpose-autocomplete']")
        .should("be.disabled")
        .should("have.value", "No purpose has been defined");
      cy.get("[name='activity']")
        .should("be.disabled")
        .should("have.value", "activity");

      //Approve/process
      cy.getByTestId("form-10000-process-button").click();
      cy.getByTestId("modal-button-1").click();
    }
  );
});
