describe("People", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Handles people", { scrollBehavior: "center" }, () => {
    cy.login("supervisor");
    cy.waitForCache();
    cy.navigateTo("people");
    cy.getByTestId("people-filter-input").type("Isak");
    cy.getByTestId("ldap-person-card-isalin-container-div").contains("isalin");
    cy.getByTestId("ldap-person-card-isalin-email-fragment").contains("isak.lindhe@maxiv.lu.se");
    cy.getByTestId("ldap-person-card-isalin-office-fragment").contains("E110046-9");
  });
});
