describe("Settings", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Sets domnisupervisor as supervisor for most forms, along with new project. Also creates custom list for emp.", () => {
    cy.login("administrator");
    //clear settings (all collections), reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      includeForms: false,
      includeCollections: true,
    });
    cy.reload();
    cy.waitForCache();
    //PURCHASE APPLICATION
    cy.navigateTo("EMPLOYMENT_REQUEST", "settings");
    //Create a supervisor with project
    cy.createSupervisorWithNewProject(
      "supervisor",
      "Employment request project"
    );

    //Create the custom list for Operations budget
    cy.getByTestId("custom-lists-new-list-button").click();
    cy.inputModal({
      name: "Operations budgets",
      description: "Operations budgets list",
    });
    cy.getByTestId("custom-lists-list-label-span").should(
      "contain",
      "Operations budgets"
    );
    //Create 2 items
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "Item1" });
    cy.getByTestId("custom-list-list-item-Item1").should("contain", "Item1");
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "Item2" });
    cy.getByTestId("custom-list-list-item-Item2").should("contain", "Item2");
    //reselect the first item and make it the default item
    cy.getByTestId("custom-list-list-item-Item1").click();
    cy.getByTestId("custom-list-item-label-input").should(
      "have.value",
      "Item1"
    );
    cy.getByTestId("custom-list-item-make-default-button").click();
    cy.getByTestId("custom-list-item-make-default-button").contains(
      "IS DEFAULT"
    );
    cy.getByTestId("custom-list-item-make-default-button").should(
      "be.disabled"
    );
    cy.getByTestId("custom-list-item-save-button").click();

    //Create the custom list for the Beamline project budget
    cy.getByTestId("custom-lists-new-list-button").click();
    cy.inputModal({
      name: "Beamline project budget",
      description: "Beamline project budget list",
    });
    cy.getByTestId("custom-lists-list-label-span").should(
      "contain",
      "Beamline project budget"
    );
    //Create 3 items
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "MicroMAX" });
    cy.getByTestId("custom-list-list-item-MicroMAX");
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "ForMAX" });
    cy.getByTestId("custom-list-list-item-ForMAX");
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "DataSTaMP" });
    cy.getByTestId("custom-list-list-item-DataSTaMP");

    //Reload and verify supervisor, project and custom list is still there
    cy.reload(); //reload with new db data
    cy.waitForCache();
    cy.getByTestId(
      `settings-supervisor-list-item-${Cypress.env("supervisor").username}`
    ).click();
    cy.getByTestId(
      "settings-project-list-item-Employment request project"
    ).click();
    //Verify that custom list has been created properly
    cy.getByTestId("custom-lists-list-select").select(
      "Beamline project budget"
    );
    cy.getByTestId("custom-list-list-item-MicroMAX").click();
    cy.getByTestId("custom-list-list-item-ForMAX").click();
    cy.getByTestId("custom-list-list-item-DataSTaMP").click();
    cy.getByTestId("custom-lists-list-select").select("Operations budgets");
    cy.getByTestId("custom-list-list-item-Item1 (default)").click();
    cy.getByTestId("custom-list-list-item-Item2").click();
    //verify that navigation within settings reloads
    cy.navigateTo("CATERING_REQUEST", "settings");
    cy.getByTestId("settings-supervisor-name-input").should(
      "have.value",
      "MAX IV Reception"
    );
    cy.navigateTo("PURCHASE_APPLICATION", "settings");
    cy.getByTestId("settings-supervisor-name-input").should(
      "have.value",
      "Alexei Preobrajenski"
    );

    //Create supervisor for purchase, travel, grant initiative and additional funding
    cy.navigateTo("PURCHASE_APPLICATION", "settings");
    cy.createSupervisorWithNewProject("supervisor", "Purchase app project");
    cy.navigateTo("TRAVEL", "settings");
    cy.createSupervisorWithNewProject("supervisor", "Travel project");
    cy.navigateTo("GRANT_INITIATIVE", "settings");
    cy.createSupervisorWithNewProject("supervisor", "Grant project");
    cy.navigateTo("ADDITIONAL_FUNDING", "settings");
    cy.createSupervisorWithNewProject(
      "supervisor",
      "Additional funding project"
    );
    cy.navigateTo("GRANT_INITIATIVE", "settings");
    cy.createSupervisorWithNewProject("supervisor", "Grant Initiative project");
    cy.navigateTo("CATERING_REQUEST", "settings");

    //Create the custom list for orders
    cy.getByTestId("custom-lists-new-list-button").click();
    cy.inputModal({
      name: "ServingType",
      description: "Available serving types",
    });
    cy.getByTestId("custom-lists-list-label-span").should(
      "contain",
      "ServingType"
    );
    //Create 2 items
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "Lunch1" });
    cy.getByTestId("custom-list-list-item-Lunch1").should("contain", "Lunch1");
    cy.getByTestId("custom-lists-add-list-item-button").click();
    cy.inputModal({ name: "Lunch2" });
    cy.getByTestId("custom-list-list-item-Lunch2").should("contain", "Lunch2");
    //reselect the first item and make it the default item
    cy.getByTestId("custom-list-list-item-Lunch1").click();
    cy.getByTestId("custom-list-item-label-input").should(
      "have.value",
      "Lunch1"
    );
    cy.getByTestId("custom-list-item-make-default-button").click();
    cy.getByTestId("custom-list-item-make-default-button").contains(
      "IS DEFAULT"
    );
    cy.getByTestId("custom-list-item-make-default-button").should(
      "be.disabled"
    );
    cy.getByTestId("custom-list-item-save-button").click();
  });
});
