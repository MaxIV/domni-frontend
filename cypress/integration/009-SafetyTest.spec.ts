describe("Safety Test (submit validation, reminder scheduling)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "SAFETY_TEST",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the employment request form
    cy.get("#SAFETY_TEST").click();
    cy.url().should("include", "safety-test/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);

    cy.get("[name='employer']").should("have.value", "MAX IV");
    cy.get("[name='phoneNumber']")
      .clear()
      .type("12345")
      .should("have.value", "12345");
    // cy.get("[name='contactPerson']")
    //   .type("Jonas Rosenqvist")
    //   .should("have.value", "Jonas Rosenqvist");
    cy.get("[name='contactPerson']").should('be.disabled');
    cy.getByTestId("safety-test-language").click();
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "safety-test/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //Continue, answer basic questions
    cy.getByTestId("safety-test-BASIC-question-0-option-2").click();
    cy.getByTestId("safety-test-BASIC-question-1-option-2").click();
    cy.getByTestId("safety-test-BASIC-question-2-option-2").click();
    cy.getByTestId("safety-test-BASIC-question-3-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-4-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-5-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-6-option-0").click();
    cy.getByTestId("safety-test-BASIC-question-7-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-8-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-9-option-2").click();
    cy.getByTestId("safety-test-BASIC-question-10-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-11-option-1").click();
    cy.getByTestId("safety-test-BASIC-question-12-option-1").click(); //Correct answer is 0

    cy.getByTestId("safety-test-check-radiation").click();
    cy.getByTestId("safety-test-RADIATION-question-0-option-0").click();
    cy.getByTestId("safety-test-RADIATION-question-1-option-0").click();
    cy.getByTestId("safety-test-RADIATION-question-2-option-1").click();
    cy.getByTestId("safety-test-RADIATION-question-3-option-1").click();
    cy.getByTestId("safety-test-RADIATION-question-4-option-1").click();
    cy.getByTestId("safety-test-RADIATION-question-5-option-0").click();
    cy.getByTestId("safety-test-RADIATION-question-6-option-0").click(); //Correct answer is 1
    cy.getByTestId("safety-test-check-chemical").click();
    cy.getByTestId("safety-test-CHEMICAL-question-0-option-1").click();
    cy.getByTestId("safety-test-CHEMICAL-question-1-option-1").click();
    cy.getByTestId("safety-test-CHEMICAL-question-2-option-1").click();
    cy.getByTestId("safety-test-CHEMICAL-question-3-option-1").click(); //Correct answer is 2
    cy.getByTestId("safety-test-check-bio").click();
    cy.getByTestId("safety-test-BIO-question-0-option-1").click();
    cy.getByTestId("safety-test-BIO-question-1-option-1").click(); //Correct answer is 2
    cy.getByTestId("safety-test-check-signature").click();

    //Submit
    cy.getByTestId("form-submit-btn").click();

    cy.getByTestId("safety-test-validation").should(
      "contain",
      "Grundläggande tillträde"
    );
    cy.getByTestId("safety-test-validation").should(
      "contain",
      "Strålsäkerhet för Kontrollerat Område"
    );
    cy.getByTestId("safety-test-validation").should(
      "contain",
      "Utökad Kemikaliesäkerhet"
    );
    cy.getByTestId("safety-test-validation").should("contain", "Biosäkerhet");

    cy.getByTestId("modal-button-1").click();
    cy.getByTestId("safety-test-BASIC-question-12-option-0").click();
    cy.getByTestId("safety-test-RADIATION-question-6-option-1").click();
    cy.getByTestId("safety-test-CHEMICAL-question-3-option-2").click();
    cy.getByTestId("safety-test-BIO-question-1-option-2").click();

    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  //Log in as admin
  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      cy.login("administrator", "safety-test/approval/10000");
      cy.waitForCache();
      //verify in approval view
      cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
      cy.getByTestId("form-10000-process-button").should("not.be.disabled");
      //navigate to form view through action menu and then back
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-view-original").click();
      cy.getByTestId("form-fieldset-disabled-form").should("be.disabled");
      cy.getByTestId("form-header-status-label").should("contain", "PENDING");
      cy.getByTestId("form-title-h2").should("contain", "Safety Test 10000");
      cy.go("back");
      //approve the submission
      cy.getByTestId("form-10000-process-button").click();
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("submit-title-h2").should("contain", "approved");
      cy.getByTestId("submit-title-h2").should("contain", "Safety Test");
      cy.getByTestId("submit-body-div").should(
        "contain",
        "Your update has been registered."
      );
      //go to and verify in archive view
      cy.navigateTo("SAFETY_TEST", "archive");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-3-div").contains("approved");
      cy.getByTestId("form-10000-step-4-div").contains("scheduled reminders");
    }
  );
});
