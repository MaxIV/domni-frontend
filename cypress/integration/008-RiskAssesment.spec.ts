describe("Risk Assessment (propId => ext. id hook)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("user");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "RISK_ASSESSMENT",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the risk assessment form
    cy.get("#RISK_ASSESSMENT").click();
    cy.url().should("include", "risk-assessment/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("user").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("user").email);
    cy.get("[name='proposalId']").type("20202020");
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "risk-assessment/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  //Log in as admin
  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      cy.login("administrator", "risk-assessment/approval/10000");
      cy.waitForCache();
      //verify in approval view
      cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
      //navigate to form view through action menu and then back
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-view-original").click();
      cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
      cy.getByTestId("form-header-status-label").should(
        "contain",
        "ADMIN EDIT"
      );
      cy.getByTestId("form-title-h2").should(
        "contain",
        "Experimental Safety Risk Assessment (ESRA) 10000"
      );
      cy.get("[name='safetyClassification']").select("Yellow");
      cy.getByTestId("form-submit-btn").click();
      cy.getByTestId("modal-button-1").click();
      cy.url().should(
        "eq",
        `${Cypress.env("localHost")}risk-assessment/approval/10000`
      );

      //Reject buttons are visible and enabled
      cy.getByTestId("form-10000-reject-button").should("not.be.disabled");
      //Make sure proposalid is set as external id
      cy.getByTestId("form-10000-external-id-button").should(
        "contain",
        "20202020"
      );
      //reassign the submission
      cy.getByTestId("form-10000-approve-button").click();
      cy.getByTestId("approval-modal-supervisor-select").select(
        "EST - Experimental Safety Team"
      );
      cy.getByTestId("modal-input-comment").type(
        "I approve of this risk assessment"
      );
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("submit-title-h2").should("contain", "reassigned");
      cy.getByTestId("submit-title-h2").should("contain", "Risk Assessment");
      cy.getByTestId("submit-body-div").should(
        "contain",
        "Your approval has been registered and an email has been sent to the new supervisor."
      );
      //go to and verify in archive view
      cy.navigateTo("RISK_ASSESSMENT", "archive");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-2-div").contains("edited");
      cy.getByTestId("form-10000-step-3-div").contains("supplanted");
      cy.getByTestId("form-10000-step-4-div").contains("approved");
      cy.getByTestId("form-10000-step-5-div").contains("pending");
      //Reject the form
      cy.getByTestId("form-10000-resolve-button").click();
      cy.getByTestId("form-10000-reject-button").click();
      cy.getByTestId("modal-input-comment").type(
        "I do not approve approve of this risk assessment"
      );
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("submit-title-h2").should("contain", "rejected");
      cy.getByTestId("submit-body-div").should(
        "contain",
        "Your rejection has been registered and an email has been sent to the original submitter."
      );
      //go to and verify in archive view
      cy.navigateTo("RISK_ASSESSMENT", "archive");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("notified");
      cy.getByTestId("form-10000-step-1-div").contains(
        Cypress.env("user").name
      );
      cy.getByTestId("form-10000-step-3-div").contains("supplanted");
      cy.getByTestId("form-10000-step-3-div").contains(
        "EST - Experimental Safety Team"
      );
      cy.getByTestId("form-10000-step-4-div").contains("approved");
      cy.getByTestId("form-10000-step-4-div").contains(
        Cypress.env("administrator").name
      );
      cy.getByTestId("form-10000-step-5-div").contains("supplanted");
      cy.getByTestId("form-10000-step-5-div").contains(
        "EST - Experimental Safety Team"
      );
      cy.getByTestId("form-10000-step-6-div").contains("rejected");
      cy.getByTestId("form-10000-step-6-div").contains(
        Cypress.env("administrator").name
      );
    }
  );
  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 3);
    cy.get("table tr").contains("Decision: Risk Assessment #10000").click();
    cy.get("table tr").contains("Reassignment: Risk Assessment #10000").click();
    cy.get("table tr").contains("Confirmation: Risk Assessment #10000").click();
    cy.get("table tr").contains("Submission: Risk Assessment #10000").click();
  });
  it("Can be printed with it's custom print component", {scrollBehavior: "center"}, () => {
    cy.login("administrator", "risk-assessment/approval/10000");
    cy.waitForCache();
    cy.navigateTo("RISK_ASSESSMENT", "archive");
    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-view-print-version").click();
    cy.getByTestId("print-risk-assessment-proposal-id").contains("PROPOSAL 20202020");
    cy.getByTestId("print-risk-assessment-safety-color").should("have.css", "color", "rgb(255, 255, 0)");
  });
});
