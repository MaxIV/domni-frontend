describe("Grant Initiative (admin edit, ext. id, custom search fields, onDecision registry hook)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "GRANT_INITIATIVE",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the Grant Initiative form
    cy.get("#GRANT_INITIATIVE").click();
    cy.url().should("include", "grant-initiative/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);
    Comment;
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "grant-initiative/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //continue
    cy.getByTestId("projectName").type("Name of the project");
    cy.getByTestId("responsible").type("MAX IV responsible");
    cy.getByTestId("coordinator").type("Grant coordinator");
    cy.getByTestId("otherParties").type("Other parties involved");
    cy.getByTestId("collaborators").type("Collaborators and partners");
    cy.getByTestId("fundingAgency").type("Funding agency");
    cy.getByTestId("projectDescription").type("Project desc.");
    cy.getByTestId("beamlineAccessType-checkbox").click();
    cy.getByTestId("beamlineAccessType").type("access type");
    cy.getByTestId("pooledResource-checkbox").click();
    cy.getByTestId("pooledResource").type("pooled resources");
    cy.getByTestId("ownership-select").select("Yes");
    cy.getByTestId("officeSpaceCount").type("2");
    cy.getByTestId("otherResources").type("Other res.");

    cy.getByTestId("funding-salaries-0").type("1");
    cy.getByTestId("funding-investments-0").type("2");
    cy.getByTestId("funding-travel-0").type("3");
    cy.getByTestId("funding-others-0").type("4");
    cy.getByTestId("funding-indirectCosts-0").type("5");
    cy.getByTestId("cofunding-salaries-0").type("6");
    cy.getByTestId("cofunding-others-0").type("7");
    cy.getByTestId("cofunding-indirectCosts-0").type("8");

    cy.getByTestId("funding-salaries-1").type("9");
    cy.getByTestId("funding-investments-1").type("10");
    cy.getByTestId("funding-travel-1").type("11");
    cy.getByTestId("funding-others-1").type("12");
    cy.getByTestId("funding-indirectCosts-1").type("13");
    cy.getByTestId("cofunding-salaries-1").type("14");
    cy.getByTestId("cofunding-others-1").type("15");
    cy.getByTestId("cofunding-indirectCosts-1").type("16");

    cy.getByTestId("funding-salaries-2").type("17");
    cy.getByTestId("funding-investments-2").type("18");
    cy.getByTestId("funding-travel-2").type("19");
    cy.getByTestId("funding-others-2").type("20");
    cy.getByTestId("funding-indirectCosts-2").type("21");
    cy.getByTestId("cofunding-salaries-2").type("22");
    cy.getByTestId("cofunding-others-2").type("23");
    cy.getByTestId("cofunding-indirectCosts-2").type("24");

    cy.getByTestId("funding-salaries-3").type("25");
    cy.getByTestId("funding-investments-3").type("26");
    cy.getByTestId("funding-travel-3").type("27");
    cy.getByTestId("funding-others-3").type("28");
    cy.getByTestId("funding-indirectCosts-3").type("29");
    cy.getByTestId("cofunding-salaries-3").type("30");
    cy.getByTestId("cofunding-others-3").type("31");
    cy.getByTestId("cofunding-indirectCosts-3").type("32");

    cy.getByTestId("funding-salaries-4").type("33");
    cy.getByTestId("funding-investments-4").type("34");
    cy.getByTestId("funding-travel-4").type("35");
    cy.getByTestId("funding-others-4").type("36");
    cy.getByTestId("funding-indirectCosts-4").type("37");
    cy.getByTestId("cofunding-salaries-4").type("38");
    cy.getByTestId("cofunding-others-4").type("39");
    cy.getByTestId("cofunding-indirectCosts-4").type("40");

    cy.getByTestId("strategyAlignment").type("Stragety alignment");
    cy.getByTestId("person-0-firstName").type("Jonas");
    cy.getByTestId("person-0-lastName").type("Rosenqvist");

    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  it(
    "Can be processed by a administrator",
    { scrollBehavior: "center" },
    () => {
      cy.login("administrator", "grant-initiative/approval/10000");
      cy.waitForCache();
      //verify in approval view
      cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
      //navigate to form view through action menu, make sure it's pending and disabled for the administrator, and then go back
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-view-original").click();
      cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
      cy.getByTestId("form-header-status-label").should(
        "contain",
        "ADMIN EDIT"
      );
      cy.getByTestId("form-title-h2").should(
        "contain",
        "Grant Initiative 10000"
      );
      cy.go("back");
      cy.url().should(
        "eq",
        `${Cypress.env("localHost")}grant-initiative/approval/10000`
      );
      //Reject buttons are visible and enabled
      cy.getByTestId("form-10000-reject-button").should("not.be.disabled");

      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("submitted");
      cy.getByTestId("form-10000-step-2-div").contains("pending");
      //Approve the form
      cy.getByTestId("form-10000-approve-button").click();
      cy.getByTestId("modal-input-comment").type(
        "I approve of this Grant Initiative"
      );
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("submit-title-h2").should("contain", "approved");
      cy.getByTestId("submit-body-div").should(
        "contain",
        "Your approval has been registered and an email has been sent to the original submitter."
      );
      //go to and verify in archive view
      cy.navigateTo("GRANT_INITIATIVE", "archive");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("notified");
      cy.getByTestId("form-10000-step-1-div").contains(
        Cypress.env("staff").name
      );
      cy.getByTestId("form-10000-step-2-div").contains("supplanted");
      cy.getByTestId("form-10000-step-2-div").contains("MAX IV Finance");
      cy.getByTestId("form-10000-step-3-div").contains("approved");
      cy.getByTestId("form-10000-step-3-div").contains(
        Cypress.env("administrator").name
      );
    }
  );

  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 3);
    cy.get("table tr").contains("Decision: Grant Initiative #10000").click();
    cy.get("table tr")
      .contains("Confirmation: Grant Initiative #10000")
      .click();
    cy.get("table tr").contains("Submission: Grant Initiative #10000").click();
  });
});
