describe("Profile, must run after form tests", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it(
    "Logs in, creates safety test and risk assessment drafts",
    { scrollBehavior: "center" },
    () => {
      cy.login("user");
      cy.waitForCache();
      //create a safetytest and riskassessment draft (also testing landing page card buttons)
      cy.getByTestId("landing-page-RISK_ASSESSMENT-btn").click();
      cy.getByTestId("form-save-draft-btn").click();
      cy.url().should("include", "risk-assessment/form/100");
      //Submit a risk assessment
      cy.go("back");
      cy.getByTestId("landing-page-RISK_ASSESSMENT-btn").click();
      cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
      cy.get("[name='proposalId']").type("21212121");
      cy.getByTestId("form-submit-btn").click();
      cy.url().should("include", "submit/?action=submitted");
      cy.get("h2").contains("submitted");
    }
  );
  it(
    "Deletes drafts, verifies id and status of submitted cases",
    { scrollBehavior: "center" },
    () => {
      cy.login("user");
      cy.waitForCache();
      //Go to profile view
      cy.navigateTo("profile");
      //verify drafts and then delete them
      cy.getByTestId("profile-draft-RISK_ASSESSMENT-10001-div").contains(
        "10001 - Experimental Safety Risk Assessment"
      );
      cy.getByTestId("profile-delete-drafts-btn").click();
      cy.getByTestId("modal-button-1").click();

      //Verify all 3 submissions
      cy.getByTestId(
        "profile-submitted-cases-RISK_ASSESSMENT-10000-id"
      ).contains("20202020");
      cy.getByTestId(
        "profile-submitted-cases-RISK_ASSESSMENT-10000-status"
      ).contains("rejected");
      cy.getByTestId(
        "profile-submitted-cases-RISK_ASSESSMENT-10002-id"
      ).contains("212121");
      cy.getByTestId(
        "profile-submitted-cases-RISK_ASSESSMENT-10002-status"
      ).contains("pending");
    }
  );
});
