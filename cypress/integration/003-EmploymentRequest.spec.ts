describe("Employment Request (admin edit, comment, ext. id)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });

  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "EMPLOYMENT_REQUEST",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the employment request form
    cy.get("#EMPLOYMENT_REQUEST").click();
    cy.url().should("include", "employment-request/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);
    cy.get("[name='position']")
      .type("Software developer")
      .should("have.value", "Software developer");
    cy.get("[name='group']").type("IMS").should("have.value", "IMS");
    cy.get("[name='team']").type("KITS").should("have.value", "KITS");
    cy.get("[name='employmentType']")
      .select("Temporary")
      .should("have.value", "Temporary");
    cy.get("[name='extent']").type("70").should("have.value", "70");
    cy.get("[name='estMonthlySalary']")
      .type("42000")
      .should("have.value", "42000");
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "employment-request/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //continue
    cy.get("[name='estStartDate']")
      .clear()
      .type("2021-02-17")
      .should("have.value", "2021-02-17");
    cy.get("[name='estEndDate']")
      .clear()
      .type("2023-02-17")
      .should("have.value", "2023-02-17");
    cy.get("[name='contactedOfficeCoordinatorCheck']")
      .check()
      .should("have.value", "true");
    cy.get("[name='rejectionAction']")
      .type("Some")
      .should("have.value", "Some");
    cy.get("[name='budgetType']").check("IN_BUDGET");
    cy.getByTestId("budgetTypeDetail")
      .select("Item2")
      .should("have.value", "Item2");
    cy.getByTestId("budgetTypeDetail2")
      .select("ForMAX")
      .should("have.value", "ForMAX");
    cy.get("[name='externallyFundedProject']").check();
    cy.getByTestId("project-select")
      .should("not.be.disabled")
      .select("PROJ VR NanoSPAM K.Thånell 134375");
    cy.get("[name='externallyFundedPercentage']")
      .type("50")
      .should("have.value", "50");
    cy.get("h2").contains("10000");

    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });
  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 1);
    cy.get("table tr").eq(1).click().contains("Employment Request #10000");
    cy.wait(1000);
    cy.get("table tr").eq(2).click().contains("Employment Request #10000");
    cy.wait(1000);
  });
  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      cy.login("administrator", "employment-request/approval/10000");
      cy.waitForCache();
      //Status is pending
      cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
      //Approved  and reject buttons are visible and enabled
      cy.getByTestId("form-10000-approve-button").should("not.be.disabled");
      cy.getByTestId("form-10000-reject-button").should("not.be.disabled");

      //Event timeline step 1 is "Submitted" by "domni staff" ("Submitter")
      cy.getByTestId("form-10000-step-1-div")
        .should("contain", "submitted")
        .should("contain", Cypress.env("staff").name)
        .should("contain", "Submitter");

      //Event timeline step 2 is "Pending" by "MAX IV Finance" ("Supervisor")
      cy.getByTestId("form-10000-step-2-div")
        .should("contain", "pending")
        .should("contain", "MAX IV Finance")
        .should("contain", "Supervisor");
      //Employment specific fields:
      cy.get("[name='position']")
        .should("be.disabled")
        .should("have.value", "Software developer");
      cy.get("[name='group']")
        .should("be.disabled")
        .should("have.value", "IMS");
      cy.get("[name='team']")
        .should("be.disabled")
        .should("have.value", "KITS");
      cy.get("[name='employmentType']")
        .should("be.disabled")
        .select("Temporary")
        .should("have.value", "Temporary");
      cy.get("[name='extent']")
        .should("be.disabled")
        .should("have.value", "70");
      cy.get("[name='estMonthlySalary']")
        .should("be.disabled")
        .should("have.value", "42000");
      cy.get("[name='estStartDate']")
        .should("be.disabled")
        .should("have.value", "2021-02-17");
      cy.get("[name='estEndDate']")
        .should("be.disabled")
        .should("have.value", "2023-02-17");
      cy.get("[name='contactedOfficeCoordinatorCheck']")
        .should("be.disabled")
        .should("have.value", "true");
      cy.get("[name='rejectionAction']")
        .should("be.disabled")
        .should("have.value", "Some");
      cy.get("[name='budgetType']").should("be.disabled");
      cy.get("[name='budgetTypeDetail']")
        .should("be.disabled")
        .should("have.value", "Item2");
      cy.get("[name='budgetTypeDetail2']")
        .should("be.disabled")
        .should("have.value", "ForMAX");
      cy.getByTestId("project-select")
        .should("be.disabled")
        .contains("PROJ VR NanoSPAM K.Thånell 134375");
      cy.get("[name='externallyFundedPercentage']")
        .should("be.disabled")
        .should("have.value", "50");
      //Add a comment
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-comments").click();
      cy.getByTestId("form-10000-comment-input").type("manual comment");
      cy.getByTestId("form-10000-comment-button").click();
      cy.getByTestId("form-10000-comment-1-div").contains("manual comment");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-comments").click();
      //add external id
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-edit-external-id").click();
      cy.getByTestId("modal-input-id").type("External id: 123");
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("form-10000-external-id-button").contains(
        "External id: 123"
      );
      //Edit a field in the form
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-view-original").click();
      cy.get("h2").contains("Employment Request 10000");
      cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
      cy.get("[name='rejectionAction']")
        .should("not.be.disabled")
        .clear()
        .type("None")
        .should("have.value", "None");
      cy.getByTestId("form-submit-btn").click();
      cy.getByTestId("modal-button-1").click();
      cy.url().should(
        "eq",
        `${Cypress.env("localHost")}employment-request/approval/10000`
      );
      //Verify that step 2 now is a comment, that step 3 is an external id, step 4 is an edit, and the pending action has been moved to step 5
      cy.getByTestId("form-10000-step-2-div")
        .should("contain", "commented")
        .should("contain", Cypress.env("administrator").name)
        .should("contain", "Admin");
      cy.getByTestId("form-10000-step-3-div")
        .should("contain", "ext. id") //actually lower case, gets capitalized through css
        .should("contain", Cypress.env("administrator").name)
        .should("contain", "Admin");
      cy.getByTestId("form-10000-step-4-div")
        .should("contain", "edited")
        .should("contain", Cypress.env("administrator").name)
        .should("contain", "Admin");
      cy.getByTestId("form-10000-step-5-div")
        .should("contain", "pending")
        .should("contain", "MAX IV Finance")
        .should("contain", "Supervisor");
      //Approve with reassignment
      cy.getByTestId("form-10000-approve-button").click();
      cy.getByTestId("approval-modal-supervisor-select").select("Ian McNulty");
      cy.getByTestId("modal-input-comment").type("Approval comment");
      cy.getByTestId("modal-button-1").click();
      cy.url().should("include", "submit/?action=reassigned");
      cy.get("h2").contains("reassigned");
    }
  );
  it("Handles archive", { scrollBehavior: "center" }, () => {
    cy.login("administrator");
    cy.waitForCache();
    cy.navigateTo("EMPLOYMENT_REQUEST", "archive");
    cy.getByTestId("form-10000-external-id-button").contains(
      "External id: 123"
    );

    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-toggle-details").click();
    cy.getByTestId("form-10000-resolve-button").click();
    cy.getByTestId("form-10000-reject-button").click();
    cy.inputModal({ comment: "My rejection comment" });
    cy.navigateTo("EMPLOYMENT_REQUEST", "archive");
    cy.getByTestId("form-10000-status-tag-div").contains("rejected");
  });
});
