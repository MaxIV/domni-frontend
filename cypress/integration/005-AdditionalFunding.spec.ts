// login as domniadministrator and add domnisupervisor as supervisor
// login as domnistaff and submit a form
//login as domnisupervisor and edit the form
//make a manual comment on the form
//approve approve the form
describe("Additional Funding (admin edit, final approval validation)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });

  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "ADDITIONAL_FUNDING",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the Additional Funding Request form
    cy.get("#ADDITIONAL_FUNDING").click();
    cy.url().should("include", "additional-funding/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);
    cy.get("[name='applicant']").type("Jonas Rosenqvist");
    cy.get("[name='organizationalUnitName']").select("BLOCH");
    cy.get("[name='totalAmountRequested']").type("600000");
    cy.get("[name='responsibleDirector']").select("domnisupervisor");

    //make sure yellow section is not visible
    cy.get("[name='contingencyAmountRemaining']").should("not.exist");
    cy.get("[name='totalAmountApproved']").should("not.exist");
    cy.get("[name='fundingSource']").should("not.exist");

    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "additional-funding/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //continue
    cy.get("[name='investmentDescription']").type(
      "A description of the purchase or investment"
    );
    cy.get("[name='investmentBackground']").type(
      "A background and a detailed cost breakdown of the purchase or investment"
    );
    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      cy.login("administrator", "additional-funding/approval/10000");
      cy.waitForCache();
      //Status is pending
      cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
      //Approved and reject buttons are visible and enabled
      cy.getByTestId("form-10000-approve-button").should("not.be.disabled");
      cy.getByTestId("form-10000-reject-button").should("not.be.disabled");

      //Event timeline step 1 is "Submitted" by "domni staff" ("Submitter")
      cy.getByTestId("form-10000-step-1-div")
        .should("contain", "submitted")
        .should("contain", Cypress.env("staff").name)
        .should("contain", "Submitter");

      //Event timeline step 2 is "Pending" by MAX IV Finance
      cy.getByTestId("form-10000-step-2-div")
        .should("contain", "pending")
        .should("contain", "MAX IV Finance") //hard-coded supervisor for this formtype
        .should("contain", "Supervisor");
      //Funding request specific fields:

      //Edit a field in the form
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-view-original").click();
      cy.get("h2").contains("Additional Funding Request 10000");
      cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");

      cy.get("[name='contingencyAmountRemaining']").type("1000");
      cy.get("[name='totalAmountApproved']").type("10000");

      cy.getByTestId("form-submit-btn").click();
      cy.getByTestId("modal-button-1").click();
      cy.url().should(
        "eq",
        `${Cypress.env("localHost")}additional-funding/approval/10000`
      );
      //Verify that step 2 now is an  edit, and the pending action has been moved to step 3
      cy.getByTestId("form-10000-step-2-div")
        .should("contain", "edited")
        .should("contain", Cypress.env("administrator").name);
      cy.getByTestId("form-10000-step-3-div")
        .should("contain", "pending")
        .should("contain", "MAX IV Finance")
        .should("contain", "Supervisor");
      //Approve with reassignment to domnisupervisor
      cy.getByTestId("form-10000-approve-button").click();
      cy.getByTestId("approval-modal-supervisor-select").select(
        "domnisupervisor"
      );
      cy.getByTestId("modal-input-comment").type(
        "Approval comment by domniadministrator"
      );
      cy.getByTestId("modal-button-1").click();
      cy.url().should("include", "submit/?action=reassigned");
      cy.get("h2").contains("reassigned");
    }
  );
  it("Can be finalized by the supervisor", { scrollBehavior: "center" }, () => {
    cy.login("supervisor", "additional-funding/approval/10000");
    cy.waitForCache();

    //Status is pending
    cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
    //Approved and reject buttons are visible and enabled
    cy.getByTestId("form-10000-approve-button").should("not.be.disabled");
    cy.getByTestId("form-10000-reject-button").should("not.be.disabled");

    //Verify all steps
    cy.getByTestId("form-10000-step-2-div")
      .should("contain", "edited")
      .should("contain", Cypress.env("administrator").name)
      .should("contain", "Administrator");
    cy.getByTestId("form-10000-step-3-div")
      .should("contain", "supplanted")
      .should("contain", "MAX IV Finance")
      .should("contain", "Supervisor");
    cy.getByTestId("form-10000-step-4-div")
      .should("contain", "approved")
      .should("contain", Cypress.env("administrator").name)
      .should("contain", "Administrator");

    cy.getByTestId("form-10000-step-5-div")
      .should("contain", "pending")
      .should("contain", Cypress.env("supervisor").name)
      .should("contain", "Supervisor");

    //Try to finalize, confirm error, cancel approval
    cy.getByTestId("form-10000-approve-button").click();
    cy.getByTestId("modal-input-comment").type("Final approval by supervisor");
    cy.getByTestId("modal-button-1").click();
    cy.getByTestId("modal-validation-result-div").should(
      "contain",
      "The approval can not be finalized as the form is incomplete"
    );
    cy.getByTestId("modal-button-2").click();

    //Edit the form to set the DM Ex contingency
    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-view-original").click();
    cy.get("h2").contains("Additional Funding Request 10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.get("[name='fundingSource']").check("DM Ex contingency");
    cy.getByTestId("form-submit-btn").click();
    cy.getByTestId("modal-button-1").click();
    cy.url().should(
      "eq",
      `${Cypress.env("localHost")}additional-funding/approval/10000`
    );

    //Finalize approval
    cy.getByTestId("form-10000-approve-button").click();
    cy.getByTestId("modal-input-comment").type("Final approval by supervisor");
    cy.getByTestId("modal-button-1").click();
  });
  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 3);
    cy.get("table tr")
      .contains("Decision: Additional Funding Request #10000")
      .click();
    cy.get("table tr")
      .contains("Confirmation: Additional Funding Request #10000")
      .click();
    cy.get("table tr")
      .contains("Submission: Additional Funding Request #10000")
      .click();
  });
});
