describe("Purchase Application (admin edit, ext. id, custom search fields, onDecision registry hook)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "PURCHASE_APPLICATION",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the Purchase Application form
    cy.get("#PURCHASE_APPLICATION").click();
    cy.url().should("include", "purchase-application/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);

    cy.getByTestId("project-select")
      .should("not.be.disabled")
      .select("Purchase app project");
    cy.getByTestId("supervisor-select").should(
      "contain",
      Cypress.env("supervisor").name
    );
    Comment;
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "purchase-application/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //continue
    cy.get("[name='comment']").type("Purchase application comment");
    cy.get("[name='product']").type("Purchase application product name");
    cy.get("[name='amount']").type("217500");
    cy.get("[name='description']").type("Purchase application description");

    cy.get("[name='supplierName']").type("Winning bid");
    cy.get("[name='bidder1']").type("bidder 1");
    cy.get("[name='bidder2']").type("bidder 2");
    cy.get("[name='bidder3']").type("bidder 3");
    cy.get("[name='motivation']").type("Purchase application motivation");
    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  //Log in as supervisor
  it("Can be processed by a supervisor", { scrollBehavior: "center" }, () => {
    cy.login("supervisor", "purchase-application/approval/10000");
    cy.waitForCache();
    //verify in approval view
    cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
    //navigate to form view through action menu, make sure it's pending and disabled for the supervisor,  and then go back
    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-view-original").click();
    cy.getByTestId("form-fieldset-disabled-form").should("be.disabled");
    cy.getByTestId("form-header-status-label").should("contain", "PENDING");
    cy.getByTestId("form-title-h2").should(
      "contain",
      "Purchase Application 10000"
    );
    cy.go("back");
    cy.url().should(
      "eq",
      `${Cypress.env("localHost")}purchase-application/approval/10000`
    );
    cy.get("[name='comment']").should(
      "have.value",
      "Purchase application comment"
    );
    cy.get("[name='product']").should(
      "have.value",
      "Purchase application product name"
    );
    cy.get("[name='amount']").should("have.value", "217500");
    cy.get("[name='description']").should(
      "have.value",
      "Purchase application description"
    );

    cy.get("[name='supplierName']").should("have.value", "Winning bid");
    cy.get("[name='bidder1']").should("have.value", "bidder 1");
    cy.get("[name='bidder2']").should("have.value", "bidder 2");
    cy.get("[name='bidder3']").should("have.value", "bidder 3");
    cy.get("[name='motivation']").should(
      "have.value",
      "Purchase application motivation"
    );

    //Reject buttons are visible and enabled
    cy.getByTestId("form-10000-reject-button").should("not.be.disabled");
    //reassign the submission
    cy.getByTestId("form-10000-approve-button").click();
    cy.getByTestId("approval-modal-supervisor-select").select("Ann Terry");
    cy.getByTestId("modal-input-comment").type(
      "I approve of this Purchase Application"
    );
    cy.getByTestId("modal-button-1").click();
    cy.getByTestId("submit-title-h2").should("contain", "reassigned");
    cy.getByTestId("submit-title-h2").should("contain", "Purchase Application");
    cy.getByTestId("submit-body-div").should(
      "contain",
      "Your approval has been registered and an email has been sent to the new supervisor."
    );
  });
  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      //go to and verify in archive view
      cy.login("administrator", "purchase-application/archive/");
      cy.waitForCache();
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("submitted");
      cy.getByTestId("form-10000-step-2-div").contains("approved");
      cy.getByTestId("form-10000-step-3-div").contains("pending");
      //Approve the form
      cy.getByTestId("form-10000-resolve-button").click();
      cy.getByTestId("form-10000-approve-button").click();
      cy.getByTestId("modal-input-comment").type(
        "I also approve of this Purchase Application"
      );
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("submit-title-h2").should("contain", "approved");
      cy.getByTestId("submit-body-div").should(
        "contain",
        "Your approval has been registered and an email has been sent to the original submitter."
      );
      //go to and verify in archive view
      cy.navigateTo("PURCHASE_APPLICATION", "archive");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("notified");
      cy.getByTestId("form-10000-step-1-div").contains(
        Cypress.env("staff").name
      );
      cy.getByTestId("form-10000-step-2-div").contains("approved");
      cy.getByTestId("form-10000-step-2-div").contains(
        Cypress.env("supervisor").name
      );
      cy.getByTestId("form-10000-step-3-div").contains("supplanted");
      cy.getByTestId("form-10000-step-3-div").contains("Ann Terry");
      cy.getByTestId("form-10000-step-4-div").contains("approved");
      cy.getByTestId("form-10000-step-4-div").contains(
        Cypress.env("administrator").name
      );
      cy.getByTestId("form-10000-step-5-div").contains("external reg.");
      cy.getByTestId("form-10000-step-5-div").contains("Domni");
      cy.getByTestId("form-10000-step-5-div").contains("System");
      //Perform an advanced search on a custom field
      cy.getByTestId("archive-advanced-search-toggle-button").click();
      cy.getByTestId("archive-advanced-search-custom-field-radio").click();
      cy.getByTestId("archive-advanced-search-status-select").select(
        "Rejected"
      );
      cy.getByTestId("archive-advanced-search-search-button").click();
      //Verify result: NONE
      cy.getByTestId("form-10000-id-container-div").should("not.exist");
      cy.getByTestId("archive-advanced-search-status-select").select(
        "Approved"
      );
      cy.getByTestId("archive-advanced-search-search-button").click();
      //Verify result: 10000
      cy.getByTestId("form-10000-id-container-div").should("exist");
      cy.getByTestId("archive-advanced-search-search-input").type(
        "Purchase application motivation"
      );
      cy.getByTestId("archive-advanced-search-search-button").click();
      //Verify results: NONE (wrong custom field selected)
      cy.getByTestId("form-10000-id-container-div").should("not.exist");
      cy.getByTestId("archive-advanced-search-custom-field-select").select(
        "Motivation"
      );
      cy.getByTestId("archive-advanced-search-search-button").click();
      //Verify result: 10000
      cy.getByTestId("form-10000-id-container-div").should("exist");
    }
  );
  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 3);
    cy.get("table tr")
      .contains("Decision: Purchase Application #10000")
      .click();
    cy.get("table tr")
      .contains("Reassignment: Purchase Application #10000")
      .click();
    cy.get("table tr")
      .contains("Confirmation: Purchase Application #10000")
      .click();
    cy.get("table tr")
      .contains("Submission: Purchase Application #10000")
      .click();
  });
});
