describe("Travel Approval (admin edit, ext. id, custom search fields, onDecision registry hook)", () => {
  afterEach(function () {
    if (this.currentTest.state === "failed") {
      (Cypress as any).runner.stop();
    }
  });
  it("Logs in and submits a form", { scrollBehavior: "center" }, () => {
    cy.login("staff");
    //clear forms, reload and wait for cache
    cy.request("DELETE", "/api/seedDb", {
      formType: "TRAVEL",
      includeForms: true,
      includeCollections: false,
    });
    cy.reload();
    cy.waitForCache();
    //Visit the Travel Approval form
    cy.get("#TRAVEL").click();
    cy.url().should("include", "travel-approval/form");
    cy.waitForCache();
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    cy.getByTestId("form-submitter-name-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").name);
    cy.getByTestId("form-submitter-email-input")
      .should("be.disabled")
      .should("have.value", Cypress.env("staff").email);

    cy.getByTestId("project-select")
      .should("not.be.disabled")
      .select("Travel project");
    cy.getByTestId("supervisor-select").should(
      "contain",
      Cypress.env("supervisor").name
    );
    Comment;
    //Save draft!
    cy.getByTestId("form-save-draft-btn").click();
    cy.url().should("include", "travel-approval/form/10000");
    cy.getByTestId("form-title-h2").should("contain", "10000");
    cy.getByTestId("form-fieldset-disabled-form").should("not.be.disabled");
    //continue
    cy.get("[name='destination']").type("Travel destination");
    cy.get("[name='travelPurpose']").type("Purpose of travel");
    cy.get("[name='startDate']").clear().type("2023-01-01");
    cy.get("[name='endDate']").clear().type("2023-01-21");
    cy.get("[name='travel']").type("100");
    cy.get("[name='accommodation']").type("200");
    cy.get("[name='conferenceFee']").type("300");
    cy.get("[name='subsistence']").type("400");
    cy.get("[name='totalExpenses']").should("have.value", "1000");
    //Submit
    cy.getByTestId("form-submit-btn").click();
    cy.url().should("include", "submit/?action=submitted");
    cy.get("h2").contains("submitted");
  });

  //Log in as supervisor
  it("Can be processed by a supervisor", { scrollBehavior: "center" }, () => {
    cy.login("supervisor", "travel-approval/approval/10000");
    cy.waitForCache();
    //verify in approval view
    cy.getByTestId("form-10000-status-tag-div").should("contain", "pending");
    //navigate to form view through action menu, make sure it's pending and disabled for the supervisor,  and then go back
    cy.getByTestId("form-10000-action-menu").click();
    cy.getByTestId("action-menu-view-original").click();
    cy.getByTestId("form-fieldset-disabled-form").should("be.disabled");
    cy.getByTestId("form-header-status-label").should("contain", "PENDING");
    cy.getByTestId("form-title-h2").should("contain", "Travel Approval 10000");
    cy.go("back");
    cy.url().should(
      "eq",
      `${Cypress.env("localHost")}travel-approval/approval/10000`
    );

    cy.getByTestId("project-select").should("contain", "Travel project");
    cy.getByTestId("supervisor-select").should(
      "contain",
      Cypress.env("supervisor").name
    );
    cy.get("[name='destination']").should("have.value", "Travel destination");
    cy.get("[name='travelPurpose']").should("have.value", "Purpose of travel");
    cy.get("[name='startDate']").should("have.value", "2023-01-01");
    cy.get("[name='endDate']").should("have.value", "2023-01-21");
    cy.get("[name='travel']").should("have.value", "100");
    cy.get("[name='accommodation']").should("have.value", "200");
    cy.get("[name='conferenceFee']").should("have.value", "300");
    cy.get("[name='subsistence']").should("have.value", "400");
    cy.get("[name='totalExpenses']").should("have.value", "1000");

    //Reject buttons are visible and enabled
    cy.getByTestId("form-10000-reject-button").should("not.be.disabled");
    //reassign the submission
    cy.getByTestId("form-10000-approve-button").click();
    cy.getByTestId("approval-modal-supervisor-select").select("Darren Spruce");
    cy.getByTestId("modal-input-comment").type(
      "I approve of this Travel Approval"
    );
    cy.getByTestId("modal-button-1").click();
    cy.getByTestId("submit-title-h2").should("contain", "reassigned");
    cy.getByTestId("submit-title-h2").should("contain", "Travel Approval");
    cy.getByTestId("submit-body-div").should(
      "contain",
      "Your approval has been registered and an email has been sent to the new supervisor."
    );
  });
  it(
    "Can be processed by an administrator",
    { scrollBehavior: "center" },
    () => {
      //go to and verify in archive view
      cy.login("administrator", "travel-approval/archive/");
      cy.waitForCache();
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("submitted");
      cy.getByTestId("form-10000-step-2-div").contains("approved");
      cy.getByTestId("form-10000-step-3-div").contains("pending");
      //Approve the form
      cy.getByTestId("form-10000-resolve-button").click();
      cy.getByTestId("form-10000-approve-button").click();
      cy.getByTestId("modal-input-comment").type(
        "I also approve approve of this Travel Approval"
      );
      cy.getByTestId("modal-button-1").click();
      cy.getByTestId("submit-title-h2").should("contain", "approved");
      cy.getByTestId("submit-body-div").should(
        "contain",
        "Your approval has been registered and an email has been sent to the original submitter."
      );
      //go to and verify in archive view
      cy.navigateTo("TRAVEL", "archive");
      cy.getByTestId("form-10000-action-menu").click();
      cy.getByTestId("action-menu-toggle-details").click();
      cy.getByTestId("form-10000-step-1-div").contains("notified");
      cy.getByTestId("form-10000-step-1-div").contains(
        Cypress.env("staff").name
      );
      cy.getByTestId("form-10000-step-2-div").contains("approved");
      cy.getByTestId("form-10000-step-2-div").contains(
        Cypress.env("supervisor").name
      );
      cy.getByTestId("form-10000-step-3-div").contains("supplanted");
      cy.getByTestId("form-10000-step-3-div").contains("Darren Spruce");
      cy.getByTestId("form-10000-step-4-div").contains("approved");
      cy.getByTestId("form-10000-step-4-div").contains(
        Cypress.env("administrator").name
      );
    }
  );
  it("Sends an email", { scrollBehavior: "center" }, () => {
    cy.visit("http://mailcatcher.maxiv.lu.se");
    cy.get("table tr").should("have.length.gt", 3);
    cy.get("table tr").contains("Decision: Travel Approval #10000").click();
    cy.get("table tr").contains("Reassignment: Travel Approval #10000").click();
    cy.get("table tr").contains("Confirmation: Travel Approval #10000").click();
    cy.get("table tr").contains("Submission: Travel Approval #10000").click();
  });
});
