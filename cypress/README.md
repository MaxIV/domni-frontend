# Running tests

Domni is using cypress for frontend testning. To run the tests with browser simulation type npm run cypress. In order for this to work domni frontend has to be running on https://local.maxiv.lu.se:3000. Setting up the MAX IV subdomain is covered in the installation part. To run with https locally, use HTTPS=true npm start (UNIX), set HTTPS=true&&npm start (Windows cmd) or ($env:HTTPS = "true") -and (npm start) (Windows Powershell).
Additionally, the backend must be running as well (without any https or subdomain requirements), and it must be connected to the cypress test database (mongodb://domni-cypress:domni-cypress@192.168.19.104:27017/domni-cypress)

# Commands

`support/index.d.ts` lists available commands. Their implementation is found in s`upport/commands.ts`. These are: `getByTestId`, `waitForCache`, `login`, `inputModal`, `navigateTo`. Note that a type definition for each commandn also has to be added to `index.d.ts`.

# Selectors

Many interactable elements have a unique test id set using the attribute `data-testid`. This file attempts to list all of these with a short description. All non-form items should have a unique `data-testid` attribute. Within form implementations the existing `name` attributes already being used for updating `formData` may be used. The format of `data-testid` attribute should roughly be `<componentName>-<elementName>-[iterableId]-<htmlTagType>` where the optional `[iterableId]` is needed if the testid wouldn't be unique without it, eg in tables or other mappings where each row correspond to a submission, formType or person.

## Landing page

- `landing-page-${formConfig.formType}-btn` The card buttons linking to various forms in the landing page

## Table and subcomponents in Archive/Approval

### Collapsed table row

- `form-${form._id}-toggle-expanded-form-button` Toggle the expanded view of the form
- `form-${form._id}-id-container-div` The div holding all content in the ID column (wrapping both id and external id, including button).
- `form-${form._id}-external-id-button` Update external id button in table row. Note that the content of this button also has the external id, if any
- `email-${email}-ldap-person-link`, clickable supervisor name in table row. Note that same widget (`LdapPersonModal` has other testids elsewhere)
- `form-${form._id}-status-tag-div`, The form status tag in table row. NEED TO BE REDONE WITH UNIQUE ID IN PROFILE (INCLUDE FORMTYPE)
- `form-${form._id}-recall-button` the recall button in the decider widget when in the approval view
- `form-${form._id}-resolve-button` The decider button/link to approval when in archive view.
- `form-${form._id}-process-button`, the 'approve' button in the decided widget in approval view iff `requiresApproval ==== false`
- `form-${form._id}-approve-button`, the approve button in the decided widget in the approval view
- `form-${form._id}-reject-button` , the reject button in the decided widget in the approval view
- `form-${form._id}-toggle-comment-button`, The show/hide comments button in table row
- `form-${form._id}-view-edit-form-link`, The 'external link' icon in formlink component in table row
- `form-${form._id}-action-menu`, The _Available Actions_ menu in table row

### Action Menu options

- `action-menu-toggle-details`
- `action-menu-view-original`
- `action-menu-view-print-version`
- `action-menu-toggle-comments`
- `action-menu-clone`
- `action-menu-archive`
- `action-menu-download-attachments`
- `action-menu-upload-attachments`
- `action-menu-edit-external-id`
- `action-menu-delete`

### Expanded form

- `form-${form._id}-step-${index}-div`, step elements in event timeline in expanded submitted form. Starting at index 1. returns deeply nested div so `cy.contains` is recommended to check for values of the `node` attributes.
- `file-${fileName.name}-download-link`, download link for a specific file
- `form-${form._id}-print-version-link`, the link to the print-friendly version

### Expanded comment

- `form-${form._id}-comment-button` The submit comment button in the comment widget
- `form-${form._id}-comment-input` The new comment input in the comment widget
- `form-${form._id}-comment-${index+1}-div` Specific comment in comment widget, starting at index 1. Returns deeply nested div so `cy.contains` is recommended to check for values of the `Comment` attributes.

## Selects

- `supervisor-select` Default value for Supervisor select. Usually safe to use in form where only one exists. Note that supervisor reassign modal uses id `approval-modal-supervisor-select`
- `project-select` All project selects, At least for now, 2 can never exist at the same time.

## Global Modal

- `modal-input-${inputId}` text input inside the global modal
- `modal-button-1` The OK/Confirm button
- `modal-button-2` The Cancel button
- `modal-validation-result-div` div containing validation results (usually errors). Currently only applicable for the approval modal.
- `approval-modal-supervisor-select` The supervisor select in the approval/reassignment modal

## Archive

Elements belonging directly to the archive view.

- `archive-no-results-span` Element containing the 'no results' message
- `archive-advanced-search-toggle-button` The gear button used to toggle the visibility of the advanced search panel
- `archive-advanced-search-default-radio` The `Default` search type radio button in advanced search
- `archive-advanced-search-comment-radio`The `Comment` search type radio button in advanced search
- `archive-advanced-search-custom-field-radio` The `Custom field` search type radio button in advanced search (conf. cond. visibility)
- `archive-advanced-search-status-select` The status select under advanced search
- `archive-advanced-search-custom-field-select` The custom field select under advanced search (conf. cond. visibility)
- `archive-advanced-search-search-button` The alternative SEARCH button only visible under advanced search
- `archive-advanced-search-search-input` The alternative search query input only visible under advanced search

## Form

- `form-fieldset-disabled-form`, the fieldset that disables non-writable forms
- `form-header-status-label`, the top-right label on any db-defined submission (contains DRAFT, PENDING etc.)
- `form-title-h2`, the main title of a submission. Contains the name of the formType and id. Does not contain external id <formType>
- `form-externalId-div` The div containing the external id (only exist if there is one)
- `form-submit-btn` The submit/Save/resubmit button at the bottom right of the form
- `form-save-draft-btn` The save draft button at the bottom left on of the form
- `form-submitter-name-input` The disabled submitter name input in the form header
- `form-submitter-email-input` The disabled submitter email input in the form header

## Submit confirmation page

- `submit-title-h2` The title of the submit page (e.g. "Approved | Safety Test")
- `submit-body-div` The body of the submit page.

## Settings

### Supervisors

- `settings-new-supervisor-button` The create new supervisor button
- `settings-delete-supervisor-button` The delete selected new supervisor button
- `settings-supervisor-name-input` The name input of selected supervisor
- `settings-supervisor-email-input` The email input of selected supervisor
- `settings-supervisor-project-select` The supervisor project select
- `settings-supervisor-project-chips-div` The parent div of all project chips. Returns deeply nested div so `cy.contains` is recommended to check project assignment
- `settings-supervisor-save-button` The save supervisor button
- `settings-supervisor-list` The list of supervisors
- `settings-supervisor-list-item-${supervisorName}` A specific supervisor list item by label (supervisor name).

### Projects

- `settings-new-project-button` The create new project button
- `settings-delete-project-button` The deleted selected project butotn
- `settings-project-project-input` The project name input
  `project-categories-select` The project category select
- `settings-project-save-button` The save project button
- `settings-project-list` The list of projects
- `settings-project-list-item-${projectName}` A specific project list item by label (project name)

### DynamicInputTable

Note- if the prop `testId:string` is set, this value is added after `dynamicInputTable` for every testId. E.g., with the value _orders_ the new button id becomes `dynamicInputTable-orders-new-row-button`. This is needed if the same view is using multiple `DynamicInputTables`, to ensure they each have a unique id.
As for the inputs in the rows, their ids can be set directly in the `columnDefinition` when rendering the component. Don't forget to include the prop `rowIndex` to make sure the ids are unique.

- `dynamicInputTable-new-row-button` The 'Add row' button.
- `dynamicInputTable-delete-row-${rowIndex}-button` The _Delete row_ button corresponding to line `rowIndex`

### Customlists

- `custom-lists-new-list-button` The 'create new custom list button'
- `custom-lists-list-select` The select of available custom lists
- `custom-lists-edit-list-button` button for name/desc input modal
- `custom-lists-delete-list-button` button for deleting list
- `custom-lists-add-list-item-button` button for adding a new item to current list
- `custom-list-list` The SelectableListWidget
- `custom-list-list-item-${itemLabel}` The item with the label `itemLabel` in the SelectableListWidget. Note that an item designated as default would have the itemLabel `<label> (default)`
- `custom-list-delete-item-button` Delete specific list item
- `custom-list-item-label-input` specify list item label
- `custom-list-item-value-input`specify list item value
- `custom-list-item-make-default-button` The 'make default' button
- `custom-list-item-save-button` the save item button.
- `custom-lists-list-label-span` a span with the title (name) of the currently selected list

## Navigation

Note: the custom command `navigateTo` can be used to handle all navigation. Doing it manually is a bit tricky because it requires resolving sub-menues after triggering mouse-over events.

- `header-menu-${menu}` Drop-down menues in the header. `menu` has the value of 'form', 'archive' or 'settings'
- `header[-menu-${menu}]-item-${item}` An item in the menu, possibly under a sub-menu. In case of form archive and settings, item is a `System.FormType`. Otherwise, it's either `people`, `profile` or `domni`. Note that only the former requires the `menu` part in the id

## People

- `people-filter-input` filter input of people
- `people-sortby-select` the sort by select
- `ldap-person-card-${person.username}-div` the container of the card
- `ldap-person-card-${person.username}-${property-fragment}` Get specific card element where `property` is fullName, title, phone, email, office or username

## Profile

- `profile-email-div` The div showing the users email address
- `profile-delete-drafts-btn` Button that deletes all user drafts. Note that this also requires confirmation (`cy.getByTestId("modal-button-1").click();`)
- `profile-drafts-domniloader` Domniloader showing while drafts are being loaded. Use `.should('not.exist');` to wait for drafts to load.
- `profile-draft-${formType}-${form._id}-div` Div container for the draft table row corresponding to given the submission
- `profile-submitted-cases-year-picker` The year picker for submitted cases (a SimpleSelect)
- `profile-submitted-cases-filter-input` The filter input for submitted cases
- `profile-submitted-cases-domniloader` Domniloader showing while submitted cases are being loaded. Use `.should('not.exist');` to wait for drafts to load.
- `profile-roles-div` Div container for all user role names and their descriptions

### Submitted cases table row

- `profile-submitted-cases-${form.formType}-${form._id}-row` The entire `TableRow` component
- `profile-submitted-cases-${formType}-${form._id}-id` The container for the id and/or external id text/link
- `profile-submitted-cases-${formType}-${form._id}-formtype-name` The name of the formtype (`formConfig.name`)
- `profile-submitted-cases-${formType}-${form._id}-summary` the div container for the `formConfig.renderSubmissionSummary` widget
- `profile-submitted-cases-${formType}-${form._id}-status` Note that this overrides the existing default global id for tag from `Helpers.StatusTag` (`form-${form._id}-status-tag-div`)
- `profile-submitted-cases-${formType}-${form._id}-supervisor` An LDAP person widget (`LdapPersonModal`). Note that this widget has different testids in different views

### Pending supervisor cases row

- `profile-pending-supervisor-cases-${form.formType}-${form._id}-row` The entire `TableRow` component
- `profile-pending-supervisor-cases-${form.formType}-${form._id}-id` The id
- `profile-pending-supervisor-cases-${form.formType}-${form._id}-summary` The `formConfig.renderSubmissionSummary` widget

## TODO - Additional things to test

- comments: automatic, toggling, reading, creating,
- read and write access from ext. users, staff, supers and admins for draft, pending, approved, rejected forms. Also for different configs of canEditAdmin, canEditSupervisor, hideApprovedForms
- proper searching of archive, including advanced searches, page size and pagination
- cloning, print version, archiving, attachment download, csv/json export
- making sure that the notify checkbox in the approval modal works as intended
