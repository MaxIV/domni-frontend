import {} from "./commands";

declare global {
  type User = "user" | "staff" | "supervisor" | "administrator";
  export namespace Cypress {
    interface Chainable {
      /**Custom command - get an element by the value of its `data-testid` attribute */
      getByTestId: (selector: string) => Cypress.Chainable<JQuery<HTMLElement>>;
      /**Custom command - wait for the global state `isLoading` to be `false` */
      waitForCache: () => Cypress.Chainable<JQuery<HTMLElement>>;
      /**Custom command - attemps to visit `redirectURL`, assumes redirect to auth where it logs in, and then verifies one is redirected back to `redirectURL` after */
      login: (
        user: "user" | "staff" | "supervisor" | "administrator",
        path?: string
      ) => Cypress.Chainable<JQuery<HTMLElement>>;
      /**Custom command - inputs the key/values of `inputMap` into the currently open modal and closes the modal by clicking on `btn1` */
      inputModal: (inputMap: {
        [id: string]: string;
      }) => Cypress.Chainable<JQuery<HTMLElement>>;
      /**Custom command - Navigate to any item in the header */
      navigateTo: (
        item: System.FormType | "people" | "profile",
        menu?: "form" | "archive" | "settings"
      ) => Cypress.Chainable<JQuery<HTMLElement>>;
      /**Custom command - Create a new supervisor. Assumes you're already in settings, cache loaded, db seeded and the Supervisor+ button is visible */
      createSupervisor: (user: User) => Cypress.Chainable<JQuery<HTMLElement>>;
      /**Custom command - Create a new supervisor, a new project, and maps the project to the supervisor. Assumes you're already in settings, cache loaded, db seeded and the Supervisor+ button is visible */
      createSupervisorWithNewProject: (
        user: User,
        project: string
      ) => Cypress.Chainable<JQuery<HTMLElement>>;
    }
  }
}
