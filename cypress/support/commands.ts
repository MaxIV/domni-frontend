export const getByTestId = (testId: string) => {
  return cy.get(`[data-testid="${testId}"]`);
};
export const waitForCache = () => {
  return cy
    .window({ timeout: 20000 })
    .should("have.nested.property", "store.isLoading", false);
};
export const login = (user: User, path?: string) => {
  cy.visit(`${Cypress.env("localHost")}${path || ""}`);
  cy.get("#inputUsername").type(Cypress.env(user).username);
  cy.get("#inputPassword").type(`${Cypress.env(user).password}{enter}`);
  return cy.url().should("eq", `${Cypress.env("localHost")}${path || ""}`);
};
export const inputModal = (inputMap: { [id: string]: string }) => {
  cy.wrap(Object.keys(inputMap))
    .each((inputValue) => {
      const tmp = inputValue as unknown as string;
      cy.getByTestId(`modal-input-${tmp}`).type(inputMap[tmp]);
    })
    .getByTestId("modal-button-1")
    .click();
};

export const navigateTo = (
  item: System.FormType | "people" | "profile" | "domni",
  menu?: "form" | "archive" | "settings"
) => {
  if (menu) {
    cy.getByTestId(`header-menu-${menu}`).trigger("mouseover");
    cy.getByTestId(`header-menu-${menu}-item-${item}`).click();
    cy.getByTestId(`header-menu-${menu}`).trigger("mouseleave");
  } else {
    cy.getByTestId(`header-item-${item}`).click();
  }
};

export const createSupervisor = (user: User) => {
  cy.getByTestId("settings-new-supervisor-button").click();
  cy.inputModal({
    name: Cypress.env(user).username,
    email: Cypress.env(user).email,
  });
  cy.getByTestId("settings-supervisor-name-input")
    .should("have.value", Cypress.env(user).username)
    .clear()
    .type(Cypress.env(user).username)
    .should("have.value", Cypress.env(user).username);
  cy.getByTestId("settings-supervisor-email-input")
    .should("have.value", Cypress.env(user).email)
    .clear()
    .type(Cypress.env(user).email)
    .should("have.value", Cypress.env(user).email);
  cy.getByTestId("settings-supervisor-save-button").click();
};

export const createSupervisorWithNewProject = (user: User, project: string) => {
  cy.getByTestId("settings-new-supervisor-button").click();
  cy.inputModal({
    name: Cypress.env(user).username,
    email: Cypress.env(user).email,
  });
  cy.getByTestId("settings-supervisor-name-input")
    .should("have.value", Cypress.env(user).username)
    .clear()
    .type(Cypress.env(user).username)
    .should("have.value", Cypress.env(user).username);
  cy.getByTestId("settings-supervisor-email-input")
    .should("have.value", Cypress.env(user).email)
    .clear()
    .type(Cypress.env(user).email)
    .should("have.value", Cypress.env(user).email);
  cy.getByTestId("settings-supervisor-save-button").click();
  cy.getByTestId("settings-new-project-button").click();
  cy.inputModal({ name: project });
  cy.getByTestId("settings-project-project-input").should(
    "have.value",
    project
  );
  cy.getByTestId("settings-supervisor-project-select").select(project);
  cy.getByTestId("settings-supervisor-project-chips-div").contains(project);
  cy.getByTestId("settings-supervisor-save-button").click();
};

Cypress.Commands.add("getByTestId", getByTestId);
Cypress.Commands.add("waitForCache", waitForCache);
Cypress.Commands.add("login", login);
Cypress.Commands.add("inputModal", inputModal);
Cypress.Commands.add("navigateTo", navigateTo);
Cypress.Commands.add("createSupervisor", createSupervisor);
Cypress.Commands.add(
  "createSupervisorWithNewProject",
  createSupervisorWithNewProject
);
